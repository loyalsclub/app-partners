/* global process console */
/* eslint no-console: ["error", { allow: ["log"] }] */

const dir = 'config/language';
const endDir = 'src/config/language/';

const fs = require('fs');
const path = require('path');

const lnames = ['es', 'en'];
const endPathName = path.resolve(endDir);

for (const lname of lnames) {
  const pathName = path.resolve(path.join(dir, lname));
  let language = {};

  // Read every file inside language folder, join into single file.
  fs.readdirSync(pathName).forEach((file) => {
    const content = require(path.join(pathName, file));
    language = {
      ...language,
      ...content,
    };
  });

  // Create languages dir if not exists.
  if (!fs.existsSync(endPathName)) {
    fs.mkdirSync(endPathName);
  }
  const res = `export default ${JSON.stringify(language)}`;
  const filePath = path.join(endPathName, `${lname}.js`);
  fs.writeFileSync(filePath, res);
  console.log(`OK ${filePath}`);
}

process.exit(0);
