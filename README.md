## Loyals Club Partners APP

This APP is built using React Native and expo platform.

### Commands of interest

Development test with production environment (sets process.env.NODE_ENV == production): 

```
expo start --no-dev --minify
```

Build for production:

```
expo build:android --release-channel prod-v1
```

Deploy for production without building again:

```
expo publish --release-channel prod-v1
```

