module.exports = {
  AUTH_FORM: {
    email: 'Email',
    password: 'Contraseña',
    firstName: 'Nombre',
    lastName: 'Apellido',
    loginSubmit: 'Iniciar Sesión',
    loginError: 'Ups! Parece que hay un error en los datos',
  },
  SIGNUP_FORM: {
    submitButton: 'Crear cuenta',
  },
  FORMS_ERROR_MESSAGES: {
    required: 'Campo requerido',
    number: 'Completá el campo con números',
    email: 'Ingresá un email válido',
    confirmPassword: 'Las contraseñas deben coincidir',
    charMin: 'Mínimo %{char} caracteres',
    invalidField: 'Ingresá un %{field} válido',
    illegalAge: 'Debes ser al menos mayor de 18 años',
  },
};
