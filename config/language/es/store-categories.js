module.exports = {
  STORE_CATEGORIES: {
    dress: 'Indumentaria',
    gastronomy: 'Gastronomía',
    coffee: 'Cafetería',
    bar: 'Bar',
    service: 'Servicio',
    market: 'Mercado',
  },
};
