import gql from 'graphql-tag';

const FIND_PENDING_USER = gql`
  query($email: String!) {
    findPendingUser(email: $email) {
      isPending
      email
      firstName
      lastName
    }
  }
`;

const CREATE_PARTNER_USER = gql`
  mutation(
    $firstName: String!
    $lastName: String!
    $email: Email!
    $password: String!
  ) {
    createPartnerUser(
      firstName: $firstName
      lastName: $lastName
      email: $email
      password: $password
    ) {
      loginToken
    }
  }
`;

const REGISTER_USER_PUSH_TOKEN = gql`
  mutation($pushToken: String!) {
    registerUserPushToken(pushToken: $pushToken) {
      id
      pushToken
    }
  }
`;

export { FIND_PENDING_USER, CREATE_PARTNER_USER, REGISTER_USER_PUSH_TOKEN };
