import gql from 'graphql-tag';

const GET_EMPLOYEE_PROFILES = gql`
  query {
    userEmployeeProfiles {
      id
      Store {
        id
        name
        category
        alias
        profileImage
      }
      User {
        id
        pushToken
      }
    }
  }
`;

const EMPLOYEE_GET_STATISTICS = gql`
  query ($storeId: ID!) {
    employeeGetStatistics(storeId: $storeId) {
      customersTotal
      level1
      level2
      level3
    }
  }
`;

const EMPLOYEE_GET_STORE_CUSTOMERS = gql`
  query ($storeId: ID!) {
    employeeGetStoreCustomers(storeId: $storeId) {
      id
      CustomerLevel {
        level
        total
      }
      User {
        firstName
        lastName
        alias
      }
    }
  }
`;

const EMPLOYEE_GET_USER = gql`
  query ($userField: String!, $storeId: ID!) {
    employeeGetUser(userField: $userField, storeId: $storeId) {
      id
      email
      firstName
      lastName
    }
  }
`;

const EMPLOYEE_GET_CUSTOMER = gql`
  query ($userField: String!, $storeId: ID!) {
    employeeGetCustomer(userField: $userField, storeId: $storeId) {
      id
      CustomerLevel {
        level
        total
      }
    }
  }
`;

const EMPLOYEE_GET_USER_AND_CUSTOMER = gql`
  query ($userField: String!, $storeId: ID!) {
    employeeGetUser(userField: $userField, storeId: $storeId) {
      id
      email
      firstName
      lastName
    }
    employeeGetCustomer(userField: $userField, storeId: $storeId) {
      id
      storeId
      CustomerLevel {
        level
        currentLevelPoints
        nextLevelPointsDif
        nextLevelPoints
        total
      }
    }
    employeeGetCustomerAvailableRewards(
      userField: $userField
      storeId: $storeId
    ) {
      id
      level
      StoreRewards {
        id
        title
        description
        isAvailable
        isConsideredUsed
        UsageFrequency {
          type
          value
        }
      }
    }
  }
`;

const EMPLOYEE_INVITE_NEW_USER = gql`
  mutation (
    $firstName: String!
    $lastName: String!
    $storeId: ID!
    $email: Email!
  ) {
    employeeInviteNewUser(
      firstName: $firstName
      lastName: $lastName
      email: $email
      storeId: $storeId
    ) {
      id
    }
  }
`;

const EMPLOYEE_CREATE_CUSTOMER = gql`
  mutation ($storeId: ID!, $userField: String!) {
    employeeCreateCustomer(userField: $userField, storeId: $storeId) {
      id
    }
  }
`;

const EMPLOYEE_LEVELS_AND_REWARDS_BY_STOREID = gql`
  query ($storeId: ID!) {
    storeLevelsAndRewardsById(storeId: $storeId) {
      id
      storeId
      StoreRewards {
        id
        title
        storeLevelId
        description
        UsageFrequency {
          type
          value
        }
      }
      level
      points
    }
  }
`;

const EMPLOYEE_LOAD_POINTS = gql`
  mutation ($storeId: ID!, $amount: Int!, $userField: String!) {
    employeeLoadPoints(
      amount: $amount
      userField: $userField
      storeId: $storeId
    ) {
      id
      amount
    }
  }
`;

const EMPLOYEE_CREATE_COUPON = gql`
  mutation ($storeId: ID!, $amount: Int!) {
    employeeCreateCoupon(amount: $amount, storeId: $storeId) {
      id
      couponCode
      points
      url
    }
  }
`;

const EMPLOYEE_STORE_INFO = gql`
  query ($alias: String!) {
    storeInfoByAlias(alias: $alias) {
      id
      name
      profileImage
      description
      countryCode
      category
      alias
      socialMedia
      StoreLocations {
        id
        website
        phone
        email
        address
        geo {
          type
          coordinates
        }
      }
    }
  }
`;

const EMPLOYEE_REGISTER_REWARD_USAGE = gql`
  mutation ($storeId: ID!, $storeRewardId: ID!, $userField: String!) {
    employeeRegisterStoreRewardUsage(
      userField: $userField
      storeId: $storeId
      storeRewardId: $storeRewardId
    ) {
      id
    }
  }
`;

const EMPLOYEE_REMOVE_STORE_REWARD = gql`
  mutation ($storeId: ID!, $rewardId: ID!) {
    employeeRemoveStoreReward(storeId: $storeId, rewardId: $rewardId)
  }
`;

const STORE_GET_CATEGORIES_AND_COUNTRIES = gql`
  query {
    storeCategories {
      name
      displayName
    }
    storeCountries {
      name
      displayName
      conversionRate
      areaCode
    }
  }
`;

const STORE_CREATE = gql`
  mutation ($store: CreateStoreInput!) {
    createStoreForUser(store: $store) {
      id
      alias
      countryCode
    }
  }
`;
const EMPLOYEE_ADD_STORE_REWARD = gql`
  mutation ($storeId: ID!, $storeReward: CreateStoreRewardInput!) {
    employeeAddStoreReward(storeId: $storeId, storeReward: $storeReward) {
      id
    }
  }
`;

const EMPLOYEE_MODIFY_STORE_REWARD = gql`
  mutation ($id: ID!, $storeId: ID!, $storeReward: CreateStoreRewardInput!) {
    employeeModifyStoreReward(
      id: $id
      storeId: $storeId
      storeReward: $storeReward
    ) {
      id
    }
  }
`;

const EMPLOYEE_MODIFY_STORE = gql`
  mutation ($storeId: ID!, $store: CreateStoreInput!) {
    employeeModifyStore(storeId: $storeId, store: $store) {
      id
      name
      alias
      category
      description
      socialMedia
      profileImage
      StoreLocations {
        id
        website
        address
        phone
        email
        geo {
          type
          coordinates
        }
      }
    }
  }
`;

const STORE_VALIDATE_ALIAS = gql`
  query ($alias: String!) {
    validateStoreAlias(data: $alias)
  }
`;

const EMPLOYEE_GET_CUSTOMER_HISTORY = gql`
  query employeeGetCustomerHistory($storeId: ID!, $customerId: ID!) {
    employeeGetCustomerHistory(storeId: $storeId, customerId: $customerId) {
      id
      type
      value
      createdAt
    }
  }
`;

export {
  EMPLOYEE_ADD_STORE_REWARD,
  EMPLOYEE_CREATE_COUPON,
  EMPLOYEE_CREATE_CUSTOMER,
  EMPLOYEE_GET_CUSTOMER,
  EMPLOYEE_GET_STATISTICS,
  EMPLOYEE_GET_STORE_CUSTOMERS,
  EMPLOYEE_GET_USER_AND_CUSTOMER,
  EMPLOYEE_GET_USER,
  EMPLOYEE_INVITE_NEW_USER,
  EMPLOYEE_LEVELS_AND_REWARDS_BY_STOREID,
  EMPLOYEE_LOAD_POINTS,
  EMPLOYEE_MODIFY_STORE_REWARD,
  EMPLOYEE_MODIFY_STORE,
  EMPLOYEE_REGISTER_REWARD_USAGE,
  EMPLOYEE_REMOVE_STORE_REWARD,
  EMPLOYEE_STORE_INFO,
  EMPLOYEE_GET_CUSTOMER_HISTORY,
  GET_EMPLOYEE_PROFILES,
  STORE_CREATE,
  STORE_GET_CATEGORIES_AND_COUNTRIES,
  STORE_VALIDATE_ALIAS,
};
