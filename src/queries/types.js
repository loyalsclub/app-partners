const { USAGE_FREQUENCIES } = require('../config/constants');

export default `
  enum UsageFrequencyType {
    ${USAGE_FREQUENCIES.FOREVER}
    ${USAGE_FREQUENCIES.ONE_TIME}
    ${USAGE_FREQUENCIES.EVERY_CERTAIN_DAYS}
    ${USAGE_FREQUENCIES.EVERY_CERTAIN_HOURS}
    ${USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS}
  }

  enum AllowedStatusCustomerProfile {
    active
    pending
    deleted
  }

  enum AllowedStatusCouponTransaction {
    pending,
    redeemed
  }

  input CreateStoreRewardInput {
    storeLevelId: ID!
    title: String!
    description: String
    usageFrequencyType: UsageFrequencyType!
    usageFrequencyValue: String
  }

`;
