import firebase from 'firebase';
import ENV from './config/env.config';

const initialize = () => {
  if (firebase.apps.length > 0) return;
  firebase.initializeApp(ENV.FIREBASE_CONFIG);
};

export default initialize;
