import themeStyles from './theme.style';
import fs from './font.styles';

export default {
  container: {
    flex: 1,
  },
  centeredCon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  centerScrollView: {
    flexGrow: 1,
    justifyContent: 'center',
  },
  genText: {
    fontFamily: themeStyles.MAIN_FONT_LIGHT,
    fontSize: 16,
  },
  text: {
    fontSize: 16,
    fontFamily: themeStyles.MAIN_FONT_LIGHT,
    alignSelf: 'center',
  },
  mainTitle: {
    fontFamily: themeStyles.MAIN_FONT_BOLD,
    color: themeStyles.BLUE_WOOD,
    fontSize: 22,
  },
  title: [
    fs.xlargeSize,
    fs.center,
    fs.blue,
    fs.normalWeight,
    { marginBottom: 20 },
  ],
  secondaryTitle: [
    fs.mediumSize,
    fs.center,
    fs.blue,
    fs.normalWeight,
    { marginBottom: 12 },
  ],
  formError: {
    fontFamily: themeStyles.MAIN_FONT_LIGHT,
    fontSize: 16,
    color: themeStyles.DANGER,
    textAlign: 'center',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingVertical: 10,
  },
  rowChild: {
    flexShrink: 1,
  },
  sectionPageTitle: [
    {
      fontFamily: themeStyles.MAIN_FONT_BOLD,
      color: themeStyles.BLUE_WOOD,
      marginTop: 20,
      marginBottom: 10,
    },
    fs.xlargeSize,
  ],
  sectionPageSubtitle: [
    fs.mediumSize,
    fs.center,
    fs.blue,
    fs.normalWeight,
    {
      color: themeStyles.GREY,
      marginBottom: 40,
    },
  ],
};
