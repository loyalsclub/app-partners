import { StyleSheet } from 'react-native';
import themeStyle from './theme.style';

export default StyleSheet.create({
  titleSize: {
    fontSize: 34,
  },
  xlargeSize: {
    fontSize: 25,
  },
  largeSize: {
    fontSize: 19,
  },
  mediumSize: {
    lineHeight: 19,
    fontSize: 16,
  },
  smallSize: {
    fontSize: 13,
  },
  boldWeight: {
    fontFamily: themeStyle.MAIN_FONT_BOLD,
  },
  normalWeight: {
    fontFamily: themeStyle.MAIN_FONT_LIGHT,
  },
  blue: {
    color: themeStyle.BLUE_WOOD,
  },
  grey: {
    color: themeStyle.GREY,
  },
  golden: {
    color: themeStyle.GOLDEN,
  },
  center: {
    textAlign: 'center',
  },
});
