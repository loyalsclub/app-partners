/** External dependencies */
import React, { useState } from 'react';
import { useMutation, useQuery } from '@apollo/react-hooks';

/** Wrapper */
import RewardsComponent from '../components/Rewards/Rewards.component';

/** Utils */
import { parseError, SERVER_ERROR } from '../utils/graphql-error.utils';
import { analyticsProvider, events } from '../providers/analytics';

/** Constants */
import {
  EMPLOYEE_ADD_STORE_REWARD,
  EMPLOYEE_LEVELS_AND_REWARDS_BY_STOREID,
  EMPLOYEE_MODIFY_STORE_REWARD,
  EMPLOYEE_REMOVE_STORE_REWARD,
} from '../queries/partner.queries';

const STEPS = {
  showRewards: 'showRewards',
  editReward: 'editReward',
};

const STEPS_RE = {
  selectLevel: 'selectLevel',
  addInfo: 'addInfo',
  reviewReward: 'reviewReward',
};

const RewardsPage = (props) => {
  const { navigation } = props;
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [currentStep, setStep] = useState(STEPS.showRewards);
  const [currentStepRE, setStepRE] = useState(STEPS_RE.selectLevel);
  const [currentReward, setCurrentReward] = useState({});

  const storeId = navigation.getParam('storeId');

  const handleError = (queryError) => {
    const pUserError = parseError(queryError);

    const errorMessage = { secondaryText: SERVER_ERROR };

    if (pUserError) {
      errorMessage.secondaryText = pUserError.text;
    }

    setError(errorMessage);

    setLoading(false);
  };

  // Get rewards
  const { data, refetch: refetchRewards } = useQuery(
    EMPLOYEE_LEVELS_AND_REWARDS_BY_STOREID,
    {
      fetchPolicy: 'cache-and-network',
      variables: { storeId },
      notifyOnNetworkStatusChange: true,
      onCompleted: () => {
        setLoading(false);
      },
      onError: handleError,
    }
  );

  const [employeeRemoveStoreReward] = useMutation(
    EMPLOYEE_REMOVE_STORE_REWARD,
    {
      variables: { storeId },
      onCompleted: () => {
        setLoading(false);
        refetchRewards();
        analyticsProvider.logEvent(events.rewards.delete_reward);
      },
      onError: handleError,
    }
  );

  const [employeeAddStoreReward] = useMutation(EMPLOYEE_ADD_STORE_REWARD, {
    variables: { storeId },
    onCompleted: () => {
      setLoading(false);
      refetchRewards();
      analyticsProvider.logEvent(events.rewards.new_reward_finished);
    },
    onError: handleError,
  });

  const [employeeModifyStoreReward] = useMutation(
    EMPLOYEE_MODIFY_STORE_REWARD,
    {
      variables: { storeId },
      onCompleted: () => {
        setLoading(false);
        refetchRewards();
        analyticsProvider.logEvent(events.rewards.edit_reward_finished);
      },
      onError: handleError,
    }
  );

  const { storeLevelsAndRewardsById } = data || {};

  const goBackOnPress = () => {
    if (currentStep === STEPS.editReward) {
      if (STEPS_RE.selectLevel === currentStepRE)
        return setStep(STEPS.showRewards);
      if (STEPS_RE.addInfo === currentStepRE)
        return setStepRE(STEPS_RE.selectLevel);
      if (STEPS_RE.reviewReward === currentStepRE)
        return setStepRE(STEPS_RE.addInfo);
    }

    return navigation.goBack();
  };

  const onContinuePressHandler = (stepData) => {
    if (STEPS_RE.selectLevel === currentStepRE)
      return setStepRE(STEPS_RE.addInfo);
    if (STEPS_RE.addInfo === currentStepRE) {
      setCurrentReward({
        ...currentReward,
        ...stepData,
        description: stepData.description.trim(),
        title: stepData.title.trim(),
      });
      return setStepRE(STEPS_RE.reviewReward);
    }
    if (STEPS_RE.reviewReward === currentStepRE) {
      setStepRE(STEPS_RE.selectLevel);
      setStep(STEPS.showRewards);
      const payload = {
        variables: {
          storeReward: currentReward,
        },
      };
      setLoading(true);

      if (currentReward.id) {
        // Editing
        payload.variables.id = currentReward.id;
        delete payload.variables.storeReward.id;
        employeeModifyStoreReward(payload);
      } else {
        // Creating
        delete payload.variables.storeReward.id;
        employeeAddStoreReward(payload);
      }
      setCurrentReward({});
    }
  };

  const onAddRewardPressHandler = (rewardId) => {
    setError(null);
    setCurrentReward({});

    // If an ID is sent it's modifying an existing reward.
    if (rewardId) {
      const rewardInfo = storeLevelsAndRewardsById
        .flatMap((rewardLevel) => rewardLevel.StoreRewards)
        .filter((reward) => reward.id === rewardId)[0];

      setCurrentReward({
        id: rewardInfo.id,
        title: rewardInfo.title,
        description: rewardInfo.description,
        storeLevelId: rewardInfo.storeLevelId,
        usageFrequencyValue: rewardInfo.UsageFrequency.value,
        usageFrequencyType: rewardInfo.UsageFrequency.type,
      });
    }
    setStep(STEPS.editReward);

    analyticsProvider.logEvent(
      rewardId ? events.rewards.edit_reward : events.rewards.new_reward
    );
  };

  const onLevelSelected = (storeLevelId) => {
    setCurrentReward({
      ...currentReward,
      storeLevelId,
    });
  };

  const deleteReward = (rewardId) => {
    setLoading(true);
    employeeRemoveStoreReward({
      variables: {
        rewardId,
      },
    });
  };

  return (
    <RewardsComponent
      loading={loading}
      currentStep={currentStep}
      STEPS={STEPS}
      STEPS_RE={STEPS_RE}
      currentReward={currentReward}
      rewardsData={storeLevelsAndRewardsById}
      error={error}
      currentStepRE={currentStepRE}
      onLevelSelected={onLevelSelected}
      onAddRewardPressHandler={onAddRewardPressHandler}
      refetch={refetchRewards}
      deleteReward={deleteReward}
      goBackOnPress={goBackOnPress}
      onContinuePressHandler={onContinuePressHandler}
    />
  );
};

export default RewardsPage;
