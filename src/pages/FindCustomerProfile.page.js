import React from 'react';
import FindCustomerProfile from '../components/FindCustomerProfile/FindCustomerProfile.component';
import { LOAD_PROFILE } from './_constants';
import { analyticsProvider, events } from '../providers/analytics';

const FindCustomerProfilePage = ({ navigation }) => {
  const storeId = navigation.getParam('storeId');
  const alias = navigation.getParam('alias');

  const onRedirectHandler = (page, initialStep = '') => () => {
    navigation.navigate(page, { storeId, alias, initialStep });

    analyticsProvider.logEvent(events.find_customer_profile[page]);
  };

  return (
    <FindCustomerProfile
      onInputAliasPressHandler={onRedirectHandler(LOAD_PROFILE, 'manualLoad')}
      onScanProfilePressHandler={onRedirectHandler(LOAD_PROFILE)}
    />
  );
};

export default FindCustomerProfilePage;
