/** External dependencies */
import React, { useCallback, useEffect, useState } from 'react';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';

/** Wrapper */
import LoadProfileComponent from '../components/LoadProfile/LoadProfile.component';

/** Constants */
import {
  EMPLOYEE_GET_USER_AND_CUSTOMER,
  EMPLOYEE_LOAD_POINTS,
  EMPLOYEE_CREATE_CUSTOMER,
  EMPLOYEE_REGISTER_REWARD_USAGE,
} from '../queries/partner.queries';

/** Utils */
import { parseError } from '../utils/graphql-error.utils';
import { analyticsProvider, events } from '../providers/analytics';

const STEPS = {
  qrScanner: 'qrScanner',
  manualLoad: 'manualLoad',
  profile: 'profile',
  history: 'history',
  rewards: 'rewards',
  loadPointsInput: 'loadPointsInput',
  addCustomer: 'addCustomer',
  stepError: 'stepError',
};

const LoadProfile = ({ navigation }) => {
  const initialStep = navigation.getParam('initialStep');
  const customerAlias = navigation.getParam('customerAlias');

  const [currentStep, setCurrentStep] = useState(
    STEPS[initialStep] ? STEPS[initialStep] : STEPS.qrScanner
  );
  const [loading, setLoading] = useState(false);
  const [hasLoadedProfileFromAlias, setHasLoadedProfileFromAlias] =
    useState(false);
  const [error, setError] = useState(null);
  const [userField, setUserField] = useState();
  const [isInitializingCustomer, setIsInitializingCustomer] = useState(
    Boolean(customerAlias)
  );

  const [loadPoints] = useMutation(EMPLOYEE_LOAD_POINTS, {
    onCompleted: () => {
      setCurrentStep(STEPS.profile);
      refetchProfile();
      setLoading(false);
      analyticsProvider.logEvent(events.profile.load_points);
    },
    onError: (queryError) => {
      const pUserError = parseError(queryError);
      if (!pUserError) return;
      setCurrentStep(STEPS.stepError);
      setError({
        secondaryText: pUserError.text,
      });
      setLoading(false);
    },
  });

  const storeId = navigation.getParam('storeId');

  const [registerStoreRewardUsage] = useMutation(
    EMPLOYEE_REGISTER_REWARD_USAGE,
    {
      variables: { storeId },
      onCompleted: () => {
        refetchProfile();
        setLoading(false);
        analyticsProvider.logEvent(events.profile.reward_usage);
      },
      onError: (queryError) => {
        const pUserError = parseError(queryError);
        if (!pUserError) return;
        setError({
          secondaryText: pUserError.text,
        });
        setCurrentStep(STEPS.stepError);
        setLoading(false);
      },
    }
  );

  const [getCustomer, { data: clientData, refetch: refetchProfile }] =
    useLazyQuery(EMPLOYEE_GET_USER_AND_CUSTOMER, {
      notifyOnNetworkStatusChange: true,
      variables: { storeId },
      fetchPolicy: 'network-only',
      onCompleted: (data) => {
        const { employeeGetCustomer: customerData } = data;

        if (!customerData) {
          // Action to became user a customer.
          setCurrentStep(STEPS.addCustomer);
          analyticsProvider.logEvent(events.load_profile.loaded_successfully);
        } else {
          setCurrentStep(STEPS.profile);
        }

        setLoading(false);
        setIsInitializingCustomer(false);
      },
      onError: (queryError) => {
        const pUserError = parseError(queryError);
        if (!pUserError) return;
        setError({
          secondaryText: pUserError.text,
        });
        setCurrentStep(STEPS.stepError);
        setLoading(false);
      },
    });

  const [addCustomer] = useMutation(EMPLOYEE_CREATE_CUSTOMER, {
    variables: { storeId },
    onCompleted: () => {
      refetchProfile();
      analyticsProvider.logEvent(events.load_profile.customer_added);
    },
    onError: (queryError) => {
      const pUserError = parseError(queryError);
      if (!pUserError) return;
      setError({
        secondaryText: pUserError.text,
      });
      setCurrentStep(STEPS.stepError);
      setLoading(false);
    },
  });

  const goBackOnPress = () => {
    switch (currentStep) {
      case STEPS.qrScanner:
      case STEPS.manualLoad:
      case STEPS.profile:
      case STEPS.stepError:
      case STEPS.addCustomer:
        return navigation.goBack();
      case STEPS.loadPointsInput:
      case STEPS.rewards:
      case STEPS.history:
        return setCurrentStep(STEPS.profile);
      default:
        return false;
    }
  };

  const stepPressHandler = (step) => () => {
    setCurrentStep(step);
  };

  const onAddClientPressHandler = () => {
    setLoading(true);
    addCustomer({ variables: { userField } });
  };

  const onLoadInfoContinuePressHandler = useCallback(
    (userFieldValue) => {
      setLoading(true);
      getCustomer({ variables: { userField: userFieldValue } });
      setUserField(userFieldValue);
    },
    [getCustomer]
  );

  const onPointsLoadedPressHandler = (amount) => {
    setLoading(true);
    loadPoints({
      variables: {
        storeId,
        amount,
        userField,
      },
    });
  };

  const onRewardUsagePressHandler = (storeRewardId) => {
    registerStoreRewardUsage({ variables: { userField, storeRewardId } });
  };

  useEffect(
    function initCustomerProfile() {
      const shouldGetCustomerFromAlias =
        customerAlias &&
        !hasLoadedProfileFromAlias &&
        currentStep !== STEPS.profile;

      if (shouldGetCustomerFromAlias) {
        onLoadInfoContinuePressHandler(customerAlias);
        setHasLoadedProfileFromAlias(true);
      }
    },
    [
      currentStep,
      customerAlias,
      hasLoadedProfileFromAlias,
      onLoadInfoContinuePressHandler,
    ]
  );

  return (
    <LoadProfileComponent
      step={currentStep}
      STEPS={STEPS}
      goBackOnPress={goBackOnPress}
      error={error}
      loading={loading}
      rewardsData={clientData && clientData.employeeGetCustomerAvailableRewards}
      customerData={clientData && clientData.employeeGetCustomer}
      userData={clientData && clientData.employeeGetUser}
      isInitializingCustomer={isInitializingCustomer}
      onManualLoadPress={stepPressHandler(STEPS.manualLoad)}
      onLoadPointsPressHandler={stepPressHandler(STEPS.loadPointsInput)}
      onHistoryPressHandler={stepPressHandler(STEPS.history)}
      onRewardsPressHandler={stepPressHandler(STEPS.rewards)}
      onRewardUsagePressHandler={onRewardUsagePressHandler}
      onPointsLoadedPressHandler={onPointsLoadedPressHandler}
      onAddClientPressHandler={onAddClientPressHandler}
      onLoadInfoContinuePressHandler={onLoadInfoContinuePressHandler}
    />
  );
};

export default LoadProfile;
