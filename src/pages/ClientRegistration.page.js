/** External dependencies */
import React, { useState } from 'react';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';

/** Wrapper */
import ClientRegistration from '../components/ClientRegistration/ClientRegistration.component';

/** Constants */
import {
  EMPLOYEE_GET_CUSTOMER,
  EMPLOYEE_GET_USER,
  EMPLOYEE_INVITE_NEW_USER,
  EMPLOYEE_CREATE_CUSTOMER,
} from '../queries/partner.queries';

/** Utils */
import { parseError } from '../utils/graphql-error.utils';
import { analyticsProvider, events } from '../providers/analytics';

const STEPS = {
  stepEmail: 'stepEmail',
  stepUser: 'stepUser',
  stepConfirmation: 'stepConfirmation',
  stepSuccess: 'stepSuccess',
  stepLoadPoints: 'stepLoadPoints',
  stepError: 'stepError',
};

const ClientRegistrationPage = (props) => {
  const [currentStep, setCurrentStep] = useState(STEPS.stepEmail);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [userData, setUserData] = useState({});
  const { navigation } = props;
  const storeId = navigation.getParam('storeId');

  const [getUser] = useLazyQuery(EMPLOYEE_GET_USER, {
    fetchPolicy: 'network-only',
    variables: {
      storeId,
    },
    onCompleted: (data) => {
      /** If it is not a user at the platform, we redirect to userStep */
      const userReqData = data.employeeGetUser;

      if (!userReqData) {
        setLoading(false);
        setCurrentStep(STEPS.stepUser);
        return;
      }

      /** If user exists we delete all the other info and we only save the email and isUser var */
      setUserData({
        email: userReqData.email,
        firstName: userReqData.firstName,
        lastName: userReqData.lastName,
        isUser: true,
      });

      getCustomer({
        variables: {
          userField: userData.email,
        },
      });
    },
    onError: (queryError) => {
      const pRewardsError = parseError(queryError);
      if (!pRewardsError) return;
      setCurrentStep(STEPS.stepError);
      setError({
        secondaryText: pRewardsError.text,
      });
      setLoading(false);
    },
  });

  const [getCustomer] = useLazyQuery(EMPLOYEE_GET_CUSTOMER, {
    fetchPolicy: 'network-only',
    variables: {
      storeId,
    },
    onCompleted: (data) => {
      /** If user is not a customer we go to confirmation step */
      if (!data.employeeGetCustomer) {
        setCurrentStep(STEPS.stepConfirmation);
        setLoading(false);
        return;
      }
      /** If user is already a customer, we notify. */
      setError({
        secondaryText: 'Parece que el usuario ya es un cliente',
      });
      setCurrentStep(STEPS.stepError);
      setLoading(false);
    },
  });

  const [employeeCreateCustomer] = useMutation(EMPLOYEE_CREATE_CUSTOMER, {
    variables: { storeId },
    options: () => ({
      fetchPolicy: 'no-cache',
    }),
    onCompleted: () => {
      setCurrentStep(STEPS.stepSuccess);
      setLoading(false);
      analyticsProvider.logEvent(events.invite_customer.invite_customer);
    },
    onError: (queryError) => {
      const pUserError = parseError(queryError);
      if (!pUserError) return;
      setCurrentStep(STEPS.stepError);
      setError({
        secondaryText: pUserError.text,
      });
      setLoading(false);
    },
  });

  const [employeeInviteNewUser] = useMutation(EMPLOYEE_INVITE_NEW_USER, {
    variables: { storeId },
    options: () => ({
      fetchPolicy: 'no-cache',
    }),
    onCompleted: () => {
      setCurrentStep(STEPS.stepSuccess);
      setLoading(false);
      analyticsProvider.logEvent(events.invite_customer.create_new_customer);
    },
    onError: (queryError) => {
      const pUserError = parseError(queryError);
      if (!pUserError) return;
      setCurrentStep(STEPS.stepError);
      setError({
        secondaryText: pUserError.text,
      });
      setLoading(false);
    },
  });

  const goBackOnPress = () => {
    switch (currentStep) {
      case STEPS.stepEmail:
      case STEPS.stepSuccess:
      case STEPS.stepError:
        return navigation.goBack();
      case STEPS.stepUser:
        return setCurrentStep(STEPS.stepEmail);
      case STEPS.stepConfirmation:
        return setCurrentStep(
          userData.isUser ? STEPS.stepEmail : STEPS.stepUser
        );
      default:
        return false;
    }
  };

  const onEmailContinuePressHandler = ({ email }) => {
    setLoading(true);
    /** In case we come from another step, is it wasnt marked as a user then
     * we save the data so its not lost.
     */
    if (!userData.isUser) {
      setUserData({
        ...userData,
        email,
      });
    } else {
      setUserData({
        email,
      });
    }

    /** We validate if user exists */
    getUser({
      variables: {
        userField: email,
      },
    });
  };

  const onUserContinuePressHandler = ({ firstName, lastName }) => {
    setUserData({
      ...userData,
      firstName,
      lastName,
    });
    setCurrentStep(STEPS.stepConfirmation);
  };

  const onConfirmationPressHandler = async () => {
    setLoading(true);

    if (userData.isUser) {
      employeeCreateCustomer({
        variables: {
          userField: userData.email,
        },
      });
    } else {
      employeeInviteNewUser({
        variables: {
          ...userData,
        },
      });
    }
  };

  return (
    <ClientRegistration
      step={currentStep}
      STEPS={STEPS}
      error={error}
      loading={loading}
      goBackOnPress={goBackOnPress}
      clientRegistrationData={userData}
      onEmailContinuePressHandler={onEmailContinuePressHandler}
      onUserContinuePressHandler={onUserContinuePressHandler}
      onConfirmationPressHandler={onConfirmationPressHandler}
    />
  );
};

export default ClientRegistrationPage;
