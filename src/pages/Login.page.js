/** External dependencies */
import firebase from 'firebase';
import React, { Component } from 'react';

/** Wrapper */
import Login from '../components/Login/Login.component';
import { RECOVER_PASSWORD } from './_constants';

class LoginPage extends Component {
  state = {
    loginForm: {
      isFetching: false,
      error: null,
      fields: {
        emailHasError: false,
        emailErrorMsg: '',
        passwordHasError: false,
        passwordErrorMsg: '',
        showPassword: true,
      },
      value: {},
    },
  };

  onPress = async () => {
    const { email, password } = this.state.loginForm.value;

    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);
    } catch (error) {
      this.setState({
        ...this.state,
        loginForm: {
          ...this.state.loginForm,
          error: 'AUTH_FORM.loginError',
        },
      });
    }
  };

  onChange = (value) => {
    this.setState({
      ...this.state,
      loginForm: { ...this.state.loginForm, value },
    });
  };

  render() {
    const { loginForm } = this.state;
    const { navigation } = this.props;

    const loginFormProp = {
      onPress: this.onPress,
      isFetching: loginForm.isFetching,
      fields: loginForm.fields,
      value: loginForm.value,
      error: loginForm.error,
      onChange: this.onChange,
    };

    return (
      <Login
        goBackOnPress={() => navigation.goBack()}
        recoverPasswordOnPress={() => navigation.navigate(RECOVER_PASSWORD)}
        loginForm={loginFormProp}
      />
    );
  }
}

export default LoginPage;
