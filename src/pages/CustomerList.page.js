import { useQuery } from '@apollo/react-hooks';
import React, { useEffect } from 'react';
import CustomerList from '../components/CustomerList/CustomerList.component';
import { analyticsProvider, events } from '../providers/analytics';
import { EMPLOYEE_GET_STORE_CUSTOMERS } from '../queries/partner.queries';
import { LOAD_PROFILE } from './_constants';

const CustomerListPage = ({ navigation }) => {
  const storeId = navigation.getParam('storeId');

  const {
    loading,
    data = [],
    refetch,
  } = useQuery(EMPLOYEE_GET_STORE_CUSTOMERS, {
    fetchPolicy: 'cache-and-network',
    variables: { storeId },
  });

  useEffect(() => {
    const subscription = navigation.addListener('willFocus', () => {
      refetch();
    });

    return subscription.remove;
  }, [navigation, refetch]);

  const onRedirectHandler =
    (page) =>
    (customerAlias = '') => {
      navigation.navigate(page, { storeId, customerAlias });

      analyticsProvider.logEvent(events.customer_list[page]);
    };

  return (
    <CustomerList
      onCustomerProfilePressHandler={onRedirectHandler(LOAD_PROFILE)}
      customers={data.employeeGetStoreCustomers || []}
      loading={loading}
    />
  );
};

export default CustomerListPage;
