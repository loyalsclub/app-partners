/** External dependencies */
import React, { Component } from 'react';
import { withApollo } from '@apollo/react-hoc';
import * as WebBrowser from 'expo-web-browser';
import { Linking } from 'react-native';
import { signOut } from '../utils/auth.utils';
import { WHATSAPP_CONTACT } from '../config/constants';

/** Wrapper */
import Menu from '../components/Menu/Menu.component';
import { analyticsProvider, events } from '../providers/analytics';
import translate from '../utils/language.utils';

class MenuPage extends Component {
  onSignOutPressHandler = async () => {
    const { client } = this.props;
    await signOut();
    client.resetStore();
    analyticsProvider.logEvent(events.menu.log_out_button);
    analyticsProvider.clearUserProperties();
  };

  render = () => {
    const { navigation } = this.props;
    return (
      <Menu
        goBackOnPress={navigation.goBack}
        onHelpAndSupportPressHandler={() => {
          WebBrowser.openBrowserAsync(
            'https://partners.loyals.club/FAQ?utm_source=app_partners&utm_medium=hyperlink&utm_content=menu_help_and_support'
          );
          analyticsProvider.logEvent(events.menu.help_and_support_button);
        }}
        onWhatsAppContactPressHandler={() =>
          Linking.openURL(
            `https://wa.me/${WHATSAPP_CONTACT}?text=${encodeURIComponent(
              translate('MENU.wppPrefilledMessage')
            )}`
          )
        }
        onSignOutPressHandler={this.onSignOutPressHandler}
      />
    );
  };
}

export default withApollo(MenuPage);
