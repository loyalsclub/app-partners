import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import StoreProfile from '../components/StoreProfile/StoreProfile.component';
import {
  EMPLOYEE_STORE_INFO,
  STORE_GET_CATEGORIES_AND_COUNTRIES,
} from '../queries/partner.queries';
import { parseError } from '../utils/graphql-error.utils';
import { STORE_PROFILE_EDIT } from './_constants';

const StoreProfilePage = ({ navigation }) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState();

  const { data = {} } = useQuery(EMPLOYEE_STORE_INFO, {
    variables: {
      alias: navigation.getParam('alias'),
    },
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
    onCompleted: () => {
      setLoading(false);
    },
    onError: (queryError) => {
      const pUserError = parseError(queryError);
      if (!pUserError) {
        return;
      }

      setError({
        secondaryText: pUserError.text,
      });

      setLoading(false);
    },
  });

  const { data: categoriesAndCountriesData = {} } = useQuery(
    STORE_GET_CATEGORIES_AND_COUNTRIES,
    {
      onCompleted: () => setLoading(false),
    }
  );

  const {
    storeCategories = [],
    storeCountries = [],
  } = categoriesAndCountriesData;

  const onEditPressHandler = (property) => () => {
    navigation.push(STORE_PROFILE_EDIT, {
      storeId: navigation.getParam('storeId'),
      alias: navigation.getParam('alias'),
      property,
    });
  };

  return (
    <StoreProfile
      store={data.storeInfoByAlias || {}}
      storeCategories={storeCategories}
      storeCountries={storeCountries}
      loading={loading}
      error={error}
      onEditPressHandler={onEditPressHandler}
    />
  );
};

export default StoreProfilePage;
