import React, { useState } from 'react';
import { View, Image } from 'react-native';
import { Asset } from 'expo-asset';
import AppLoading from 'expo-app-loading';
// FIXME: Installed expo-font manually, version ~8.2.1 and it was having issues to load fonts. Disabling eslint error to be able to commit file.
// eslint-disable-next-line import/no-extraneous-dependencies
import * as Font from 'expo-font';
import firebase from 'firebase';
import { analyticsProvider } from '../providers/analytics';
import { storageProvider, storageKeys } from '../providers/storage';

export default ({ navigation }) => {
  const [isReady, setIsReady] = useState(false);

  const cacheImages = (images) => {
    return Promise.all(
      images.map((image) => {
        if (typeof image === 'string') {
          return Image.prefetch(image);
        }
        return Asset.fromModule(image).downloadAsync();
      })
    );
  };

  const loadFonts = () => {
    return Font.loadAsync({
      AccordAlt_Medium: require('../assets/fonts/AccordAlt_Medium.otf'),
      AccordAlt_Bold: require('../assets/fonts/AccordAlt_Bold.otf'),
      AccordAlt_Light: require('../assets/fonts/AccordAlt_Light.otf'),
      LoyalIcons: require('../assets/icons/icomoon.ttf'),
    });
  };

  const loadIntroScreenValidation = () => {
    return storageProvider.get(storageKeys.INTRO_SCREEN_DISPLAYED, true);
  };

  // Fetch the token from storage then navigate to our appropriate place
  const bootstrapAsync = () => {
    return new Promise((resolve) => {
      firebase.auth().onAuthStateChanged((user) => {
        navigation.navigate(user ? 'App' : 'Auth');

        if (user) {
          analyticsProvider.setUserId(user.uid);
        }

        resolve();
      });
    });
  };

  const loadAssetsAsync = () => {
    // Load images on App Start.
    const imageAssets = cacheImages([]);

    // Load fonts on App Start.
    const fontAssets = loadFonts();

    const hasAlreadySeenIntroScreen = loadIntroScreenValidation();

    return Promise.all([fontAssets, imageAssets, hasAlreadySeenIntroScreen])
      .catch((err) => console.error(err))
      .then(bootstrapAsync);
  };

  return !isReady ? (
    <AppLoading
      startAsync={loadAssetsAsync}
      onFinish={() => setIsReady(true)}
      onError={console.warn}
    />
  ) : (
    <View />
  );
};
