/** External dependencies */
import React, { useCallback, useState } from 'react';
import { useMutation, useLazyQuery } from '@apollo/react-hooks';
import { analyticsProvider, events } from '../providers/analytics';

/** Wrapper */
import RegisterComponent from '../components/Register/Register.component';
import { parseError } from '../utils/graphql-error.utils';

/** Queries */
import {
  CREATE_PARTNER_USER,
  FIND_PENDING_USER,
} from '../queries/user.queries';
import {
  isEmailVerified,
  signInWithCustomToken,
  sendVerificationEmail,
  isUserLoggedIn,
} from '../utils/auth.utils';

const STEPS = {
  email: 'email',
  personal_information: 'personal_information',
  password: 'password',
  success: 'success',
  error: 'error',
};

const Register = ({ navigation }) => {
  const [currentStep, setCurrentStep] = useState(STEPS.email);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const [findPendingUserQuery] = useLazyQuery(FIND_PENDING_USER, {
    fetchPolicy: 'network-only',
    onCompleted: (data) => {
      if (data.findPendingUser && !data.findPendingUser.isPending) {
        return setError('Ups, parece que el email ya se encuentra registrado!');
      }

      analyticsProvider.logEvent(events.user_registration.email_validated);

      return setCurrentStep(STEPS.personal_information);
    },
    onError: (queryError) => {
      const parsedError = parseError(queryError);

      setCurrentStep(STEPS.error);
      setError(parsedError.text || '');
      setLoading(false);
    },
  });

  const findPendingUser = (email) => {
    setError(null);

    findPendingUserQuery({ variables: { email } });
  };

  const [createPartnerUserMutation] = useMutation(CREATE_PARTNER_USER, {
    onCompleted: ({ createPartnerUser }) => {
      const { loginToken } = createPartnerUser;

      signInWithCustomToken(loginToken)
        .then(() => {
          if (isUserLoggedIn() && !isEmailVerified()) {
            analyticsProvider.logEvent(
              events.user_registration.email_verification_sent
            );

            return sendVerificationEmail();
          }

          return Promise.resolve();
        })
        .then(() => {
          navigation.navigate('Home');
        });

      analyticsProvider.logEvent(events.user_registration.user_created);
    },
    onError: (queryError) => {
      const parsedError = parseError(queryError);

      setCurrentStep(STEPS.error);
      setError(parsedError.text || '');
      setLoading(false);
    },
  });

  const createPartnerUser = useCallback(
    (user) => {
      setLoading(true);

      createPartnerUserMutation({
        variables: user,
      });

      analyticsProvider.logEvent(events.user_registration.password_completed);
    },
    [createPartnerUserMutation]
  );

  const goBackOnPress = () => {
    switch (currentStep) {
      case STEPS.email:
      case STEPS.error:
        return navigation.goBack();
      case STEPS.personal_information:
        return setCurrentStep(STEPS.email);
      case STEPS.password:
        return setCurrentStep(STEPS.personal_information);
      default:
        return '';
    }
  };

  const onPasswordStepPressHandler = () => {
    setCurrentStep(STEPS.password);

    analyticsProvider.logEvent(
      events.user_registration.personal_information_completed
    );
  };

  return (
    <RegisterComponent
      step={currentStep}
      STEPS={STEPS}
      error={error}
      loading={loading}
      createPartnerUser={createPartnerUser}
      onPasswordStepPressHandler={onPasswordStepPressHandler}
      goBackOnPress={goBackOnPress}
      findPendingUser={findPendingUser}
      clearError={() => setError('')}
    />
  );
};

export default Register;
