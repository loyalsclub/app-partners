/** External dependencies */
import React, { useState } from 'react';
import { useMutation, useQuery } from '@apollo/react-hooks';

/** Wrapper */
import GenerateCouponComponent from '../components/GenerateCoupon/GenerateCoupon.component';

/** Constants */
import {
  EMPLOYEE_CREATE_COUPON,
  EMPLOYEE_STORE_INFO,
  STORE_GET_CATEGORIES_AND_COUNTRIES,
} from '../queries/partner.queries';
import { analyticsProvider, events } from '../providers/analytics';

/** Utils */
import { parseError } from '../utils/graphql-error.utils';

const STEPS = {
  loadPointsInput: 'loadPointsInput',
  couponInfo: 'couponInfo',
  newContact: 'newContact',
  stepError: 'stepError',
};

const GenerateCoupon = (props) => {
  const { navigation } = props;
  const storeId = navigation.getParam('storeId');
  const alias = navigation.getParam('alias');
  const [currentStep, setCurrentStep] = useState(STEPS.loadPointsInput);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [couponInfo, setCouponInfo] = useState(null);

  const { data: { storeInfoByAlias: store } = {} } = useQuery(
    EMPLOYEE_STORE_INFO,
    {
      variables: { alias },
      fetchPolicy: 'cache-and-network',
      notifyOnNetworkStatusChange: true,
      onCompleted: () => {
        setLoading(false);
      },
      onError: (queryError) => {
        const pUserError = parseError(queryError);
        if (!pUserError) return;
        setCurrentStep(STEPS.stepError);
        setError({
          secondaryText: pUserError.text,
        });
        setLoading(false);
      },
    }
  );

  const { data: { storeCountries } = { storeCountries: [] } } = useQuery(
    STORE_GET_CATEGORIES_AND_COUNTRIES
  );

  const [generateCoupon] = useMutation(EMPLOYEE_CREATE_COUPON, {
    onCompleted: (couponData) => {
      setCouponInfo({
        ...couponInfo,
        ...couponData.employeeCreateCoupon,
      });
      setCurrentStep(STEPS.couponInfo);
      setLoading(false);
      analyticsProvider.logEvent(events.generate_coupon.coupon_generated);
    },
    onError: (queryError) => {
      const pUserError = parseError(queryError);
      if (!pUserError) return;
      setCurrentStep(STEPS.stepError);
      setError({
        secondaryText: pUserError.text,
      });
      setLoading(false);
    },
  });

  const goBackOnPress = () => navigation.goBack();

  const stepPressHandler = (step) => () => {
    setCurrentStep(step);
  };

  const onPointsLoadedPressHandler = (amount) => {
    setLoading(true);
    setCouponInfo({
      amount,
    });
    generateCoupon({
      variables: {
        storeId,
        amount,
      },
    });
  };

  return (
    <GenerateCouponComponent
      step={currentStep}
      STEPS={STEPS}
      goBackOnPress={goBackOnPress}
      error={error}
      loading={loading}
      couponInfo={couponInfo}
      onLoadPointsPressHandler={stepPressHandler(STEPS.loadPointsInput)}
      onPointsLoadedPressHandler={onPointsLoadedPressHandler}
      onNewContactPressHandler={stepPressHandler(STEPS.newContact)}
      store={store}
      storeCountries={storeCountries}
    />
  );
};

export default GenerateCoupon;
