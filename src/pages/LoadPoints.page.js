import React from 'react';
import LoadPoints from '../components/LoadPoints/LoadPoints.component';
import { LOAD_PROFILE, GENERATE_COUPON } from './_constants';
import { analyticsProvider, events } from '../providers/analytics';

const LoadPointsPage = ({ navigation }) => {
  const storeId = navigation.getParam('storeId');
  const alias = navigation.getParam('alias');

  const onRedirectHandler = (page) => () => {
    navigation.navigate(page, { storeId, alias });

    analyticsProvider.logEvent(events.load_points[page]);
  };

  return (
    <LoadPoints
      onSendPointsPressHandler={onRedirectHandler(GENERATE_COUPON)}
      onScanProfilePressHandler={onRedirectHandler(LOAD_PROFILE)}
    />
  );
};

export default LoadPointsPage;
