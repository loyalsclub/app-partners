/** External dependencies */
import React, { useCallback, useEffect, useState } from 'react';
import { useQuery, useMutation, useLazyQuery } from '@apollo/react-hooks';

/** Wrapper */
import CreatePartnerComponent from '../components/CreatePartner/CreatePartner.component';

/** Utils */
import { parseError } from '../utils/graphql-error.utils';

/** Queries */
import {
  EMPLOYEE_LEVELS_AND_REWARDS_BY_STOREID,
  STORE_CREATE,
  STORE_GET_CATEGORIES_AND_COUNTRIES,
} from '../queries/partner.queries';
import { analyticsProvider, events } from '../providers/analytics';

const STEPS = {
  profileImage: 'profileImage',
  information: 'information',
  contact: 'contact',
  points: 'points',
  review: 'review',
  success: 'success',
  error: 'error',
};

const CreatePartner = ({ navigation }) => {
  const isHelpMode = navigation.getParam('helpMode');
  const storeId = navigation.getParam('storeId');
  const storeAlias = navigation.getParam('alias');

  const [createdStore, setCreatedStore] = useState(
    storeAlias && { alias: storeAlias }
  );
  const [currentStep, setCurrentStep] = useState(
    isHelpMode ? STEPS.success : STEPS.profileImage
  );
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const handleError = (queryError) => {
    const parsedError = parseError(queryError);

    setCurrentStep(STEPS.error);
    setError(parsedError.text);
    setLoading(false);
  };

  const { data = {} } = useQuery(STORE_GET_CATEGORIES_AND_COUNTRIES, {
    onCompleted: () => {
      if (!isHelpMode) {
        setLoading(false);
      }
    },
  });

  const [getStoreLevelsAndRewards, { data: dataStoreLevelAndRewards = {} }] =
    useLazyQuery(EMPLOYEE_LEVELS_AND_REWARDS_BY_STOREID, {
      fetchPolicy: 'network',
      onCompleted: () => {
        setLoading(false);
      },
      onError: handleError,
    });

  const { storeCategories = [], storeCountries = [] } = data;

  const { storeLevelsAndRewardsById } = dataStoreLevelAndRewards;

  const [createStoreMutation] = useMutation(STORE_CREATE, {
    onCompleted: (successData) => {
      const { createStoreForUser } = successData;

      getStoreLevelsAndRewards({
        variables: {
          storeId: createStoreForUser.id,
        },
      });

      setCurrentStep(STEPS.success);

      setCreatedStore(createStoreForUser);

      analyticsProvider.logEvent(events.partner_registration.partner_created);
    },
    onError: handleError,
  });

  const createStore = useCallback(
    (store) => {
      setLoading(true);

      createStoreMutation({
        variables: { store },
      });
    },
    [createStoreMutation]
  );

  const goBackOnPress = () => {
    switch (currentStep) {
      case STEPS.profileImage:
      case STEPS.error:
        return navigation.goBack();
      case STEPS.information:
        return setCurrentStep(STEPS.profileImage);
      case STEPS.contact:
        return setCurrentStep(STEPS.information);
      case STEPS.points:
        return setCurrentStep(STEPS.contact);
      case STEPS.review:
        return setCurrentStep(STEPS.points);
      case STEPS.success:
      default:
        return false;
    }
  };

  const stepPressHandler = (step) => () => setCurrentStep(step);

  const onFinishCreationPressHandler = useCallback(
    () =>
      navigation.navigate('Home', {
        refetchEmployeeProfiles: true,
      }),
    [navigation]
  );

  useEffect(
    function redirectToHomeOnLeave() {
      return onFinishCreationPressHandler;
    },
    [onFinishCreationPressHandler]
  );

  useEffect(
    function handleHelpMode() {
      if (isHelpMode && storeId) {
        getStoreLevelsAndRewards({ variables: { storeId } });
      }
    },
    [getStoreLevelsAndRewards, isHelpMode, storeId]
  );

  return (
    <CreatePartnerComponent
      step={currentStep}
      STEPS={STEPS}
      error={error}
      loading={loading}
      storeCategories={storeCategories}
      storeCountries={storeCountries}
      createStore={createStore}
      storeLevelsAndRewardsById={storeLevelsAndRewardsById || []}
      onInformationStepPressHandler={stepPressHandler(STEPS.information)}
      onContactStepPressHandler={stepPressHandler(STEPS.contact)}
      onPointsStepPressHandler={stepPressHandler(STEPS.points)}
      onReviewStepPressHandler={stepPressHandler(STEPS.review)}
      onFinishCreationPressHandler={onFinishCreationPressHandler}
      goBackOnPress={goBackOnPress}
      createdStore={createdStore}
    />
  );
};

export default CreatePartner;
