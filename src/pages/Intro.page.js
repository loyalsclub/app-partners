import React, { useCallback, useEffect } from 'react';
import { AUTH_HOME } from './_constants';
import IntroComponent from '../components/AuthHome/Intro/Intro.component';
import { storageKeys, storageProvider } from '../providers/storage';

export default ({ navigation }) => {
  const redirectToAuthHome = useCallback(
    () => navigation.navigate(AUTH_HOME),
    [navigation]
  );

  useEffect(
    function checkIfAlreadySeenIntro() {
      const hasAlreadySeenIntroScreen = storageProvider.getSync(
        storageKeys.INTRO_SCREEN_DISPLAYED
      );

      if (hasAlreadySeenIntroScreen) {
        redirectToAuthHome();
      }
    },
    [navigation, redirectToAuthHome]
  );

  return <IntroComponent onStartPressHandler={redirectToAuthHome} />;
};
