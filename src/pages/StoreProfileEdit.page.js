import React, { useCallback, useState } from 'react';
import { useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import {
  EMPLOYEE_STORE_INFO,
  EMPLOYEE_MODIFY_STORE,
  STORE_GET_CATEGORIES_AND_COUNTRIES,
  STORE_VALIDATE_ALIAS,
} from '../queries/partner.queries';
import { parseError } from '../utils/graphql-error.utils';
import StoreProfileEdit from '../components/StoreProfileEdit/StoreProfileEdit.component';
import translate from '../utils/language.utils';

const StoreProfileEditPage = ({ navigation }) => {
  const alias = navigation.getParam('alias');
  const propertyToEdit = navigation.getParam('property');

  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const { data = {}, refetch: refetchStoreInfo } = useQuery(
    EMPLOYEE_STORE_INFO,
    {
      variables: {
        alias,
      },
      onCompleted: () => {
        setLoading(false);
      },
      onError: (queryError) => {
        const pUserError = parseError(queryError);
        if (!pUserError) {
          return;
        }

        setError({
          secondaryText: pUserError.text,
        });

        setLoading(false);
      },
    }
  );

  const { data: categoriesAndCountriesData = {} } = useQuery(
    STORE_GET_CATEGORIES_AND_COUNTRIES,
    {
      onCompleted: () => setLoading(false),
    }
  );

  const { storeCategories = [], storeCountries = [] } =
    categoriesAndCountriesData;

  const [modifyStoreMutation] = useMutation(EMPLOYEE_MODIFY_STORE, {
    onCompleted: () => {
      refetchStoreInfo();
      navigation.goBack();
    },
    onError: (queryError) => {
      setLoading(false);

      const parsedError = parseError(queryError);

      if (!parsedError) {
        return;
      }

      setError({ secondaryText: parsedError.text || '' });
    },
  });

  const updateStore = useCallback(
    (id, store) => {
      setLoading(true);

      modifyStoreMutation({
        variables: {
          storeId: id,
          store,
        },
      });
    },
    [modifyStoreMutation]
  );

  const [validateStoreAliasQuery] = useLazyQuery(STORE_VALIDATE_ALIAS, {
    onCompleted: ({ validateStoreAlias: isValid }) => {
      if (!isValid) {
        setError({ alias: translate('FORMS_ERROR_MESSAGES.invalidAlias') });
      } else {
        setError(null);
      }
    },
    onError: (queryError) => {
      setLoading(false);

      const parsedError = parseError(queryError);

      if (!parsedError) {
        return;
      }

      setError({ secondaryText: parsedError.text || '' });
    },
  });

  const validateStoreAlias = (newAlias) =>
    validateStoreAliasQuery({ variables: { alias: newAlias } });

  return (
    <StoreProfileEdit
      loading={loading}
      error={error}
      updateStore={updateStore}
      validateStoreAlias={validateStoreAlias}
      storeCategories={storeCategories}
      storeCountries={storeCountries}
      store={data.storeInfoByAlias || {}}
      propertyToEdit={propertyToEdit}
    />
  );
};

export default StoreProfileEditPage;
