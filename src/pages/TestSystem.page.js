/** External dependencies */
import firebase from 'firebase';
import React from 'react';

/** Wrapper */
import TestSystem from '../components/TestSystem/TestSystem.component';
import { analyticsProvider, events } from '../providers/analytics';

export default (props) => {
  const { navigation } = props;
  const STORES = {
    HELADERIA: 'HELADERIA',
    CAFE: 'CAFE',
    DECORACION: 'DECORACION',
    PIZZERIA: 'PIZZERIA',
  };

  const testLogin = (storeType) => async () => {
    let email;
    let event;
    const password = 'Qwe12345';

    switch (storeType) {
      case STORES.HELADERIA:
        email = 'nocetti.tomas+heladeriasabor@gmail.com';
        event = events.test_system.test_heladeria_sabor;
        break;
      case STORES.CAFE:
        email = 'nocetti.tomas+cafeloshermanos@gmail.com';
        event = events.test_system.test_cafe_hermanos;
        break;
      case STORES.DECORACION:
        email = 'nocetti.tomas+decoracionluna@gmail.com';
        event = events.test_system.test_decoracion_luna;
        break;
      case STORES.PIZZERIA:
        email = 'nocetti.tomas+pizzeriagomez@gmail.com';
        event = events.test_system.test_pizzeria_gomez;
        break;
      default:
        return;
    }

    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);

      analyticsProvider.logEvent(event);

      analyticsProvider.setUserProperties({
        testMode: true,
      });
    } catch (error) {
      console.warn(error);
    }
  };

  return (
    <TestSystem
      stores={STORES}
      goBackOnPress={() => {
        navigation.goBack();
      }}
      testLogin={testLogin}
    />
  );
};
