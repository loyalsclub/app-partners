import React, { useState } from 'react';
import RecoverPassword from '../components/RecoverPassword/RecoverPassword.component';
import { recoverPassword } from '../utils/auth.utils';
import { LOGIN } from './_constants';

const RecoverPasswordPage = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [recoverEmailSent, setRecoverEmailSent] = useState(false);

  const onRedirectHandler = (page) => () => navigation.navigate(page);

  const onSubmitPressHandler = async ({ email }) => {
    setLoading(true);

    try {
      await recoverPassword(email);
    } catch (err) {
      console.info('[Recover Password] Error', err);
    }

    setRecoverEmailSent(true);
    setLoading(false);
  };

  return (
    <RecoverPassword
      loading={loading}
      recoverEmailSent={recoverEmailSent}
      onSubmitPressHandler={onSubmitPressHandler}
      onLoginPressHandler={onRedirectHandler(LOGIN)}
    />
  );
};

export default RecoverPasswordPage;
