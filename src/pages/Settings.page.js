import React from 'react';
import Settings from '../components/Settings/Settings.component';
import { REWARDS, STORE_PROFILE } from './_constants';
import { analyticsProvider, events } from '../providers/analytics';

const SettingsPage = ({ navigation }) => {
  const storeId = navigation.getParam('storeId');
  const alias = navigation.getParam('alias');

  const onRedirectHandler = (page) => () => {
    navigation.navigate(page, { storeId, alias });

    analyticsProvider.logEvent(events.settings[page]);
  };

  return (
    <Settings
      onEditInformationPressHandler={onRedirectHandler(STORE_PROFILE)}
      onManageRewardsPressHandler={onRedirectHandler(REWARDS)}
    />
  );
};

export default SettingsPage;
