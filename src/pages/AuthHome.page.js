import React from 'react';
import { LOGIN, TEST_SYSTEM, REGISTER, INTRO } from './_constants';
import AuthHome from '../components/AuthHome/AuthHome.component';
import { analyticsProvider, events } from '../providers/analytics';

export default ({ navigation }) => {
  const onRedirectHandler = (page) => () => {
    navigation.navigate(page);

    analyticsProvider.logEvent(events.initial_screen[page]);
  };

  return (
    <AuthHome
      onLoginPressHandler={onRedirectHandler(LOGIN)}
      onRegisterPressHandler={onRedirectHandler(REGISTER)}
      onTestSystem={onRedirectHandler(TEST_SYSTEM)}
      onIntroPressHandler={onRedirectHandler(INTRO)}
    />
  );
};
