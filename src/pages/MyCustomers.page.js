import React from 'react';
import MyCustomers from '../components/MyCustomers/MyCustomers.component';
import { STATISTICS, CUSTOMER_LIST } from './_constants';
import { analyticsProvider, events } from '../providers/analytics';

const MyCustomersPage = ({ navigation }) => {
  const storeId = navigation.getParam('storeId');
  const alias = navigation.getParam('alias');

  const onRedirectHandler = (page) => () => {
    navigation.navigate(page, { storeId, alias });

    analyticsProvider.logEvent(events.my_customers[page]);
  };

  return (
    <MyCustomers
      onCustomerListPressHandler={onRedirectHandler(CUSTOMER_LIST)}
      onStatisticsPressHandler={onRedirectHandler(STATISTICS)}
    />
  );
};

export default MyCustomersPage;
