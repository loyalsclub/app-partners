/** External dependencies */
import React from 'react';
import { useQuery } from '@apollo/react-hooks';

/** Wrapper */
import StatisticsComponent from '../components/Statistics/Statistics.component';

/** Utils */
import { parseError } from '../utils/graphql-error.utils';

/** Constants */
import { EMPLOYEE_GET_STATISTICS } from '../queries/partner.queries';

const StatisticsPage = (props) => {
  const { navigation } = props;

  const { error, loading, data, refetch } = useQuery(EMPLOYEE_GET_STATISTICS, {
    fetchPolicy: 'network-only',
    variables: {
      storeId: Number(navigation.getParam('storeId')),
    },
  });

  const { employeeGetStatistics } = data || {};

  return (
    <StatisticsComponent
      loading={loading}
      employeeGetStatistics={employeeGetStatistics}
      error={parseError(error)}
      refetch={refetch}
      goBackOnPress={navigation.goBack}
    />
  );
};

export default StatisticsPage;
