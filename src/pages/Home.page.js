/** External dependencies */
import React, { useEffect, useState } from 'react';
import { useMutation, useQuery } from '@apollo/react-hooks';

/** Wrapper */
import { Alert } from 'react-native';
import Home from '../components/Home/Home.component';

/** Utils */
import { parseError } from '../utils/graphql-error.utils';
import { getTokenForPushNotifications } from '../utils/push.utils';

/** Constants */
import {
  MENU,
  CREATE_PARTNER,
  LOAD_POINTS,
  FIND_CUSTOMER_PROFILE,
  MY_CUSTOMERS,
  MY_STORE,
  SETTINGS,
} from './_constants';
import { GET_EMPLOYEE_PROFILES } from '../queries/partner.queries';
import {
  isEmailVerified,
  reloadUser,
  sendVerificationEmail,
} from '../utils/auth.utils';
import { analyticsProvider, events } from '../providers/analytics';
import { REGISTER_USER_PUSH_TOKEN } from '../queries/user.queries';

const HomePage = ({ navigation }) => {
  const refetchEmployeeProfiles = navigation.getParam(
    'refetchEmployeeProfiles'
  );
  const [isLoadingAuthRequest, setIsLoadingAuthRequest] = useState(false);
  const [pushToken, setPushToken] = useState();

  const {
    refetch,
    loading,
    error,
    data = {},
  } = useQuery(GET_EMPLOYEE_PROFILES, {
    onCompleted: (response) => {
      const employeeProfiles = response.userEmployeeProfiles || [];

      const employeeProfile =
        employeeProfiles.length > 0 ? employeeProfiles[0] : null;

      if (employeeProfile) {
        analyticsProvider.setUserProperties({
          storeAlias: employeeProfile.Store.alias,
        });
      }
    },
  });

  const [registerUserPushToken] = useMutation(REGISTER_USER_PUSH_TOKEN);

  const employeeProfiles = data.userEmployeeProfiles || [];

  const employeeProfile =
    employeeProfiles.length > 0 ? employeeProfiles[0] : null;

  const { Store = {}, User = {} } = employeeProfile || {};

  useEffect(
    function getPushToken() {
      if (User.id) {
        getTokenForPushNotifications().then(setPushToken);
      }
    },
    [User.id]
  );

  useEffect(
    function saveUserPushToken() {
      const hasPushTokenChanged = pushToken && pushToken !== User.pushToken;

      if (hasPushTokenChanged) {
        registerUserPushToken({
          variables: { pushToken: pushToken !== 'denied' ? pushToken : '' },
        });
      }
    },
    [User.pushToken, pushToken, registerUserPushToken]
  );

  const onRedirectHandler = (page) => () => {
    navigation.navigate(page, {
      storeId: Store.id,
      alias: Store.alias,
    });

    analyticsProvider.logEvent(events.home[page]);
  };

  const checkEmailVerification = async () => {
    try {
      setIsLoadingAuthRequest(true);

      await reloadUser();

      if (!isEmailVerified()) {
        Alert.alert(
          'Lo sentimos',
          'Parece que todavía no verificaste tu email'
        );
      } else {
        analyticsProvider.logEvent(events.user_registration.email_verified);
      }
    } catch (err) {
      Alert.alert(
        '¡Ups!',
        'Parece que ha ocurrido un error, intentá nuevamente en unos instantes.'
      );
    } finally {
      setIsLoadingAuthRequest(false);
    }
  };

  const resendVerificationEmail = async () => {
    try {
      setIsLoadingAuthRequest(true);

      await sendVerificationEmail();

      analyticsProvider.logEvent(
        events.user_registration.resend_email_verification_button
      );

      Alert.alert(
        'Enviado',
        'Aguardá unos instantes y revisá tu casilla de correo'
      );
    } catch (err) {
      Alert.alert(
        '¡Ups!',
        'Parece que ha ocurrido un error, intentá nuevamente en unos instantes.'
      );
    } finally {
      setIsLoadingAuthRequest(false);
    }
  };

  if (refetchEmployeeProfiles) {
    refetch();
  }

  return (
    <Home
      onMenuPressHandler={onRedirectHandler(MENU)}
      onCreatePartnerPressHandler={onRedirectHandler(CREATE_PARTNER)}
      onLoadPointsPressHandler={onRedirectHandler(LOAD_POINTS)}
      onFindCustomerProfilePressHandler={onRedirectHandler(
        FIND_CUSTOMER_PROFILE
      )}
      onMyCustomersPressHandler={onRedirectHandler(MY_CUSTOMERS)}
      onMyStorePressHandler={onRedirectHandler(MY_STORE)}
      onSettingsPressHandler={onRedirectHandler(SETTINGS)}
      employeeProfile={employeeProfile}
      loading={loading}
      error={parseError(error)}
      refetch={refetch}
      checkEmailVerification={checkEmailVerification}
      resendVerificationEmail={resendVerificationEmail}
      isLoadingAuthRequest={isLoadingAuthRequest}
    />
  );
};

export default HomePage;
