import React from 'react';
import { Linking, Share } from 'react-native';
import MyStore from '../components/MyStore/MyStore.component';
import { analyticsProvider, events } from '../providers/analytics';
import { USERS_BASE_URL } from '../config/env.config';

const MyStorePage = ({ navigation }) => {
  const alias = navigation.getParam('alias');

  const url = `${USERS_BASE_URL}/${alias}`;

  const onVisitStorePressHandler = () => {
    analyticsProvider.logEvent(events.my_store.view_store);

    return Linking.openURL(url);
  };

  const onShareStorePressHandler = () => {
    analyticsProvider.logEvent(events.my_store.share_store);

    try {
      Share.share({
        message: `¡Sumate a nuestro Club de Clientes y obtené beneficios!\n${url}?utm_source=app`,
      });
    } catch {
      console.error("ERROR - can't access share functionality");
    }
  };

  return (
    <MyStore
      onVisitStorePressHandler={onVisitStorePressHandler}
      onShareStorePressHandler={onShareStorePressHandler}
    />
  );
};

export default MyStorePage;
