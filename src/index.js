import React, { useEffect } from 'react';
import { StatusBar } from 'react-native';
import { ApolloProvider } from '@apollo/react-hooks';
import { ActionSheetProvider } from '@expo/react-native-action-sheet';
import * as Notifications from 'expo-notifications';
import Router from './routes/index';
import client from './apollo.client';
import initialize from './firebase.client';
import { analyticsProvider } from './providers/analytics';
import Reports from './utils/reports.utils';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

const LoyalsPartner = () => {
  useEffect(() => {
    initialize();
    Reports.initialize();
    analyticsProvider.initialize();

    Notifications.addNotificationReceivedListener(
      handleForegroundedNotification
    );

    Notifications.addNotificationResponseReceivedListener(
      handleUserInteractionOnNotification
    );
  }, []);

  const handleForegroundedNotification = () => {
    // implement logic
  };

  const handleUserInteractionOnNotification = () => {
    // implement logic
  };

  return (
    <ActionSheetProvider>
      <ApolloProvider client={client}>
        <StatusBar backgroundColor="#F9F9F9" barStyle="dark-content" />
        <Router />
      </ApolloProvider>
    </ActionSheetProvider>
  );
};

export default LoyalsPartner;
