import { STORE_PROPERTIES } from '../config/constants';

export const calculateLevelsPoints = (
  spendingTimes,
  spendingAmount,
  country
) => {
  const yearlySpending = Number(spendingAmount) * Number(spendingTimes);
  const total = yearlySpending / country.conversionRate;
  const percentage = total * 0.4;

  return {
    level3: total - (total % 10),
    level2: percentage - (percentage % 10),
    level1: 0,
  };
};

export const getCountryPhoneMask = (country) => {
  let mask = `+${country.areaCode} `;
  const maxLimitOfNumbers = 15;
  const limitOfNumber = maxLimitOfNumbers - country.areaCode.length;

  for (let i = 0; i < limitOfNumber; i++) {
    mask += '9';
  }

  return mask;
};

export const isSocialMediaProperty = (property) =>
  [
    STORE_PROPERTIES.facebook,
    STORE_PROPERTIES.twitter,
    STORE_PROPERTIES.instagram,
    STORE_PROPERTIES.whatsapp,
  ].includes(property);

export const isStoreLocationProperty = (property) =>
  [
    STORE_PROPERTIES.website,
    STORE_PROPERTIES.phone,
    STORE_PROPERTIES.email,
    STORE_PROPERTIES.address,
  ].includes(property);
