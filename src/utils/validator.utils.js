import t from 'tcomb-form-native';

const Email = t.subtype(t.Str, (email) => {
  const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return reg.test(email);
});

const StringOnly = t.subtype(t.Str, (string) => {
  const reg = /^[a-zA-Z ]+$/;
  return reg.test(string);
});

const NumberOnly = t.subtype(t.Str, (string) => {
  const reg = /^[0-9]*$/;
  return reg.test(string);
});

export { Email, StringOnly, NumberOnly };
