const formatDate = (dateString) => {
  return format(new Date(dateString), 'D-M-YYYY', {
    locale: locales.es,
  });
};

const formatDateTime = (dateString) => {
  if (dateString) {
    const date = new Date(dateString);
    return `${date
      .toLocaleDateString()
      .split('/')
      .join('-')} | ${date.toTimeString().substr(0, 5)}`;
  }
  return null;
};

const daysInMonth = (month, year) => {
  switch (Number(month)) {
    case 2:
      return (year % 4 === 0 && year % 100) || year % 400 === 0 ? 29 : 28;
    case 4:
    case 6:
    case 9:
    case 11:
      return 30;
    default:
      return 31;
  }
};

const isValidCombinationForDate = (day, month, year) => {
  return day <= daysInMonth(month, year);
};

export { formatDate, formatDateTime, isValidCombinationForDate };
