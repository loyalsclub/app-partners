import * as Sentry from 'sentry-expo';
import { SENTRY_API } from '../config/env.config';

const useReporting = process.env.NODE_ENV !== 'development';

const initialize = () => {
  Sentry.init({
    dsn: SENTRY_API,
    environment: process.env.NODE_ENV,
    debug: true,
  });
};

const captureException = (ErrorObj) => {
  if (!useReporting) return;
  try {
    Sentry.Native.captureException(ErrorObj);
  } catch (err) {
    console.log(`[ERROR Sentry] ${err}`);
  }
};

const captureMessage = (ErrorObj) => {
  if (!useReporting) return;
  try {
    Sentry.Native.captureMessage(ErrorObj);
  } catch (err) {
    console.log(`[ERROR Sentry] ${err}`);
  }
};

export default { initialize, captureException, captureMessage };
