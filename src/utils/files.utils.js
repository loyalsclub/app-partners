import { ReactNativeFile } from 'apollo-upload-client';
import * as mime from 'react-native-mime-types';

export const generateRNFile = (uri) => {
  const type = mime.lookup(uri);
  const extension = type.split('/').pop();

  return new ReactNativeFile({
    uri,
    type,
    name: `image.${extension}`,
  });
};
