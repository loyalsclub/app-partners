import firebase from 'firebase';

const getAuthTokenIfLogged = () =>
  new Promise((resolver) => {
    const subscription = firebase.auth().onAuthStateChanged(async (user) => {
      if (!user) return resolver();

      const token = await user.getIdToken();
      subscription();
      return resolver(token);
    });
  });

const signOut = () => firebase.auth().signOut();

const signInWithCustomToken = (token) =>
  firebase.auth().signInWithCustomToken(token);

const isUserLoggedIn = () => Boolean(firebase.auth().currentUser);

const isEmailVerified = () => firebase.auth().currentUser.emailVerified;

const getUserEmail = () => firebase.auth().currentUser.email;

const reloadUser = () => firebase.auth().currentUser.reload();

const sendVerificationEmail = () =>
  firebase.auth().currentUser.sendEmailVerification();

const recoverPassword = (email) =>
  firebase.auth().sendPasswordResetEmail(email);

export {
  getAuthTokenIfLogged,
  signOut,
  signInWithCustomToken,
  isUserLoggedIn,
  isEmailVerified,
  sendVerificationEmail,
  reloadUser,
  getUserEmail,
  recoverPassword,
};
