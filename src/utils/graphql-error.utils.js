import Reports from './reports.utils';
/** This function recieves an object that has the following format
var err = {
  graphQLErrors: {
    code: ----
    response: -----
  },
  networkError: [{
    message: STRING
  }]
}
And returns the adequated message for the error.
*/
const parseError = (err = {}) => {
  // Handle Network error

  if (Object.keys(err).length > 0) {
    Reports.captureException(err);
  }

  if (err.networkError) {
    if (err.networkError.statusCode === 400) {
      // Server Error DANGER. Submit response to some system.
      return SERVER_ERROR;
    }

    // Another network Error.
    return NETWORK_ERROR;
  }

  // Handle Server API Errors
  if (err.graphQLErrors && err.graphQLErrors.length > 0) {
    const error = err.graphQLErrors[0];
    const message = API_ERRORS[error.message];
    if (message) return message;

    // NOTIFY SENTRY SOME ERRORS NOT BEING HANDLED!
    return GENERIC_ERROR;
  }

  return null;
};

const GENERIC_ERROR = {
  text: 'Parece que hubo un problema. Prueba nuevamente',
};

const SERVER_ERROR = {
  text: 'Parece que hay un problema con nuestro sistema. \n Por favor probá nuevamente mas tarde o comunicate con info@loyals.club',
};

const NETWORK_ERROR = {
  text: 'Parece que hay un error en la red ! \n Chequeá tu conectividad',
};

const API_ERRORS = {
  'auth/user-is-not-authorized': {
    text: 'No está autorizado para realizar la acción',
  },
  'api/user-not-found': {
    text: 'Usuario no encontrado',
  },
  'api/customer-exists': {
    text: 'El ciente ya existe',
  },
  'api/transaction-not-found': {
    text: 'No se reconoce el código del cupón',
  },
  'api/transaction-redeemed': {
    text: 'Este cupón ya fue utilizado',
  },
  'api/store-modification-forbidden': {
    text: 'No se puede modificar este Club de Clientes',
  },
};

export { parseError, SERVER_ERROR };
