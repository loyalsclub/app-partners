import I18n from 'i18n-js'; // You can import i18n-js as well if you don't want the app to set default locale from the device locale.
import es from '../config/language/es';
import en from '../config/language/en';
import { USAGE_FREQUENCIES } from '../config/constants';

// If an english translation is not available in en.js, it will look inside hi.js
I18n.fallbacks = true;
I18n.missingBehaviour = 'guess'; // It will convert HOME_noteTitle to 'HOME note title' if the value of HOME_noteTitle doesn't exist in any of the translation files.
I18n.defaultLocale = 'es'; // If the current locale in device is not en or hi
I18n.locale = 'es'; // If we do not want the framework to use the phone's locale by default

I18n.translations = {
  es,
  en,
};

const translate = I18n.translate.bind(I18n);

export const setLocale = (locale) => {
  I18n.locale = locale;
};

// It will be used to define intial language state in reducer.
export const getCurrentLocale = () => I18n.locale;

/* translateHeaderText:
 screenProps => coming from react-navigation(defined in app.container.js)
 langKey => will be passed from the routes file depending on the screen
*/
export const translateHeaderText = (langKey) => ({ screenProps }) => {
  const title = I18n.translate(langKey, screenProps.language);
  return { title };
};

export const translateCategory = (category) =>
  translate(`STORE_CATEGORIES.${category}`);

export const getFrequency = (UsageFrequency = {}) => {
  const FREQUENCY_TYPE = {};
  FREQUENCY_TYPE[USAGE_FREQUENCIES.FOREVER] = 'Siempre disponible';
  FREQUENCY_TYPE[USAGE_FREQUENCIES.ONE_TIME] = 'Disponible para uso único';
  FREQUENCY_TYPE[
    USAGE_FREQUENCIES.EVERY_CERTAIN_DAYS
  ] = `Disponible cada ${Number(UsageFrequency.value)} días`;
  FREQUENCY_TYPE[
    USAGE_FREQUENCIES.EVERY_CERTAIN_HOURS
  ] = `Disponible cada ${Number(UsageFrequency.value)} horas`;

  if (USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS === UsageFrequency.type) {
    const days = UsageFrequency.value.split(',');
    const weekdays = [
      'Lunes',
      'Martes',
      'Miércoles',
      'Jueves',
      'Viernes',
      'Sábados',
      'Domingos',
    ];
    if (days.length === 1) {
      return `Disponible los días ${weekdays[Number(days[0]) - 1]}`;
    }
    if (days.length > 1) {
      // To format as 'Lunes a Viernes'
      const hasFormat = days.every((val, index) => {
        if (index + 1 === days.length) return true;
        if (Number(val) + 1 === Number(days[index + 1])) return true;
        return false;
      });

      if (hasFormat && days.length > 2) {
        const startDay = weekdays[Number(days[0]) - 1];
        const endDay = weekdays[Number(days[days.length - 1]) - 1];
        return `Disponible de ${startDay} a ${endDay}`;
      }

      let ret = 'Disponible los ';
      ret += days
        .map((value, index) => {
          const day = weekdays[Number(value) - 1];
          if (index === 0) return `${day}`;
          if (index + 1 === days.length) return ` y ${day}`;
          return `, ${day}`;
        })
        .join('');
      return ret;
    }
  }

  if (
    !UsageFrequency ||
    !UsageFrequency.type ||
    !FREQUENCY_TYPE[UsageFrequency.type]
  )
    return 'Disponible';

  return FREQUENCY_TYPE[UsageFrequency.type];
};

export default translate;
