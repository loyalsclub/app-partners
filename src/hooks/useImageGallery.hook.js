import { Platform, Alert } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { useCallback } from 'react';

const useImageGallery = (onPickSuccess) => {
  const isPermissionGranted = useCallback(async () => {
    if (Platform.OS !== 'web') return true;

    const { status } = await ImagePicker.requestCameraRollPermissionsAsync();

    return status === 'granted';
  }, []);

  const pickImage = useCallback(async () => {
    const isGrantedAccess = await isPermissionGranted();

    if (!isGrantedAccess) {
      Alert.alert(
        '¡Ups!',
        'Necesitamos permisos para acceder a tu biblioteca.'
      );
      return;
    }

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      aspect: [1, 1],
      quality: 1,
    });

    if (!result.cancelled) {
      onPickSuccess(result.uri);
    }
  }, [isPermissionGranted, onPickSuccess]);

  return pickImage;
};

export default useImageGallery;
