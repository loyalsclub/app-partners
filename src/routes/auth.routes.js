import { createStackNavigator } from 'react-navigation-stack';

import IntroPage from '../pages/Intro.page';
import LoginPage from '../pages/Login.page';
import AuthHomePage from '../pages/AuthHome.page';
import TestSystemPage from '../pages/TestSystem.page';
import RegisterPage from '../pages/Register.page';
import RecoverPasswordPage from '../pages/RecoverPassword.page';

export default createStackNavigator(
  {
    IntroPage,
    AuthHomePage,
    LoginPage,
    TestSystemPage,
    RegisterPage,
    RecoverPasswordPage,
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
      cardStyle: {
        backgroundColor: '#FFFFFF',
      },
    },
  }
);
