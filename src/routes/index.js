import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import AppLoading from '../pages/AppLoading.page';
import AppMainStack from './partner.routes';
import AuthStack from './auth.routes';

const Router = createSwitchNavigator(
  {
    AppLoading: {
      screen: AppLoading,
    },
    Auth: AuthStack,
    App: AppMainStack,
  },
  {
    initialRouteName: 'AppLoading',
  }
);

export default createAppContainer(Router);
