import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

// Pages
import Home from '../pages/Home.page';
import MenuPage from '../pages/Menu.page';
import ClientRegistrationPage from '../pages/ClientRegistration.page';
import LoadProfilePage from '../pages/LoadProfile.page';
import StatisticsPage from '../pages/Statistics.page';
import RewardsPage from '../pages/Rewards.page';
import GenerateCouponPage from '../pages/GenerateCoupon.page';
import CreatePartnerPage from '../pages/CreatePartner.page';
import LoadPointsPage from '../pages/LoadPoints.page';
import FindCustomerProfilePage from '../pages/FindCustomerProfile.page';
import MyCustomersPage from '../pages/MyCustomers.page';
import MyStorePage from '../pages/MyStore.page';
import SettingsPage from '../pages/Settings.page';
import CustomerListPage from '../pages/CustomerList.page';
import StoreProfilePage from '../pages/StoreProfile.page';
import StoreProfileEditPage from '../pages/StoreProfileEdit.page';

// styles
import styles from '../styles/theme.style';
import TabBarComponent from '../components/Common/Layout/Tabbar.component';
import Icons from '../components/Common/Miscellaneous/Icons.component';

const HomeStack = createStackNavigator(
  {
    Home,
    CreatePartnerPage,
    LoadProfilePage,
    StatisticsPage,
    GenerateCouponPage,
    RewardsPage,
    ClientRegistrationPage,
    LoadPointsPage,
    FindCustomerProfilePage,
    MyCustomersPage,
    MyStorePage,
    SettingsPage,
    CustomerListPage,
    StoreProfilePage,
    StoreProfileEditPage,
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
      cardStyle: {
        backgroundColor: styles.CREAM_WHITE,
      },
    },
  }
);

const AppBottomTabs = createBottomTabNavigator(
  {
    Home: HomeStack,
  },
  {
    tabBarComponent: function tabBarComponent(props) {
      return <TabBarComponent {...props} />;
    },
    initialRouteName: 'Home',
    tabBarOptions: {
      activeTintColor: styles.BLUE_WOOD,
      inactiveTintColor: styles.BLUE_WOOD,
    },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: function tabBarIcon(payload) {
        const { focused } = payload;
        const { routeName } = navigation.state;
        let iconName;
        const color = focused ? styles.GOLDEN : styles.BLUE_WOOD;
        if (routeName === 'Home') {
          iconName = 'store';
        }

        return (
          <Icons
            style={{ width: 32 }}
            name={iconName}
            size={25}
            color={color}
          />
        );
      },
    }),
  }
);

export default createStackNavigator(
  {
    Main: AppBottomTabs,
    MenuPage,
  },
  {
    mode: 'modal',
    headerMode: 'none',
  }
);
