/** External Dependencies */
import React, { Component } from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

/** Components */
import MainContainer from '../../Common/Layout/MainContainer.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';
import themeStyles from '../../../styles/theme.style';
import globalStyles from '../../../styles/global.styles';

export default class StepConfirmation extends Component {
  render() {
    const { onContinuePressHandler, clientRegistrationData } = this.props;

    const { firstName, lastName, email } = clientRegistrationData;

    return (
      <MainContainer style={globalStyles.centeredCon}>
        <Text style={styles.heading}>Confirmar alta de cliente ?</Text>
        <Text style={styles.information}>
          {firstName} {lastName} | {email}
        </Text>
        <GradientButton
          style={styles.actionButton}
          mode="active"
          onPress={onContinuePressHandler}
          text="Continuar"
        />
      </MainContainer>
    );
  }
}

const styles = {
  heading: {
    fontFamily: themeStyles.MAIN_FONT_BOLD,
    color: themeStyles.BLUE_WOOD,
    marginTop: 10,
    fontSize: 20,
  },
  information: {
    fontFamily: themeStyles.MAIN_FONT_LIGHT,
    color: themeStyles.GREY,
    marginVertical: 25,
    fontSize: 17,
  },
};

StepConfirmation.propTypes = {
  clientRegistrationData: PropTypes.object,
  onContinuePressHandler: PropTypes.func,
};
