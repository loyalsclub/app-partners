/** External Dependencies */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

/** Components */
import MainContainer from '../../Common/Layout/MainContainer.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';
import PassiveNotification from '../../Common/Miscellaneous/PassiveNotification.component';

/** Styles */
import globalStyles from '../../../styles/global.styles';

export default class StepSuccess extends Component {
  render() {
    const { onContinuePressHandler } = this.props;

    return (
      <MainContainer style={globalStyles.centeredCon}>
        <PassiveNotification
          mainText="Alta Exitosa!"
          secondaryText="El cliente fué registrado en tu sistema exitosamente"
          type="genericSuccess"
        />
        <GradientButton
          style={styles.button}
          mode="dark"
          text="Volver al inicio"
          onPress={onContinuePressHandler}
        />
      </MainContainer>
    );
  }
}

const styles = {
  button: {
    marginTop: 15,
  },
};

StepSuccess.propTypes = {
  onContinuePressHandler: PropTypes.func,
};
