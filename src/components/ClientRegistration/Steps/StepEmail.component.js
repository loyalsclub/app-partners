/** External Dependencies */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

/** Components */
import MainContainer from '../../Common/Layout/MainContainer.component';
import IconComponent from '../../Common/Miscellaneous/Icons.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';
import formstyles from '../../Common/AuthForm/OriginalTemplates.styles';

/** Styles */
import themeStyle from '../../../styles/theme.style';
import globalStyles from '../../../styles/global.styles';

/** Utils */
import { Email } from '../../../utils/validator.utils';

const t = require('tcomb-form-native');

const { Form } = t.form;

export default class StepEmail extends Component {
  constructor(props) {
    super();
    /** We set the starting info if we get is as a prop */

    const { clientRegistrationData } = props;
    this.state = {
      formData: {
        ...clientRegistrationData,
      },
    };
  }

  onContinuePressHandler = () => {
    const value = this.refs.form.getValue();
    if (!value) return;

    const { onContinuePressHandler } = this.props;
    const { formData } = this.state;
    onContinuePressHandler(formData);
  };

  onChangeForm = (formData) => {
    this.setState({
      ...this.state,
      formData,
    });
    return formData;
  };

  render() {
    const { formData } = this.state;

    const email = {
      placeholder: 'Ingresá el correo electronico',
      config: {
        labelAnimation: false,
      },
      keyboardType: 'email-address',
      autoCapitalize: 'none',
      autoCorrect: false,
      autoComplete: false,
      autoFocus: true,
      error: 'Ingrese un email correcto',
      returnKeyType: 'next',
    };

    const authForm = t.struct({
      email: Email,
    });
    const form = 'form';
    const options = {
      stylesheet: formstyles,
      fields: {
        email,
      },
    };

    return (
      <MainContainer style={globalStyles.centeredCon}>
        <IconComponent
          style={styles.bigIconStyle}
          name="email-line"
          color={themeStyle.GOLDEN}
          size={70}
        />
        <Form
          ref={form}
          type={authForm}
          options={options}
          value={formData}
          onChange={this.onChangeForm}
        />
        <GradientButton
          style={styles.actionButton}
          mode="active"
          onPress={this.onContinuePressHandler}
          text="Continuar"
        />
      </MainContainer>
    );
  }
}

const styles = {
  bigIconStyle: {
    marginBottom: 20,
  },
};

StepEmail.propTypes = {
  clientRegistrationData: PropTypes.object,
  onContinuePressHandler: PropTypes.func,
};
