/** External Dependencies */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

/** Components */
import MainContainer from '../../Common/Layout/MainContainer.component';
import IconComponent from '../../Common/Miscellaneous/Icons.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';
import datepicker from '../../Common/AuthForm/datepicker';

/** Styles */
import themeStyle from '../../../styles/theme.style';
import formstyles from '../../Common/AuthForm/OriginalTemplates.styles';
import globalStyles from '../../../styles/global.styles';

/** Utils */
import { formatDate } from '../../../utils/date.utils';
import { StringOnly } from '../../../utils/validator.utils';

const t = require('tcomb-form-native');

const { Form } = t.form;

export default class StepUser extends Component {
  constructor(props) {
    super();
    /** We set the starting info if we get is as a prop */

    const { clientRegistrationData } = props;
    this.state = {
      formData: {
        ...clientRegistrationData,
      },
    };
  }

  onContinuePressHandler = () => {
    /** First we validate the form */
    const value = this.refs.form.getValue();
    if (!value) return;

    const { onContinuePressHandler } = this.props;
    const { formData } = this.state;
    onContinuePressHandler(formData);
  };

  onChangeForm = (formData) => {
    this.setState({
      ...this.state,
      formData,
    });
    return formData;
  };

  render() {
    const { formData } = this.state;

    const lastName = {
      placeholder: 'Ingresá el Apellido',
      autoCapitalize: 'words',
      autoCorrect: false,
      autoComplete: false,
      label: 'Apellido',
      error: 'Ingrese un Apellido válido',
    };

    const firstName = {
      placeholder: 'Ingresá el Nombre',
      autoCapitalize: 'words',
      autoCorrect: false,
      autoComplete: false,
      label: 'Nombre',
      autoFocus: true,
      error: 'Ingrese un Nombre válido',
    };

    const authForm = t.struct({
      firstName: StringOnly,
      lastName: StringOnly,
    });
    const form = 'form';

    const options = {
      stylesheet: formstyles,
      fields: {
        firstName,
        lastName,
      },
    };

    return (
      <MainContainer style={globalStyles.centeredCon}>
        <IconComponent
          style={styles.bigIconStyle}
          name="user-line"
          color={themeStyle.GOLDEN}
          size={70}
        />
        <Form
          ref={form}
          type={authForm}
          options={options}
          value={formData}
          onChange={this.onChangeForm}
        />
        <GradientButton
          style={styles.actionButton}
          mode="active"
          onPress={this.onContinuePressHandler}
          text="Continuar"
        />
      </MainContainer>
    );
  }
}

const styles = {
  actionButton: {
    marginBottom: 10,
  },
  bigIconStyle: {
    marginBottom: 20,
  },
};

StepUser.propTypes = {
  clientRegistrationData: PropTypes.object,
  onContinuePressHandler: PropTypes.func,
};
