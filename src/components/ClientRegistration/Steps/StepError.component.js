/** External Dependencies */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

/** Components */
import MainContainer from '../../Common/Layout/MainContainer.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';
import PassiveNotification from '../../Common/Miscellaneous/PassiveNotification.component';

/** Styles */
import globalStyles from '../../../styles/global.styles';

export default class StepError extends Component {
  render() {
    const { onContinuePressHandler, error } = this.props;

    return (
      <MainContainer style={globalStyles.centeredCon}>
        <PassiveNotification
          type="genericError"
          secondaryText={error.secondaryText}
          mainText={error.mainText}
        />
        <GradientButton
          style={styles.button}
          mode="dark"
          text="Volver al inicio"
          onPress={onContinuePressHandler}
        />
      </MainContainer>
    );
  }
}

const styles = {
  button: {
    marginTop: 15,
  },
};

StepError.propTypes = {
  error: PropTypes.object,
  onContinuePressHandler: PropTypes.func,
};

StepError.defaultProps = {
  error: {
    mainText: null,
    secondaryText: null,
  },
};
