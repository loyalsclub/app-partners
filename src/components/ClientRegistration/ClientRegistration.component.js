/** External Dependencies */
import React, { Component } from 'react';
import { KeyboardAvoidingView, Platform } from 'react-native';
import PropTypes from 'prop-types';

/** Components */
import GenericHeader, {
  getHeaderHeight,
} from '../Common/Miscellaneous/GenericHeader.component';
import PageHeaderContainer from '../Common/Layout/PageHeaderContainer.component';
import BackgroundPattern from '../Common/Miscellaneous/BackgroundPattern.component';
import StepEmail from './Steps/StepEmail.component';
import StepUser from './Steps/StepUser.component';
import StepConfirmation from './Steps/StepConfirmation.component';
import StepSuccess from './Steps/StepSuccess.component';
import StepError from './Steps/StepError.component';
import StepLoadPoints from './Steps/StepLoadPoints.component';

/** Styles */
import globalStyles from '../../styles/global.styles';

export default class ClientRegistration extends Component {
  renderHeader = () => {
    const { goBackOnPress } = this.props;
    return <GenericHeader goBackOnPress={goBackOnPress} mode="goBack" />;
  };

  renderBackground = () => <BackgroundPattern type="golden" />;

  renderContent = () => {
    const {
      STEPS,
      step,
      error,
      clientRegistrationData,
      onEmailContinuePressHandler,
      onUserContinuePressHandler,
      onConfirmationPressHandler,
      goBackOnPress,
    } = this.props;

    return (
      <KeyboardAvoidingView
        style={globalStyles.centeredCon}
        behavior={Platform.OS === 'ios' ? 'padding' : null}
      >
        {step === STEPS.stepEmail ? (
          <StepEmail
            clientRegistrationData={clientRegistrationData}
            onContinuePressHandler={onEmailContinuePressHandler}
          />
        ) : null}
        {step === STEPS.stepUser ? (
          <StepUser
            clientRegistrationData={clientRegistrationData}
            onContinuePressHandler={onUserContinuePressHandler}
          />
        ) : null}
        {step === STEPS.stepLoadPoints ? (
          <StepLoadPoints
            clientRegistrationData={clientRegistrationData}
            onContinuePressHandler={onUserContinuePressHandler}
          />
        ) : null}
        {step === STEPS.stepConfirmation ? (
          <StepConfirmation
            clientRegistrationData={clientRegistrationData}
            onContinuePressHandler={onConfirmationPressHandler}
          />
        ) : null}
        {step === STEPS.stepSuccess ? (
          <StepSuccess onContinuePressHandler={goBackOnPress} />
        ) : null}
        {step === STEPS.stepError ? (
          <StepError onContinuePressHandler={goBackOnPress} error={error} />
        ) : null}
      </KeyboardAvoidingView>
    );
  };

  render() {
    const { loading } = this.props;

    return (
      <PageHeaderContainer
        keyboardShouldPersistTaps="always"
        style={{ scrollView: globalStyles.centeredCon }}
        renderHeader={this.renderHeader}
        renderContent={this.renderContent}
        renderBackground={this.renderBackground}
        headerHeight={getHeaderHeight()}
        refreshing={loading}
      />
    );
  }
}

ClientRegistration.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.object,
  step: PropTypes.string.isRequired,
  clientRegistrationData: PropTypes.object.isRequired,
  STEPS: PropTypes.object.isRequired,
  goBackOnPress: PropTypes.func.isRequired,
  onEmailContinuePressHandler: PropTypes.func.isRequired,
  onUserContinuePressHandler: PropTypes.func.isRequired,
  onConfirmationPressHandler: PropTypes.func.isRequired,
};
