/** External Dependencies */
import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text } from 'react-native';

/** Components */
import GenericHeader, {
  getHeaderHeight,
} from '../Common/Miscellaneous/GenericHeader.component';
import LinearSeparatorComponent from '../Common/Miscellaneous/LinearSeparator.component';
import PageHeaderContainer from '../Common/Layout/PageHeaderContainer.component';
import PassiveNotification from '../Common/Miscellaneous/PassiveNotification.component';
import MainContainer from '../Common/Layout/MainContainer.component';

/** Styles */
import globalStyles from '../../styles/global.styles';
import fontStyles from '../../styles/font.styles';
import translate from '../../utils/language.utils';

const styles = {
  buttonElement: {
    paddingVertical: 15,
  },
};

const MenuComponent = (props) => {
  const {
    goBackOnPress,
    onSignOutPressHandler,
    onHelpAndSupportPressHandler,
    onWhatsAppContactPressHandler,
  } = props;

  const renderHeader = () => (
    <GenericHeader goBackOnPress={goBackOnPress} mode="modal" />
  );

  const renderError = () => {
    const { error } = props;
    return (
      <PassiveNotification type="genericError" secondaryText={error.text} />
    );
  };

  const renderContent = () => (
    <MainContainer>
      <TouchableOpacity
        onPress={onWhatsAppContactPressHandler}
        style={styles.buttonElement}
      >
        <Text
          style={[
            fontStyles.blue,
            fontStyles.xlargeSize,
            fontStyles.center,
            fontStyles.boldWeight,
          ]}
        >
          {translate('MENU.contactWpp')}
        </Text>
      </TouchableOpacity>

      <LinearSeparatorComponent />

      <TouchableOpacity
        onPress={onHelpAndSupportPressHandler}
        style={styles.buttonElement}
      >
        <Text
          style={[
            fontStyles.blue,
            fontStyles.xlargeSize,
            fontStyles.center,
            fontStyles.boldWeight,
          ]}
        >
          {translate('MENU.support')}
        </Text>
      </TouchableOpacity>

      <LinearSeparatorComponent />

      <TouchableOpacity
        onPress={onSignOutPressHandler}
        style={styles.buttonElement}
      >
        <Text
          style={[
            fontStyles.blue,
            fontStyles.xlargeSize,
            fontStyles.center,
            fontStyles.boldWeight,
          ]}
        >
          {translate('MENU.logOut')}
        </Text>
      </TouchableOpacity>
    </MainContainer>
  );

  const { loading, error, refetch } = props;

  return (
    <PageHeaderContainer
      error={error != null}
      style={{ scrollView: globalStyles.centerScrollView }}
      renderHeader={renderHeader}
      renderContent={renderContent}
      renderError={renderError}
      headerHeight={getHeaderHeight()}
      refreshing={loading}
      onRefresh={refetch}
    />
  );
};

export default MenuComponent;

MenuComponent.propTypes = {
  onHelpAndSupportPressHandler: PropTypes.func.isRequired,
  onWhatsAppContactPressHandler: PropTypes.func.isRequired,
  onSignOutPressHandler: PropTypes.func.isRequired,
  goBackOnPress: PropTypes.func.isRequired,
  error: PropTypes.shape({
    text: PropTypes.string,
  }),
};

MenuComponent.defaultProps = {
  error: null,
};
