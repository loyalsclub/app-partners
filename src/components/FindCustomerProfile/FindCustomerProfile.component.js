import React, { useMemo } from 'react';
import AliasIcon from '../Common/Icons/Alias.icon';
import InviteQRIcon from '../Common/Icons/InviteQR.icon';
import SectionPage from '../Common/Layout/SectionPage.component';
import VerifiedClientIcon from '../Common/Icons/VerifiedClient.icon';
import translate from '../../utils/language.utils';

const FindCustomerProfile = ({
  onInputAliasPressHandler,
  onScanProfilePressHandler,
}) => {
  const actions = useMemo(
    () => [
      {
        icon: <AliasIcon />,
        title: translate('FIND_CUSTOMER_PROFILE.actionInputAliasTitle'),
        description: translate(
          'FIND_CUSTOMER_PROFILE.actionInputAliasDescription'
        ),
        onPressHandler: onInputAliasPressHandler,
      },
      {
        icon: <InviteQRIcon />,
        title: translate('FIND_CUSTOMER_PROFILE.actionScanProfileTitle'),
        description: translate(
          'FIND_CUSTOMER_PROFILE.actionScanProfileDescription'
        ),
        onPressHandler: onScanProfilePressHandler,
      },
    ],
    [onScanProfilePressHandler, onInputAliasPressHandler]
  );

  return (
    <SectionPage
      icon={
        <VerifiedClientIcon
          width={80}
          height={80}
          style={{ marginBottom: 40 }}
        />
      }
      title={translate('FIND_CUSTOMER_PROFILE.title')}
      description={translate('FIND_CUSTOMER_PROFILE.description')}
      actions={actions}
    />
  );
};

export default FindCustomerProfile;
