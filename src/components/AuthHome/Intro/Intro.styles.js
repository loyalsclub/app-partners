export default {
  container: {
    flexGrow: 1,
  },
  mainView: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 50,
  },
  startButton: {
    marginBottom: 20,
  },
};
