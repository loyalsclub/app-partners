/** External Dependencies */
import React from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
  Text,
  Image,
  Linking,
} from 'react-native';

/** Components */
import { ScrollView } from 'react-native-gesture-handler';
import translate from '../../../utils/language.utils';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';
import BackgroundPattern from '../../Common/Miscellaneous/BackgroundPattern.component';

/** Styles */
import globalStyles from '../../../styles/global.styles';
import ExplanationItem from '../../Common/Miscellaneous/ExplanationItem/ExplanationItem.component';
import HouseIcon from '../../Common/Icons/House.icon';
import TicketIcon from '../../Common/Icons/Ticket.icon';
import ClientsIcon from '../../Common/Icons/Clients.icon';
import styles from './Intro.styles';
import { storageKeys, storageProvider } from '../../../providers/storage';

const IntroComponent = ({ onStartPressHandler }) => {
  const onStart = () =>
    storageProvider
      .set(storageKeys.INTRO_SCREEN_DISPLAYED, true)
      .then(onStartPressHandler);

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />

      <BackgroundPattern type="golden" />

      <SafeAreaView style={styles.container}>
        <View style={styles.mainView}>
          <Image
            style={{ marginBottom: 80 }}
            source={require('../../../assets/images/partners-logo/img.png')}
          />

          <Text style={[globalStyles.mainTitle, { marginBottom: 40 }]}>
            {translate('INTRO_SCREEN.title')}
          </Text>

          <ExplanationItem
            icon={<HouseIcon width={30} height={30} />}
            title={translate('INTRO_SCREEN.stepOneTitle')}
            description={translate('INTRO_SCREEN.stepOneDescription')}
          />

          <ExplanationItem
            icon={<TicketIcon />}
            title={translate('INTRO_SCREEN.stepTwoTitle')}
            description={translate('INTRO_SCREEN.stepTwoDescription')}
          />

          <ExplanationItem
            icon={<ClientsIcon />}
            title={translate('INTRO_SCREEN.stepThreeTitle')}
            description={translate('INTRO_SCREEN.stepThreeDescription')}
          />

          <GradientButton
            mode="active"
            onPress={onStart}
            style={styles.startButton}
            text={translate('INTRO_SCREEN.startButton')}
          />

          <GradientButton
            mode="minimal"
            onPress={() => {
              Linking.openURL(
                'https://partners.loyals.club?utm_source=app_partners&utm_medium=hyperlink&utm_content=intro_know_more'
              );
            }}
            text={translate('INTRO_SCREEN.moreInfo')}
          />
        </View>
      </SafeAreaView>
    </ScrollView>
  );
};

IntroComponent.propTypes = {};

export default IntroComponent;
