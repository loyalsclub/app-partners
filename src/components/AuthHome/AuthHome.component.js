/** External Dependencies */
import React from 'react';
import {
  SafeAreaView,
  View,
  Image,
  StatusBar,
  Text,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';

/** Components */
import { ScrollView } from 'react-native-gesture-handler';
import translate from '../../utils/language.utils';
import GradientButton from '../Common/Miscellaneous/GradientButton.component';
import BackgroundPattern from '../Common/Miscellaneous/BackgroundPattern.component';

/** Styles */
import styles from './AuthHome.component.style';
import HelpIcon from '../Common/Icons/Help.icon';
import globalStyles from '../../styles/global.styles';

const AuthHome = ({
  onLoginPressHandler,
  onTestSystem,
  onRegisterPressHandler,
  onIntroPressHandler,
}) => {
  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />

      <BackgroundPattern type="golden" />

      <SafeAreaView style={{ flexGrow: 1 }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Image
            style={styles.logoImage}
            resizeMode="contain"
            source={require('../../assets/images/partners-logo/img.png')}
          />

          <TouchableOpacity
            style={{
              position: 'absolute',
              right: 15,
            }}
            onPress={onIntroPressHandler}
          >
            <HelpIcon />
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexGrow: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Image
            resizeMode="contain"
            style={{
              height: 240,
            }}
            source={require('../../assets/images/crown-mockups/img.png')}
          />

          <Text style={[globalStyles.mainTitle, styles.welcomeTitle]}>
            {translate('AUTH_HOME.welcome')}
          </Text>

          <GradientButton
            style={styles.actionButton}
            mode="active"
            onPress={onLoginPressHandler}
            text={translate('AUTH_HOME.logIn')}
          />

          <GradientButton
            style={styles.actionButton}
            mode="dark"
            onPress={onRegisterPressHandler}
            text={translate('AUTH_HOME.register')}
          />

          <GradientButton
            style={[styles.actionButton, { marginBottom: 20 }]}
            mode="minimal"
            onPress={onTestSystem}
            text={translate('AUTH_HOME.testSystem')}
          />
        </View>
      </SafeAreaView>
    </ScrollView>
  );
};

AuthHome.propTypes = {
  onLoginPressHandler: PropTypes.func.isRequired,
  onTestSystem: PropTypes.func.isRequired,
};

export default AuthHome;
