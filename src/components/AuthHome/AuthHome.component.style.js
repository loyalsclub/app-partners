import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  logoImage: {
    height: 50,
    marginVertical: 20,
  },
  actionButton: {
    marginTop: 20,
  },
  welcomeTitle: {
    textAlign: 'center',
    paddingHorizontal: 50,
    paddingVertical: 20,
  },
});
