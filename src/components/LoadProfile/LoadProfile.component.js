/** External Dependencies */
import React from 'react';
import PropTypes from 'prop-types';
import { KeyboardAvoidingView, Platform, View } from 'react-native';

/** Components */
import GenericHeader, {
  getHeaderHeight,
} from '../Common/Miscellaneous/GenericHeader.component';
import PageHeaderContainer from '../Common/Layout/PageHeaderContainer.component';
import BackgroundPattern from '../Common/Miscellaneous/BackgroundPattern.component';
import StepManualLoad from './Steps/StepManualLoad.component';
import StepAddCustomer from './Steps/StepAddCustomer.component';
import Error from '../Common/Miscellaneous/Error.component';
import QrScanner from '../QrScanner/QrScanner.component';
import StepProfile from './Steps/StepProfile.component';
import StepLoadPointsInput from '../Common/Steps/StepLoadPointsInput.component';

/** Styles */
import globalStyles from '../../styles/global.styles';
import themeStyle from '../../styles/theme.style';
import { analyticsProvider, events } from '../../providers/analytics';
import StepRewards from './Steps/StepRewards.component';
import StepHistory from './Steps/StepHistory/StepHistory.component';

const LoadProfile = ({
  goBackOnPress,
  step,
  STEPS,
  onLoadPointsPressHandler,
  error,
  onQrScannerPressHandler,
  onLoadInfoContinuePressHandler,
  onAddClientPressHandler,
  onPointsLoadedPressHandler,
  onRewardUsagePressHandler,
  onHistoryPressHandler,
  rewardsData,
  customerData,
  userData,
  loading,
  onManualLoadPress,
  isInitializingCustomer,
  onRewardsPressHandler,
}) => {
  const renderHeader = () => (
    <View style={{ backgroundColor: themeStyle.CREAM_WHITE }}>
      <GenericHeader
        goBackOnPress={goBackOnPress}
        mode="goBack"
        customAction={
          [STEPS.profile, STEPS.rewards, STEPS.history].includes(step)
            ? onLoadPointsPressHandler
            : undefined
        }
        customActionIcon="points"
      />
    </View>
  );

  const renderBackground = () => <BackgroundPattern type="golden" />;

  const renderContent = () => (
    <View>
      {step === STEPS.manualLoad ? (
        <KeyboardAvoidingView
          style={globalStyles.centeredCon}
          behavior={Platform.OS === 'ios' ? 'padding' : null}
        >
          <StepManualLoad
            onQrScannerPressHandler={onQrScannerPressHandler}
            onContinuePressHandler={onLoadInfoContinuePressHandler}
          />
        </KeyboardAvoidingView>
      ) : null}

      {step === STEPS.profile ? (
        <StepProfile
          customerData={customerData}
          userData={userData}
          rewardsData={rewardsData}
          onLoadPointsPressHandler={onLoadPointsPressHandler}
          onRewardsPressHandler={onRewardsPressHandler}
          onHistoryPressHandler={onHistoryPressHandler}
        />
      ) : null}

      {step === STEPS.rewards ? (
        <StepRewards
          onLoadPointsPressHandler={onLoadPointsPressHandler}
          onRewardUsagePressHandler={onRewardUsagePressHandler}
          customerData={customerData}
          userData={userData}
          rewardsData={rewardsData}
        />
      ) : null}

      {step === STEPS.history ? (
        <StepHistory
          onLoadPointsPressHandler={onLoadPointsPressHandler}
          customerData={customerData}
          userData={userData}
        />
      ) : null}

      {step === STEPS.loadPointsInput ? (
        <KeyboardAvoidingView
          style={globalStyles.centeredCon}
          behavior={Platform.OS === 'ios' ? 'padding' : null}
        >
          <StepLoadPointsInput
            onPointsLoadedPressHandler={onPointsLoadedPressHandler}
          />
        </KeyboardAvoidingView>
      ) : null}

      {step === STEPS.addCustomer ? (
        <StepAddCustomer
          onGoBackPressHandler={goBackOnPress}
          onAddClientPressHandler={onAddClientPressHandler}
        />
      ) : null}

      {step === STEPS.stepError ? (
        <Error
          onContinuePressHandler={goBackOnPress}
          mainText={error.mainText}
          secondaryText={error.secondaryText}
        />
      ) : null}
    </View>
  );

  if (step !== STEPS.qrScanner) {
    return (
      <PageHeaderContainer
        style={
          step !== STEPS.rewards ? { scrollView: globalStyles.centeredCon } : {}
        }
        keyboardShouldPersistTaps="always"
        renderHeader={renderHeader}
        renderContent={renderContent}
        renderBackground={renderBackground}
        headerHeight={getHeaderHeight()}
        refreshing={loading}
      />
    );
  }

  return !isInitializingCustomer ? (
    <QrScanner
      actionButton
      onActionButtonPress={() => {
        onManualLoadPress();
        analyticsProvider.logEvent(events.load_profile.email_alias_button);
      }}
      actionButtonMessage="Ingresar Alias / Email"
      goBackOnPress={goBackOnPress}
      onManualLoadPress={onManualLoadPress}
      onBarCodeScanned={onLoadInfoContinuePressHandler}
    />
  ) : null;
};

LoadProfile.propTypes = {
  loading: PropTypes.bool,
  onAddClientPressHandler: PropTypes.func.isRequired,
  customerData: PropTypes.shape({
    CustomerLevel: PropTypes.shape({
      level: PropTypes.number,
      total: PropTypes.number,
    }),
    id: PropTypes.string,
  }),
  userData: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }),
  error: PropTypes.shape({
    mainText: PropTypes.string,
    secondaryText: PropTypes.string,
  }),
  rewardsData: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      level: PropTypes.number,
      StoreRewards: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string,
          title: PropTypes.string,
        })
      ),
    })
  ),
  step: PropTypes.string.isRequired,
  STEPS: PropTypes.shape({
    qrScanner: PropTypes.string.isRequired,
    manualLoad: PropTypes.string.isRequired,
    profile: PropTypes.string.isRequired,
    loadPointsInput: PropTypes.string.isRequired,
    addCustomer: PropTypes.string.isRequired,
    stepError: PropTypes.string.isRequired,
  }).isRequired,
  goBackOnPress: PropTypes.func.isRequired,
};

LoadProfile.defaultProps = {
  loading: false,
  rewardsData: null,
  userData: null,
  customerData: null,
  error: {
    mainText: null,
    secondaryText: null,
  },
};

export default LoadProfile;
