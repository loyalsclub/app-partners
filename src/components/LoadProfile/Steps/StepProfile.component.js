/** External Dependencies */
import React, { useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';

import { Animated, Text, View } from 'react-native';

/** Components */
import MainContainer from '../../Common/Layout/MainContainer.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';
import PorcentageIndicator from './PorcentageIndicator/PorcentageIndicator.component';
import HistoryIcon from '../../Common/Icons/History.icon';
import TicketIcon from '../../Common/Icons/Ticket.icon';

/** Styles */
import globalStyles from '../../../styles/global.styles';
import fontStyles from '../../../styles/font.styles';
import ActionCard from '../../Common/Miscellaneous/ActionCard/ActionCard.component';
import translate from '../../../utils/language.utils';

const styles = {
  userName: { paddingBottom: 10 },
  actionButton: { marginBottom: 15 },
};

const StepProfile = ({
  onLoadPointsPressHandler,
  onRewardsPressHandler,
  onHistoryPressHandler,
  customerData,
  userData,
  rewardsData,
}) => {
  const {
    CustomerLevel: {
      level,
      total,
      currentLevelPoints,
      nextLevelPointsDif,
      nextLevelPoints,
    },
  } = customerData;

  const progressAV = useMemo(() => new Animated.Value(0), []);

  // Progress [0, 1], if max level we default to 1
  const progress =
    level === rewardsData.length
      ? 1
      : (total - currentLevelPoints) / nextLevelPointsDif;

  useEffect(() => {
    if (progress === undefined) return;

    Animated.parallel([
      Animated.timing(progressAV, {
        toValue: progress,
        duration: 1000,
        useNativeDriver: true,
      }),
    ]).start();
  }, [progressAV, progress]);

  return (
    <MainContainer
      style={[globalStyles.centeredCon, { justifyContent: 'space-around' }]}
    >
      <View style={{ alignItems: 'center' }}>
        <Text
          style={[
            fontStyles.blue,
            fontStyles.xlargeSize,
            fontStyles.boldWeight,
            fontStyles.center,
            styles.userName,
          ]}
        >
          {`${userData.firstName} ${userData.lastName}`}
        </Text>

        <PorcentageIndicator
          progressAV={progressAV}
          level={level}
          size={250}
          currentPoints={total}
          totalPoints={nextLevelPoints}
        />

        <ActionCard
          style={{ marginTop: 10 }}
          icon={<HistoryIcon width={30} />}
          title={translate('LOAD_PROFILE.historyAction')}
          onPress={onHistoryPressHandler}
        />

        <ActionCard
          icon={<TicketIcon width={30} />}
          title={translate('LOAD_PROFILE.rewardsAction')}
          onPress={onRewardsPressHandler}
        />
      </View>

      <GradientButton
        style={styles.actionButton}
        mode="active"
        onPress={onLoadPointsPressHandler}
        text="Registrar consumo"
      />
    </MainContainer>
  );
};

export default StepProfile;

StepProfile.propTypes = {
  onLoadPointsPressHandler: PropTypes.func.isRequired,
  customerData: PropTypes.shape({
    CustomerLevel: PropTypes.shape({
      level: PropTypes.number,
      total: PropTypes.number,
    }),
    id: PropTypes.string,
  }).isRequired,
  userData: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }).isRequired,
  rewardsData: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      level: PropTypes.number,
      StoreRewards: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string,
          title: PropTypes.string,
        })
      ),
    })
  ).isRequired,
};
