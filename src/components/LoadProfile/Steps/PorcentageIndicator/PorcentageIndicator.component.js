/** External Dependencies */
import React from 'react';
import PropTypes from 'prop-types';
import Svg, { Circle, Defs, LinearGradient, Stop } from 'react-native-svg';
import { Animated, View, Text, Image } from 'react-native';
import AnimatedMath from 'react-native-animated-math';

/** Components */
import { Icon } from '../../../Common/Miscellaneous/Icons.component';

/** Styles */
import globalStyles from '../../../../styles/global.styles';
import fontStyles from '../../../../styles/font.styles';

const AnimatedIcon = Animated.createAnimatedComponent(Icon);
const AnimatedCircle = Animated.createAnimatedComponent(Circle);

const LEVEL_SETTINGS = {
  1: {
    gradient: [
      'rgba(171,185,207,1)',
      'rgba(196, 214, 231, 1)',
      'rgba(171,185,207,1)',
      'rgba(196, 214, 231, 1)',
      'rgba(97,124,160,1)',
    ],
    image: require('../../../../assets/images/crown-silver/img.png'),
  },
  2: {
    gradient: ['rgba(236,198,53,1)', 'rgba(210, 136, 26, 1)'],
    image: require('../../../../assets/images/crown-gold/img.png'),
  },
  3: {
    gradient: [
      'rgba(0,175,141,1)',
      'rgba(189,255,255,1)',
      'rgba(0,175,141,1)',
      'rgba(189,255,255,1)',
      'rgba(0,175,141,1)',
    ],
    image: require('../../../../assets/images/crown-zafire/img.png'),
  },
};

const PorcentageIndicator = (props) => {
  const {
    progressAV, // Progress of Animated Value
    level, // Level user is at. Will define crown color, gradient colors
    currentPoints,
    totalPoints,
  } = props;
  let { size } = props;

  const SETTINGS = LEVEL_SETTINGS[level];
  const strokeWidth = 8; // Width of the stroke
  const iconCircleContainerRadius = 12; // Radius of the crown container
  const radius = size / 2 - iconCircleContainerRadius; // Radius of progress circle
  const iconSize = 12;
  const padding = 4;
  const iconOffset = radius + padding / 2 + iconSize / 2;

  size += padding;

  const progressAnim = progressAV.interpolate({
    inputRange: [0, 1],
    outputRange: [2 * Math.PI * radius, 0],
  });

  const iconProgress = progressAV.interpolate({
    inputRange: [0, 1],
    outputRange: [-Math.PI / 2, (3 / 2) * Math.PI], // Goes from [-90, 270]
  });

  // Styles
  const styles = {
    innerContainer: {
      position: 'absolute',
      width: size,
      height: size,
    },
    crownImage: {
      height: size / 3,
      width: size / 3,
      resizeMode: 'contain',
    },
    levelIndicator: {
      fontSize: 18,
      paddingTop: 4,
    },
    pointsContainer: {
      paddingTop: 4,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
    currentPointsText: {
      fontSize: 22,
    },
    totalPointsText: {
      fontSize: 19,
    },
  };

  return (
    <View style={{ position: 'relative' }}>
      <Svg height={size} width={size}>
        <Defs>
          <LinearGradient
            id="progressGrad"
            x1="0"
            y1="0"
            x2={size * (2 / 3)}
            y2="0"
          >
            {SETTINGS.gradient.map((gradient, index, arr) => (
              <Stop
                key={`progressGrad${Math.random()}`}
                offset={(1 / arr.length) * (index + 1)}
                stopColor={gradient}
                stopOpacity="1"
              />
            ))}
          </LinearGradient>
          <LinearGradient
            id="circleGrad"
            x1="0"
            y1={iconCircleContainerRadius * 2}
            x2={iconCircleContainerRadius * 2}
            y2="0"
          >
            {SETTINGS.gradient.map((gradient, index, arr) => (
              <Stop
                key={`circleGrad${Math.random()}`}
                offset={(1 / arr.length) * (index + 1)}
                stopColor={gradient}
                stopOpacity="1"
              />
            ))}
          </LinearGradient>
          <LinearGradient
            id="circleGradStroke"
            x1="0"
            y1={iconCircleContainerRadius}
            x2={iconCircleContainerRadius}
            y2="0"
          >
            {SETTINGS.gradient.map((gradient, index, arr) => (
              <Stop
                key={`circleGradStroke${Math.random()}`}
                offset={(1 / arr.length) * (index + 1)}
                stopColor={gradient}
                stopOpacity="1"
              />
            ))}
          </LinearGradient>
        </Defs>
        <Circle
          stroke="rgba(35,46,64, 0.1)"
          cx={size / 2}
          cy={size / 2}
          fill="none"
          r={radius - strokeWidth / 4} // Half for inner circle
          strokeWidth={strokeWidth / 2}
        />
        <Circle
          stroke="rgba(35,46,64, 0.2)"
          cx={size / 2}
          cy={size / 2}
          fill="none"
          r={radius + strokeWidth / 4} // Half for outer circle
          strokeWidth={strokeWidth / 2}
        />
        <AnimatedCircle
          cx={size / 2}
          cy={size / 2}
          fill="none"
          r={radius}
          strokeWidth={strokeWidth}
          rotation="-90"
          origin={`${size / 2}, ${size / 2}`}
          stroke="url(#progressGrad)"
          strokeLinecap="round"
          strokeDasharray={`${2 * Math.PI * radius}`}
          strokeDashoffset={progressAnim}
        />
        <AnimatedCircle
          r={iconCircleContainerRadius}
          cx={Animated.add(
            Animated.multiply(AnimatedMath.cos(iconProgress), radius),
            size / 2
          )}
          cy={Animated.add(
            Animated.multiply(AnimatedMath.sin(iconProgress), radius),
            size / 2
          )}
          stroke="url(#circleGradStroke)"
          strokeWidth={4}
          fill="url(#circleGrad)"
        />
      </Svg>
      <AnimatedIcon
        style={{
          position: 'absolute',
          transform: [
            {
              translateX: Animated.add(
                // This add 1 is for visual correction.
                Animated.multiply(AnimatedMath.cos(iconProgress), radius),
                iconOffset - 1
              ),
            },
            {
              translateY: Animated.add(
                Animated.multiply(AnimatedMath.sin(iconProgress), radius),
                iconOffset
              ),
            },
          ],
        }}
        color="white"
        name="crown"
        size={iconSize}
      />
      <View style={[styles.innerContainer, globalStyles.centeredCon]}>
        <Image source={SETTINGS.image} style={styles.crownImage} />
        <Text
          style={[
            fontStyles.normalWeight,
            fontStyles.grey,
            styles.levelIndicator,
          ]}
        >
          {totalPoints > currentPoints ? 'Siguiente nivel' : '¡Máximo Nivel!'}
        </Text>
        <View style={styles.pointsContainer}>
          <Text
            style={[
              fontStyles.boldWeight,
              fontStyles.grey,
              styles.currentPointsText,
            ]}
          >
            {currentPoints}
          </Text>
          <Text
            style={[
              fontStyles.normalWeight,
              fontStyles.grey,
              styles.totalPointsText,
            ]}
          >
            {`/${totalPoints}`}
          </Text>
        </View>
      </View>
    </View>
  );
};

PorcentageIndicator.propTypes = {
  progressAV: PropTypes.instanceOf(Animated.Value).isRequired,
  level: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
};

export default PorcentageIndicator;
