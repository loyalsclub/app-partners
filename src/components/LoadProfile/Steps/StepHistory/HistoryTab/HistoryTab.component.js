import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import themeStyle from '../../../../../styles/theme.style';

const HistoryTab = ({ name, selectedValue, text, onPress }) => {
  return (
    <TouchableOpacity
      style={{
        backgroundColor:
          selectedValue !== name ? themeStyle.LIGHT_GREY : 'transparent',
        padding: 7,
        borderRadius: 5,
      }}
      onPress={() => onPress(name)}
    >
      <Text>{text}</Text>
    </TouchableOpacity>
  );
};

export default HistoryTab;
