/** External Dependencies */
import React, { useEffect, useState } from 'react';

import { ScrollView, Text, View } from 'react-native';

/** Components */
import { useLazyQuery } from '@apollo/react-hooks';
import MainContainer from '../../../Common/Layout/MainContainer.component';

/** Styles */
import globalStyles from '../../../../styles/global.styles';
import fontStyles from '../../../../styles/font.styles';
import translate from '../../../../utils/language.utils';
import { EMPLOYEE_GET_CUSTOMER_HISTORY } from '../../../../queries/partner.queries';
import HistoryRow from './HistoryRow/HistoryRow.component';
import HistoryTab from './HistoryTab/HistoryTab.component';
import { CUSTOMER_HISTORY_TYPES } from '../../../../config/constants';
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';

const StepHistory = ({ userData, customerData, onLoadPointsPressHandler }) => {
  const [
    getStoreHistory,
    { data: { employeeGetCustomerHistory: storeHistory = [] } = {} },
  ] = useLazyQuery(EMPLOYEE_GET_CUSTOMER_HISTORY, {
    fetchPolicy: 'cache-and-network',
    variables: { storeId: customerData.storeId, customerId: customerData.id },
  });

  useEffect(() => {
    getStoreHistory();
  }, [getStoreHistory]);

  const [filter, setFilter] = useState(CUSTOMER_HISTORY_TYPES.ALL);

  const isFilterSet = filter !== CUSTOMER_HISTORY_TYPES.ALL;

  const filteredHistory = !isFilterSet
    ? storeHistory
    : storeHistory.filter((history) => history.type === filter);

  return (
    <MainContainer
      style={{
        flex: 1,
        justifyContent: 'space-around',
        marginTop: 20,
      }}
    >
      <View style={{ height: '80%' }}>
        <Text
          style={[
            fontStyles.blue,
            fontStyles.xlargeSize,
            fontStyles.boldWeight,
            fontStyles.center,
          ]}
        >
          {`${userData.firstName} ${userData.lastName}`}
        </Text>

        <Text
          style={[fontStyles.blue, fontStyles.mediumSize, fontStyles.center]}
        >
          {translate('LOAD_PROFILE.historyAction')}
        </Text>

        <View
          style={{
            marginVertical: 20,
            flexDirection: 'row',
            justifyContent: 'space-around',
          }}
        >
          <HistoryTab
            name={CUSTOMER_HISTORY_TYPES.ALL}
            selectedValue={filter}
            text={translate('LOAD_PROFILE.optionAll')}
            onPress={setFilter}
          />

          <HistoryTab
            name={CUSTOMER_HISTORY_TYPES.POINT}
            selectedValue={filter}
            text={translate('LOAD_PROFILE.optionPoints')}
            onPress={setFilter}
          />

          <HistoryTab
            name={CUSTOMER_HISTORY_TYPES.STORE_REWARD_USAGE}
            selectedValue={filter}
            text={translate('LOAD_PROFILE.optionRewards')}
            onPress={setFilter}
          />
        </View>

        {!storeHistory.length ? (
          <Text
            style={[globalStyles.sectionPageSubtitle, fontStyles.largeSize]}
          >
            {translate('LOAD_PROFILE.noHistory')}
          </Text>
        ) : (
          <>
            {!filteredHistory.length && filter && (
              <Text
                style={[globalStyles.sectionPageSubtitle, fontStyles.largeSize]}
              >
                {translate('LOAD_PROFILE.emptySearch')}
              </Text>
            )}

            <ScrollView>
              {filteredHistory.map((history) => (
                <HistoryRow
                  key={`${history.type}-${history.id}`}
                  history={history}
                />
              ))}
            </ScrollView>
          </>
        )}
      </View>

      <GradientButton
        mode="active"
        onPress={onLoadPointsPressHandler}
        text="Registrar consumo"
      />
    </MainContainer>
  );
};

export default StepHistory;
