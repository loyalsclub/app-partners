import React from 'react';
import { Text, View } from 'react-native';
import styles from './HistoryRow.styles';

const HistoryRow = ({ history }) => (
  <View key={history.id} style={styles.row}>
    <View style={styles.columnLeft}>
      <Text
        style={[
          styles.text,
          styles.secondaryText,
          { textAlign: 'right', flex: 1 },
        ]}
      >
        {new Date(history.createdAt).toLocaleDateString('es')}
      </Text>

      <Text
        style={[
          styles.text,
          {
            textAlign: 'left',
            marginLeft: 10,
            flex: 1,
          },
        ]}
      >
        {history.type === 'POINT' ? 'Puntos' : 'Beneficio'}
      </Text>
    </View>

    <Text style={[styles.text, styles.secondaryText, { flex: 1 }]}>
      {history.value}
    </Text>
  </View>
);

export default HistoryRow;
