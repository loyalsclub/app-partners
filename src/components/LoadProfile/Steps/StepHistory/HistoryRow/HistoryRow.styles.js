import fontStyles from '../../../../../styles/font.styles';
import themeStyle from '../../../../../styles/theme.style';

export default {
  row: {
    flexDirection: 'row',
    marginBottom: 10,
    padding: 5,
    borderRadius: 8,
    alignItems: 'flex-start',
  },
  columnLeft: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  text: {
    ...fontStyles.smallSize,
  },
  secondaryText: { color: themeStyle.GREY },
};
