/** External Dependencies */
import React from 'react';
import PropTypes from 'prop-types';

/** Components */
import MainContainer from '../../Common/Layout/MainContainer.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';
import PassiveNotification from '../../Common/Miscellaneous/PassiveNotification.component';

/** Styles */
import globalStyles from '../../../styles/global.styles';

const StepAddCustomer = (props) => {
  const { onGoBackPressHandler, onAddClientPressHandler } = props;

  return (
    <MainContainer style={globalStyles.centeredCon}>
      <PassiveNotification
        mainText="Agregar Cliente"
        secondaryText={`El usuario no forma parte de tu programa de beneficios. \n ¿ Querés sumarlo ?`}
        type="notPartner"
      />
      <GradientButton
        style={styles.button}
        mode="active"
        text="Agregar cliente"
        onPress={onAddClientPressHandler}
      />
      <GradientButton
        style={styles.button}
        mode="dark"
        text="Volver al inicio"
        onPress={onGoBackPressHandler}
      />
    </MainContainer>
  );
};

const styles = {
  button: {
    marginTop: 15,
  },
};

StepAddCustomer.propTypes = {
  onGoBackPressHandler: PropTypes.func.isRequired,
  onAddClientPressHandler: PropTypes.func.isRequired,
};

export default StepAddCustomer;
