/** External Dependencies */
import React from 'react';
import PropTypes from 'prop-types';

import { Text, Alert, View } from 'react-native';

/** Components */
import MainContainer from '../../Common/Layout/MainContainer.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';
import RewardsList from '../../Common/Miscellaneous/RewardsList.component';

/** Styles */
import globalStyles from '../../../styles/global.styles';
import fontStyles from '../../../styles/font.styles';
import translate from '../../../utils/language.utils';

const styles = {
  userName: { paddingBottom: 10 },
  actionButton: { marginBottom: 15 },
};

const StepRewards = ({
  onLoadPointsPressHandler,
  onRewardUsagePressHandler,
  customerData,
  userData,
  rewardsData,
}) => {
  const {
    CustomerLevel: { level },
  } = customerData;

  const alertRewardUsage = (id) => () => {
    const rewardToUse = rewardsData
      .flatMap((rewardLevel) => rewardLevel.StoreRewards)
      .filter((reward) => reward.id === id)[0];

    Alert.alert(
      'Usar beneficio',
      `Se marcará como utilizado el beneficio:\n"${rewardToUse.title}"`,
      [
        {
          text: 'Cancelar',
          style: 'cancel',
        },
        {
          text: 'Usar',
          onPress: () => onRewardUsagePressHandler(rewardToUse.id),
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <MainContainer
      style={[
        globalStyles.centeredCon,
        { marginTop: 20, justifyContent: 'space-around' },
      ]}
    >
      <View style={{ alignSelf: 'stretch', alignItems: 'center' }}>
        <Text
          style={[
            fontStyles.blue,
            fontStyles.xlargeSize,
            fontStyles.boldWeight,
            fontStyles.center,
            styles.userName,
          ]}
        >
          {`${userData.firstName} ${userData.lastName}`}
        </Text>

        <Text
          style={[fontStyles.blue, fontStyles.mediumSize, fontStyles.center]}
        >
          {translate('LOAD_PROFILE.rewardsAction')}
        </Text>

        <RewardsList
          rewardsData={rewardsData}
          currentLevel={level}
          actionType="usage"
          onAction={alertRewardUsage}
        />
      </View>

      <GradientButton
        style={styles.actionButton}
        mode="active"
        onPress={onLoadPointsPressHandler}
        text="Registrar consumo"
      />
    </MainContainer>
  );
};

export default StepRewards;

StepRewards.propTypes = {
  onLoadPointsPressHandler: PropTypes.func.isRequired,
  onRewardUsagePressHandler: PropTypes.func.isRequired,
  customerData: PropTypes.shape({
    CustomerLevel: PropTypes.shape({
      level: PropTypes.number,
      total: PropTypes.number,
    }),
    id: PropTypes.string,
  }).isRequired,
  userData: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }).isRequired,
  rewardsData: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      level: PropTypes.number,
      StoreRewards: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string,
          title: PropTypes.string,
        })
      ),
    })
  ).isRequired,
};
