/** External Dependencies */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import t from 'tcomb-form-native';

/** Components */
import MainContainer from '../../Common/Layout/MainContainer.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';

/** Styles */
import globalStyles from '../../../styles/global.styles';
import formstyles from '../../Common/AuthForm/OriginalTemplates.styles';

const { Form } = t.form;

export default class StepManualLoad extends Component {
  onContinuePressHandler = () => {
    const value = this.form.getValue();
    if (!value) return;

    const { onContinuePressHandler } = this.props;

    onContinuePressHandler(value.userField);
  };

  onChangeForm = (formData) => {
    this.setState({
      ...this.state,
      formData,
    });
    return formData;
  };

  render() {
    const userField = {
      placeholder: 'EJ: 232123 ó manuel@gmail.com',
      label: 'Ingrese el ALIAS ó el email',
      autoCapitalize: 'none',
      autoComplete: false,
      minLength: 2,
      size: 50,
      autoFocus: true,
      autoCorrect: false,
      error: 'Ingrese un código valido',
      returnKeyLabel: 'Aceptar',
      returnKeyType: 'go',
      onSubmitEditing: this.onContinuePressHandler,
    };

    const authForm = t.struct({
      userField: t.String,
    });

    const options = {
      stylesheet: formstyles,
      fields: {
        userField,
      },
    };

    return (
      <MainContainer style={globalStyles.centeredCon}>
        <Form
          ref={(ref) => {
            this.form = ref;
          }}
          type={authForm}
          options={options}
        />
        <GradientButton
          style={styles.actionButton}
          mode="active"
          onPress={this.onContinuePressHandler}
          text="Continuar"
        />
      </MainContainer>
    );
  }
}

const styles = {
  bigIconStyle: {
    marginBottom: 20,
  },
  button: {
    marginTop: 15,
  },
};

StepManualLoad.propTypes = {
  onContinuePressHandler: PropTypes.func.isRequired,
};
