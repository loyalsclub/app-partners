import React from 'react';
import { Text, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Controller, useForm } from 'react-hook-form';
import fontStyles from '../../styles/font.styles';
import globalStyles from '../../styles/global.styles';
import translate from '../../utils/language.utils';
import CustomTextInput from '../Common/Form/CustomTextInput.component';
import CustomerRow from './CustomerRow/CustomerRow.component';
import SectionPage from '../Common/Layout/SectionPage.component';

const filterOptions = ['firstName', 'lastName', 'alias'];

const CustomerList = ({
  customers,
  onCustomerProfilePressHandler,
  loading,
}) => {
  const { control, watch } = useForm({ mode: 'onChange' });

  const { filter } = watch();
  const isFilterSet = Boolean(filter);

  const filteredCustomers = !isFilterSet
    ? customers
    : customers.filter((customer) =>
        filterOptions.some(
          (option) =>
            customer.User &&
            customer.User[option] &&
            customer.User[option]
              .toLowerCase()
              .includes(filter && filter.toLowerCase())
        )
      );

  return (
    <SectionPage
      title={translate('CUSTOMER_LIST.title')}
      loading={loading}
      customBody={
        <View style={{ marginTop: 20 }}>
          {!customers.length ? (
            <Text
              style={[globalStyles.sectionPageSubtitle, fontStyles.largeSize]}
            >
              {translate('CUSTOMER_LIST.noCustomers')}
            </Text>
          ) : (
            <>
              <View>
                <Controller
                  control={control}
                  as={
                    <CustomTextInput
                      inputProps={{ autoCorrect: false }}
                      label={translate('CUSTOMER_LIST.filter')}
                    />
                  }
                  name="filter"
                  defaultValue=""
                />
              </View>

              {!filteredCustomers.length && filter && (
                <Text
                  style={[
                    globalStyles.sectionPageSubtitle,
                    fontStyles.largeSize,
                  ]}
                >
                  {translate('CUSTOMER_LIST.emptySearch')}
                </Text>
              )}

              {filteredCustomers.map((customer) => (
                <CustomerRow
                  key={customer.id}
                  customer={customer}
                  onPressHandler={onCustomerProfilePressHandler}
                />
              ))}
            </>
          )}
        </View>
      }
    />
  );
};

export default withNavigation(CustomerList);
