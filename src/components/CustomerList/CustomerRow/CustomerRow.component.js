import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import ChevronRightIcon from '../../Common/Icons/ChevronRight.icon';
import styles from './CustomerRow.styles';

const imagesByStoreLevel = {
  1: require('../../../assets/images/crown-silver/img.png'),
  2: require('../../../assets/images/crown-gold/img.png'),
  3: require('../../../assets/images/crown-zafire/img.png'),
};

const CustomerRow = ({ onPressHandler, customer }) => (
  <TouchableOpacity
    style={styles.row}
    onPress={() => onPressHandler(customer.User.alias)}
  >
    <View style={{ flex: 2 }}>
      <Text
        numberOfLines={1}
        style={[styles.cell, styles.highlightedCell]}
      >{`${customer.User.firstName} ${customer.User.lastName}`}</Text>
    </View>

    <View style={{ flex: 1 }}>
      <Text style={[styles.cell, styles.secondaryCell]}>
        {customer.User.alias}
      </Text>
    </View>

    <View style={styles.compoundCell}>
      <Image
        source={imagesByStoreLevel[customer.CustomerLevel.level]}
        style={styles.pointsImage}
      />

      <Text style={[styles.cell, styles.secondaryCell]}>
        {customer.CustomerLevel.total}
      </Text>
    </View>

    <View>
      <ChevronRightIcon />
    </View>
  </TouchableOpacity>
);

export default CustomerRow;
