import fontStyles from '../../../styles/font.styles';
import themeStyle from '../../../styles/theme.style';

export default {
  row: {
    backgroundColor: themeStyle.LIGHT_GREY,
    flexDirection: 'row',
    marginBottom: 10,
    padding: 15,
    borderRadius: 8,
    alignItems: 'center',
  },
  cell: [fontStyles.mediumSize],
  highlightedCell: {
    fontFamily: themeStyle.MAIN_FONT_MEDIUM,
  },
  secondaryCell: {
    fontFamily: themeStyle.MAIN_FONT_LIGHT,
    color: themeStyle.GREY,
  },
  compoundCell: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  pointsImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    marginRight: 10,
  },
};
