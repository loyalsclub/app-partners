import React, { useMemo } from 'react';
import ClientsIcon from '../Common/Icons/Clients.icon';
import SectionPage from '../Common/Layout/SectionPage.component';
import BulletListIcon from '../Common/Icons/BulletList.icon';
import StatisticsIcon from '../Common/Icons/Statistics.icon';
import translate from '../../utils/language.utils';

const MyCustomers = ({
  onCustomerListPressHandler,
  onStatisticsPressHandler,
}) => {
  const actions = useMemo(
    () => [
      {
        icon: <BulletListIcon />,
        title: translate('MY_CUSTOMERS.actionClientListTitle'),
        description: translate('MY_CUSTOMERS.actionClientListDescription'),
        onPressHandler: onCustomerListPressHandler,
      },
      {
        icon: <StatisticsIcon />,
        title: translate('MY_CUSTOMERS.actionStatisticsTitle'),
        description: translate('MY_CUSTOMERS.actionStatisticsDescription'),
        onPressHandler: onStatisticsPressHandler,
      },
    ],
    [onStatisticsPressHandler, onCustomerListPressHandler]
  );

  return (
    <SectionPage
      icon={<ClientsIcon width={80} height={80} style={{ marginBottom: 40 }} />}
      title={translate('MY_CUSTOMERS.title')}
      description={translate('MY_CUSTOMERS.description')}
      actions={actions}
    />
  );
};

export default MyCustomers;
