/** External Dependencies */
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';

/** Components */
import GradientButton from '../Common/Miscellaneous/GradientButton.component';
import BackgroundPattern from '../Common/Miscellaneous/BackgroundPattern.component';
import PageHeaderContainer from '../Common/Layout/PageHeaderContainer.component';
import GenericHeader, {
  getHeaderHeight,
} from '../Common/Miscellaneous/GenericHeader.component';
import MainContainer from '../Common/Layout/MainContainer.component';

/** Styles */
import globalStyles from '../../styles/global.styles';
import f from '../../styles/font.styles';
import Anchor from '../Common/Miscellaneous/Anchor.component';

const styles = {
  title: [
    f.blue,
    f.center,
    f.xlargeSize,
    {
      marginVertical: 10,
    },
  ],
  description: [f.blue, f.normalWeight, f.mediumSize, f.center],
  descriptionLink: {
    paddingTop: 10,
  },
  storeTitle: [f.center, f.blue, f.largeSize, f.boldWeight],
  storeDescription: [f.center, f.blue, f.mediumSize, f.normalWeight],
  actionButton: {
    marginVertical: 10,
  },
  storesContent: {
    marginVertical: 20,
  },
  storeItem: {
    alignItems: 'center',
    marginVertical: 15,
  },
};

export default class TestSystem extends Component {
  renderContent = () => {
    const { testLogin, stores } = this.props;
    return (
      /* eslint-disable-next-line */
      <MainContainer style={globalStyles.centeredCon}>
        <Text style={styles.title}>¡Bienvenido!</Text>
        <Text style={styles.description}>
          A continuación te dejamos 4 ejemplos de comercios para ver como ellos
          sacan el mejor provecho de nuestra herramienta. Recordá que Loyals
          Club está pensada para afianzar la relación entre tus clientes y tu
          comercio, generando un vínculo de beneficio mutuo.
        </Text>
        <Text style={[styles.description, styles.descriptionLink]}>
          Mas información en:
          <Anchor href="https://partners.loyals.club?utm_source=app_partners&utm_medium=hyperlink&utm_content=test_system_know_more">
            <Text style={[f.boldWeight, f.mediumSize]}>
              {' '}
              partners.loyals.club
            </Text>
          </Anchor>
        </Text>
        <View style={styles.storesContent}>
          <View style={styles.storeItem}>
            <Text style={styles.storeTitle}>Heladería Sabor</Text>
            <Text style={styles.storeDescription}>
              Probá nuestra plataforma como una heladería artesanal, ofreciendo
              beneficios con un descuento fijo que motive a tus clientes a
              consumir un monto objetivo.
            </Text>
            <GradientButton
              style={styles.actionButton}
              onPress={testLogin(stores.HELADERIA)}
              mode="active"
              text="Probar - Heladería Sabor"
            />
          </View>
          <View style={styles.storeItem}>
            <Text style={styles.storeTitle}>Decoración Luna</Text>
            <Text style={styles.storeDescription}>
              Probá nuestra plataforma como una casa de decoración, que
              incorpora beneficios reservados para los niveles más altos de
              fidelidad como herramienta para incentivar la frecuencia de
              consumo en su comercio.
            </Text>
            <GradientButton
              style={styles.actionButton}
              onPress={testLogin(stores.DECORACION)}
              mode="active"
              text="Probar - Decoración Luna"
            />
          </View>
          <View style={styles.storeItem}>
            <Text style={styles.storeTitle}>Café Los Hermanos</Text>
            <Text style={styles.storeDescription}>
              Probá la plataforma como un emprendimiento de cafetería familiar,
              que aprovecha nuestro sistema de fidelización para fortalecer el
              consumo en días particulares de la semana a través de los
              beneficios que ofrece.
            </Text>
            <GradientButton
              style={styles.actionButton}
              onPress={testLogin(stores.CAFE)}
              mode="active"
              text="Probar - Café Los Hermanos"
            />
          </View>
          <View style={styles.storeItem}>
            <Text style={styles.storeTitle}>Pizzería Gómez</Text>
            <Text style={styles.storeDescription}>
              Probá la plataforma como una pizzería de barrio con 15 años de
              trayectoria, que fideliza a sus clientes ofreciendo promociones
              que mejoran en los niveles más altos.
            </Text>
            <GradientButton
              style={styles.actionButton}
              onPress={testLogin(stores.PIZZERIA)}
              mode="active"
              text="Probar - Pizzería Gómez"
            />
          </View>
        </View>
      </MainContainer>
    );
  };

  renderBackground = () => <BackgroundPattern type="golden" />;

  renderHeader = () => {
    const { goBackOnPress } = this.props;
    return <GenericHeader goBackOnPress={goBackOnPress} mode="goBack" />;
  };

  render() {
    return (
      <PageHeaderContainer
        style={{ scrollView: globalStyles.centerScrollView }}
        renderHeader={this.renderHeader}
        renderContent={this.renderContent}
        renderBackground={this.renderBackground}
        headerHeight={getHeaderHeight()}
      />
    );
  }
}

TestSystem.propTypes = {
  testLogin: PropTypes.func.isRequired,
};
