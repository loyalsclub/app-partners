import React, { useMemo } from 'react';
import ClientsIcon from '../Common/Icons/Clients.icon';
import SectionPage from '../Common/Layout/SectionPage.component';
import ShareIcon from '../Common/Icons/Share.icon';
import HouseIcon from '../Common/Icons/House.icon';
import translate from '../../utils/language.utils';

const MyStore = ({ onVisitStorePressHandler, onShareStorePressHandler }) => {
  const actions = useMemo(
    () => [
      {
        icon: <HouseIcon />,
        title: translate('MY_STORE.actionVisitStoreTitle'),
        description: translate('MY_STORE.actionVisitStoreDescription'),
        onPressHandler: onVisitStorePressHandler,
      },
      {
        icon: <ShareIcon />,
        title: translate('MY_STORE.actionShareStoreTitle'),
        description: translate('MY_STORE.actionShareStoreDescription'),
        onPressHandler: onShareStorePressHandler,
      },
    ],
    [onShareStorePressHandler, onVisitStorePressHandler]
  );

  return (
    <SectionPage
      icon={<ClientsIcon width={80} height={80} style={{ marginBottom: 40 }} />}
      title={translate('MY_STORE.title')}
      description={translate('MY_STORE.description')}
      actions={actions}
    />
  );
};

export default MyStore;
