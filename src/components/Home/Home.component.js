import React, { useState } from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';
import BackgroundPattern from '../Common/Miscellaneous/BackgroundPattern.component';
import PageHeaderContainer from '../Common/Layout/PageHeaderContainer.component';
import GenericHeader, {
  getHeaderHeight,
} from '../Common/Miscellaneous/GenericHeader.component';
import MainContainer from '../Common/Layout/MainContainer.component';
import StoreLogoComponent from '../Common/Miscellaneous/StoreLogo.component';
import PassiveNotification from '../Common/Miscellaneous/PassiveNotification.component';
import GradientButton from '../Common/Miscellaneous/GradientButton.component';
import { isEmailVerified, getUserEmail } from '../../utils/auth.utils';
import globalStyles from '../../styles/global.styles';
import styles from './Home.component.styles';
import ActionCard from '../Common/Miscellaneous/ActionCard/ActionCard.component';
import ClientsIcon from '../Common/Icons/Clients.icon';
import GearIcon from '../Common/Icons/Gear.icon';
import VerifiedClientIcon from '../Common/Icons/VerifiedClient.icon';
import StarsIcon from '../Common/Icons/Stars.icon';
import HouseIcon from '../Common/Icons/House.icon';
import translate from '../../utils/language.utils';
import HelpIcon from '../Common/Icons/Help.icon';
import { CREATE_PARTNER } from '../../pages/_constants';

const Home = ({
  error,
  refetch,
  onCreatePartnerPressHandler,
  checkEmailVerification,
  resendVerificationEmail,
  isLoadingAuthRequest,
  employeeProfile,
  onLoadPointsPressHandler,
  onFindCustomerProfilePressHandler,
  onMyCustomersPressHandler,
  onMyStorePressHandler,
  onSettingsPressHandler,
  navigation,
  onMenuPressHandler,
  loading,
}) => {
  const [storeNameFS, setStoreNameFS] = useState(28);

  const renderContent = () => {
    const { Store = {} } = employeeProfile || {};

    if (error) {
      return (
        <MainContainer style={globalStyles.centeredCon}>
          <PassiveNotification type="genericError" secondaryText={error.text} />
          <GradientButton
            style={{ marginVertical: 15 }}
            mode="dark"
            text="Refrescar"
            onPress={() => {
              refetch();
            }}
          />
        </MainContainer>
      );
    }

    if (!isEmailVerified()) {
      return (
        <MainContainer style={globalStyles.centeredCon}>
          <PassiveNotification
            type="verifyEmail"
            textVariables={{ email: getUserEmail() }}
          />

          <GradientButton
            style={{ marginVertical: 20 }}
            mode={isLoadingAuthRequest ? 'disabled' : 'active'}
            text="¡Ya verifiqué mi email!"
            onPress={checkEmailVerification}
          />

          <GradientButton
            mode={isLoadingAuthRequest ? 'disabled' : 'dark'}
            text="Reenviar email de verificación"
            onPress={resendVerificationEmail}
          />
        </MainContainer>
      );
    }

    if (!employeeProfile) {
      return (
        <PassiveNotification
          type="notPartner"
          shouldShowActionButton
          buttonText="¡Comencemos!"
          onPressHandler={onCreatePartnerPressHandler}
        />
      );
    }

    return (
      <MainContainer style={[globalStyles.centeredCon]}>
        <StoreLogoComponent size={70} source={Store.profileImage} />

        <View style={[globalStyles.row, { paddingTop: 20, paddingBottom: 40 }]}>
          <View style={globalStyles.rowChild}>
            <Text
              style={[
                styles.mainTitle,
                {
                  fontSize: storeNameFS,
                  textAlign: 'center',
                },
              ]}
              onTextLayout={(e) => {
                const { lines } = e.nativeEvent;

                if (lines.length > 2) {
                  setStoreNameFS(storeNameFS - 1);
                }
              }}
            >
              {Store.name}
            </Text>
          </View>
        </View>

        <ActionCard
          icon={<StarsIcon />}
          title={translate('HOME.actionLoadPointsTitle')}
          description={translate('HOME.actionLoadPointsDescription')}
          onPress={onLoadPointsPressHandler}
        />

        <ActionCard
          icon={<VerifiedClientIcon />}
          title={translate('HOME.actionFindCustomerProfileTitle')}
          description={translate('HOME.actionFindCustomerProfileDescription')}
          onPress={onFindCustomerProfilePressHandler}
        />

        <ActionCard
          icon={<ClientsIcon />}
          title={translate('HOME.actionMyCustomersTitle')}
          description={translate('HOME.actionMyCustomersDescription')}
          onPress={onMyCustomersPressHandler}
        />

        <ActionCard
          icon={<HouseIcon />}
          title={translate('HOME.actionMyStoreTitle')}
          description={translate('HOME.actionMyStoreDescription')}
          onPress={onMyStorePressHandler}
        />

        <ActionCard
          icon={<GearIcon />}
          title={translate('HOME.actionSettingsTitle')}
          description={translate('HOME.actionSettingsDescription')}
          onPress={onSettingsPressHandler}
        />
      </MainContainer>
    );
  };

  const renderBackground = () => <BackgroundPattern type="golden" />;

  const renderHeader = () => {
    const { Store = {} } = employeeProfile || {};

    return (
      <GenericHeader
        navigation={navigation}
        mode="menu"
        menuPressHandler={onMenuPressHandler}
        customActionIcon={<HelpIcon />}
        customAction={() =>
          navigation.push(CREATE_PARTNER, {
            storeId: Store.id,
            alias: Store.alias,
            helpMode: true,
          })
        }
      />
    );
  };

  return (
    <PageHeaderContainer
      style={{ scrollView: globalStyles.centerScrollView }}
      renderHeader={renderHeader}
      renderContent={renderContent}
      renderBackground={renderBackground}
      headerHeight={getHeaderHeight()}
      refreshing={loading}
    />
  );
};

Home.propTypes = {
  error: PropTypes.shape({
    text: PropTypes.string,
  }),
  employeeProfile: PropTypes.shape({
    store: PropTypes.shape({
      name: PropTypes.string.isRequired,
      profileImage: PropTypes.string.isRequired,
    }),
  }),
  navigation: PropTypes.object.isRequired,
  refetch: PropTypes.func.isRequired,
  onLoadPointsPressHandler: PropTypes.func.isRequired,
  onFindCustomerProfilePressHandler: PropTypes.func.isRequired,
  onMyCustomersPressHandler: PropTypes.func.isRequired,
  onMyStorePressHandler: PropTypes.func.isRequired,
  onSettingsPressHandler: PropTypes.func.isRequired,
  onMenuPressHandler: PropTypes.func.isRequired,
  onCreatePartnerPressHandler: PropTypes.func.isRequired,
  checkEmailVerification: PropTypes.func.isRequired,
  resendVerificationEmail: PropTypes.func.isRequired,
  isLoadingAuthRequest: PropTypes.bool,
  loading: PropTypes.bool,
};

Home.defaultProps = {
  error: null,
  employeeProfile: null,
  isLoadingAuthRequest: false,
  loading: false,
};

export default withNavigation(Home);
