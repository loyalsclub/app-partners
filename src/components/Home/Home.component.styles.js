import themeStyles from '../../styles/theme.style';

export default {
  mainTitle: {
    fontFamily: themeStyles.MAIN_FONT_BOLD,
    color: themeStyles.BLUE_WOOD,
  },
};
