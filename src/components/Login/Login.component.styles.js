import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  signInButton: {
    marginVertical: 10,
  },
});
