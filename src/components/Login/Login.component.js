/** External Dependencies */
import React from 'react';
import { KeyboardAvoidingView, Text, View, Platform } from 'react-native';
import PropTypes from 'prop-types';

/** Components */
import AuthForm from '../Common/AuthForm/AuthForm.component';
import GradientButton from '../Common/Miscellaneous/GradientButton.component';

/** Utils */
import translate from '../../utils/language.utils';

/** Styles */
import styles from './Login.component.styles';
import globalStyles from '../../styles/global.styles';
import LayoutPage from '../Common/Layout/LayoutPage.component';

/** Constants */
const { LOGIN } = require('../../config/constants').default;

const Login = ({ loginForm, recoverPasswordOnPress }) => {
  const submitButtonValue = translate('AUTH_FORM.loginSubmit');

  return (
    <LayoutPage>
      <KeyboardAvoidingView
        style={globalStyles.centeredCon}
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        enabled
      >
        <View>
          <AuthForm
            formType={LOGIN}
            isFetching={loginForm.isFetching}
            fields={loginForm.fields}
            value={loginForm.value}
            onChange={loginForm.onChange}
          />

          {loginForm.error && (
            <Text style={globalStyles.formError}>
              {translate(loginForm.error)}
            </Text>
          )}
        </View>

        <GradientButton
          style={styles.signInButton}
          mode="active"
          onPress={loginForm.onPress}
          text={submitButtonValue}
        />

        <GradientButton
          mode="minimal"
          onPress={recoverPasswordOnPress}
          text={translate('AUTH_FORM.recoverPassword')}
        />
      </KeyboardAvoidingView>
    </LayoutPage>
  );
};

Login.propTypes = {
  loginForm: PropTypes.object,
};

export default Login;
