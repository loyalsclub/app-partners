import React, { useMemo } from 'react';
import StarsIcon from '../Common/Icons/Stars.icon';
import WhatsAppStarsIcon from '../Common/Icons/WhatsAppStars.icon';
import InviteQRIcon from '../Common/Icons/InviteQR.icon';
import SectionPage from '../Common/Layout/SectionPage.component';
import translate from '../../utils/language.utils';

const LoadPoints = ({
  onSendPointsPressHandler,
  onScanProfilePressHandler,
}) => {
  const actions = useMemo(
    () => [
      {
        icon: <WhatsAppStarsIcon />,
        title: translate('LOAD_POINTS.actionSendPointsTitle'),
        description: translate('LOAD_POINTS.actionSendPointsDescription'),
        onPressHandler: onSendPointsPressHandler,
      },
      {
        icon: <InviteQRIcon />,
        title: translate('LOAD_POINTS.actionScanProfileTitle'),
        description: translate('LOAD_POINTS.actionScanProfileDescription'),
        onPressHandler: onScanProfilePressHandler,
      },
    ],
    [onScanProfilePressHandler, onSendPointsPressHandler]
  );

  return (
    <SectionPage
      icon={<StarsIcon width={80} height={80} style={{ marginBottom: 40 }} />}
      title={translate('LOAD_POINTS.title')}
      description={translate('LOAD_POINTS.description')}
      actions={actions}
    />
  );
};

export default LoadPoints;
