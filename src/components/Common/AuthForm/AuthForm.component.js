/**
 * # LoginForm.js
 *
 * This class utilizes the ```tcomb-form-native``` library and just
 * sets up the options required for the 3 states of Login, namely
 * Login, Register or Reset Password
 *
 */

/**
 * ## Import
 *
 * React
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import translate from '../../../utils/language.utils';
import formstyles from './AuthForm.component.style';
import textInputCustom from './TextInput.component';

/**
 * States of login display
 */
const {
  REGISTER,
  LOGIN,
  FORGOT_PASSWORD,
} = require('../../../config/constants').default;

/**
 *  The fantastic little form library
 */
const t = require('tcomb-form-native');

const { Form } = t.form;

export default class AuthForm extends Component {
  /**
   * ## render
   *
   * setup all the fields using the props and default messages
   *
   */
  render() {
    const { formType, isFetching, fields, value, onChange } = this.props;
    const form = 'form';

    const email = {
      label: translate('AUTH_FORM.email'),
      config: {
        labelAnimation: true,
      },
      keyboardType: 'email-address',
      placeholder: translate('AUTH_FORM.email'),
      autoCapitalize: 'none',
      editable: !isFetching,
      autoCorrect: false,
      hasError: fields.emailHasError,
      error: fields.emailErrorMsg,
      factory: textInputCustom,
      returnKeyType: 'next',
    };

    const password = {
      label: translate('AUTH_FORM.password'),
      secureTextEntry: true,
      config: {
        labelAnimation: true,
      },
      editable: !isFetching,
      autoCorrect: false,
      placeholder: translate('AUTH_FORM.password'),
      autoCapitalize: 'none',
      hasError: fields.passwordHasError,
      factory: textInputCustom,
      returnKeyType: 'next',
      error: fields.passwordErrorMsg,
    };

    let authForm;
    let options;

    switch (formType) {
      /**
       * ### Registration
       * The registration form has 4 fields
       */
      case REGISTER:
        authForm = t.struct({
          firstName: t.String,
          lastName: t.String,
          email: t.String,
          password: t.String,
        });

        options = {
          fields: {
            firstName: {
              label: translate('AUTH_FORM.firstName'),
              placeholder: translate('AUTH_FORM.firstName'),
              config: {
                labelAnimation: true,
              },
              autoCapitalize: 'words',
              autoCorrect: false,
              editable: !isFetching,
              factory: textInputCustom,
              hasError: fields.firstNameHasError,
              error: fields.firstNameErrorMsg,
            },
            lastName: {
              label: translate('AUTH_FORM.lastName'),
              placeholder: translate('AUTH_FORM.lastName'),
              config: {
                labelAnimation: true,
              },
              autoCapitalize: 'words',
              autoCorrect: false,
              factory: textInputCustom,
              editable: !isFetching,
              hasError: fields.lastNameHasError,
              error: fields.lastNameErrorMsg,
            },
            email,
            password,
          },
        };
        break;

      /**
       * ### Login
       * The login form has only 2 fields
       */
      case LOGIN:
        authForm = t.struct({
          email: t.String,
          password: t.String,
        });

        options = { fields: { email, password } };
        break;

      /**
       * ### Reset password
       * The password reset form has only 1 field
       */
      case FORGOT_PASSWORD:
        authForm = t.struct({ email: t.String });
        options = { fields: { email } };
        break;
      default: {
        authForm = t.struct({});
        options = {};
      }
    }

    /**
     * ### Return
     * returns the Form component with the correct structures
     */
    options.stylesheet = formstyles;

    return (
      <Form
        ref={form}
        type={authForm}
        options={options}
        value={value}
        onChange={onChange}
      />
    );
  }
}

/**
 * ## authForm class
 *
 * * form: the properties to set into the UI form
 * * value: the values to set in the input fields
 * * onChange: function to call when user enters text
 * * onPress: function to call when user submits the form
 */
AuthForm.propTypes = {
  formType: PropTypes.string,
  isFetching: PropTypes.bool,
  fields: PropTypes.object,
  value: PropTypes.object,
  onChange: PropTypes.func,
};
