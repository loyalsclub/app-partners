import { Platform, Dimensions } from 'react-native';
import themeStyles from '../../../styles/theme.style';

const LABEL_COLOR = '#000000';
const INPUT_COLOR = '#000000';
const ERROR_COLOR = '#a94442';
const HELP_COLOR = '#999999';
const BORDER_COLOR = 'transparent';
const DISABLED_COLOR = '#777777';
const DISABLED_BACKGROUND_COLOR = '#eeeeee';
const FONT_SIZE = 14;
const FONT_SIZE_SMALL = 11;
const FONT_WEIGHT = '300';
const FULL_WIDTH = Dimensions.get('window').width;

const stylesheet = Object.freeze({
  fieldset: {},
  // the style applied to the container of all inputs
  formGroup: {
    normal: {
      marginBottom: 30,
      borderWidth: 0,
      flexDirection: 'column',
    },
    error: {
      marginBottom: 30,
    },
  },
  controlLabel: {
    normal: {
      color: themeStyles.BLUE_WOOD,
      fontFamily: themeStyles.MAIN_FONT_LIGHT,
      fontSize: FONT_SIZE,
      marginBottom: 7,
    },
    // the style applied when a validation error occours
    error: {
      color: themeStyles.BLUE_WOOD,
      fontFamily: themeStyles.MAIN_FONT_LIGHT,
      fontSize: FONT_SIZE,
      marginBottom: 7,
    },
  },
  helpBlock: {
    normal: {
      color: HELP_COLOR,
      fontSize: FONT_SIZE,
      marginBottom: 2,
    },
    // the style applied when a validation error occours
    error: {
      color: HELP_COLOR,
      fontSize: FONT_SIZE,
      marginBottom: 2,
    },
  },
  errorBlock: {
    fontSize: FONT_SIZE,
    marginBottom: 2,
    color: themeStyles.DANGER,
  },
  textboxView: {
    normal: {},
    error: {},
    notEditable: {},
  },
  textbox: {
    normal: {
      width: FULL_WIDTH * 0.8,
      flexDirection: 'row',
      color: themeStyles.BLUE_WOOD,
      fontFamily: themeStyles.MAIN_FONT_MEDIUM,
      fontSize: themeStyles.INPUT_FONT_SIZE,
      height: 36,
      paddingVertical: Platform.OS === 'ios' ? 7 : 0,
      paddingHorizontal: 7,
      borderColor: 'transparent',
      borderBottomColor: themeStyles.GREY_OP,
      borderWidth: 1,
      marginBottom: 5,
    },
    // the style applied when a validation error occours
    error: {
      width: FULL_WIDTH * 0.8,
      flexDirection: 'row',
      color: themeStyles.BLUE_WOOD,
      fontFamily: themeStyles.MAIN_FONT_MEDIUM,
      fontSize: themeStyles.INPUT_FONT_SIZE,
      height: 36,
      paddingVertical: Platform.OS === 'ios' ? 7 : 0,
      paddingHorizontal: 7,
      borderColor: 'transparent',
      borderBottomColor: themeStyles.DANGER,
      borderWidth: 1,
      marginBottom: 5,
    },
    // the style applied when the textbox is not editable
    notEditable: {
      fontSize: themeStyles.INPUT_FONT_SIZE,
      height: 36,
      paddingVertical: Platform.OS === 'ios' ? 7 : 0,
      paddingHorizontal: 7,
      borderRadius: 4,
      borderColor: 'transparent',
      borderWidth: 0,
      marginBottom: 5,
      color: themeStyles.LIGHT_GREY,
      backgroundColor: 'transparent',
    },
  },
  checkbox: {
    normal: {
      marginBottom: 4,
    },
    // the style applied when a validation error occours
    error: {
      marginBottom: 4,
    },
  },
  pickerContainer: {
    normal: {
      marginBottom: 4,
      borderRadius: 4,
      borderColor: BORDER_COLOR,
      borderWidth: 1,
    },
    error: {
      marginBottom: 4,
      borderRadius: 4,
      borderColor: ERROR_COLOR,
      borderWidth: 1,
    },
    open: {
      // Alter styles when select container is open
    },
  },
  select: {
    normal: Platform.select({
      android: {
        paddingLeft: 7,
        color: INPUT_COLOR,
      },
      ios: {},
    }),
    // the style applied when a validation error occours
    error: Platform.select({
      android: {
        paddingLeft: 7,
        color: ERROR_COLOR,
      },
      ios: {},
    }),
  },
  pickerTouchable: {
    normal: {
      height: 44,
      flexDirection: 'row',
      alignItems: 'center',
    },
    error: {
      height: 44,
      flexDirection: 'row',
      alignItems: 'center',
    },
    active: {
      borderBottomWidth: 1,
      borderColor: BORDER_COLOR,
    },
  },
  pickerValue: {
    normal: {
      fontSize: FONT_SIZE,
      paddingLeft: 7,
    },
    error: {
      fontSize: FONT_SIZE,
      paddingLeft: 7,
    },
  },
  datepicker: {
    normal: {},
    // the style applied when a validation error occours
    error: {
      marginBottom: 4,
    },
  },
  dateTouchable: {
    normal: {
      width: FULL_WIDTH * 0.8,
      flexDirection: 'row',
      borderColor: 'transparent',
      borderBottomColor: themeStyles.GREY_OP,
      borderWidth: 0,
      borderBottomWidth: 1,
    },
    error: {
      width: FULL_WIDTH * 0.8,
      flexDirection: 'row',
      borderColor: 'transparent',
      borderBottomColor: themeStyles.DANGER,
      marginBottom: 4, // margin necessary for label.
      borderWidth: 0,
      borderBottomWidth: 1,
    },
  },
  dateValue:
    Platform.OS !== 'ios'
      ? {
          normal: {
            color: themeStyles.BLUE_WOOD,
            fontFamily: themeStyles.MAIN_FONT_MEDIUM,
            fontSize: themeStyles.INPUT_FONT_SIZE,
            padding: 7,
            borderBottomColor: themeStyles.GREY_OP,
            borderBottomWidth: 1,
            marginBottom: 5,
            width: FULL_WIDTH * 0.8,
          },
          empty: {
            width: FULL_WIDTH * 0.8,
            color: themeStyles.GREY_OP,
            fontFamily: themeStyles.MAIN_FONT_MEDIUM,
            fontSize: themeStyles.INPUT_FONT_SIZE,
            padding: 7,
            borderBottomColor: themeStyles.GREY_OP,
            borderBottomWidth: 1,
            marginBottom: 5,
          },
          error: {
            color: themeStyles.BLUE_WOOD,
            width: FULL_WIDTH * 0.8,
            fontFamily: themeStyles.MAIN_FONT_MEDIUM,
            fontSize: themeStyles.INPUT_FONT_SIZE,
            padding: 7,
            borderBottomColor: themeStyles.DANGER,
            borderBottomWidth: 1,
            marginBottom: 5,
          },
        }
      : {
          normal: {
            color: themeStyles.BLUE_WOOD,
            fontFamily: themeStyles.MAIN_FONT_MEDIUM,
            fontSize: themeStyles.INPUT_FONT_SIZE,
            borderColor: 'transparent',
            width: FULL_WIDTH * 0.8,
            padding: 7,
          },
          empty: {
            width: FULL_WIDTH * 0.8,
            color: themeStyles.GREY_OP,
            fontFamily: themeStyles.MAIN_FONT_MEDIUM,
            fontSize: themeStyles.INPUT_FONT_SIZE,
            borderColor: 'transparent',
            padding: 7,
          },
          error: {
            color: themeStyles.BLUE_WOOD,
            fontFamily: themeStyles.MAIN_FONT_MEDIUM,
            fontSize: themeStyles.INPUT_FONT_SIZE,
            borderColor: 'transparent',
            width: FULL_WIDTH * 0.8,
            padding: 7,
          },
        },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center',
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 1,
    marginTop: 20,
    marginBottom: 10,
    alignSelf: 'center',
    justifyContent: 'center',
    width: FULL_WIDTH * 0.8,
  },
});

export default stylesheet;
