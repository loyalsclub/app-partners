/** External dependencies */
import React from 'react';

/** Styles */
import themeStyles from '../../../styles/theme.style';

const { View, Text, TextInput, Animated } = require('react-native');
const t = require('tcomb-form-native');

const { Textbox } = t.form;

/**
 * For label animation to work a config element needs to be sent as part of
 * locals.config {
 *  labelAnimation: (Bool)
 * }
 */
export default class CustomTextInput extends Textbox {
  state = {
    ...this.state,
    borderColor: themeStyles.GREY_OP,
    labelFocus: new Animated.Value(0),
  };

  getTemplate() {
    return (locals) => {
      if (locals.hidden) {
        return null;
      }

      const { stylesheet } = locals;
      const { labelFocus, borderColor } = this.state;

      /** Styles */

      let formGroupStyle = stylesheet.formGroup.normal;
      let textboxStyle = stylesheet.textbox.normal;
      const helpBlockStyle = stylesheet.helpBlock.normal;
      const errorBlockStyle = stylesheet.errorBlock;
      const { labelAnimation } = locals.config;

      const controlLabelStyle = labelAnimation
        ? {
            position: 'absolute',
            left: 0,
            top: labelFocus.interpolate({
              inputRange: [0, 1],
              outputRange: [10, -10],
            }),
            fontFamily: themeStyles.MAIN_FONT_LIGHT,
            fontSize: labelFocus.interpolate({
              inputRange: [0, 1],
              outputRange: [themeStyles.INPUT_FONT_SIZE, 14],
            }),
            color: labelFocus.interpolate({
              inputRange: [0, 1],
              outputRange: [themeStyles.GREY, themeStyles.BLUE_WOOD],
            }),
          }
        : stylesheet.controlLabel.normal;

      if (locals.hasError) {
        formGroupStyle = stylesheet.formGroup.error;
        textboxStyle = stylesheet.textbox.error;
      }

      if (locals.editable === false) {
        textboxStyle = stylesheet.textbox.notEditable;
      }

      /** Handlers */
      const onFocus = () => {
        locals.onFocus && locals.onFocus();

        Animated.timing(labelFocus, {
          toValue: 1,
          duration: 200,
          useNativeDriver: false,
        }).start();

        this.setState({
          ...this.state,
          borderColor: themeStyles.GOLDEN_OP,
        });
      };

      const onBlur = () => {
        locals.onBlur && locals.onBlur();

        if (!locals.value) {
          Animated.timing(labelFocus, {
            toValue: 0,
            duration: 200,
            useNativeDriver: false,
          }).start();
        }

        this.setState({
          ...this.state,
          borderColor: !locals.value
            ? themeStyles.GREY_OP
            : themeStyles.GOLDEN_OP,
        });
      };

      /** JSX */
      const label = locals.label ? (
        labelAnimation ? (
          <Animated.Text style={controlLabelStyle}>
            {locals.label}
          </Animated.Text>
        ) : (
          <Text style={controlLabelStyle}>{locals.label}</Text>
        )
      ) : null;

      const help = locals.help ? (
        <Text style={helpBlockStyle}>{locals.help}</Text>
      ) : null;

      const error =
        locals.hasError && locals.error ? (
          <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>
            {locals.error}
          </Text>
        ) : null;

      return (
        <View style={formGroupStyle}>
          <View>
            {label}
            <TextInput
              // accessibilityLabel={locals.label}
              ref="input"
              allowFontScaling={locals.allowFontScaling}
              underlineColorAndroid="transparent"
              placeholder={labelAnimation ? null : locals.placeholder}
              autoCapitalize={locals.autoCapitalize}
              autoCorrect={locals.autoCorrect}
              autoFocus={locals.autoFocus}
              blurOnSubmit={locals.blurOnSubmit}
              editable={locals.editable}
              keyboardType={locals.keyboardType}
              maxLength={locals.maxLength}
              multiline={locals.multiline}
              onBlur={onBlur}
              onEndEditing={locals.onEndEditing}
              onFocus={onFocus}
              onLayout={locals.onLayout}
              onSelectionChange={locals.onSelectionChange}
              onSubmitEditing={locals.onSubmitEditing}
              onContentSizeChange={locals.onContentSizeChange}
              secureTextEntry={locals.secureTextEntry}
              selectTextOnFocus={locals.selectTextOnFocus}
              selectionColor={locals.selectionColor}
              numberOfLines={locals.numberOfLines}
              clearButtonMode={locals.clearButtonMode}
              clearTextOnFocus={locals.clearTextOnFocus}
              enablesReturnKeyAutomatically={
                locals.enablesReturnKeyAutomatically
              }
              keyboardAppearance={locals.keyboardAppearance}
              onKeyPress={locals.onKeyPress}
              returnKeyType={locals.returnKeyType}
              selectionState={locals.selectionState}
              onChangeText={(value) => locals.onChange(value)}
              onChange={locals.onChangeNative}
              style={textboxStyle}
              value={locals.value}
              testID={locals.testID}
              textContentType={locals.textContentType}
            />
          </View>
          {help}
          {error}

          {/* Border line underneath input */}
          <Animated.View
            style={{ backgroundColor: borderColor, padding: 0.5 }}
          />
        </View>
      );
    };
  }
}
