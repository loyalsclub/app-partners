import React from 'react';
import PropTypes from 'prop-types';
import t from 'tcomb-form-native';
import {
  Text,
  View,
  Animated,
  DatePickerIOS,
  TouchableOpacity,
} from 'react-native';
import IconComponent from '../Miscellaneous/Icons.component';

const UIPICKER_HEIGHT = 216;

/** Example of usage
 * let birthDate = {
      mode: 'date',
      label: 'Fecha de Nacimiento',
      factory: datepicker,
      maximumDate: new Date(),
      config: {
        format: formatDate,
        locale: 'es',
        dialogMode: 'spinner',
        defaultValueText: 'Ingresá el Nacimiento',
      }
    };
 */
class CollapsibleDatePickerIOS extends React.Component {
  constructor(props) {
    super(props);
    this._onDateChange = this.onDateChange.bind(this);
    this._onPress = this.onPress.bind(this);
    this.state = {
      isCollapsed: true,
      height: new Animated.Value(0),
    };
  }

  onDateChange(value) {
    this.props.locals.onChange(value);
  }

  onPress() {
    const { locals } = this.props;
    let animation = Animated.timing;
    let animationConfig = {
      duration: 200,
      useNativeDriver: false,
    };
    if (locals.config) {
      if (locals.config.animation) {
        animation = locals.config.animation;
      }
      if (locals.config.animationConfig) {
        animationConfig = locals.config.animationConfig;
      }
    }
    animation(this.state.height, {
      toValue: this.state.isCollapsed ? UIPICKER_HEIGHT : 0,
      ...animationConfig,
    }).start();
    this.setState({ isCollapsed: !this.state.isCollapsed });
    if (typeof locals.onPress === 'function') {
      locals.onPress();
    }
  }

  render() {
    const { locals } = this.props;
    const { stylesheet } = locals;
    let touchableStyle = stylesheet.dateTouchable.normal;
    let datepickerStyle = stylesheet.datepicker.normal;
    let dateValueStyle = stylesheet.dateValue.normal;

    if (locals.hasError) {
      touchableStyle = stylesheet.dateTouchable.error;
      datepickerStyle = stylesheet.datepicker.error;
      dateValueStyle = stylesheet.dateValue.error;
    }

    /** Custom */
    if (stylesheet.dateValue.empty && !locals.value) {
      dateValueStyle = stylesheet.dateValue.empty;
    }

    if (locals.disabled) {
      touchableStyle = stylesheet.dateTouchable.notEditable;
    }

    let formattedValue = locals.value ? String(locals.value) : '';
    if (locals.config) {
      if (locals.config.format && formattedValue) {
        formattedValue = locals.config.format(locals.value);
      } else if (!formattedValue) {
        formattedValue = locals.config.defaultValueText
          ? locals.config.defaultValueText
          : 'Tap here to select a date';
      }
    }
    const height = this.state.isCollapsed ? 0 : UIPICKER_HEIGHT;

    return (
      <View>
        <TouchableOpacity
          style={[touchableStyle, { position: 'relative' }]}
          disabled={locals.disabled}
          onPress={this._onPress}
        >
          <Text style={dateValueStyle}>{formattedValue}</Text>
          <IconComponent
            style={[
              {
                fontSize: dateValueStyle.fontSize,
                color: dateValueStyle.color,
                position: 'absolute',
                right: 0,
                bottom: dateValueStyle.padding,
              },
            ]}
            size={dateValueStyle.fontSize}
            name={this.state.isCollapsed ? 'chevron-down' : 'chevron-up'}
          />
        </TouchableOpacity>
        <Animated.View
          style={{ height: this.state.height, overflow: 'hidden' }}
        >
          <DatePickerIOS
            ref="input"
            accessibilityLabel={locals.label}
            date={locals.value || new Date()}
            initialDate={new Date()}
            maximumDate={locals.maximumDate}
            locale={locals.config.locale}
            minimumDate={locals.minimumDate}
            minuteInterval={locals.minuteInterval}
            mode={locals.mode}
            onDateChange={this._onDateChange}
            timeZoneOffsetInMinutes={locals.timeZoneOffsetInMinutes}
            style={[datepickerStyle, { height }]}
          />
        </Animated.View>
      </View>
    );
  }
}

CollapsibleDatePickerIOS.propTypes = {
  locals: PropTypes.object.isRequired,
};

export default class CustomDatePicker extends t.form.DatePicker {
  constructor(props) {
    super(props);
  }

  getTemplate() {
    return (locals) => {
      if (locals.hidden) {
        return null;
      }

      const { stylesheet } = locals;
      let formGroupStyle = stylesheet.formGroup.normal;
      let controlLabelStyle = stylesheet.controlLabel.normal;
      let helpBlockStyle = stylesheet.helpBlock.normal;
      const errorBlockStyle = stylesheet.errorBlock;

      if (locals.hasError) {
        formGroupStyle = stylesheet.formGroup.error;
        controlLabelStyle = stylesheet.controlLabel.error;
        helpBlockStyle = stylesheet.helpBlock.error;
      }

      const label = locals.label ? (
        <Text style={controlLabelStyle}>{locals.label}</Text>
      ) : null;
      const help = locals.help ? (
        <Text style={helpBlockStyle}>{locals.help}</Text>
      ) : null;
      const error =
        locals.hasError && locals.error ? (
          <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>
            {locals.error}
          </Text>
        ) : null;

      return (
        <View style={formGroupStyle}>
          {label}
          <CollapsibleDatePickerIOS locals={locals} />
          {help}
          {error}
        </View>
      );
    };
  }
}
