import React from 'react';
import Svg, { Path, G } from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const FourIcon = ({ pathColor }) => (
  <Svg
    version="1.1"
    id="four-icon"
    x="0px"
    y="0px"
    viewBox="0 0 13.92 19.09"
    style={{
      enableBackground: 'new 0 0 13.92 19.09',
    }}
    xmlSpace="preserve"
    width={20}
    height={20}
  >
    <G>
      <G>
        <Path
          fill={pathColor || themeStyle.BLUE_WOOD}
          d="M5.29,0h4.14L4.14,11.75h5.85V4.7h3.94v14.39H9.99v-3.67H0v-3.67L5.29,0z"
        />
      </G>
    </G>
  </Svg>
);

export default FourIcon;
