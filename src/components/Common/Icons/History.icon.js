import React from 'react';
import Svg, { Path, G, Stop, LinearGradient } from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const HistoryIcon = ({ pathColor, width, height, style }) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 20.79 20.76"
    width={width || 40}
    height={height || 40}
    style={[style]}
  >
    <LinearGradient
      id="Degradado_sin_nombre_11"
      x1="7.79"
      y1="9.73"
      x2="14.3"
      y2="9.73"
      gradientUnits="userSpaceOnUse"
    >
      <Stop offset="0" stopColor="#f9cf27" />
      <Stop offset="0.12" stopColor="#f8cc25" stopOpacity="0.97" />
      <Stop offset="0.3" stopColor="#f4c320" stopOpacity="0.9" />
      <Stop offset="0.52" stopColor="#eeb618" stopOpacity="0.77" />
      <Stop offset="0.76" stopColor="#e6a30d" stopOpacity="0.6" />
      <Stop offset="1" stopColor="#dd8d00" stopOpacity="0.4" />
    </LinearGradient>

    <G id="Layer_2" data-name="Layer 2">
      <G id="Content">
        <Path
          className="cls-1"
          style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
          d="M9.59,20.76a11.75,11.75,0,0,1-1.79-.31A10.2,10.2,0,0,1,3.37,18,10.3,10.3,0,0,1,0,9.56,10.46,10.46,0,0,1,1.21,5.51a10.2,10.2,0,0,1,2-2.65A10.3,10.3,0,0,1,4.78,1.63,10.1,10.1,0,0,1,9.13.06L9.59,0h1.62A12.77,12.77,0,0,1,13,.33a10.61,10.61,0,0,1,4.61,2.55c.11.1.16.06.24,0L19.81.93a1.46,1.46,0,0,1,.34-.26.39.39,0,0,1,.6.27,1.14,1.14,0,0,1,0,.37V7.1a.65.65,0,0,1-.7.7H14.23c-.24,0-.49,0-.6-.28s.07-.46.23-.63l1.37-1.38c.18-.18.36-.37.55-.54s.13-.18,0-.29a10.83,10.83,0,0,0-1-.75A8.16,8.16,0,0,0,12.92,3a7.47,7.47,0,0,0-2.81-.42A7.66,7.66,0,0,0,4.49,5.35a7.48,7.48,0,0,0-1.79,4A7.56,7.56,0,0,0,4.24,15.1a7.62,7.62,0,0,0,5.18,3,7.53,7.53,0,0,0,3.37-.31,6.69,6.69,0,0,0,2.07-1,6.84,6.84,0,0,0,1.25-1.09,7.5,7.5,0,0,0,1.39-2.14c.11-.31.25-.6.35-.91a1.3,1.3,0,0,1,1.39-.94,1.33,1.33,0,0,1,1.17,1.13,1.19,1.19,0,0,1-.06.59,10.28,10.28,0,0,1-4.67,6,10.69,10.69,0,0,1-1.6.77,9.91,9.91,0,0,1-2.73.61l-.14,0Z"
          transform="translate(0)"
        />

        <Path
          className="cls-2"
          fill="url(#Degradado_sin_nombre_11)"
          d="M11,13h-2a1.29,1.29,0,0,1-1.28-1.28v-4a1.31,1.31,0,0,1,2.62,0c0,.79,0,1.58,0,2.37,0,.19.06.24.24.24H13A1.31,1.31,0,1,1,13,13Z"
          transform="translate(0)"
        />
      </G>
    </G>
  </Svg>
);

export default HistoryIcon;
