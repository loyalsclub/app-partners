import React from 'react';
import Svg, {
  Path,
  G,
  Stop,
  LinearGradient,
  Polygon,
  Rect,
} from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const InviteQRIcon = ({ pathColor, width, height }) => (
  <Svg
    id="invite-qr-icon"
    x="0px"
    y="0px"
    viewBox="0 0 35.96 26.71"
    width={width || 40}
    height={height || 40}
    style={{
      enableBackground: 'new 0 0 26.41 26.08',
    }}
    xmlSpace="preserve"
  >
    <G>
      <G>
        <G>
          <Path
            style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
            d="M6.8,7.25V0h7.35v7.25H6.8z M12.47,5.6V1.65H8.47V5.6H12.47z"
          />

          <Path
            style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
            d="M6.8,26.28v-7.25h7.35v7.25H6.8z M12.47,24.63v-3.95H8.47v3.95H12.47z"
          />

          <Path
            style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
            d="M26.1,7.25V0h7.35v7.25H26.1z M31.78,5.6V1.65h-4.01V5.6H31.78z"
          />

          <Polygon
            style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
            points="26.1,18.44 26.1,16.79 31.78,16.79 31.78,10.08 33.45,10.08 33.45,18.44 			"
          />

          <Polygon
            style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
            points="31.78,26.28 31.78,22.92 27.77,22.92 27.77,26.28 26.1,26.28 26.1,21.27 33.45,21.27 33.45,26.28 
							"
          />

          <Polygon
            style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
            points="21.56,7.25 21.56,1.65 17.02,1.65 17.02,0 23.23,0 23.23,7.25 			"
          />

          <Polygon
            style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
            points="21.7,26.29 21.6,20.7 17.06,20.77 16.96,14.65 21.5,14.57 21.42,10.09 27.63,9.99 27.7,13.87 
				26.03,13.9 25.99,11.66 23.12,11.71 23.2,16.19 18.66,16.27 18.7,19.1 23.25,19.02 23.37,26.26 			"
          />

          <Rect
            x="17.02"
            y="23.51"
            style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
            width="1.67"
            height="2.77"
          />

          <Rect
            x="11.34"
            y="14.56"
            style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
            width="2.81"
            height="1.65"
          />

          <G>
            <Polygon
              style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
              points="17.02,3.9 17.02,9.5 7.12,9.5 7.12,11.4 8.47,11.4 8.47,11.15 18.69,11.15 18.69,3.9 				"
            />

            <Rect
              x="7.12"
              y="15.4"
              style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
              width="1.35"
              height="1.95"
            />
          </G>
        </G>
      </G>

      <G>
        <G>
          <G>
            <LinearGradient
              id="invite-qr-lg-1"
              gradientUnits="userSpaceOnUse"
              x1="0"
              y1="13.4038"
              x2="10.0065"
              y2="13.4038"
            >
              <Stop offset="0" stopColor="#F9CF2B" />
              <Stop offset="0.1873" stopColor="#F6C92B" stopOpacity={0.8876} />
              <Stop offset="0.4686" stopColor="#EEB829" stopOpacity={0.7188} />
              <Stop offset="0.8074" stopColor="#E39E27" stopOpacity={0.5156} />
              <Stop offset="1" stopColor="#DB8E27" stopOpacity={0.4} />
            </LinearGradient>

            <Path
              fill="url(#invite-qr-lg-1)"
              d="M10.01,14.25H5.94v3.82H4.08v-3.82H0v-1.69h4.08V8.74h1.86v3.82h4.06V14.25z"
            />
          </G>
        </G>
      </G>
    </G>
  </Svg>
);

export default InviteQRIcon;
