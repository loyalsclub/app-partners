import React from 'react';
import Svg, { Path, G, Stop, LinearGradient } from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const AliasIcon = ({ pathColor, width, height, style }) => (
  <Svg
    id="alias-icon"
    x="0px"
    y="0px"
    width={width || 40}
    height={height || 40}
    viewBox="0 0 39.4 33.31"
    style={[
      {
        enableBackground: 'new 0 0 39.4 33.31',
      },
      style,
    ]}
    xmlSpace="preserve"
  >
    <G>
      <G>
        <Path
          style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
          d="M4.4,0h1.98l4.4,12.13H8.81L7.5,8.51H3.28l-1.31,3.62H0L4.4,0z M3.94,6.72h2.89L5.39,2.72L3.94,6.72z"
        />
      </G>

      <G>
        <LinearGradient
          id="alias-lg-1"
          gradientUnits="userSpaceOnUse"
          x1="18.4226"
          y1="11.6082"
          x2="22.8439"
          y2="11.6082"
        >
          <Stop offset="0" stopColor="#F9CF2B" />
          <Stop offset="0.1873" stopColor="#F6C92B" stopOpacity={0.8876} />
          <Stop offset="0.4686" stopColor="#EEB829" stopOpacity={0.7188} />
          <Stop offset="0.8074" stopColor="#E39E27" stopOpacity={0.5156} />
          <Stop offset="1" stopColor="#DB8E27" stopOpacity={0.4} />
        </LinearGradient>

        <Path
          fill="url(#alias-lg-1)"
          d="M18.42,5.55h4.42v12.13H20.9V7.34h-2.48V5.55z"
        />
      </G>

      <G>
        <Path
          style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
          d="M9.84,31.52h5v1.79H6.88v-1.79c3.75-3.15,6.03-4.59,6.03-6.59c0-1.14-0.63-2.2-2.43-2.2
			c-1.16,0-2.39,0.56-3.1,1.1l-0.67-1.64c0.86-0.71,2.18-1.25,3.77-1.25c2.28,0,4.37,1.34,4.37,3.99
			C14.84,27.51,12.53,29.25,9.84,31.52z"
        />
      </G>

      <G>
        <Path
          style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
          d="M34.23,20.28h0.71c1.85,0,2.33-0.75,2.33-1.7c0-1.04-0.56-1.7-2.2-1.7c-1.06,0-2.11,0.49-2.8,1.04l-0.71-1.64
			c0.9-0.67,1.96-1.19,3.51-1.19c2.65,0,4.14,1.34,4.14,3.49c0,1.21-0.67,2.11-1.59,2.59c0.97,0.5,1.77,1.46,1.77,2.74
			c0,2.43-1.57,3.79-4.33,3.79c-1.62,0-2.76-0.52-3.71-1.29l0.71-1.64c0.71,0.65,1.88,1.14,3,1.14c1.77,0,2.39-0.62,2.39-2
			c0-1.04-0.69-1.98-2.52-1.98h-0.71V20.28z"
        />
      </G>
    </G>
  </Svg>
);

export default AliasIcon;
