import React from 'react';
import Svg, { Path, G } from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const ChevronRightIcon = ({ pathColor }) => (
  <Svg
    version="1.1"
    id="chevron-right-icon"
    x="0px"
    y="0px"
    width={15}
    height={15}
    viewBox="0 0 8.61 15.12"
    style={{
      enableBackground: 'new 0 0 8.61 15.12',
    }}
    xmlSpace="preserve"
  >
    <G>
      <G>
        <Path
          style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
          d="M1.81,14.81c2.17-2.17,4.33-4.33,6.5-6.5c0.4-0.4,0.41-1.09,0-1.5c-2.17-2.17-4.33-4.33-6.5-6.5
			c-0.97-0.97-2.47,0.53-1.5,1.5c2.17,2.17,4.33,4.33,6.5,6.5c0-0.5,0-1,0-1.5c-2.17,2.17-4.33,4.33-6.5,6.5
			C-0.65,14.28,0.85,15.78,1.81,14.81L1.81,14.81z"
        />
      </G>
    </G>
  </Svg>
);

export default ChevronRightIcon;
