import React from 'react';
import Svg, { Path, G, Stop, LinearGradient } from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const WhatsAppInstagramIcon = ({ pathColor, width, height, style }) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 41 37.98"
    width={width || 40}
    height={height || 40}
    style={style}
  >
    <LinearGradient
      id="Degradado_sin_nombre_11"
      x1="24.82"
      y1="29.9"
      x2="41"
      y2="29.9"
      gradientUnits="userSpaceOnUse"
    >
      <Stop offset="0" stopColor="#f9cf27" />
      <Stop offset="0.12" stopColor="#f8cc25" stopOpacity="0.97" />
      <Stop offset="0.3" stopColor="#f4c320" stopOpacity="0.9" />
      <Stop offset="0.52" stopColor="#eeb618" stopOpacity="0.77" />
      <Stop offset="0.76" stopColor="#e6a30d" stopOpacity="0.6" />
      <Stop offset="1" stopColor="#dd8d00" stopOpacity="0.4" />
    </LinearGradient>

    <LinearGradient
      id="Degradado_sin_nombre_12"
      x1="28.73"
      y1="29.9"
      x2="37.09"
      y2="29.9"
    >
      <Stop offset="0" stopColor="#f9cf27" />
      <Stop offset="0.12" stopColor="#f8cc25" stopOpacity="0.97" />
      <Stop offset="0.3" stopColor="#f4c320" stopOpacity="0.9" />
      <Stop offset="0.52" stopColor="#eeb618" stopOpacity="0.77" />
      <Stop offset="0.76" stopColor="#e6a30d" stopOpacity="0.6" />
      <Stop offset="1" stopColor="#dd8d00" stopOpacity="0.4" />
    </LinearGradient>

    <LinearGradient
      id="Degradado_sin_nombre_13"
      x1="36.19"
      y1="25.57"
      x2="38.32"
      y2="25.57"
    >
      <Stop offset="0" stopColor="#f9cf27" />
      <Stop offset="0.12" stopColor="#f8cc25" stopOpacity="0.97" />
      <Stop offset="0.3" stopColor="#f4c320" stopOpacity="0.9" />
      <Stop offset="0.52" stopColor="#eeb618" stopOpacity="0.77" />
      <Stop offset="0.76" stopColor="#e6a30d" stopOpacity="0.6" />
      <Stop offset="1" stopColor="#dd8d00" stopOpacity="0.4" />
    </LinearGradient>

    <G id="Layer_2" data-name="Layer 2">
      <G id="Content">
        <Path
          className="cls-1"
          style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
          d="M1,23c.33-1.54.67-3,.93-4.4a1.7,1.7,0,0,0-.18-1.08C-.64,13.23-.64,9,2.11,5A10.74,10.74,0,0,1,13.38.19a10.89,10.89,0,0,1,9.28,8.64A11.42,11.42,0,0,1,10.84,22.9a12.27,12.27,0,0,1-4.52-1.19,1.55,1.55,0,0,0-1-.07C3.89,22,2.5,22.49,1,23Zm1.84-2c1.07-.34,2-.66,2.94-.91a1.55,1.55,0,0,1,1,.1,10.13,10.13,0,0,0,6.05,1.08,9.85,9.85,0,1,0-9.65-4.48,1.62,1.62,0,0,1,.25,1.43C3.2,19,3.05,19.93,2.83,21Z"
        />

        <Path
          className="cls-1"
          style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
          d="M14.39,17.16a7.28,7.28,0,0,1-5.47-2.62,16.47,16.47,0,0,1-2.19-3A5.17,5.17,0,0,1,6.18,8,2.66,2.66,0,0,1,8.11,5.84.76.76,0,0,1,9,6.39,11.62,11.62,0,0,0,9.63,8a1.24,1.24,0,0,1-.37,1.69.69.69,0,0,0-.14,1.13q1.43,1.63,2.92,3.2a.63.63,0,0,0,1,0,1.4,1.4,0,0,1,1.81-.27c.43.23.91.37,1.34.59a1.07,1.07,0,0,1,.37,1.81A2.46,2.46,0,0,1,14.39,17.16Z"
        />

        <Path
          className="cls-2"
          fill="url(#Degradado_sin_nombre_11)"
          d="M32.9,38H29.34A4.48,4.48,0,0,1,25,34.73a4.57,4.57,0,0,1-.17-1.27q0-3.55,0-7.11a4.42,4.42,0,0,1,1-2.92A4.34,4.34,0,0,1,28.05,22a4.58,4.58,0,0,1,1.33-.17c2.36,0,4.73,0,7.1,0a4.35,4.35,0,0,1,2.89,1,4.42,4.42,0,0,1,1.47,2.24A4.84,4.84,0,0,1,41,26.33c0,2.38,0,4.76,0,7.13A4.43,4.43,0,0,1,36.46,38Zm6.63-8.07h0V26.24a2.88,2.88,0,0,0-.85-2,2.82,2.82,0,0,0-2.07-.91c-2.46,0-4.91,0-7.37,0A3,3,0,0,0,26.3,26.2c0,2.46,0,4.91,0,7.37a3,3,0,0,0,2.92,2.94q3.69,0,7.36,0a3,3,0,0,0,2.94-2.92C39.54,32.36,39.53,31.13,39.53,29.91Z"
        />

        <Path
          className="cls-3"
          fill="url(#Degradado_sin_nombre_12)"
          d="M28.73,29.9a4.18,4.18,0,1,1,4.18,4.18A4.18,4.18,0,0,1,28.73,29.9Zm6.88,0a2.7,2.7,0,1,0-2.71,2.7A2.67,2.67,0,0,0,35.61,29.9Z"
        />

        <Path
          className="cls-4"
          fill="url(#Degradado_sin_nombre_13)"
          d="M36.19,25.56a1.06,1.06,0,0,1,1.06-1.06,1.07,1.07,0,1,1,0,2.14A1.07,1.07,0,0,1,36.19,25.56Z"
        />
      </G>
    </G>
  </Svg>
);

export default WhatsAppInstagramIcon;
