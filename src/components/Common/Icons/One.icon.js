import React from 'react';
import Svg, { Path, G } from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const OneIcon = ({ pathColor }) => (
  <Svg
    version="1.1"
    id="one-icon"
    x="0px"
    y="0px"
    viewBox="0 0 7.87 19.09"
    style={{
      enableBackground: 'new 0 0 7.87 19.09',
    }}
    xmlSpace="preserve"
    width={20}
    height={20}
  >
    <G>
      <G>
        <Path
          fill={pathColor || themeStyle.BLUE_WOOD}
          d="M0,0h7.87v19.09H3.94V3.67H0V0z"
        />
      </G>
    </G>
  </Svg>
);

export default OneIcon;
