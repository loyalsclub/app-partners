import React from 'react';
import Svg, { Path, G } from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const LinkIcon = ({ pathColor }) => (
  <Svg
    version="1.1"
    id="link-icon"
    x="0px"
    y="0px"
    width={30}
    height={30}
    viewBox="0 0 22.72 22.74"
    style={{
      enableBackground: 'new 0 0 22.72 22.74',
    }}
    xmlSpace="preserve"
  >
    <G>
      <G>
        <Path
          style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
          d="M4.75,14.55c2.6,2.59,6.8,2.59,9.4,0l1.97-1.97l-2.13-2.13l-1.96,1.97c-1.42,1.42-3.72,1.42-5.15,0l-2.8-2.8
			c-1.42-1.42-1.42-3.72,0-5.14c0,0,0,0,0,0l0.39-0.39c1.42-1.42,3.72-1.42,5.14,0c0,0,0,0,0,0l0.35,0.35l1.04-1.04l1.15-1
			l-0.42-0.45c-2.6-2.59-6.8-2.59-9.4,0l-0.4,0.4c-2.59,2.6-2.59,6.8,0,9.4L4.75,14.55z"
        />
        <Path
          style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
          d="M6.37,10.4l2.13,2.13l2.2-2.2c1.42-1.42,3.72-1.42,5.14,0c0,0,0,0,0,0l2.8,2.8c1.42,1.42,1.42,3.72,0,5.14
			c0,0,0,0,0,0l-0.39,0.39c-1.42,1.42-3.72,1.42-5.14,0c0,0,0,0,0,0l-0.35-0.35l-2.12,2.13l0.35,0.35c2.6,2.59,6.8,2.59,9.4,0
			l0.39-0.39c2.59-2.6,2.59-6.8,0-9.4l-2.8-2.81c-2.6-2.59-6.8-2.59-9.4,0L6.37,10.4z"
        />
      </G>
    </G>
  </Svg>
);

export default LinkIcon;
