import React from 'react';
import Svg, { Circle, Path, G } from 'react-native-svg';

const HelpIcon = ({ circleColor, pathColor }) => (
  <Svg viewBox="0 0 50 50" height={30} width={30}>
    <Circle
      cx="25"
      cy="25"
      r="24.5"
      style={{ fill: circleColor || 'rgba(204,204,204,0.8)' }}
    />
    <G>
      <Path
        style={{ fill: pathColor || 'rgba(39,50,72,0.4)' }}
        d="M28.6,26.8c-1.7,0.7-2.5,1.6-2.5,2.7h-5.2c0-3.1,2.2-5.3,4.6-6.4c2.2-1.2,4.9-1.8,4.9-4.4
		c0-2.1-1.6-3.2-4.8-3.2c-2,0-4.2,0.6-5.2,1.1l-1.9-4.5c1.4-0.7,3.9-1.5,7.1-1.5c5.8,0,10,2.7,10,8C35.6,23.7,31.2,25.6,28.6,26.8z
		 M23.5,31.2c1.7,0,3,1.4,3,3c0,1.7-1.4,3-3,3c-1.7,0-3-1.4-3-3C20.5,32.5,21.8,31.2,23.5,31.2z"
      />
    </G>
  </Svg>
);

export default HelpIcon;
