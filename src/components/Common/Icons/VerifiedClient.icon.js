import React from 'react';
import Svg, { Path, G, Stop, Polygon, LinearGradient } from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const VerifiedClientIcon = ({ pathColor, width, height }) => (
  <Svg
    id="verified-client-icon"
    x="0px"
    y="0px"
    width={width || 40}
    height={height || 40}
    viewBox="0 0 42.41 32.46"
    style={{
      enableBackground: 'new 0 0 42.41 32.46',
    }}
    xmlSpace="preserve"
  >
    <G>
      <Path
        style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
        d="M13.52,32.46c6.39,0,13.52-2.14,13.52-5.2c0-3.26-3.48-7.16-8.18-8.98c2.75-1.77,4.59-4.85,4.59-8.36
		c0-5.47-4.45-9.93-9.93-9.93S3.59,4.45,3.59,9.93c0,3.51,1.83,6.59,4.59,8.35C3.48,20.1,0,24,0,27.26
		C0,30.32,7.12,32.46,13.52,32.46z M6.19,9.93c0-4.04,3.29-7.33,7.33-7.33s7.33,3.29,7.33,7.33s-3.29,7.33-7.33,7.33
		S6.19,13.97,6.19,9.93z M13.52,19.85c6.09,0,10.76,4.77,10.92,7.28c-0.62,0.85-4.94,2.73-10.92,2.73c-5.97,0-10.29-1.89-10.92-2.73
		C2.76,24.62,7.42,19.85,13.52,19.85z"
      />
    </G>

    <G>
      <LinearGradient
        id="verified-client-lg-1"
        gradientUnits="userSpaceOnUse"
        x1="30.5327"
        y1="14.2714"
        x2="39.1548"
        y2="14.2714"
      >
        <Stop offset="0" stopColor="#F9CF2B" />
        <Stop offset="0.1873" stopColor="#F6C92B" stopOpacity={0.8876} />
        <Stop offset="0.4686" stopColor="#EEB829" stopOpacity={0.7188} />
        <Stop offset="0.8074" stopColor="#E39E27" stopOpacity={0.5156} />
        <Stop offset="1" stopColor="#DB8E27" stopOpacity={0.4} />
      </LinearGradient>

      <Polygon
        fill="url(#verified-client-lg-1)"
        points="33.78,14.86 31.91,13.01 30.53,14.42 33.78,17.66 39.15,12.28 37.75,10.88 	"
      />

      <LinearGradient
        id="verified-client-lg-2"
        gradientUnits="userSpaceOnUse"
        x1="27.1363"
        y1="14.2309"
        x2="42.4069"
        y2="14.2309"
      >
        <Stop offset="0" stopColor="#F9CF2B" />
        <Stop offset="0.1873" stopColor="#F6C92B" stopOpacity={0.8876} />
        <Stop offset="0.4686" stopColor="#EEB829" stopOpacity={0.7188} />
        <Stop offset="0.8074" stopColor="#E39E27" stopOpacity={0.5156} />
        <Stop offset="1" stopColor="#DB8E27" stopOpacity={0.4} />
      </LinearGradient>

      <Path
        fill="url(#verified-client-lg-2)"
        d="M34.77,6.6c-4.21,0-7.64,3.42-7.64,7.63c0,4.21,3.42,7.64,7.64,7.64c2.05,0,3.98-0.8,5.42-2.26
		c1.44-1.44,2.22-3.36,2.21-5.38C42.41,10.02,38.98,6.6,34.77,6.6z M40.52,14.23c0,3.17-2.58,5.75-5.75,5.75s-5.75-2.58-5.75-5.75
		c0-3.17,2.58-5.75,5.75-5.75S40.52,11.06,40.52,14.23z"
      />
    </G>
  </Svg>
);

export default VerifiedClientIcon;
