import React from 'react';
import Svg, { Path, G } from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const TwoIcon = ({ pathColor }) => (
  <Svg
    version="1.1"
    id="two-icon"
    x="0px"
    y="0px"
    viewBox="0 0 13.69 19.56"
    style={{
      enableBackground: 'new 0 0 13.69 19.56',
    }}
    xmlSpace="preserve"
    width={20}
    height={20}
  >
    <G>
      <G>
        <Path
          fill={pathColor || themeStyle.BLUE_WOOD}
          d="M6.43,15.89h7.26v3.67H0.29v-3.67c6.37-4.96,9.46-6.52,9.46-9.14c0-1.62-0.79-3.08-3.55-3.08
			c-1.64,0-3.76,0.79-4.85,1.64L0,1.97C1.41,0.82,3.61,0,6.2,0c3.91,0,7.49,2.26,7.49,6.76C13.69,10.34,10.93,12.51,6.43,15.89z"
        />
      </G>
    </G>
  </Svg>
);

export default TwoIcon;
