import React from 'react';
import Svg, { Path, G } from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const ThreeIcon = ({ pathColor }) => (
  <Svg
    version="1.1"
    id="three-icon"
    x="0px"
    y="0px"
    viewBox="0 0 13.39 20.03"
    style={{
      enableBackground: 'new 0 0 13.39 20.03',
    }}
    xmlSpace="preserve"
    width={20}
    height={20}
  >
    <G>
      <G>
        <Path
          fill={pathColor || themeStyle.BLUE_WOOD}
          d="M4.67,7.87h1.12c2.73,0,3.35-0.94,3.35-2.14c0-1.29-0.73-2.06-2.94-2.06c-1.67,0-3.38,0.79-4.46,1.59
			L0.29,1.88C1.82,0.82,3.61,0,6.2,0c4.23,0,6.9,2.14,6.9,5.73c0,1.64-0.97,3.11-2.32,3.94c1.5,0.85,2.61,2.41,2.61,4.17
			c0,3.94-2.85,6.2-7.2,6.2c-2.7,0-4.58-0.82-6.2-2.06l1.44-3.38c1.15,0.97,2.97,1.76,4.76,1.76c2.41,0,3.23-0.79,3.23-2.53
			c0-1.41-0.91-2.58-3.64-2.58H4.67V7.87z"
        />
      </G>
    </G>
  </Svg>
);

export default ThreeIcon;
