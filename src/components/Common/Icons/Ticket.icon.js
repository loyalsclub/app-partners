import React from 'react';
import Svg, { Path, G, Stop, LinearGradient } from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const TicketIcon = ({ pathColor, width, height }) => (
  <Svg
    version="1.1"
    id="ticket-icon"
    x="0px"
    y="0px"
    viewBox="0 0 35.96 26.71"
    width={width || 40}
    height={height || 40}
    style={{
      enableBackground: 'new 0 0 35.96 26.71',
    }}
    xmlSpace="preserve"
  >
    <G>
      <G>
        <G>
          <Path
            style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
            className="st0"
            d="M30.96,0l1.8,6.56c-1.04,0.29-1.96,0.46-2.56,1.5c-0.7,1.24-0.25,2.88,0.98,3.58
				c1.05,0.6,1.92,0.27,2.97-0.01l0.2,0.75l1.6,5.82L5,26.71l-1.8-6.56c1.04-0.29,1.89-0.44,2.56-1.5c0.7-1.24,0.25-2.88-0.98-3.58
				c-1.12-0.57-1.92-0.27-2.97,0.01L0,8.5L30.96,0z M9.51,7.5l4.18,15.22l20.37-5.59l-0.98-3.58c-0.94,0.1-1.88-0.13-2.64-0.56
				c-2-1.14-2.69-3.67-1.56-5.67c0.43-0.76,1.13-1.43,2.01-1.75l-1-3.66L9.51,7.5z M12.2,23.12L8.02,7.91L1.9,9.59l1,3.66
				c0.92-0.17,1.86,0.05,2.62,0.48c2,1.14,2.69,3.67,1.56,5.67c-0.43,0.76-1.13,1.43-1.98,1.83l0.98,3.58L12.2,23.12z"
          />
        </G>
      </G>

      <LinearGradient
        id="ticket-linear"
        gradientUnits="userSpaceOnUse"
        x1="15.04"
        y1="13.1633"
        x2="24.5601"
        y2="13.1633"
      >
        <Stop offset="0" stopColor="#F9CF2B" />
        <Stop offset="0.1873" stopColor="#F6C92B" stopOpacity={0.8876} />
        <Stop offset="0.4686" stopColor="#EEB829" stopOpacity={0.7188} />
        <Stop offset="0.8074" stopColor="#E39E27" stopOpacity={0.5156} />
        <Stop offset="1" stopColor="#DB8E27" stopOpacity={0.4} />
      </LinearGradient>

      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        className="st1"
        fill="url(#ticket-linear)"
        d="M19.24,8.43L21,11.08l3.06-0.84c0.37-0.1,0.65,0.3,0.41,0.61l-1.97,2.47l1.69,2.66
		c0.25,0.33-0.11,0.75-0.47,0.61l-2.95-1.11l-1.97,2.47c-0.24,0.31-0.69,0.11-0.72-0.28l0.17-3.18l-2.95-1.11
		c-0.36-0.14-0.33-0.63,0.04-0.73l3.06-0.84l0.1-3.16C18.54,8.23,19.06,8.08,19.24,8.43z"
      />
    </G>
  </Svg>
);

export default TicketIcon;
