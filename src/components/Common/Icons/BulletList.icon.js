import React from 'react';
import Svg, { Path, G, Stop, LinearGradient } from 'react-native-svg';
import themeStyle from '../../../styles/theme.style';

const BulletListIcon = ({ pathColor, width, height, style }) => (
  <Svg
    id="bullet-list-icon"
    x="0px"
    y="0px"
    width={width || 40}
    height={height || 40}
    viewBox="0 0 37.42 33.94"
    style={[
      {
        enableBackground: 'new 0 0 37.42 33.94',
      },
      style,
    ]}
    xmlSpace="preserve"
  >
    <G>
      <Path
        fill="#fff"
        d="M26.4,2.35c-3.24,0-6.47,0-9.71,0c-0.93,0-1.47,0.44-1.48,1.16c-0.01,0.72,0.53,1.16,1.45,1.18
		c0.07,0,0.15,0,0.22,0c6.28,0,12.56,0,18.84,0c0.24,0,0.49,0.01,0.73-0.04c0.59-0.13,0.95-0.52,0.96-1.12
		c0.01-0.61-0.35-1-0.94-1.14c-0.23-0.05-0.48-0.04-0.73-0.04C32.63,2.35,29.52,2.35,26.4,2.35z M26.37,17.56c3.19,0,6.38,0,9.57,0
		c0.93,0,1.47-0.43,1.49-1.14c0.02-0.74-0.54-1.19-1.51-1.2c-0.07,0-0.15,0-0.22,0c-6.26,0-12.51,0-18.77,0
		c-0.27,0-0.59-0.07-0.79,0.06c-0.34,0.22-0.73,0.52-0.85,0.88c-0.1,0.29,0.07,0.81,0.31,1.03c0.27,0.24,0.74,0.35,1.13,0.36
		C19.94,17.57,23.15,17.56,26.37,17.56z M26.31,28.09c-3.12,0-6.23,0-9.35,0c-0.24,0-0.49-0.02-0.73,0.03
		c-0.63,0.12-1.01,0.51-1.01,1.15c0.01,0.64,0.4,1.03,1.03,1.14c0.21,0.04,0.44,0.02,0.66,0.02c6.28,0,12.56,0,18.84,0
		c0.22,0,0.44,0.01,0.66-0.03c0.63-0.12,1.01-0.51,1.01-1.15c-0.01-0.64-0.4-1.03-1.03-1.14c-0.21-0.04-0.44-0.02-0.66-0.02
		C32.59,28.09,29.45,28.09,26.31,28.09z M4.67,5.38C3.8,4.51,3.03,3.66,2.18,2.91C1.85,2.63,1.31,2.38,0.9,2.43
		c-0.96,0.13-1.19,1.26-0.44,2.04c1,1.04,2.02,2.04,3.04,3.05c0.91,0.9,1.43,0.9,2.34-0.01c1.72-1.72,3.45-3.44,5.16-5.17
		c0.2-0.2,0.5-0.44,0.52-0.68c0.04-0.42,0.01-0.93-0.2-1.26c-0.28-0.43-0.82-0.53-1.31-0.24c-0.27,0.16-0.51,0.38-0.73,0.6
		C7.77,2.27,6.26,3.79,4.67,5.38z M4.65,18.27c-0.89-0.9-1.69-1.75-2.52-2.56c-0.69-0.67-1.5-0.62-1.93,0.08
		c-0.4,0.64-0.13,1.16,0.33,1.63c1,1,1.99,2,3,3c0.88,0.88,1.42,0.88,2.29,0.01c1.76-1.75,3.51-3.51,5.27-5.27
		c0.19-0.19,0.46-0.41,0.47-0.63c0.02-0.42,0.02-0.99-0.23-1.24c-0.25-0.25-0.82-0.29-1.24-0.25c-0.26,0.02-0.52,0.34-0.74,0.56
		C7.8,15.12,6.27,16.65,4.65,18.27z M4.66,31.13c-0.9-0.91-1.71-1.75-2.55-2.56c-0.68-0.66-1.51-0.6-1.93,0.1
		c-0.37,0.61-0.15,1.12,0.3,1.58c1.01,1.02,2.03,2.04,3.05,3.05c0.87,0.87,1.41,0.87,2.29-0.01c1.74-1.74,3.48-3.47,5.21-5.22
		c0.19-0.19,0.37-0.39,0.5-0.62c0.28-0.49,0.17-0.96-0.21-1.33c-0.38-0.38-0.85-0.48-1.34-0.19c-0.25,0.15-0.47,0.35-0.67,0.56
		C7.79,28,6.26,29.53,4.66,31.13z"
      />

      <Path
        style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
        d="M26.4,2.35c3.12,0,6.23,0,9.35,0c0.24,0,0.49-0.01,0.73,0.04c0.59,0.14,0.94,0.53,0.94,1.14
		c-0.01,0.61-0.36,0.99-0.96,1.12c-0.23,0.05-0.48,0.04-0.73,0.04c-6.28,0-12.56,0-18.84,0c-0.07,0-0.15,0-0.22,0
		c-0.92-0.02-1.46-0.46-1.45-1.18c0.01-0.72,0.55-1.16,1.48-1.16C19.93,2.35,23.17,2.35,26.4,2.35z"
      />

      <Path
        style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
        d="M26.37,17.56c-3.21,0-6.43,0.01-9.64-0.02c-0.39,0-0.86-0.11-1.13-0.36c-0.24-0.21-0.41-0.74-0.31-1.03
		c0.12-0.35,0.51-0.66,0.85-0.88c0.19-0.13,0.52-0.06,0.79-0.06c6.26,0,12.51,0,18.77,0c0.07,0,0.15,0,0.22,0
		c0.97,0.01,1.53,0.45,1.51,1.2c-0.02,0.72-0.56,1.14-1.49,1.14C32.74,17.56,29.55,17.56,26.37,17.56z"
      />

      <Path
        style={{ fill: pathColor || themeStyle.BLUE_WOOD }}
        d="M26.31,28.09c3.14,0,6.28,0,9.42,0c0.22,0,0.44-0.02,0.66,0.02c0.63,0.11,1.02,0.49,1.03,1.14
		c0.01,0.64-0.38,1.03-1.01,1.15c-0.21,0.04-0.44,0.03-0.66,0.03c-6.28,0-12.56,0-18.84,0c-0.22,0-0.44,0.02-0.66-0.02
		c-0.63-0.11-1.02-0.49-1.03-1.14c-0.01-0.64,0.38-1.04,1.01-1.15c0.24-0.04,0.48-0.03,0.73-0.03
		C20.08,28.09,23.19,28.09,26.31,28.09z"
      />

      <LinearGradient
        id="bullet-list-lg-1"
        gradientUnits="userSpaceOnUse"
        x1="0.0207"
        y1="4.0988"
        x2="11.5427"
        y2="4.0988"
      >
        <Stop offset="0" stopColor="#F9CF2B" />
        <Stop offset="0.1873" stopColor="#F6C92B" stopOpacity={0.8876} />
        <Stop offset="0.4686" stopColor="#EEB829" stopOpacity={0.7188} />
        <Stop offset="0.8074" stopColor="#E39E27" stopOpacity={0.5156} />
        <Stop offset="1" stopColor="#DB8E27" stopOpacity={0.4} />
      </LinearGradient>

      <Path
        fill="url(#bullet-list-lg-1)"
        d="M4.67,5.38c1.59-1.6,3.1-3.11,4.62-4.62c0.22-0.22,0.46-0.44,0.73-0.6c0.49-0.28,1.03-0.19,1.31,0.24
		c0.21,0.33,0.24,0.84,0.2,1.26c-0.02,0.24-0.32,0.47-0.52,0.68C9.29,4.07,7.57,5.79,5.85,7.51C4.93,8.42,4.41,8.42,3.51,7.52
		C2.49,6.51,1.46,5.51,0.47,4.47C-0.28,3.69-0.06,2.56,0.9,2.43c0.4-0.05,0.95,0.19,1.27,0.48C3.03,3.66,3.8,4.51,4.67,5.38z"
      />

      <LinearGradient
        id="bullet-list-lg-2"
        gradientUnits="userSpaceOnUse"
        x1="4.388401e-04"
        y1="17.0443"
        x2="11.5446"
        y2="17.0443"
      >
        <Stop offset="0" stopColor="#F9CF2B" />
        <Stop offset="0.1873" stopColor="#F6C92B" stopOpacity={0.8876} />
        <Stop offset="0.4686" stopColor="#EEB829" stopOpacity={0.7188} />
        <Stop offset="0.8074" stopColor="#E39E27" stopOpacity={0.5156} />
        <Stop offset="1" stopColor="#DB8E27" stopOpacity={0.4} />
      </LinearGradient>

      <Path
        fill="url(#bullet-list-lg-2)"
        d="M4.65,18.27c1.62-1.62,3.15-3.16,4.68-4.68c0.22-0.22,0.48-0.53,0.74-0.56c0.41-0.04,0.99-0.01,1.24,0.25
		c0.25,0.25,0.25,0.82,0.23,1.24c-0.01,0.22-0.28,0.44-0.47,0.63c-1.75,1.76-3.51,3.52-5.27,5.27c-0.87,0.87-1.41,0.86-2.29-0.01
		c-1-1-2-1.99-3-3c-0.47-0.47-0.73-0.99-0.33-1.63c0.43-0.7,1.24-0.74,1.93-0.08C2.96,16.52,3.76,17.37,4.65,18.27z"
      />

      <LinearGradient
        id="bullet-list-lg-3"
        gradientUnits="userSpaceOnUse"
        x1="0"
        y1="29.8463"
        x2="11.6919"
        y2="29.8463"
      >
        <Stop offset="0" stopColor="#F9CF2B" />
        <Stop offset="0.1873" stopColor="#F6C92B" stopOpacity={0.8876} />
        <Stop offset="0.4686" stopColor="#EEB829" stopOpacity={0.7188} />
        <Stop offset="0.8074" stopColor="#E39E27" stopOpacity={0.5156} />
        <Stop offset="1" stopColor="#DB8E27" stopOpacity={0.4} />
      </LinearGradient>

      <Path
        fill="url(#bullet-list-lg-3)"
        d="M4.66,31.13c1.6-1.6,3.13-3.13,4.66-4.66c0.21-0.2,0.42-0.41,0.67-0.56c0.48-0.29,0.96-0.18,1.34,0.19
		c0.38,0.37,0.49,0.85,0.21,1.33c-0.13,0.23-0.31,0.43-0.5,0.62c-1.73,1.74-3.47,3.48-5.21,5.22c-0.88,0.87-1.41,0.87-2.29,0.01
		c-1.02-1.01-2.04-2.03-3.05-3.05c-0.45-0.45-0.67-0.97-0.3-1.58c0.42-0.7,1.25-0.76,1.93-0.1C2.95,29.38,3.76,30.22,4.66,31.13z"
      />
    </G>
  </Svg>
);

export default BulletListIcon;
