/** External dependencies */
import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';

const LinearSeparatorComponent = () => (
  <LinearGradient
    colors={['rgba(35,46,64,0)', 'rgba(35,46,64,0.2)', 'rgba(35,46,64,0)']}
    start={[0, 0.5]}
    end={[1, 0.5]}
    style={{ padding: 0.5 }}
  />
);

const styles = {};

LinearSeparatorComponent.propTypes = {};

export default LinearSeparatorComponent;
