/** External dependencies */
import React from 'react';
import { Image } from 'react-native';
import PropTypes from 'prop-types';
import { LinearGradient } from 'expo-linear-gradient';

/** Styles */
import themeStyles from '../../../styles/theme.style';

const StoreLogoComponent = ({ source, size, style }) => {
  const borderWidth = 6;

  return (
    <LinearGradient
      style={[
        style,
        styles.itemImageContainer,
        {
          marginTop: 20,
          height: size + borderWidth,
          width: size + borderWidth,
          borderRadius: (size + borderWidth) / 2,
        },
      ]}
      colors={['rgba(39,50,72,0.1)', 'rgba(39,50,72,0)', 'rgba(39,50,72,0.1)']}
      start={[0.5, 0]}
      end={[0.5, 1]}
    >
      <Image
        source={{ uri: source }}
        style={[
          styles.itemImage,
          {
            height: size,
            width: size,
            borderRadius: size / 2,
            top: borderWidth / 2,
            left: borderWidth / 2,
          },
        ]}
      />
    </LinearGradient>
  );
};

const styles = {
  itemImageContainer: {
    backgroundColor: themeStyles.LIGHT_GREY,
    position: 'relative',
  },
  itemImage: {
    position: 'absolute',
  },
};

StoreLogoComponent.propTypes = {
  source: PropTypes.string,
  size: PropTypes.number,
  style: PropTypes.object,
};

StoreLogoComponent.defaultProps = {
  size: 60,
  source: '',
  style: {},
};

export default StoreLogoComponent;
