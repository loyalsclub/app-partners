/** External Dependencies */
import React from 'react';
import PropTypes from 'prop-types';
import { LinearGradient } from 'expo-linear-gradient';
import { View, Text, Image } from 'react-native';

/** Components */
import MainContainer, { width } from '../Layout/MainContainer.component';

/** Styles */
import RewardCard from './RewardCard.component';
import fontStyles from '../../../styles/font.styles';

const LEVEL_SETTINGS = {
  1: {
    image: require('../../../assets/images/crown-silver/img.png'),
  },
  2: {
    image: require('../../../assets/images/crown-gold/img.png'),
  },
  3: {
    image: require('../../../assets/images/crown-zafire/img.png'),
  },
};

const imageSize = 30;
const separatorsWidth = width / 2 - 2 * imageSize;

const styles = {
  rewardsListContainer: {
    paddingVertical: 10,
    paddingBottom: 15,
  },
  levelHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
  },
  crownImage: {
    height: imageSize,
    width: imageSize,
    resizeMode: 'contain',
  },
  textReward: {
    paddingVertical: 2,
  },
  disabled: {
    opacity: 0.2,
  },
};

const RewardsList = (props) => {
  const { rewardsData, currentLevel, onAction, actionType } = props;

  return (
    <MainContainer style={styles.rewardsListContainer}>
      {rewardsData.map((levelObject) => {
        const elements = [];
        const { level, StoreRewards } = levelObject;
        const disabled = currentLevel && level > currentLevel;

        elements.push(
          <View key={`levelHeader${level}`} style={styles.levelHeader}>
            <LinearGradient
              key={`levelHeaderG1${level}`}
              colors={[
                'rgba(35,46,64,0)',
                'rgba(35,46,64,0.2)',
                'rgba(35,46,64,0)',
              ]}
              start={[0, 0.5]}
              end={[1, 0.5]}
              style={{ marginRight: 7, width: separatorsWidth, height: 2 }}
            />

            <Image
              key={`levelHeaderIM${level}`}
              source={LEVEL_SETTINGS[level].image}
              style={styles.crownImage}
            />

            <LinearGradient
              key={`levelHeaderG2${level}`}
              colors={[
                'rgba(35,46,64,0)',
                'rgba(35,46,64,0.2)',
                'rgba(35,46,64,0)',
              ]}
              start={[0, 0.5]}
              end={[1, 0.5]}
              style={{ marginLeft: 7, width: separatorsWidth, height: 2 }}
            />
          </View>
        );

        const rewardsElements = StoreRewards.map((reward) => (
          <RewardCard
            disabled={disabled}
            onAction={onAction}
            actionType={actionType}
            key={`rewardElement${reward.id}`}
            StoreReward={reward}
            level={level}
          />
        ));

        if (rewardsElements.length === 0) {
          elements.push(
            <Text
              style={[
                fontStyles.blue,
                fontStyles.normalWeight,
                fontStyles.mediumSize,
                {
                  alignSelf: 'center',
                  paddingVertical: 20,
                },
              ]}
              key={`noRewards-${level}`}
            >
              Todavía no agregaste beneficios para este nivel
            </Text>
          );
        }

        elements.push(rewardsElements);

        return (
          <View key={Math.random()} style={disabled ? styles.disabled : {}}>
            {elements}
          </View>
        );
      })}
    </MainContainer>
  );
};

RewardsList.propTypes = {
  rewardsData: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      level: PropTypes.number,
      StoreRewards: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string,
          title: PropTypes.string,
        })
      ),
    })
  ).isRequired,
  currentLevel: PropTypes.number,
};

RewardsList.defaultProps = {
  currentLevel: undefined,
};

export default RewardsList;
