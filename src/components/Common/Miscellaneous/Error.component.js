/** External Dependencies */
import React from 'react';
import PropTypes from 'prop-types';

/** Components */
import MainContainer from '../Layout/MainContainer.component';
import GradientButton from './GradientButton.component';
import PassiveNotification from './PassiveNotification.component';

/** Styles */
import globalStyles from '../../../styles/global.styles';

const ErrorComponent = (props) => {
  const { onContinuePressHandler, mainText, secondaryText } = props;

  return (
    <MainContainer style={globalStyles.centeredCon}>
      <PassiveNotification
        type="genericError"
        secondaryText={secondaryText}
        mainText={mainText}
      />
      <GradientButton
        style={styles.button}
        mode="dark"
        text="Volver al inicio"
        onPress={onContinuePressHandler}
      />
    </MainContainer>
  );
};

const styles = {
  button: {
    marginTop: 15,
  },
};

ErrorComponent.propTypes = {
  mainText: PropTypes.string,
  secondaryText: PropTypes.string.isRequired,
  onContinuePressHandler: PropTypes.func.isRequired,
};

ErrorComponent.defaultProps = {
  mainText: '¡Upss!',
};

export default ErrorComponent;
