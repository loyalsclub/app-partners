import themeStyle from '../../../../styles/theme.style';

export default {
  card: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 15,
    padding: 20,
    elevation: 4,
    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: { width: 2.85, height: 2.85 },
    shadowOpacity: 0.25,
    marginBottom: 20,
  },
  cardNoDescription: {
    paddingVertical: 10,
  },
  iconWrapper: { marginRight: 15 },
  textWrapper: {
    flexGrow: 1,
    flexShrink: 1,
    marginRight: 15,
  },
  title: {
    fontFamily: themeStyle.MAIN_FONT_MEDIUM,
    fontSize: 16,
  },
  description: {
    marginTop: 5,
    fontFamily: themeStyle.MAIN_FONT_LIGHT,
    color: themeStyle.GREY,
    fontSize: 14,
  },
};
