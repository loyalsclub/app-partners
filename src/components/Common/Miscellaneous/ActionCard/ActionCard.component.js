import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import ChevronRightIcon from '../../Icons/ChevronRight.icon';
import styles from './ActionCard.styles';

const ActionCard = ({ icon, title, description, style, onPress }) => (
  <TouchableOpacity
    activeOpacity={0.5}
    onPress={onPress}
    style={[styles.card, !description ? styles.cardNoDescription : {}, style]}
  >
    <View style={styles.iconWrapper}>{icon}</View>

    <View style={styles.textWrapper}>
      <Text style={styles.title}>{title}</Text>
      {description && <Text style={styles.description}>{description}</Text>}
    </View>

    <ChevronRightIcon />
  </TouchableOpacity>
);

export default ActionCard;
