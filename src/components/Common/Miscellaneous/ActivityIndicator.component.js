/** External dependencies */
import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const CustomActivityIndicator = (props) => {
  if (!props.loading) return null;

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <ActivityIndicator size="large" />
    </View>
  );
};

export default CustomActivityIndicator;
