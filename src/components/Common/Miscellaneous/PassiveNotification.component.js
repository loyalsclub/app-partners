/** External dependencies */
import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

/** Internal dependencies */
import GradientButton from './GradientButton.component';

/** Styles */
import themeStyles from '../../../styles/theme.style';

const TYPES = {
  notPartner: {
    image: require('../../../assets/images/illustration-spaceship/img.png'),
    mainText: 'Bienvenido a Loyals Club',
    secondaryText: 'Configuremos tu Club de Clientes',
  },
  genericSuccess: {
    image: require('../../../assets/images/success-crown/img.png'),
    mainText: '¡Acción Exitosa!',
    secondaryText: 'La acción fue realizada con éxito.',
  },
  genericError: {
    image: require('../../../assets/images/error-crown/img.png'),
    mainText: '¡Upss!',
    secondaryText: 'Parece que hubo un inconveniente',
  },
  verifyEmail: {
    image: require('../../../assets/images/illustration-spaceship/img.png'),
    mainText: 'Verificá tu email',
    secondaryText:
      'Te enviamos un email a {{email}} para verificar tu cuenta de correo.',
  },
};

const PassiveNotification = ({
  type,
  style,
  mainText,
  secondaryText,
  shouldShowActionButton,
  buttonText,
  buttonMode,
  onPressHandler,
  textVariables = {},
}) => {
  const d = TYPES[type];

  if (!d) return null;

  let explanationText = secondaryText || d.secondaryText;

  Object.keys(textVariables).forEach((key) => {
    explanationText = explanationText.replace(`{{${key}}}`, textVariables[key]);
  });

  return (
    <View style={[style, { alignItems: 'center' }]}>
      <Image source={d.image} style={{ width: 150, resizeMode: 'contain' }} />
      <Text style={styles.mainText}>{mainText || d.mainText}</Text>
      <Text style={styles.explanationText}>{explanationText}</Text>

      {shouldShowActionButton && (
        <GradientButton
          style={{ marginTop: 30 }}
          text={buttonText || d.buttonText}
          mode={buttonMode || d.buttonMode}
          onPress={onPressHandler}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  mainText: {
    fontFamily: themeStyles.MAIN_FONT_BOLD,
    fontSize: 21,
    maxWidth: 250,
    marginTop: 20,
    lineHeight: 25,
    color: themeStyles.BLUE_WOOD,
    textAlign: 'center',
  },
  explanationText: {
    fontFamily: themeStyles.MAIN_FONT_LIGHT,
    fontSize: 17,
    marginTop: 12,
    textAlign: 'center',
    color: '#808080',
    maxWidth: 300,
  },
});

PassiveNotification.propTypes = {
  mainText: PropTypes.string,
  secondaryText: PropTypes.string,
  type: PropTypes.string.isRequired,
  style: PropTypes.object,
  shouldShowActionButton: PropTypes.bool,
  buttonText: PropTypes.string,
  buttonMode: PropTypes.string,
};

PassiveNotification.defaultProps = {
  style: {},
  mainText: null,
  secondaryText: null,
  shouldShowActionButton: false,
  buttonText: '',
  buttonMode: 'active',
};

export default PassiveNotification;
