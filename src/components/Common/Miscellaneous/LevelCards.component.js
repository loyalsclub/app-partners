/** External Dependencies */
import React, { useState } from 'react';
import { View, Text, Image } from 'react-native';

/** Styles */
import fontStyles from '../../../styles/font.styles';
import themeStyle from '../../../styles/theme.style';

const LEVEL_SETTINGS = {
  1: {
    image: require('../../../assets/images/crown-silver/img.png'),
    title: 'Nivel Plata',
    description: 'Pensado para clientes ocasionales',
  },
  2: {
    image: require('../../../assets/images/crown-gold/img.png'),
    title: 'Nivel Oro',
    description: 'Pensado para clientes frecuentes',
  },
  3: {
    image: require('../../../assets/images/crown-zafire/img.png'),
    title: 'Nivel Zafiro',
    description: 'Pensado para tus clientes ideales',
  },
};

const LevelCards = (props) => {
  const { levelsData } = props;

  const styles = {
    imageSection: {
      flexDirection: 'column',
      alignItems: 'center',
      flexBasis: 60,
    },
    descriptionSection: {
      marginHorizontal: 15,
      flexDirection: 'column',
      flexShrink: 1,
      justifyContent: 'space-around',
      flexGrow: 1,
    },
    container: {
      flexDirection: 'row',
      flexWrap: 'nowrap',
      alignItems: 'center',
      backgroundColor: themeStyle.CREAM_WHITE,
      borderRadius: 25,
      borderWidth: 1,
      borderColor: themeStyle.GREY,
      margin: 10,
      paddingVertical: 20,
      paddingHorizontal: 15,
    },
    crownImage: {
      height: 40,
      width: 40,
      resizeMode: 'contain',
      marginBottom: 10,
    },
  };

  const [currentFont, setCurrentFont] = useState(19);
  return (
    <View>
      {levelsData.map((levelObject) => {
        const { level, points } = levelObject;

        return (
          <View style={styles.container} key={level}>
            <View style={styles.imageSection}>
              <Image
                key={level}
                source={LEVEL_SETTINGS[level].image}
                style={styles.crownImage}
              />
              <Text
                style={[
                  fontStyles.blue,
                  fontStyles.normalWeight,
                  {
                    fontSize: currentFont,
                  },
                ]}
                onTextLayout={(e) => {
                  const { lines } = e.nativeEvent;
                  if (lines.length > 1) setCurrentFont(currentFont - 1);
                }}
              >
                {points}
              </Text>
            </View>
            <View style={styles.descriptionSection}>
              <Text
                style={[
                  fontStyles.blue,
                  fontStyles.boldWeight,
                  fontStyles.largeSize,
                  {
                    marginBottom: 6,
                  },
                ]}
              >
                {LEVEL_SETTINGS[level].title}
              </Text>
              <Text
                style={[
                  fontStyles.dark,
                  fontStyles.normalWeight,
                  fontStyles.mediumSize,
                ]}
              >
                {LEVEL_SETTINGS[level].description}
              </Text>
            </View>
          </View>
        );
      })}
    </View>
  );
};

LevelCards.propTypes = {};

LevelCards.defaultProps = {};

export default LevelCards;
