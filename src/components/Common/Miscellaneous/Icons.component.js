/** External dependencies */
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { createIconSetFromIcoMoon } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import icoMoonConfig from '../../../assets/icons/selection.json';

/** Styles */
import themeStyles from '../../../styles/theme.style';

const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'LoyalIcons');

const IconComponent = (props) => {
  const { color, style, size, name, badgeNumber, onPress } = props;
  const translateYVal = -10;

  const noOnPress = () => (
    <Icon style={style} size={size} color={color} name={name} />
    // <View style={{ position: 'relative' }}>
    //   {/* {badgeNumber > 0 &&
    //     <View style={styles.badge} transform={[{ translateY: translateYVal }, { translateX: 10 / 3 }]}>
    //       <Text style={styles.badgeText}>{badgeNumber}</Text>
    //     </View>
    //   } */}
    // </View>
  );

  if (onPress)
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={onPress}
        style={props.style}
      >
        {noOnPress()}
      </TouchableOpacity>
    );
  return noOnPress();
};

const styles = {
  icon: {
    position: 'relative',
  },
  badge: {
    width: 20,
    height: 20,
    borderRadius: 10,
    top: 0,
    right: 0,
    backgroundColor: themeStyles.BLUE_WOOD,
    position: 'absolute',
  },
  badgeText: {
    alignSelf: 'center',
    color: 'white',
    fontFamily: themeStyles.MAIN_FONT_LIGHT,
    lineHeight: 21,
  },
};

IconComponent.propTypes = {
  badgeNumber: PropTypes.number,
  onPress: PropTypes.func,
  style: PropTypes.any,
  size: PropTypes.number,
  color: PropTypes.string,
  name: PropTypes.string.isRequired,
};

IconComponent.defaultProps = {
  badgeNumber: 0,
  style: {},
  size: 25,
  color: 'white',
};

export default IconComponent;
export { Icon };
