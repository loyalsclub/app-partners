/** External dependencies */
import React from 'react';
import { Text, Dimensions, TouchableOpacity, View } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import PropTypes from 'prop-types';

/** Styles */
import themeStyles from '../../../styles/theme.style';
import fontStyles from '../../../styles/font.styles';
import themeStyle from '../../../styles/theme.style';

const { width } = Dimensions.get('window');

const GradientButton = (props) => {
  const { mode, text, style, onPress } = props;
  let { icon = null } = props;
  let backGroundColor;

  switch (mode) {
    case 'active':
      backGroundColor = [themeStyles.GOLDEN, 'rgba(221,141,0,0.4)'];
      break;
    case 'activeWhite':
      backGroundColor = [themeStyles.CREAM_WHITE, 'rgba(255,255,255,0.8)'];
      break;
    case 'dark':
      backGroundColor = ['rgba(39,50,72,0.4)', 'rgba(39,50,72,0.1)'];
      break;
    case 'disabled':
      backGroundColor = ['rgba(39,50,72,0.1)', 'rgba(39,50,72,0)'];
      break;
    case 'minimal':
      backGroundColor = ['transparent', 'transparent'];
      icon = (
        <Text
          style={[
            fontStyles.largeSize,
            {
              color: themeStyle.GOLDEN,
              lineHeight: fontStyles.largeSize.fontSize,
              position: 'absolute',
              right: -20,
              top: '50%',
              transform: [{ translateY: fontStyles.largeSize.fontSize / -2 }],
            },
          ]}
        >
          {'>'}
        </Text>
      );
      break;
    default:
      break;
  }
  const getContent = () => (
    <LinearGradient
      colors={backGroundColor}
      start={[0, 0.5]}
      end={[1, 0.5]}
      style={[{ ...styles }, style]}
    >
      <View>
        <Text
          style={{
            backgroundColor: 'transparent',
            fontSize: 17,
            fontFamily: themeStyles.MAIN_FONT_LIGHT,
            color: 'black',
          }}
        >
          {text}
        </Text>

        {icon}
      </View>
    </LinearGradient>
  );

  if (mode === 'disabled') {
    return getContent();
  }

  return <TouchableOpacity onPress={onPress}>{getContent()}</TouchableOpacity>;
};

const styles = {
  paddingVertical: 16,
  paddingHorizontal: 15,
  alignItems: 'center',
  borderRadius: 25,
  width: width * 0.8,
  maxWidth: 350,
};

GradientButton.propTypes = {
  mode: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  text: PropTypes.string.isRequired,
  style: PropTypes.any,
};

export default GradientButton;
