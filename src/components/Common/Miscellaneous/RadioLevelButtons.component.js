/** External Dependencies */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image, TouchableOpacity } from 'react-native';

/** Components */
import IconComponent from './Icons.component';

/** Styles */
import fontStyles from '../../../styles/font.styles';
import themeStyle from '../../../styles/theme.style';

const LEVEL_SETTINGS = {
  1: {
    image: require('../../../assets/images/crown-silver/img.png'),
    title: 'Nivel Plata',
    description: 'Pensado para clientes ocasionales',
  },
  2: {
    image: require('../../../assets/images/crown-gold/img.png'),
    title: 'Nivel Oro',
    description: 'Pensado para clientes frecuentes',
  },
  3: {
    image: require('../../../assets/images/crown-zafire/img.png'),
    title: 'Nivel Zafiro',
    description: 'Pensado para tus clientes ideales',
  },
};

const RadioLevelButtons = (props) => {
  const { rewardsData, currentLevel, onPress } = props;

  const styles = {
    imageSection: {
      flexDirection: 'column',
      alignItems: 'center',
      flexBasis: 60,
    },
    descriptionSection: {
      marginHorizontal: 15,
      flexDirection: 'column',
      flexShrink: 1,
      justifyContent: 'space-around',
      flexGrow: 1,
    },
    container: {
      flexDirection: 'row',
      flex: 1,
      flexWrap: 'nowrap',
      alignItems: 'center',
      backgroundColor: themeStyle.CREAM_WHITE,
      borderRadius: 25,
      borderWidth: 1,
      borderColor: themeStyle.GREY,
      margin: 10,
      paddingVertical: 15,
      paddingHorizontal: 15,
    },
    crownImage: {
      height: 40,
      width: 40,
      resizeMode: 'contain',
      marginBottom: 10,
    },
  };

  const [currentFont, setCurrentFont] = useState(19);
  return (
    <View style={styles.rewardsListContainer}>
      {rewardsData.map((levelObject) => {
        const { id, level, points } = levelObject;

        return (
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={onPress(id)}
            key={id}
            style={styles.container}
          >
            <View style={styles.imageSection}>
              <Image
                key={level}
                source={LEVEL_SETTINGS[level].image}
                style={styles.crownImage}
              />
              <Text
                style={[
                  fontStyles.blue,
                  fontStyles.normalWeight,
                  {
                    fontSize: currentFont,
                  },
                ]}
                onTextLayout={(e) => {
                  const { lines } = e.nativeEvent;
                  if (lines.length > 1) setCurrentFont(currentFont - 1);
                }}
              >
                {points}
              </Text>
            </View>

            <View style={styles.descriptionSection}>
              <Text
                style={[
                  fontStyles.blue,
                  fontStyles.boldWeight,
                  fontStyles.largeSize,
                  {
                    marginBottom: 6,
                  },
                ]}
              >
                {LEVEL_SETTINGS[level].title}
              </Text>

              <Text
                style={[
                  fontStyles.dark,
                  fontStyles.normalWeight,
                  fontStyles.mediumSize,
                ]}
              >
                {LEVEL_SETTINGS[level].description}
              </Text>
            </View>

            <View style={styles.actionSection}>
              <IconComponent
                name={currentLevel === id ? 'radio-checked' : 'radio-unchecked'}
                color={
                  currentLevel === id ? themeStyle.BLUE_WOOD : themeStyle.GREY
                }
                size={22}
              />
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

RadioLevelButtons.propTypes = {
  currentLevel: PropTypes.string,
};

RadioLevelButtons.defaultProps = {
  currentLevel: undefined,
};

export default RadioLevelButtons;
