import React from 'react';
import { Text, View } from 'react-native';
import styles from './ExplanationItem.styles';

const ExplanationItem = ({ title, description, style, icon }) => {
  return (
    <View style={[styles.wrapper, style]}>
      <View style={styles.iconWrapper}>{icon}</View>

      <View style={styles.innerWrapper}>
        <Text style={styles.title}>{title}</Text>

        <Text style={styles.description}>{description}</Text>
      </View>
    </View>
  );
};

export default ExplanationItem;
