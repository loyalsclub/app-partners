import themeStyle from '../../../../styles/theme.style';

export default {
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'flex-start',
    marginBottom: 40,
  },
  iconWrapper: {
    backgroundColor: '#fff',
    border: '1px solid red',
    borderRadius: 50,
    width: 60,
    height: 60,
    marginRight: 15,
    alignSelf: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 4,
    shadowColor: '#000',
    shadowOffset: { width: 4, height: 4 },
    shadowOpacity: 0.3,
  },
  innerWrapper: {
    flexBasis: 0,
    flexGrow: 1,
  },
  title: {
    fontFamily: themeStyle.MAIN_FONT_MEDIUM,
    fontSize: 16,
    marginBottom: 5,
  },
  description: {
    fontFamily: themeStyle.MAIN_FONT_LIGHT,
    color: themeStyle.GREY,
    fontSize: 14,
  },
};
