/** This is a generic component for the background-patterned */

/** External dependencies */
import React from 'react';
import { Image, Dimensions } from 'react-native';
import PropTypes from 'prop-types';

const BackgroundPattern = ({ type, headerHeight, tabbarHeight }) => {
  const { width, height } = Dimensions.get('window');

  const TYPES = {
    gray: {
      image: require('../../../assets/images/grey-background-pattern/img.png'),
      height: width * 1.162,
    },
    golden: {
      image: require('../../../assets/images/golden-background-pattern/img.png'),
      height: width * 1.162,
    },
  };

  const d = TYPES[type];

  const top =
    (height - d.height - tabbarHeight - headerHeight) / 2 + headerHeight;
  if (!d) return null;

  return (
    <Image
      source={d.image}
      style={{ position: 'absolute', width, top, height: d.height }}
    />
  );
};

BackgroundPattern.propTypes = {
  type: PropTypes.string,
  headerHeight: PropTypes.number,
  tabbarHeight: PropTypes.number,
};

BackgroundPattern.defaultProps = {
  type: 'gray',
  headerHeight: 80,
  tabbarHeight: 60,
};

export default BackgroundPattern;
