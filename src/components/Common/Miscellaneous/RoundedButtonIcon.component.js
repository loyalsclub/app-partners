/** External dependencies */
import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import { LinearGradient } from 'expo-linear-gradient';

/** Internal dependencies */
import IconComponent from './Icons.component';

/** Styles */
import themeStyles from '../../../styles/theme.style';

const RoundedButtonComponent = (props) => {
  const { size, style, name, text, onPress, plus } = props;
  const borderWidth = 4;
  const fullSize = size + borderWidth;
  const GREY_BUTTON = '#F0F0F0';

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={onPress}
      style={{ marginHorizontal: 5 }}
    >
      <LinearGradient
        style={[
          style,
          styles.itemImageContainer,
          {
            height: fullSize,
            width: fullSize,
            borderRadius: fullSize / 2,
            alignSelf: 'center',
          },
        ]}
        colors={[
          'rgba(39,50,72,0.1)',
          'rgba(39,50,72,0)',
          'rgba(39,50,72,0.1)',
        ]}
        start={[0.5, 0]}
        end={[0.5, 1]}
      >
        <View
          style={{
            height: size,
            width: size,
            position: 'absolute',
            top: borderWidth / 2,
            left: borderWidth / 2,
            borderRadius: size / 2,
            backgroundColor: GREY_BUTTON,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <IconComponent
            color={themeStyles.BLUE_WOOD}
            size={(size * 3) / 6}
            name={name}
          />
          {plus && (
            <IconComponent
              style={{
                position: 'absolute',
                top: fullSize / 2 - 10,
                left: (size * 1) / 6 - 10,
              }}
              color={themeStyles.GOLDEN}
              size={20}
              name="plus"
            />
          )}
        </View>
      </LinearGradient>
      <Text style={[styles.buttonText, { fontSize: size / 7 }]}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = {
  itemImageContainer: {
    backgroundColor: themeStyles.LIGHT_GREY,
  },
  itemImage: {
    position: 'absolute',
  },
  buttonText: {
    fontFamily: themeStyles.MAIN_FONT_LIGHT,
    alignSelf: 'center',
    marginTop: 5,
  },
};

RoundedButtonComponent.propTypes = {
  plus: PropTypes.bool,
  onPress: PropTypes.func,
  name: PropTypes.string,
  size: PropTypes.number,
  text: PropTypes.string,
  style: PropTypes.any,
};

RoundedButtonComponent.defaultProps = {
  plus: false,
  size: 60,
};

export default RoundedButtonComponent;
