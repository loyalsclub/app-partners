import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import ChevronRightIcon from '../../Icons/ChevronRight.icon';
import styles from './EditableItem.styles';

const EditableItem = ({ title, value, style, editable, onPress }) => {
  const getContent = () => (
    <>
      <Text style={styles.title}>{`${title}: `}</Text>

      <Text style={styles.value} numberOfLines={1} ellipsizeMode="tail">
        {value}
      </Text>

      {editable && <ChevronRightIcon />}
    </>
  );

  return editable ? (
    <TouchableOpacity style={[styles.wrapper, style]} onPress={onPress}>
      {getContent()}
    </TouchableOpacity>
  ) : (
    <View style={[styles.wrapper, style]}>{getContent()}</View>
  );
};

EditableItem.defaultProps = {
  editable: true,
};

export default EditableItem;
