import themeStyle from '../../../../styles/theme.style';

export default {
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginBottom: 40,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: themeStyle.GREY,
  },
  title: {
    fontFamily: themeStyle.MAIN_FONT_MEDIUM,
    fontSize: 16,
  },
  value: {
    flexGrow: 1,
    flexShrink: 1,
    fontFamily: themeStyle.MAIN_FONT_LIGHT,
    color: themeStyle.GREY,
    fontSize: 16,
  },
};
