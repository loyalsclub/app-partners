/** External dependencies */
import React, { Component } from 'react';
import {
  View,
  SafeAreaView,
  Image,
  Platform,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import Constants from 'expo-constants';
import PropTypes from 'prop-types';
import IconComponent from './Icons.component';

/** Styles */
import themeStyles from '../../../styles/theme.style';

const STATUS_BAR_HEIGHT =
  Platform.OS === 'android' ? 0 : Constants.statusBarHeight;
const PADDING_TOP = 6;
const PADDING_BOTTOM = 6;
const CONTENT_HEIGHT = 36;
const NAV_BAR_HEIGHT = CONTENT_HEIGHT + PADDING_TOP + PADDING_BOTTOM;
const HEADER_HEIGHT = STATUS_BAR_HEIGHT + NAV_BAR_HEIGHT;

const styles = {
  safeArea: {
    backgroundColor: themeStyles.CREAM_WHITE,
  },
  headerContainer: {
    position: 'relative',
    backgroundColor: themeStyles.CREAM_WHITE,
    height: NAV_BAR_HEIGHT,
    paddingTop: 0,
    paddingBottom: PADDING_BOTTOM,
  },
  rightHeaderIcon: {
    paddingRight: 15,
    paddingLeft: 25,
    right: 0,
    height: NAV_BAR_HEIGHT,
    top: 0,
    marginTop: PADDING_TOP,
    marginBottom: PADDING_BOTTOM,
    position: 'absolute',
  },
  leftHeaderIcon: {
    left: 0,
    top: 0,
    paddingLeft: 15,
    paddingRight: 25,
    height: NAV_BAR_HEIGHT,
    marginTop: PADDING_TOP,
    marginBottom: PADDING_BOTTOM,
    position: 'absolute',
  },
  topHeaderLogo: {
    alignSelf: 'center',
    resizeMode: 'contain',
    marginTop: PADDING_TOP,
    height: CONTENT_HEIGHT,
  },
};
class GenericHeader extends Component {
  get content() {
    const {
      mode,
      menuPressHandler,
      goBackOnPress,
      customAction,
      customActionIcon,
    } = this.props;

    switch (mode) {
      case 'menu':
        return (
          <View style={styles.headerContainer}>
            <StatusBar
              backgroundColor={themeStyles.CREAM_WHITE}
              barStyle="dark-content"
            />

            <Image
              source={require('../../../assets/images/partners-logo/img.png')}
              style={[styles.topHeaderLogo]}
            />

            <IconComponent
              name="menu"
              color={themeStyles.BLUE_WOOD}
              onPress={menuPressHandler}
              style={styles.leftHeaderIcon}
            />

            {customAction && customActionIcon && (
              <TouchableOpacity
                style={styles.rightHeaderIcon}
                onPress={customAction}
              >
                {customActionIcon}
              </TouchableOpacity>
            )}
          </View>
        );
      case 'goBack':
        return (
          <View style={styles.headerContainer}>
            <StatusBar
              backgroundColor={themeStyles.CREAM_WHITE}
              barStyle="dark-content"
            />
            <Image
              source={require('../../../assets/images/partners-logo/img.png')}
              style={[styles.topHeaderLogo]}
            />
            <IconComponent
              name="back-arrow"
              color={themeStyles.DARK_BLUE}
              onPress={() => {
                goBackOnPress();
              }}
              style={styles.leftHeaderIcon}
            />
            {customAction ? (
              <IconComponent
                size={28}
                name={customActionIcon}
                color={themeStyles.DARK_BLUE}
                onPress={() => {
                  customAction();
                }}
                style={styles.rightHeaderIcon}
              />
            ) : null}
          </View>
        );
      case 'modal':
        return (
          <View style={styles.headerContainer}>
            <StatusBar
              backgroundColor={themeStyles.CREAM_WHITE}
              barStyle="dark-content"
            />
            <Image
              source={require('../../../assets/images/partners-logo/img.png')}
              style={[styles.topHeaderLogo]}
            />
            <IconComponent
              size={22}
              name="cross"
              color={themeStyles.BLUE_WOOD}
              onPress={() => {
                goBackOnPress();
              }}
              style={[
                styles.leftHeaderIcon,
                {
                  paddingTop: 10,
                },
              ]}
            />
          </View>
        );
      case 'logoOnly':
        return (
          <View style={styles.headerContainer}>
            <StatusBar
              backgroundColor={themeStyles.CREAM_WHITE}
              barStyle="dark-content"
            />
            <Image
              source={require('../../../assets/images/partners-logo/img.png')}
              style={[styles.topHeaderLogo]}
            />
          </View>
        );
      default:
        return null;
    }
  }

  render() {
    return <SafeAreaView style={styles.safeArea}>{this.content}</SafeAreaView>;
  }
}

export const getHeaderHeight = () => HEADER_HEIGHT;

GenericHeader.propTypes = {
  mode: PropTypes.string,
  goBackOnPress: PropTypes.func,
  customActionIcon: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  customAction: PropTypes.func,
  menuPressHandler: PropTypes.func,
};

GenericHeader.defaultProps = {
  mode: 'goBack',
  customActionIcon: undefined,
  customAction: undefined,
  goBackOnPress: () => {},
  menuPressHandler: () => {},
};

export default GenericHeader;
