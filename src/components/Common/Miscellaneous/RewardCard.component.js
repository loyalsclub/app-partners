/** External Dependencies */
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

/** Components */
import IconComponent from './Icons.component';

/** Utils */
import { getFrequency } from '../../../utils/language.utils';

/** Styles */
import fontStyles from '../../../styles/font.styles';
import themeStyles from '../../../styles/theme.style';

const styles = {
  rewardCardContainer: {
    borderRadius: 20,
    position: 'relative',
    padding: 15,
    marginVertical: 10,
    textAlign: 'left',
    backgroundColor: 'white',
    maxWidth: 500,
    zIndex: 1,
    borderWidth: 1,
  },
  actionCard: {
    paddingRight: 50,
  },
  cardTitle: [
    fontStyles.blue,
    fontStyles.normalWeight,
    fontStyles.largeSize,
    {
      marginBottom: 5,
    },
  ],
  cardMeta: {
    marginVertical: 4,
    flexDirection: 'row',
  },
  actionIcon: {
    position: 'absolute',
    right: 12,
    top: 15,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  cardMetaUsed: [
    fontStyles.boldWeight,
    fontStyles.mediumSize,
    {
      color: themeStyles.DANGER,
    },
  ],
  cardMetaText: [
    fontStyles.grey,
    fontStyles.normalWeight,
    fontStyles.mediumSize,
    {
      marginLeft: 7,
    },
  ],
  level1: {
    borderColor: themeStyles.SILVER,
  },
  level2: {
    borderColor: themeStyles.GOLDEN,
  },
  level3: {
    borderColor: themeStyles.SAFIRE,
  },
};

const RewardCard = ({
  StoreReward,
  level,
  actionType,
  onAction,
  disabled = false,
}) => {
  const actionCard = actionType && !disabled;
  const isManagementMode = actionType === 'edit';

  const getCardContent = () => {
    const rewardContainerStyles = [
      styles.rewardCardContainer,
      styles[`level${level}`],
    ];

    if (actionCard) {
      rewardContainerStyles.push(styles.actionCard);
    }

    return (
      <View style={rewardContainerStyles}>
        <Text style={styles.cardTitle}>{StoreReward.title}</Text>

        <View style={styles.cardMeta}>
          <IconComponent name="frequency" size={16} color={themeStyles.GREY} />

          <Text style={styles.cardMetaText}>
            {getFrequency(StoreReward.UsageFrequency)}

            <Text style={styles.cardMetaUsed}>
              {StoreReward.isConsideredUsed && ' - USADO'}
            </Text>
          </Text>
        </View>

        {StoreReward.description && StoreReward.description.length > 0 ? (
          <Text
            style={[
              fontStyles.blue,
              fontStyles.normalWeight,
              fontStyles.mediumSize,
            ]}
          >
            {StoreReward.description}
          </Text>
        ) : null}

        {actionCard ? (
          <View style={styles.actionIcon}>
            <IconComponent
              name={isManagementMode ? 'edit' : 'front-arrow'}
              size={20}
              color={
                StoreReward.isAvailable || isManagementMode
                  ? themeStyles.BLUE_WOOD
                  : themeStyles.GREY_OP
              }
            />
          </View>
        ) : null}
      </View>
    );
  };

  if (!isManagementMode && (!actionCard || !StoreReward.isAvailable)) {
    return getCardContent();
  }

  return (
    <TouchableOpacity activeOpacity={0.6} onPress={onAction(StoreReward.id)}>
      {getCardContent()}
    </TouchableOpacity>
  );
};

RewardCard.propTypes = {
  level: PropTypes.number.isRequired,
  StoreReward: PropTypes.shape({
    title: PropTypes.string.isRequired,
    isAvailable: PropTypes.bool,
    isConsideredUsed: PropTypes.bool,
  }).isRequired,
};

export default RewardCard;
