/** This component was created because react native headers dont allow transparency, so to
 * gain that effect this is the component you use.
 *
 * Structure:
 *
 * Header component (Absolute Top)
 * Scroll View
 * Background
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, RefreshControl } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ActivityIndicator from '../Miscellaneous/ActivityIndicator.component';
import themeStyles from '../../../styles/theme.style';

const styles = StyleSheet.create({
  container: {
    backgroundColor: themeStyles.CREAM_WHITE,
    flex: 1,
  },
  bar: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
});

class PageHeaderContainer extends Component {
  scrollTop() {
    this.scrollViewRef.scrollTo({ x: 0, y: 0, animated: true });
  }

  renderHeader() {
    const { renderHeader, headerHeight } = this.props;

    return (
      <View style={[styles.bar, { height: headerHeight }]}>
        {renderHeader()}
      </View>
    );
  }

  renderScrollView() {
    const {
      renderContent,
      onRefresh,
      refreshing,
      headerHeight,
      marginTopContainer,
      style,
      error,
      renderError,
      keyboardShouldPersistTaps,
    } = this.props;

    return (
      <KeyboardAwareScrollView
        contentContainerStyle={[style.scrollView]}
        ref={(ref) => {
          this.scrollViewRef = ref;
        }}
        keyboardShouldPersistTaps={keyboardShouldPersistTaps}
        refreshControl={
          onRefresh && (
            <RefreshControl
              onRefresh={onRefresh}
              tintColor="transparent"
              colors={['transparent']}
              style={{ backgroundColor: 'transparent' }}
              refreshing={refreshing}
            />
          )
        }
      >
        <View
          style={[
            { marginTop: headerHeight + marginTopContainer },
            style.contentView,
          ]}
        >
          <ActivityIndicator loading={refreshing} />
          {!refreshing && !error && renderContent()}
          {!refreshing && error && renderError()}
        </View>
      </KeyboardAwareScrollView>
    );
  }

  renderFixedBackground() {
    /** Fixed background under header */

    const { renderBackground } = this.props;
    if (!renderBackground) return null;

    return <View style={{ position: 'absolute' }}>{renderBackground()}</View>;
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderFixedBackground() /** Fixed background under header */}
        {this.renderScrollView() /** Scroll view were content will be placed */}
        {this.renderHeader() /** Header background image */}
      </View>
    );
  }
}

PageHeaderContainer.propTypes = {
  error: PropTypes.bool,
  style: PropTypes.objectOf(PropTypes.object),
  renderError: PropTypes.func,
  renderHeader: PropTypes.func,
  renderContent: PropTypes.func.isRequired,
  renderBackground: PropTypes.func,
  headerHeight: PropTypes.number,
  marginTopContainer: PropTypes.number,
  onRefresh: PropTypes.func,
  refreshing: PropTypes.bool,
  keyboardShouldPersistTaps: PropTypes.string,
};

PageHeaderContainer.defaultProps = {
  style: {},
  refreshing: false,
  error: false,
  renderError: () => null,
  renderBackground: () => null,
  onRefresh: null,
  renderHeader: () => <View />,
  marginTopContainer: 0,
  headerHeight: 0,
  keyboardShouldPersistTaps: 'handled',
};

export default PageHeaderContainer;
