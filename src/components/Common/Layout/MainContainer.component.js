/** External dependencies */
import React from 'react';
import { View, Dimensions } from 'react-native';
import PropTypes from 'prop-types';

const maxWidth = 400;
const width =
  Dimensions.get('window').width > maxWidth
    ? maxWidth
    : Dimensions.get('window').width;

const MainContainer = ({ style, children }) => {
  const innerConteiner = width * 0.85;

  return (
    <View
      style={[
        style,
        {
          alignSelf: 'center',
          width: innerConteiner,
          maxWidth,
        },
      ]}
    >
      {children}
    </View>
  );
};

MainContainer.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

MainContainer.defaultProps = {
  style: {},
};

export default MainContainer;
export { width };
