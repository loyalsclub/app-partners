import React from 'react';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';
import BackgroundPattern from '../Miscellaneous/BackgroundPattern.component';
import PageHeaderContainer from './PageHeaderContainer.component';
import GenericHeader, {
  getHeaderHeight,
} from '../Miscellaneous/GenericHeader.component';
import globalStyles from '../../../styles/global.styles';

const LayoutPage = ({ layout, navigation, loading, children }) => {
  const renderContent = () => children;

  const renderBackground = () => <BackgroundPattern type="golden" />;

  const renderHeader = () => (
    <GenericHeader mode="goBack" goBackOnPress={() => navigation.goBack()} />
  );

  return (
    <PageHeaderContainer
      style={{
        scrollView: layout === 'center' ? globalStyles.centeredCon : {},
      }}
      renderHeader={renderHeader}
      renderContent={renderContent}
      renderBackground={renderBackground}
      headerHeight={getHeaderHeight()}
      refreshing={loading}
    />
  );
};

LayoutPage.defaultProps = {
  layout: 'center',
};

LayoutPage.propTypes = {
  layout: PropTypes.oneOf(['center', 'top']),
};

export default withNavigation(LayoutPage);
