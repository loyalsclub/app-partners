import React from 'react';
import { Text } from 'react-native';
import MainContainer from './MainContainer.component';
import globalStyles from '../../../styles/global.styles';
import ActionCard from '../Miscellaneous/ActionCard/ActionCard.component';
import LayoutPage from './LayoutPage.component';

const SectionPage = ({
  icon,
  title,
  description,
  actions,
  customBody,
  loading,
}) => (
  <LayoutPage layout="top" loading={loading}>
    <MainContainer style={[globalStyles.centeredCon, { marginTop: 40 }]}>
      {icon}

      <Text style={globalStyles.sectionPageTitle}>{title}</Text>

      {description && (
        <Text style={globalStyles.sectionPageSubtitle}>{description}</Text>
      )}

      {customBody}

      {actions.map(
        ({
          icon: actionIcon,
          title: actionTitle,
          description: actionDescription,
          onPressHandler,
        }) => (
          <ActionCard
            key={actionTitle}
            icon={actionIcon}
            title={actionTitle}
            description={actionDescription}
            onPress={onPressHandler}
          />
        )
      )}
    </MainContainer>
  </LayoutPage>
);

SectionPage.defaultProps = {
  actions: [],
};

export default SectionPage;
