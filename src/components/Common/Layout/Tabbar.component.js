import React from 'react';
import { View, Platform, Keyboard } from 'react-native';
import { BottomTabBar } from 'react-navigation-tabs';
import { LinearGradient } from 'expo-linear-gradient';

import { isIphoneX } from '../../../utils/device.utils';

const TABBAR_HEIGHT = 60;
export default class TabBarComponent extends React.Component {
  state = {
    visible: true,
  };

  componentDidMount() {
    if (Platform.OS === 'android') {
      this.keyboardEventListeners = [
        Keyboard.addListener('keyboardDidShow', this.visible(false)),
        Keyboard.addListener('keyboardDidHide', this.visible(true)),
      ];
    }
  }

  componentWillUnmount() {
    this.keyboardEventListeners &&
      this.keyboardEventListeners.forEach((eventListener) =>
        eventListener.remove()
      );
  }

  visible = (visible) => () => this.setState({ visible });

  render() {
    if (!this.state.visible) return null;
    return (
      <View>
        <LinearGradient
          colors={[
            'rgba(236,177,21,0.2)',
            'rgba(249,207,39,0.7)',
            'rgba(236,177,21,0.2)',
          ]}
          start={[0, 0.5]}
          end={[1, 0.5]}
          style={{ padding: 0.5 }}
        />
        <BottomTabBar
          {...this.props}
          style={{
            borderTopColor: '#EAEBED',
            backgroundColor: '#EAEBED',
            height: TABBAR_HEIGHT,
            paddingBottom: 5,
            paddingTop: 5,
            paddingRight: 40,
            paddingLeft: 40,
          }}
        />
      </View>
    );
  }
}

export const getTabbarHeight = () => {
  const BOTTOM_SPACE = isIphoneX() ? 34 : 0;
  return TABBAR_HEIGHT + BOTTOM_SPACE;
};
