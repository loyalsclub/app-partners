/** External Dependencies */
import React, { useState } from 'react';
import {
  Alert,
  Text,
  KeyboardAvoidingView,
  Platform,
  View,
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text';

/** Components */
import GradientButton from '../Miscellaneous/GradientButton.component';

/** Styles */
import f from '../../../styles/font.styles';
import globalStyles from '../../../styles/global.styles';
import themeStyle from '../../../styles/theme.style';

const StepLoadPointsInput = (props) => {
  const { onPointsLoadedPressHandler } = props;

  const [value, setValue] = useState();
  const [error, setError] = useState();

  const parseValue = () =>
    value.replace('$', '').replace('.', '').replace(',', '');
  const validValue = () => {
    if (!value || parseValue() <= 0) {
      setError('El monto debe ser mayor a $ 0');
      return false;
    }
    return true;
  };

  const onPointsLoaded = () => {
    if (!validValue()) return;

    Alert.alert(
      'Cargar Puntos',
      `Se le cargaran puntos por un total de ${value}`,
      [
        {
          text: 'Cancelar',
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => onPointsLoadedPressHandler(Number(parseValue())),
        },
      ],
      { cancelable: true }
    );
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      enabled
    >
      <View
        style={[globalStyles.centeredCon, { width: 300, marginVertical: 80 }]}
      >
        <Text
          style={[
            f.xlargeSize,
            f.blue,
            f.center,
            {
              marginBottom: 30,
            },
          ]}
        >
          ¿ Cuanto gastó hoy en tu comercio ?
        </Text>
        <TextInputMask
          type="money"
          hidden
          options={{
            precision: 0,
            separator: ',',
            delimiter: '.',
            unit: '$',
            suffixUnit: '',
          }}
          autoFocus
          placeholder="$0"
          returnKeyLabel="Aceptar"
          returnKeyType="go"
          value={value}
          style={[
            f.titleSize,
            f.blue,
            {
              marginBottom: 30,
              borderBottomWidth: 2,
              borderBottomColor: themeStyle.BLUE_WOOD,
            },
          ]}
          onChangeText={(text) => {
            setValue(text);
          }}
        />
        {error && <Text style={globalStyles.formError}>{error}</Text>}
        <GradientButton
          style={{ marginVertical: 10 }}
          mode={!value ? 'disabled' : 'active'}
          onPress={onPointsLoaded}
          text="Registrar consumo"
        />
      </View>
    </KeyboardAvoidingView>
  );
};

export default StepLoadPointsInput;
