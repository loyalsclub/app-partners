/** External dependencies */
import React, { useState, useEffect } from 'react';
import { View, TextInput, Animated } from 'react-native';

/** Styles */
import themeStyles from '../../../styles/theme.style';
import stylesheet from './Form.styles';
import FormErrorComponent from './FormError.component';
import FormGroupComponent from './FormGroup.component';

/**
 * For label animation to work a config element needs to be sent as part of
 * locals.config {
 *  labelAnimation: (Bool)
 * }
 */
/** Styles */
const textboxStyle = stylesheet.textbox.normal;
const charCounterStyle = stylesheet.charCounter;
const INITIAL_INPUT_SIZE = 22;

export default React.forwardRef((props, ref) => {
  const {
    value,
    label,
    error,
    resize,
    inputProps = {}, // Native Input Props
    minLines = 1,
    maxLines = 5,
    charCounter = false,
  } = props;
  const [borderColor, setBorderColor] = useState(themeStyles.GREY_OP);
  const [labelFocus] = useState(
    new Animated.Value(typeof value === 'undefined' || value === '' ? 0 : 1)
  );
  const [inputHeight, setInputHeight] = useState(minLines * INITIAL_INPUT_SIZE);

  useEffect(() => {
    let newBorderColor = value ? themeStyles.GOLDEN_OP : themeStyles.GREY_OP;

    if (error) newBorderColor = themeStyles.DANGER;

    setBorderColor(newBorderColor);
  }, [error, value]);

  const controlLabelStyle = {
    position: 'absolute',
    left: 0,
    top: labelFocus.interpolate({
      inputRange: [0, 1],
      outputRange: [10, -16],
    }),
    fontFamily: themeStyles.MAIN_FONT_LIGHT,
    fontSize: labelFocus.interpolate({
      inputRange: [0, 1],
      outputRange: [themeStyles.INPUT_FONT_SIZE, 16],
    }),
    color: labelFocus.interpolate({
      inputRange: [0, 1],
      outputRange: [themeStyles.GREY, themeStyles.BLUE_WOOD],
    }),
  };

  /** Handlers */
  const onFocusHandler = () => {
    if (props.onFocus) props.onFocus();

    Animated.timing(labelFocus, {
      toValue: 1,
      duration: 200,
      useNativeDriver: false,
    }).start();

    setBorderColor(error ? themeStyles.DANGER : themeStyles.GOLDEN_OP);
  };

  const onBlurHandler = () => {
    props.onChange(value && value.trim());
    if (props.onBlur) props.onBlur();
    if (typeof value === 'undefined' || value === '') {
      Animated.timing(labelFocus, {
        toValue: 0,
        duration: 200,
        useNativeDriver: false,
      }).start();
    }
    let newBorderColor = value ? themeStyles.GOLDEN_OP : themeStyles.GREY_OP;

    if (error) newBorderColor = themeStyles.DANGER;

    setBorderColor(newBorderColor);
  };

  const labelNode = (
    <Animated.Text style={controlLabelStyle}>{label}</Animated.Text>
  );

  const errorNode = error ? <FormErrorComponent content={error} /> : null;

  const charCounterNode =
    charCounter && inputProps.maxLength ? (
      <Animated.Text
        accessibilityLiveRegion="polite"
        style={[
          charCounterStyle,
          {
            color: labelFocus.interpolate({
              inputRange: [0, 1],
              outputRange: [themeStyles.GREY, themeStyles.BLUE_WOOD],
            }),
          },
        ]}
      >
        {value ? value.length : 0}/{inputProps.maxLength}
      </Animated.Text>
    ) : null;

  const { style = {}, ...customInputProps } = inputProps;

  return (
    <FormGroupComponent>
      <View>
        {labelNode}
        <TextInput
          style={[
            textboxStyle,
            {
              height: inputHeight,
              lineHeight: 20,
              ...style,
            },
          ]}
          {...customInputProps}
          ref={ref}
          value={value}
          onChangeText={(newVal) => props.onChange(newVal)}
          onBlur={onBlurHandler}
          onFocus={onFocusHandler}
          onContentSizeChange={(e) => {
            if (!inputProps.multiline) return;
            const newSize = e.nativeEvent.contentSize.height;
            let newLines = Math.ceil(newSize / INITIAL_INPUT_SIZE);

            if (!resize) return;
            newLines = newLines < minLines ? minLines : newLines;
            newLines = newLines > maxLines ? maxLines : newLines;

            setInputHeight(newLines * INITIAL_INPUT_SIZE);
          }}
        />
      </View>
      {/* Border line underneath input */}
      <Animated.View
        style={{
          backgroundColor: borderColor,
          paddingBottom: 1,
          marginBottom: 2,
        }}
      />
      {/* {help} */}
      <View
        style={{
          position: 'relative',
        }}
      >
        {errorNode}
        {charCounterNode}
      </View>
    </FormGroupComponent>
  );
});
