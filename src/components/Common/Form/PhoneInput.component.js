import React from 'react';
import { TextInputMask } from 'react-native-masked-text';
import fontStyles from '../../../styles/font.styles';
import { getCountryPhoneMask } from '../../../utils/stores.utils';
import FormGroupComponent from './FormGroup.component';

export default ({ label, value, storeCountries, countryCode, onChange }) => {
  const selectedCountry = storeCountries.find(
    (country) => country.name === countryCode
  );

  const mask = (selectedCountry && getCountryPhoneMask(selectedCountry)) || '';

  return (
    <FormGroupComponent>
      <TextInputMask
        type="custom"
        hidden
        options={{ mask }}
        autoFocus
        placeholder={label}
        keyboardType="numeric"
        returnKeyLabel="Aceptar"
        returnKeyType="go"
        value={value || mask.split('9')[0]}
        style={[
          fontStyles.largeSize,
          fontStyles.blue,
          fontStyles.normalWeight,
          {
            margin: 10,
            borderBottomWidth: 1,
            padding: 4,
            textAlign: 'center',
          },
        ]}
        onChangeText={onChange}
      />
    </FormGroupComponent>
  );
};
