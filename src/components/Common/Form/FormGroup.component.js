import React from 'react';
import { View } from 'react-native';

import stylesheet from './Form.styles';

export default ({ children }) => (
  <View style={stylesheet.formGroup.normal}>{children}</View>
);
