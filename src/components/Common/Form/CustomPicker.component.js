/** External dependencies */
import React from 'react';
import { View, Animated, Text, Picker, Platform } from 'react-native';

/** Styles */
import themeStyles from '../../../styles/theme.style';
import stylesheet from './Form.styles';

const formGroupStyle = stylesheet.formGroup.normal;
const pickerStyle = stylesheet.picker.normal;
const pickerValueStyle = stylesheet.pickerValue;

export default (props) => {
  const {
    value,
    label,
    items,
    onChange,
    pickerProps = {}, // Native Input Props
  } = props;

  const controlLabelStyle = {
    ...stylesheet.controlLabel.normal,
    top: Platform.OS === 'ios' ? 0 : -18,
  };

  return (
    <View
      style={[
        formGroupStyle,
        Platform.OS === 'ios'
          ? {
              marginTop: 10,
              marginBottom: 10,
            }
          : {},
      ]}
    >
      <View>
        <Text style={controlLabelStyle}>{label}</Text>
        <Picker
          style={pickerStyle}
          {...pickerProps}
          selectedValue={value}
          itemStyle={pickerValueStyle}
          onValueChange={(itemValue) => onChange(itemValue)}
        >
          {items.map((c) => (
            <Picker.Item key={c.value} label={c.label} value={c.value} />
          ))}
        </Picker>
      </View>
      {Platform.OS !== 'ios' ? (
        <Animated.View
          style={{
            backgroundColor: themeStyles.GREY_OP,
            paddingBottom: 1,
            marginBottom: 2,
          }}
        />
      ) : null}
    </View>
  );
};
