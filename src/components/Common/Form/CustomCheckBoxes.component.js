import React from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';

/** Components */
import IconComponent from '../Miscellaneous/Icons.component';

/** Styles */
import themeStyles from '../../../styles/theme.style';
import stylesheet from './Form.styles';
import FormErrorComponent from './FormError.component';
import FormGroupComponent from './FormGroup.component';

const controlLabelStyle = stylesheet.controlLabel.normal;
const checkBoxesStyle = stylesheet.checkBoxes;
const checkBoxItemStyle = stylesheet.checkBoxItem;

const styles = {
  checkBoxWrapperStyle: {
    minHeight: 45,
    justifyContent: 'center',
    marginLeft: 20,
    marginRight: 20,
    position: 'relative',
  },
};

export default (props) => {
  const { items = [], label, error, onChange, value = {} } = props;

  const changeValue = (key) => () => {
    const newObj = { ...value };
    newObj[key] = !value[key];
    onChange(newObj);
  };

  const errorNode = error ? <FormErrorComponent content={error} /> : null;

  return (
    <FormGroupComponent>
      <View style={checkBoxesStyle}>
        <Text style={controlLabelStyle}>{label}</Text>
        {items.map((c, index) => (
          <TouchableWithoutFeedback
            onPress={changeValue(c.value)}
            key={c.value}
          >
            <View
              style={[
                styles.checkBoxWrapperStyle,
                {
                  borderBottomColor:
                    items.length > 1 && index + 1 === items.length
                      ? 'transparent'
                      : themeStyles.GREY_OP,
                  borderBottomWidth: 1,
                  paddingRight: 30,
                },
              ]}
            >
              <Text style={checkBoxItemStyle}>{c.label}</Text>
              <View style={{ position: 'absolute', right: 10 }}>
                <IconComponent
                  name={
                    value[c.value] ? 'checkbox-checked' : 'checkbox-unchecked'
                  }
                  color={
                    value[c.value] ? themeStyles.BLUE_WOOD : themeStyles.GREY
                  }
                  size={22}
                />
              </View>
            </View>
          </TouchableWithoutFeedback>
        ))}
      </View>
      {errorNode}
    </FormGroupComponent>
  );
};
