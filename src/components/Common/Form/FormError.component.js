import React from 'react';
import { View, Text } from 'react-native';

import stylesheet from './Form.styles';

export default ({ content }) => (
  <View>
    <Text style={stylesheet.errorBlock}>{content}</Text>
  </View>
);
