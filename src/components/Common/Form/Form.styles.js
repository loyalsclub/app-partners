import { Platform, Dimensions } from 'react-native';
import themeStyles from '../../../styles/theme.style';

const INPUT_COLOR = '#000000';
const ERROR_COLOR = '#a94442';
const BORDER_COLOR = 'transparent';
const FONT_SIZE = 14;
const FULL_WIDTH = Dimensions.get('window').width;
const MAX_WIDTH = FULL_WIDTH * 0.8 > 400 ? 400 : FULL_WIDTH * 0.8;

const stylesheet = {
  formGroup: {
    normal: {
      marginBottom: 20,
      maxWidth: MAX_WIDTH,
      width: FULL_WIDTH * 0.8,
      paddingTop: 18,
      borderWidth: 0,
      flexDirection: 'column',
    },
  },
  controlLabel: {
    normal: {
      position: 'absolute',
      left: 0,
      top: -18,
      fontFamily: themeStyles.MAIN_FONT_LIGHT,
      fontSize: 16,
      color: themeStyles.BLUE_WOOD,
    },
    // the style applied when a validation error occours
    error: {
      color: themeStyles.BLUE_WOOD,
      fontFamily: themeStyles.MAIN_FONT_LIGHT,
      fontSize: FONT_SIZE,
      marginBottom: 7,
    },
  },
  checkBoxes: {
    width: FULL_WIDTH * 0.8,
  },
  checkBoxItem: {
    paddingLeft: 6,
    fontFamily: themeStyles.MAIN_FONT_LIGHT,
    fontSize: 16,
  },
  errorBlock: {
    fontSize: FONT_SIZE,
    marginBottom: 2,
    marginRight: 60,
    color: themeStyles.DANGER,
  },
  charCounter: {
    position: 'absolute',
    right: 0,
    top: 3,
    fontFamily: themeStyles.MAIN_FONT_MEDIUM,
    color: themeStyles.BLUE_WOOD,
  },
  textbox: {
    normal: {
      width: FULL_WIDTH * 0.8,
      flexDirection: 'row',
      color: themeStyles.BLUE_WOOD,
      fontFamily: themeStyles.MAIN_FONT_LIGHT,
      fontSize: themeStyles.INPUT_FONT_SIZE,
      // height:  ,
      // backgroundColor: 'blue',
      marginVertical: 8,
      paddingHorizontal: 7,
      borderColor: 'transparent',
      borderWidth: 0,
    },
    // the style applied when a validation error occours
    notEditable: {
      fontSize: themeStyles.INPUT_FONT_SIZE,
      height: 36,
      paddingVertical: Platform.OS === 'ios' ? 5 : 0,
      paddingHorizontal: 7,
      borderRadius: 4,
      borderColor: 'transparent',
      borderWidth: 0,
      marginBottom: 5,
      color: themeStyles.LIGHT_GREY,
      backgroundColor: 'transparent',
    },
  },
  pickerContainer: {
    normal: {
      marginBottom: 4,
      borderRadius: 4,
      borderColor: BORDER_COLOR,
      borderWidth: 1,
    },
    error: {
      marginBottom: 4,
      borderRadius: 4,
      borderColor: ERROR_COLOR,
      borderWidth: 1,
    },
    open: {
      // Alter styles when select container is open
    },
  },
  select: {
    normal: Platform.select({
      android: {
        paddingLeft: 7,
        color: INPUT_COLOR,
      },
      ios: {},
    }),
    // the style applied when a validation error occours
    error: Platform.select({
      android: {
        paddingLeft: 7,
        color: ERROR_COLOR,
      },
      ios: {},
    }),
  },
  pickerTouchable: {
    normal: {
      height: 44,
      flexDirection: 'row',
      alignItems: 'center',
    },
    error: {
      height: 44,
      flexDirection: 'row',
      alignItems: 'center',
    },
    active: {
      borderBottomWidth: 1,
      borderColor: BORDER_COLOR,
    },
  },
  picker: {
    normal: {
      width: FULL_WIDTH * 0.8,
      height: Platform.OS === 'ios' ? 200 : 36,
    },
    error: {
      fontSize: FONT_SIZE,
      paddingLeft: 7,
    },
  },
  pickerValue: {
    fontFamily: themeStyles.MAIN_FONT_LIGHT,
    fontSize: themeStyles.INPUT_FONT_SIZE,
  },
  datepicker: {
    normal: {},
    // the style applied when a validation error occours
    error: {
      marginBottom: 4,
    },
  },
  dateTouchable: {
    normal: {
      width: FULL_WIDTH * 0.8,
      flexDirection: 'row',
      borderColor: 'transparent',
      borderBottomColor: themeStyles.GREY_OP,
      borderWidth: 0,
      borderBottomWidth: 1,
    },
    error: {
      width: FULL_WIDTH * 0.8,
      flexDirection: 'row',
      borderColor: 'transparent',
      borderBottomColor: themeStyles.DANGER,
      marginBottom: 4, // margin necessary for label.
      borderWidth: 0,
      borderBottomWidth: 1,
    },
  },
  dateValue:
    Platform.OS !== 'ios'
      ? {
          normal: {
            color: themeStyles.BLUE_WOOD,
            fontFamily: themeStyles.MAIN_FONT_MEDIUM,
            fontSize: themeStyles.INPUT_FONT_SIZE,
            padding: 7,
            borderBottomColor: themeStyles.GREY_OP,
            borderBottomWidth: 1,
            marginBottom: 5,
            width: FULL_WIDTH * 0.8,
          },
          empty: {
            width: FULL_WIDTH * 0.8,
            color: themeStyles.GREY_OP,
            fontFamily: themeStyles.MAIN_FONT_MEDIUM,
            fontSize: themeStyles.INPUT_FONT_SIZE,
            padding: 7,
            borderBottomColor: themeStyles.GREY_OP,
            borderBottomWidth: 1,
            marginBottom: 5,
          },
          error: {
            color: themeStyles.BLUE_WOOD,
            width: FULL_WIDTH * 0.8,
            fontFamily: themeStyles.MAIN_FONT_MEDIUM,
            fontSize: themeStyles.INPUT_FONT_SIZE,
            padding: 7,
            borderBottomColor: themeStyles.DANGER,
            borderBottomWidth: 1,
            marginBottom: 5,
          },
        }
      : {
          normal: {
            color: themeStyles.BLUE_WOOD,
            fontFamily: themeStyles.MAIN_FONT_MEDIUM,
            fontSize: themeStyles.INPUT_FONT_SIZE,
            borderColor: 'transparent',
            width: FULL_WIDTH * 0.8,
            padding: 7,
          },
          empty: {
            width: FULL_WIDTH * 0.8,
            color: themeStyles.GREY_OP,
            fontFamily: themeStyles.MAIN_FONT_MEDIUM,
            fontSize: themeStyles.INPUT_FONT_SIZE,
            borderColor: 'transparent',
            padding: 7,
          },
          error: {
            color: themeStyles.BLUE_WOOD,
            fontFamily: themeStyles.MAIN_FONT_MEDIUM,
            fontSize: themeStyles.INPUT_FONT_SIZE,
            borderColor: 'transparent',
            width: FULL_WIDTH * 0.8,
            padding: 7,
          },
        },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center',
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 1,
    marginTop: 20,
    marginBottom: 10,
    alignSelf: 'center',
    justifyContent: 'center',
    width: FULL_WIDTH * 0.8,
  },
};

export default stylesheet;
