/** External Dependencies */
import React from 'react';
import { Text, Share, Alert } from 'react-native';

/** Components */
import MainContainer from '../../Common/Layout/MainContainer.component';
import ActionCard from '../../Common/Miscellaneous/ActionCard/ActionCard.component';

/** Styles */
import globalStyles from '../../../styles/global.styles';
import fs from '../../../styles/font.styles';
import { analyticsProvider, events } from '../../../providers/analytics';
import WhatsAppInstagramIcon from '../../Common/Icons/WhatsAppInstagram.icon';
import WhatsAppStarsIcon from '../../Common/Icons/WhatsAppStars.icon';
import translate from '../../../utils/language.utils';

const styles = {
  label: {
    marginBottom: 8,
  },
  textInfo: {
    marginBottom: 15,
  },
  fitstButton: {
    marginTop: 40,
  },
  button: {
    marginTop: 15,
  },
};

const StepAddCustomer = ({ couponInfo, onNewContactPressHandler }) => {
  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `${translate('GENERATE_COUPON.loadPointsMessage')} \n ${
          couponInfo.url
        }`,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          analyticsProvider.logEvent(events.generate_coupon.coupon_shared);
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      Alert.alert(error.message);
    }
  };

  return (
    <MainContainer style={globalStyles.centeredCon}>
      <Text
        numberOfLines={1}
        style={[fs.normalWeight, fs.largeSize, fs.blue, styles.label]}
      >
        {translate('GENERATE_COUPON.price')}
      </Text>
      <Text
        numberOfLines={1}
        style={[fs.boldWeight, fs.titleSize, fs.blue, styles.textInfo]}
      >
        {`$ ${couponInfo.amount}`}
      </Text>
      <Text
        numberOfLines={1}
        style={[fs.normalWeight, fs.largeSize, fs.blue, styles.label]}
      >
        {translate('GENERATE_COUPON.pointsToLoad')}
      </Text>
      <Text
        numberOfLines={1}
        style={[fs.boldWeight, fs.titleSize, fs.blue, styles.textInfo]}
      >
        {couponInfo.points}
      </Text>

      <ActionCard
        icon={<WhatsAppInstagramIcon />}
        title={translate('GENERATE_COUPON.sendToContactActionTitle')}
        description={translate(
          'GENERATE_COUPON.sendToContactActionDescription'
        )}
        onPress={onShare}
      />

      <ActionCard
        icon={<WhatsAppStarsIcon />}
        title={translate('GENERATE_COUPON.newContactActionTitle')}
        description={translate('GENERATE_COUPON.newContactActionDescription')}
        onPress={onNewContactPressHandler}
      />
    </MainContainer>
  );
};

export default StepAddCustomer;
