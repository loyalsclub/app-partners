/** External Dependencies */
import React from 'react';
import { Text, Linking } from 'react-native';

/** Components */
import { Controller, useForm } from 'react-hook-form';
import MainContainer from '../../Common/Layout/MainContainer.component';

/** Styles */
import translate from '../../../utils/language.utils';
import globalStyles from '../../../styles/global.styles';
import PhoneInputComponent from '../../Common/Form/PhoneInput.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';

const StepNewContact = ({
  couponInfo,
  store,
  storeCountries,
  onGoBackPressHandler,
}) => {
  const { control, handleSubmit, watch } = useForm({
    defaultValues: {
      phoneNumber: '',
    },
  });

  const phoneNumber = watch('phoneNumber');

  const onSubmit = () => {
    if (phoneNumber) {
      const message = `${translate('GENERATE_COUPON.loadPointsMessage')} \n ${
        couponInfo.url
      }`;

      Linking.openURL(`https://wa.me/${phoneNumber}?text=${message}`);

      onGoBackPressHandler();
    }
  };

  return (
    <MainContainer style={globalStyles.centeredCon}>
      <Text style={globalStyles.title}>
        {translate('GENERATE_COUPON.newContactTitle')}
      </Text>

      <Text style={globalStyles.secondaryTitle}>
        {translate('GENERATE_COUPON.newContactDescription')}
      </Text>

      <Controller
        name="phoneNumber"
        control={control}
        render={({ value, onChange }) => (
          <PhoneInputComponent
            label={translate('GENERATE_COUPON.newContactPlaceholder')}
            onChange={(newValue) => onChange(newValue.replace(/\s/g, ''))}
            value={value}
            countryCode={store.countryCode}
            storeCountries={storeCountries}
          />
        )}
      />

      <GradientButton
        text="Enviar"
        onPress={handleSubmit(onSubmit)}
        mode={phoneNumber ? 'active' : 'disabled'}
      />
    </MainContainer>
  );
};

export default StepNewContact;
