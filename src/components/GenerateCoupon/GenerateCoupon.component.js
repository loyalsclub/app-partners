/** External Dependencies */
import React from 'react';
import PropTypes from 'prop-types';
import { KeyboardAvoidingView, Platform, View } from 'react-native';

/** Components */
import GenericHeader, {
  getHeaderHeight,
} from '../Common/Miscellaneous/GenericHeader.component';
import PageHeaderContainer from '../Common/Layout/PageHeaderContainer.component';
import BackgroundPattern from '../Common/Miscellaneous/BackgroundPattern.component';
import Error from '../Common/Miscellaneous/Error.component';
import StepLoadPointsInput from '../Common/Steps/StepLoadPointsInput.component';
import StepCouponInfo from './Steps/StepCouponInfo.component';

/** Styles */
import globalStyles from '../../styles/global.styles';
import themeStyle from '../../styles/theme.style';
import StepNewContact from './Steps/StepNewContact.component';

const GenerateCoupon = ({
  goBackOnPress,
  STEPS,
  step,
  error,
  couponInfo,
  onPointsLoadedPressHandler,
  onNewContactPressHandler,
  loading,
  store,
  storeCountries,
}) => {
  const renderHeader = () => (
    <View style={{ backgroundColor: themeStyle.CREAM_WHITE }}>
      <GenericHeader goBackOnPress={goBackOnPress} mode="goBack" />
    </View>
  );

  const renderBackground = () => <BackgroundPattern type="golden" />;

  const renderContent = () => (
    <View>
      {step === STEPS.loadPointsInput ? (
        <KeyboardAvoidingView
          style={globalStyles.centeredCon}
          behavior={Platform.OS === 'ios' ? 'padding' : null}
        >
          <StepLoadPointsInput
            onPointsLoadedPressHandler={onPointsLoadedPressHandler}
          />
        </KeyboardAvoidingView>
      ) : null}

      {step === STEPS.couponInfo ? (
        <StepCouponInfo
          onNewContactPressHandler={onNewContactPressHandler}
          couponInfo={couponInfo}
        />
      ) : null}

      {step === STEPS.newContact ? (
        <StepNewContact
          onGoBackPressHandler={goBackOnPress}
          couponInfo={couponInfo}
          store={store}
          storeCountries={storeCountries}
        />
      ) : null}

      {step === STEPS.stepError ? (
        <Error
          onContinuePressHandler={goBackOnPress}
          mainText={error.mainText}
          secondaryText={error.secondaryText}
        />
      ) : null}
    </View>
  );

  return (
    <PageHeaderContainer
      style={{ scrollView: globalStyles.centeredCon }}
      keyboardShouldPersistTaps="always"
      renderHeader={renderHeader}
      renderContent={renderContent}
      renderBackground={renderBackground}
      headerHeight={getHeaderHeight()}
      refreshing={loading}
    />
  );
};

GenerateCoupon.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.shape({
    mainText: PropTypes.string,
    secondaryText: PropTypes.string,
  }),
  couponInfo: PropTypes.shape({
    couponCode: PropTypes.string,
    points: PropTypes.number,
    url: PropTypes.string,
    amount: PropTypes.number,
  }),
  step: PropTypes.string.isRequired,
  STEPS: PropTypes.object.isRequired,
  goBackOnPress: PropTypes.func.isRequired,
};

GenerateCoupon.defaultProps = {
  loading: false,
  couponInfo: null,
  error: {
    mainText: null,
    secondaryText: null,
  },
};

export default GenerateCoupon;
