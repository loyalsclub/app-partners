/** External Dependencies */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Image, View } from 'react-native';

/** Components */
import GenericHeader, {
  getHeaderHeight,
} from '../Common/Miscellaneous/GenericHeader.component';
import PageHeaderContainer from '../Common/Layout/PageHeaderContainer.component';
import BackgroundPattern from '../Common/Miscellaneous/BackgroundPattern.component';
import MainContainer from '../Common/Layout/MainContainer.component';

/** Styles */
import globalStyles from '../../styles/global.styles';
import themeStyle from '../../styles/theme.style';
import StepEmail from './Steps/StepEmail/StepEmail.component';
import StepPersonalInformation from './Steps/StepPersonalInformation/StepPersonalInformation.component';
import StepPassword from './Steps/StepPassword/StepPassword.component';
import StepRegisterError from './Steps/StepRegisterError/StepRegisterError.component';

const firstStepImage = require('../../assets/images/three-steps-form/first-step/img.png');
const secondStepImage = require('../../assets/images/three-steps-form/second-step/img.png');
const thirdStepImage = require('../../assets/images/three-steps-form/third-step/img.png');

const Register = ({
  goBackOnPress,
  onPasswordStepPressHandler,
  STEPS,
  step,
  loading,
  error,
  createPartnerUser,
  findPendingUser,
  clearError,
}) => {
  const [user, setUser] = useState({});

  const handleNextStepClick = (values) => {
    const updatedUser = { ...user, ...values };

    const isEmailStep = step === STEPS.email;
    const isPersonalInformation = step === STEPS.personal_information;
    const isPasswordStep = step === STEPS.password;

    if (isEmailStep) {
      findPendingUser(values.email);
    } else if (isPersonalInformation) {
      onPasswordStepPressHandler();
    } else if (isPasswordStep) {
      const { email, firstName, lastName, password } = updatedUser;

      createPartnerUser({
        email,
        firstName,
        lastName,
        password,
      });
    }

    setUser(updatedUser);
  };

  let stepNumber = firstStepImage;

  if (step === STEPS.personal_information) {
    stepNumber = secondStepImage;
  } else if (step === STEPS.password) {
    stepNumber = thirdStepImage;
  }

  return (
    <PageHeaderContainer
      style={{
        scrollView: { flexGrow: 1 },
        contentView: globalStyles.container,
      }}
      renderHeader={() => (
        <View style={{ backgroundColor: themeStyle.CREAM_WHITE }}>
          <GenericHeader goBackOnPress={goBackOnPress} mode="goBack" />
        </View>
      )}
      renderContent={() => (
        <MainContainer style={[{ flexGrow: 1 }]}>
          {step !== STEPS.error && (
            <View
              style={{
                paddingVertical: 15,
              }}
            >
              <Image
                source={stepNumber}
                resizeMode="contain"
                style={{
                  alignSelf: 'center',
                  height: 50,
                }}
              />
            </View>
          )}

          <View style={[globalStyles.centeredCon, { flexGrow: 1 }]}>
            {step === STEPS.email && (
              <StepEmail
                currentUser={user}
                onContinuePressHandler={handleNextStepClick}
                error={error}
                loading={loading}
                clearError={clearError}
              />
            )}

            {step === STEPS.personal_information && (
              <StepPersonalInformation
                currentUser={user}
                onContinuePressHandler={handleNextStepClick}
              />
            )}

            {step === STEPS.password && (
              <StepPassword
                currentUser={user}
                onContinuePressHandler={handleNextStepClick}
              />
            )}

            {step === STEPS.error && <StepRegisterError errorMessage={error} />}
          </View>
        </MainContainer>
      )}
      renderBackground={() => <BackgroundPattern type="golden" />}
      headerHeight={getHeaderHeight()}
      refreshing={loading}
    />
  );
};

Register.propTypes = {
  loading: PropTypes.bool,
  step: PropTypes.string.isRequired,
  STEPS: PropTypes.shape({
    [PropTypes.string]: PropTypes.string,
  }).isRequired,
  goBackOnPress: PropTypes.func.isRequired,
  createPartnerUser: PropTypes.func.isRequired,
  error: PropTypes.string,
};

Register.defaultProps = {
  loading: false,
  error: '',
};

export default Register;
