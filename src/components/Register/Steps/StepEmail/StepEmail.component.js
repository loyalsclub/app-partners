import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';
import { Text } from 'react-native';
import { yupResolver } from '@hookform/resolvers';
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import CustomTextInput from '../../../Common/Form/CustomTextInput.component';
import translate from '../../../../utils/language.utils';
import globalStyles from '../../../../styles/global.styles';

const schema = yup.object().shape({
  email: yup
    .string()
    .email(translate('FORMS_ERROR_MESSAGES.email'))
    .required(translate('FORMS_ERROR_MESSAGES.required')),
});

export default ({
  onContinuePressHandler,
  currentUser,
  error,
  loading,
  clearError,
}) => {
  const { control, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  return (
    <>
      <Text style={globalStyles.title}>Creá tu usuario</Text>

      <Text style={globalStyles.secondaryTitle}>
        Para comenzar a utilizar nuestra plataforma primero tenés que
        registrarte. ¡Sólo lleva un minuto!
      </Text>

      <Controller
        name="email"
        control={control}
        render={({ value, onChange }) => (
          <CustomTextInput
            value={value}
            inputProps={{
              defaultValue: currentUser.email || '',
              autoCorrect: false,
              autoCapitalize: 'none',
              autoCompleteType: 'email',
              textContentType: 'emailAddress',
              keyboardType: 'email-address',
            }}
            error={(errors.email && errors.email.message) || error}
            label="Email"
            onChange={(newValue) => {
              clearError();
              onChange(newValue);
            }}
          />
        )}
        defaultValue={currentUser.email || ''}
      />

      <GradientButton
        style={{ marginVertical: 15 }}
        mode={loading ? 'disabled' : 'active'}
        onPress={handleSubmit(onContinuePressHandler)}
        text="Continuar"
      />
    </>
  );
};
