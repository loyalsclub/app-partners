import React from 'react';
import PassiveNotification from '../../../Common/Miscellaneous/PassiveNotification.component';

export default ({ errorMessage }) => (
  <PassiveNotification
    type="genericError"
    secondaryText={
      errorMessage ||
      'Ha ocurrido un problema al crear tu usuario de Loyals Club. Intentá nuevamente en unos instantes por favor.'
    }
  />
);
