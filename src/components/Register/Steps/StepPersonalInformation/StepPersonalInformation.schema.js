import * as yup from 'yup';

const currentYear = new Date().getFullYear();
const minValidYear = currentYear - 100;
const maxValidYear = currentYear - 18;

const spanishrex = /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/g;

export const nameSchema = yup.string().required().matches(spanishrex);

export const bdaySchema = yup
  .number()
  .integer()
  .positive()
  .min(1)
  .max(31)
  .required();

export const bmonthSchema = yup
  .number()
  .integer()
  .positive()
  .min(1)
  .max(12)
  .required();

export const byearSchema = yup
  .number()
  .integer()
  .positive()
  .min(minValidYear)
  .max(maxValidYear)
  .required();
