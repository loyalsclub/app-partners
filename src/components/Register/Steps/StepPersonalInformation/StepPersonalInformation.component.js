import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Text, View } from 'react-native';
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import CustomTextInput from '../../../Common/Form/CustomTextInput.component';
import { nameSchema } from './StepPersonalInformation.schema';
import translate from '../../../../utils/language.utils';
import globalStyles from '../../../../styles/global.styles';

const invalidNameMessage = translate('FORMS_ERROR_MESSAGES.invalidField', {
  field: 'nombre',
});

const invalidSurnameMessage = translate('FORMS_ERROR_MESSAGES.invalidField', {
  field: 'apellido',
});

export default ({ onContinuePressHandler, currentUser }) => {
  const { control, handleSubmit, errors } = useForm({});

  return (
    <View>
      <Text style={globalStyles.title}>Completá tus datos</Text>

      <Controller
        name="firstName"
        control={control}
        as={
          <CustomTextInput
            inputProps={{
              defaultValue: currentUser.firstName || '',
              autoCorrect: false,
            }}
            error={errors.firstName && errors.firstName.message}
            label="Nombre"
          />
        }
        defaultValue={currentUser.firstName || ''}
        rules={{
          validate: async (value) =>
            (await nameSchema.isValid(value)) || invalidNameMessage,
        }}
      />

      <Controller
        name="lastName"
        control={control}
        as={
          <CustomTextInput
            inputProps={{
              defaultValue: currentUser.lastName || '',
              autoCorrect: false,
            }}
            error={errors.lastName && errors.lastName.message}
            label="Apellido"
          />
        }
        defaultValue={currentUser.lastName || ''}
        rules={{
          validate: async (value) =>
            (await nameSchema.isValid(value)) || invalidSurnameMessage,
        }}
      />

      <GradientButton
        style={{ marginVertical: 15 }}
        mode="active"
        onPress={handleSubmit(onContinuePressHandler)}
        text="Continuar"
      />
    </View>
  );
};
