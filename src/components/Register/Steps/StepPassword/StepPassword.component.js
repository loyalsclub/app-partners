import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Text } from 'react-native';
import { yupResolver } from '@hookform/resolvers';
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import CustomTextInput from '../../../Common/Form/CustomTextInput.component';
import schema from './StepPassword.schema';
import fontStyles from '../../../../styles/font.styles';
import globalStyles from '../../../../styles/global.styles';

export default ({ onContinuePressHandler, currentUser, error, loading }) => {
  const { control, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  return (
    <>
      <Text style={globalStyles.title}>Generá tu contraseña</Text>

      <Controller
        name="password"
        control={control}
        as={
          <CustomTextInput
            inputProps={{
              defaultValue: currentUser.password || '',
              autoCorrect: false,
              autoCapitalize: 'none',
              secureTextEntry: true,
            }}
            error={(errors.password && errors.password.message) || error}
            label="Contraseña"
          />
        }
        defaultValue={currentUser.password || ''}
      />

      <Controller
        name="confirmPassword"
        control={control}
        as={
          <CustomTextInput
            inputProps={{
              defaultValue: currentUser.confirmPassword || '',
              autoCorrect: false,
              autoCapitalize: 'none',
              secureTextEntry: true,
            }}
            error={
              (errors.confirmPassword && errors.confirmPassword.message) ||
              error
            }
            label="Confirmar Contraseña"
          />
        }
        defaultValue={currentUser.confirmPassword || ''}
      />

      <GradientButton
        style={{ marginVertical: 15 }}
        mode={loading ? 'disabled' : 'active'}
        onPress={handleSubmit(onContinuePressHandler)}
        text="Continuar"
      />
    </>
  );
};
