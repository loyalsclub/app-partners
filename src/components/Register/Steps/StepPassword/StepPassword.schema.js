import * as yup from 'yup';
import translate from '../../../../utils/language.utils';

const requiredFieldMessage = translate('FORMS_ERROR_MESSAGES.required');

export default yup.object().shape({
  password: yup
    .string()
    .min(6, translate('FORMS_ERROR_MESSAGES.charMin', { char: 6 }))
    .required(requiredFieldMessage),
  confirmPassword: yup
    .string()
    .required(requiredFieldMessage)
    .oneOf(
      [yup.ref('password'), null],
      translate('FORMS_ERROR_MESSAGES.confirmPassword')
    ),
});
