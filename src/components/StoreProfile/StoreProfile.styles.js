import themeStyle from '../../styles/theme.style';

export default {
  mainContainer: { marginTop: 40 },
  wrapper: { alignItems: 'center' },
  imageButton: { paddingHorizontal: 10 },
  profileImage: {
    height: 90,
    width: 90,
    borderRadius: 45,
    alignSelf: 'center',
  },
  editIcon: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    top: -10,
    left: 30,
    backgroundColor: themeStyle.CREAM_WHITE,
    padding: 8,
    zIndex: 1,
    borderRadius: 25,
    marginTop: -10,
    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: { width: 2.85, height: 2.85 },
    shadowOpacity: 0.25,
  },
};
