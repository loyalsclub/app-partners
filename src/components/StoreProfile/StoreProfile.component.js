import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import LayoutPage from '../Common/Layout/LayoutPage.component';
import MainContainer from '../Common/Layout/MainContainer.component';
import globalStyles from '../../styles/global.styles';
import IconComponent from '../Common/Miscellaneous/Icons.component';
import themeStyle from '../../styles/theme.style';
import EditableItem from '../Common/Miscellaneous/EditableItem/EditableItem.component';
import translate from '../../utils/language.utils';
import styles from './StoreProfile.styles';
import { STORE_PROPERTIES } from '../../config/constants';

const StoreProfile = ({
  store,
  storeCategories,
  storeCountries,
  loading,
  onEditPressHandler,
}) => {
  const { displayName: countryLabel } =
    storeCountries.find((country) => store.countryCode === country.name) || {};

  const { displayName: categoryLabel } =
    storeCategories.find((category) => store.category === category.name) || {};

  const socialMedia = store.socialMedia || {};

  const storeLocation =
    (store.StoreLocations &&
      store.StoreLocations.length &&
      store.StoreLocations[0]) ||
    {};

  return (
    <LayoutPage layout="top" loading={loading}>
      <MainContainer style={styles.mainContainer}>
        <View style={styles.wrapper}>
          <TouchableOpacity
            style={styles.imageButton}
            onPress={onEditPressHandler(STORE_PROPERTIES.profileImage)}
          >
            <Image
              source={{ uri: store.profileImage }}
              style={styles.profileImage}
            />

            <View style={styles.editIcon}>
              <IconComponent
                name="edit"
                color={themeStyle.BLUE_WOOD}
                size={15}
              />
            </View>
          </TouchableOpacity>

          <Text style={globalStyles.sectionPageTitle}>
            {translate('STORE_PROFILE.title')}
          </Text>

          <EditableItem
            title={translate(`STORE_PROFILE.${STORE_PROPERTIES.name}`)}
            onPress={onEditPressHandler(STORE_PROPERTIES.name)}
            value={store.name}
            style={{ marginTop: 40 }}
          />

          <EditableItem
            title={translate(`STORE_PROFILE.${STORE_PROPERTIES.description}`)}
            onPress={onEditPressHandler(STORE_PROPERTIES.description)}
            value={store.description}
          />

          <EditableItem
            title={translate(`STORE_PROFILE.${STORE_PROPERTIES.category}`)}
            onPress={onEditPressHandler(STORE_PROPERTIES.category)}
            value={categoryLabel}
          />

          <EditableItem
            title={translate(`STORE_PROFILE.${STORE_PROPERTIES.country}`)}
            value={countryLabel}
            editable={false}
          />

          <EditableItem
            title={translate(`STORE_PROFILE.${STORE_PROPERTIES.alias}`)}
            onPress={onEditPressHandler(STORE_PROPERTIES.alias)}
            value={store.alias}
            editable={false}
          />

          <EditableItem
            title={translate(`STORE_PROFILE.${STORE_PROPERTIES.facebook}`)}
            onPress={onEditPressHandler(STORE_PROPERTIES.facebook)}
            value={socialMedia.facebook}
          />

          <EditableItem
            title={translate(`STORE_PROFILE.${STORE_PROPERTIES.twitter}`)}
            onPress={onEditPressHandler(STORE_PROPERTIES.twitter)}
            value={socialMedia.twitter}
          />

          <EditableItem
            title={translate(`STORE_PROFILE.${STORE_PROPERTIES.instagram}`)}
            onPress={onEditPressHandler(STORE_PROPERTIES.instagram)}
            value={socialMedia.instagram}
          />

          <EditableItem
            title={translate(`STORE_PROFILE.${STORE_PROPERTIES.whatsapp}`)}
            onPress={onEditPressHandler(STORE_PROPERTIES.whatsapp)}
            value={socialMedia.whatsapp}
          />

          <EditableItem
            title={translate(`STORE_PROFILE.${STORE_PROPERTIES.website}`)}
            onPress={onEditPressHandler(STORE_PROPERTIES.website)}
            value={storeLocation.website}
          />

          <EditableItem
            title={translate(`STORE_PROFILE.${STORE_PROPERTIES.phone}`)}
            onPress={onEditPressHandler(STORE_PROPERTIES.phone)}
            value={storeLocation.phone}
          />

          <EditableItem
            title={translate(`STORE_PROFILE.${STORE_PROPERTIES.address}`)}
            onPress={onEditPressHandler(STORE_PROPERTIES.address)}
            value={storeLocation.address}
          />
        </View>
      </MainContainer>
    </LayoutPage>
  );
};

StoreProfile.propTypes = {
  storeCategories: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
    })
  ).isRequired,
  storeCountries: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
      conversionRate: PropTypes.number.isRequired,
    })
  ).isRequired,
  store: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  onEditPressHandler: PropTypes.func.isRequired,
};

export default StoreProfile;
