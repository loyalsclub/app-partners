/** External Dependencies */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, Dimensions } from 'react-native';
import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';

/** Components */
import GenericHeader, {
  getHeaderHeight,
} from '../Common/Miscellaneous/GenericHeader.component';
import MainContainer from '../Common/Layout/MainContainer.component';
import GradientButton from '../Common/Miscellaneous/GradientButton.component';
import PassiveNotification from '../Common/Miscellaneous/PassiveNotification.component';
import { getTabbarHeight } from '../Common/Layout/Tabbar.component';

/** Styles */
import themeStyle from '../../styles/theme.style';
import fontStyle from '../../styles/font.styles';
import globalStyles from '../../styles/global.styles';
import CustomActivityIndicator from '../Common/Miscellaneous/ActivityIndicator.component';
import { analyticsProvider, events } from '../../providers/analytics';

// For Android the window dimension will exclude the size used by
// the status bar (if not translucent) and bottom navigation bar
const SCREEN_HEIGHT = Dimensions.get('window').height;

const SCREEN_WIDTH = Dimensions.get('window').width;
const MESSAGE_SECTION_HEIGHT = 60;
const ACTION_SECTION_HEIGHT = 49;

const QR_SQUARE_RATIO = 0.8;
const QR_SQUARE =
  SCREEN_WIDTH * QR_SQUARE_RATIO >= 400 ? 400 : SCREEN_WIDTH * QR_SQUARE_RATIO;

const HEADER_TOTAL = getHeaderHeight() + MESSAGE_SECTION_HEIGHT;
const SQUARE_MARGINS_H = (SCREEN_WIDTH - QR_SQUARE) / 2;
const SQUARE_MARGINS_V =
  (SCREEN_HEIGHT - QR_SQUARE - HEADER_TOTAL - getTabbarHeight()) / 2;

export default class QrScanner extends Component {
  constructor() {
    super();
    this.state = {
      hasCameraPermission: null,
      scanned: false,
    };
  }

  async componentDidMount() {
    await this.requestPermission();
    setTimeout(() => {
      this.setState((state) => ({ ...state, loaded: true }));
    }, 500);
  }

  get noPermissionScreen() {
    const { hasCameraPermission } = this.state;
    const {
      goBackOnPress,
      actionButtonMessage,
      onActionButtonPress,
    } = this.props;
    return (
      <View style={globalStyles.container}>
        <GenericHeader goBackOnPress={goBackOnPress} />
        {hasCameraPermission !== null && (
          <MainContainer style={globalStyles.centeredCon}>
            <PassiveNotification
              type="genericError"
              secondaryText={
                'Parece que no tenemos permisos para acceder a la cámara. \n En caso de que el botón "Solicitar Permisos" no funcione, accedé a las preferencias del dispositivo.'
              }
            />
            <GradientButton
              style={{ marginVertical: 15 }}
              mode="active"
              onPress={this.requestPermission}
              text="Solicitar Permisos"
            />
            <GradientButton
              style={{ marginVertical: 15 }}
              mode="dark"
              text={actionButtonMessage}
              onPress={onActionButtonPress}
            />
          </MainContainer>
        )}
        {hasCameraPermission === null && <CustomActivityIndicator loading />}
      </View>
    );
  }

  get qrScannerContent() {
    const { scanned, loaded } = this.state;
    const {
      message,
      actionButton,
      actionButtonMessage,
      onActionButtonPress,
      goBackOnPress,
    } = this.props;

    return (
      <View style={styles.mainContainer}>
        {!loaded && <CustomActivityIndicator loading />}

        {loaded && (
          <BarCodeScanner
            barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
            onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
            style={styles.barCodeComponent}
          />
        )}

        {/* LEFT BACKGROUND SIDE        */}
        {loaded && (
          <View
            style={[
              styles.horizontalScannerBackground,
              { left: 0, right: QR_SQUARE + SQUARE_MARGINS_H },
            ]}
          />
        )}

        {/* RIGHT BACKGROUND SIDE        */}
        {loaded && (
          <View
            style={[
              styles.horizontalScannerBackground,
              { right: 0, left: QR_SQUARE + SQUARE_MARGINS_H },
            ]}
          />
        )}

        {/* BOTTOM BACKGROUND SIDE        */}
        {loaded && (
          <View
            style={[
              styles.verticalScannerBackground,
              { top: HEADER_TOTAL + SQUARE_MARGINS_V + QR_SQUARE, bottom: 0 },
            ]}
          />
        )}

        {/* TOP BACKGROUND SIDE        */}
        {loaded && (
          <View
            style={[
              styles.verticalScannerBackground,
              { top: HEADER_TOTAL, bottom: SQUARE_MARGINS_V + QR_SQUARE },
            ]}
          />
        )}

        {/* SQUARE        */}
        {loaded && <View style={styles.centeredSquare} />}

        {/* ACTION SECTION */}
        {loaded && actionButton && (
          <View style={styles.bottomActionSection}>
            <GradientButton
              mode="activeWhite"
              text={actionButtonMessage}
              onPress={onActionButtonPress}
            />
          </View>
        )}

        <View style={styles.messageSection}>
          <Text
            style={[
              fontStyle.normalWeight,
              fontStyle.center,
              fontStyle.largeSize,
              {
                alignSelf: 'center',
              },
            ]}
          >
            {message}
          </Text>
        </View>
        <View style={styles.headerContainer}>
          <GenericHeader goBackOnPress={goBackOnPress} />
        </View>
      </View>
    );
  }

  handleBarCodeScanned = ({ data }) => {
    this.setState({ scanned: true });

    const { onBarCodeScanned } = this.props;
    onBarCodeScanned(data);
    analyticsProvider.logEvent(events.load_profile.qr);
  };

  requestPermission = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  };

  render() {
    const { hasCameraPermission } = this.state;

    if (!hasCameraPermission) {
      return this.noPermissionScreen;
    }

    return this.qrScannerContent;
  }
}

QrScanner.propTypes = {
  message: PropTypes.string,
  actionButton: PropTypes.bool,
  onActionButtonPress: PropTypes.func,
  actionButtonMessage: PropTypes.string,
  onBarCodeScanned: PropTypes.func.isRequired,
  goBackOnPress: PropTypes.func.isRequired,
};

QrScanner.defaultProps = {
  message: 'Escaneá el código QR',
  actionButtonMessage: '',
  actionButton: false,
  onActionButtonPress: () => {},
};

const styles = {
  mainContainer: {
    backgroundColor: themeStyle.CREAM_WHITE,
    flex: 1,
  },
  barCodeComponent: {
    position: 'absolute',
    top: 0,
    bottom: -50,
    left: 0,
    right: 0,
  },
  headerContainer: {
    elevation: 1,
    shadowColor: themeStyle.GREY_OP,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 0.3,
    position: 'absolute',
    height: getHeaderHeight(),
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: themeStyle.CREAM_WHITE,
  },
  messageSection: {
    position: 'absolute',
    top: getHeaderHeight(),
    height: MESSAGE_SECTION_HEIGHT,
    left: 0,
    right: 0,
    backgroundColor: themeStyle.CREAM_WHITE,
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  horizontalScannerBackground: {
    position: 'absolute',
    top: HEADER_TOTAL,
    bottom: 0,
    backgroundColor: 'rgba(221,141,0,0.4)',
  },
  verticalScannerBackground: {
    position: 'absolute',
    right: SQUARE_MARGINS_H,
    left: SQUARE_MARGINS_H,
    backgroundColor: 'rgba(221,141,0,0.4)',
  },
  bottomActionSection: {
    position: 'absolute',
    height: ACTION_SECTION_HEIGHT,
    left: 0,
    right: 0,
    // We place it right on the middle of the QR BOX and the bottom.
    bottom: (SQUARE_MARGINS_V - ACTION_SECTION_HEIGHT) / 2,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  centeredSquare: {
    position: 'absolute',
    top: HEADER_TOTAL + SQUARE_MARGINS_V,
    left: SQUARE_MARGINS_H,
    right: SQUARE_MARGINS_H,
    bottom: SQUARE_MARGINS_V,
    borderColor: themeStyle.CREAM_WHITE,
    borderWidth: 1,
  },
};
