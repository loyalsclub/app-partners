/** External Dependencies */
import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Alert } from 'react-native';
import { connectActionSheet } from '@expo/react-native-action-sheet';

/** Components */
import GenericHeader, {
  getHeaderHeight,
} from '../Common/Miscellaneous/GenericHeader.component';
import PageHeaderContainer from '../Common/Layout/PageHeaderContainer.component';
import BackgroundPattern from '../Common/Miscellaneous/BackgroundPattern.component';
import RewardsList from '../Common/Miscellaneous/RewardsList.component';
import PassiveNotification from '../Common/Miscellaneous/PassiveNotification.component';
import RewardsAddComponent from './RewardsAdd.component';

/** Styles */
import globalStyles from '../../styles/global.styles';
import fontStyles from '../../styles/font.styles';

const RewardsComponent = (props) => {
  const {
    loading,
    error,
    refetch,
    currentReward,
    goBackOnPress,
    rewardsData,
    onAddRewardPressHandler,
    deleteReward,
    currentStep,
    STEPS,
    currentStepRE,
    STEPS_RE,
    onContinuePressHandler,
    showActionSheetWithOptions,
    onLevelSelected,
  } = props;

  const renderHeader = () => (
    <GenericHeader
      goBackOnPress={goBackOnPress}
      mode="goBack"
      customAction={
        currentStep === STEPS.showRewards ? onAddRewardPressHandler : undefined
      }
      customActionIcon={currentStep === STEPS.showRewards ? 'add' : undefined}
    />
  );

  const renderBackground = () => <BackgroundPattern type="golden" />;

  const renderError = () => (
    <PassiveNotification
      type="genericError"
      secondaryText={error.secondaryText}
    />
  );

  const triggerActionSheet = (rewardId) => {
    const options = ['Eliminar', 'Editar', 'Cancelar'];
    const destructiveButtonIndex = 0;
    const cancelButtonIndex = 2;

    showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex,
        destructiveButtonIndex,
      },
      (buttonIndex) => {
        if (buttonIndex === 0) return triggerDelete(rewardId);
        if (buttonIndex === 1) return onAddRewardPressHandler(rewardId);
        return true;
      }
    );
  };

  const triggerDelete = (rewardId) => {
    const rewardToUse = rewardsData
      .flatMap((rewardLevel) => rewardLevel.StoreRewards)
      .filter((reward) => reward.id === rewardId)[0];

    Alert.alert(
      'Eliminar beneficio',
      `Desea eliminar el beneficio \n"${rewardToUse.title}" ?`,
      [
        {
          text: 'Cancelar',
          style: 'cancel',
        },
        { text: 'OK', onPress: () => deleteReward(rewardId) },
      ],
      { cancelable: true }
    );
  };

  const renderContent = () => {
    if (currentStep === STEPS.showRewards) {
      return (
        <View>
          <View style={globalStyles.row}>
            <Text
              style={[
                fontStyles.xlargeSize,
                fontStyles.blue,
                fontStyles.boldWeight,
              ]}
            >
              Beneficios
            </Text>
          </View>

          <RewardsList
            rewardsData={rewardsData}
            actionType="edit"
            onAction={(rewardId) => () => triggerActionSheet(rewardId)}
          />
        </View>
      );
    }

    if (currentStep === STEPS.editReward) {
      return (
        <RewardsAddComponent
          currentStepRE={currentStepRE}
          rewardsData={rewardsData}
          currentReward={currentReward}
          onLevelSelected={onLevelSelected}
          STEPS_RE={STEPS_RE}
          onContinuePressHandler={onContinuePressHandler}
        />
      );
    }
    return null;
  };

  return (
    <PageHeaderContainer
      error={error != null}
      style={{ scrollView: globalStyles.centerScrollView }}
      renderHeader={renderHeader}
      renderContent={renderContent}
      renderBackground={renderBackground}
      renderError={renderError}
      headerHeight={getHeaderHeight()}
      refreshing={loading}
      onRefresh={refetch}
    />
  );
};

RewardsComponent.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.shape({
    text: PropTypes.string,
  }),
};

RewardsComponent.defaultProps = {
  error: null,
};

export default connectActionSheet(RewardsComponent);
