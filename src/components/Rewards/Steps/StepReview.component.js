/** External Dependencies */
import React from 'react';
import { Text } from 'react-native';

/** Components */
import MainContainer from '../../Common/Layout/MainContainer.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';
import RewardCard from '../../Common/Miscellaneous/RewardCard.component';

/** Styles */
import fontStyles from '../../../styles/font.styles';

const StepReview = (props) => {
  const { onContinuePressHandler, rewardsData, currentReward } = props;

  return (
    <MainContainer>
      <Text
        style={[
          fontStyles.xlargeSize,
          fontStyles.center,
          fontStyles.blue,
          fontStyles.normalWeight,
          { marginBottom: 12 },
        ]}
      >
        Así verán tus clientes el beneficio
      </Text>
      <Text
        style={[
          fontStyles.mediumSize,
          fontStyles.center,
          fontStyles.blue,
          fontStyles.normalWeight,
          { marginBottom: 12 },
        ]}
      >
        Recordá que modificar tus beneficios con frecuencia puede traer
        consecuencias negativas en la experiencia de tus clientes.
      </Text>
      <RewardCard
        level={
          rewardsData.find((reward) => reward.id === currentReward.storeLevelId)
            .level
        }
        StoreReward={{
          title: currentReward.title,
          description: currentReward.description,
          UsageFrequency: {
            type: currentReward.usageFrequencyType,
            value: currentReward.usageFrequencyValue,
          },
        }}
      />
      <GradientButton
        style={{ marginVertical: 15, alignSelf: 'center' }}
        mode="active"
        onPress={onContinuePressHandler}
        text="Finalizar"
      />
    </MainContainer>
  );
};

StepReview.propTypes = {};

export default StepReview;
