/** External Dependencies */
import React from 'react';
import { Text } from 'react-native';
/** Components */
import MainContainer from '../../Common/Layout/MainContainer.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';
import RadioLevelButtons from '../../Common/Miscellaneous/RadioLevelButtons.component';

/** Styles */
import fontStyles from '../../../styles/font.styles';

const StepSelectLevel = (props) => {
  const {
    onContinuePressHandler,
    rewardsData,
    currentReward,
    onLevelSelected,
  } = props;

  return (
    <MainContainer>
      <Text
        style={[
          fontStyles.xlargeSize,
          fontStyles.center,
          fontStyles.blue,
          fontStyles.normalWeight,
          { marginBottom: 12 },
        ]}
      >
        Seleccioná el nivel para el beneficio
      </Text>

      <RadioLevelButtons
        rewardsData={rewardsData}
        currentLevel={currentReward.storeLevelId}
        onLevelSelected={onLevelSelected}
        onPress={(id) => () => onLevelSelected(id)}
      />

      <GradientButton
        style={{ marginVertical: 15, alignSelf: 'center' }}
        mode={currentReward.storeLevelId ? 'active' : 'disabled'}
        onPress={onContinuePressHandler}
        text="Continuar"
      />
    </MainContainer>
  );
};

StepSelectLevel.propTypes = {};

export default StepSelectLevel;
