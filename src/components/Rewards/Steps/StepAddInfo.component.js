/**  */
import React, { useRef } from 'react';
import { View, Text } from 'react-native';
import * as yup from 'yup';
import { useForm, Controller } from 'react-hook-form';

/** Components */
import CustomTextInput from '../../Common/Form/CustomTextInput.component';
import CustomPicker from '../../Common/Form/CustomPicker.component';
import CustomCheckBoxes from '../../Common/Form/CustomCheckBoxes.component';
import GradientButton from '../../Common/Miscellaneous/GradientButton.component';

/** Constants */
import { USAGE_FREQUENCIES } from '../../../config/constants';

/** Styles */
import globalStyles from '../../../styles/global.styles';
import fontStyles from '../../../styles/font.styles';

const StepAddInfo = ({ onContinuePressHandler, currentReward }) => {
  const schema = yup.number().positive().integer().max(365);

  const {
    control,
    clearErrors,
    handleSubmit,
    watch,
    setValue,
    errors,
  } = useForm({
    mode: 'onChange',
  });

  const FTVref = useRef(null);
  const USAGE_FREQUENCIES_TYPES = [
    {
      value: USAGE_FREQUENCIES.FOREVER,
      label: 'Siempre disponible',
    },
    {
      value: USAGE_FREQUENCIES.ONE_TIME,
      label: 'Uso único',
    },
    {
      value: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      label: 'Determinados días de la semana',
    },
    {
      value: USAGE_FREQUENCIES.EVERY_CERTAIN_DAYS,
      label: 'Intervalo de días',
    },
    {
      value: USAGE_FREQUENCIES.EVERY_CERTAIN_HOURS,
      label: 'Intervalo de horas',
    },
  ];

  const watchFT = watch(
    'usageFrequencyType',
    currentReward.usageFrequencyType || USAGE_FREQUENCIES.FOREVER
  ); // you can supply default value as second argument

  return (
    <View style={globalStyles.centeredCon}>
      <Text
        style={[
          fontStyles.xlargeSize,
          fontStyles.center,
          fontStyles.blue,
          fontStyles.normalWeight,
          { marginBottom: 12 },
        ]}
      >
        Información del beneficio
      </Text>
      <Controller
        control={control}
        as={
          <CustomTextInput
            inputProps={{
              multiline: true,
              maxLength: 60,
              defaultValue: currentReward.title || '',
              autoCorrect: false,
            }}
            resize
            minLines={1}
            maxLines={4}
            error={errors.title && errors.title.message}
            charCounter
            label="Título"
          />
        }
        name="title"
        defaultValue={currentReward.title || ''}
        rules={{ required: 'Campo requerido' }}
      />
      <Controller
        control={control}
        as={
          <CustomTextInput
            inputProps={{
              multiline: true,
              maxLength: 100,
              defaultValue: currentReward.description || '',
              autoCorrect: false,
            }}
            resize
            minLines={2}
            maxLines={4}
            error={errors.description && errors.description.message}
            charCounter
            label="Descripción"
          />
        }
        name="description"
        defaultValue={currentReward.description || ''}
      />
      <Controller
        control={control}
        render={({ onBlur, onChange, value }) => {
          const customOnChange = (val) => {
            if (val !== value) {
              setValue('usageFrequencyValue', '');
              clearErrors('usageFrequencyValue');
              if (FTVref && FTVref.current) FTVref.current.clear();
            }
            onChange(val);
          };

          return (
            <CustomPicker
              onBlur={onBlur}
              value={value}
              onChange={customOnChange}
              label="Disponibilidad"
              items={USAGE_FREQUENCIES_TYPES}
            />
          );
        }}
        name="usageFrequencyType"
        rules={{ required: true }}
        defaultValue={
          currentReward.usageFrequencyType || USAGE_FREQUENCIES.FOREVER
        }
      />
      <Controller
        control={control}
        render={(props) => {
          const noValue =
            watchFT === USAGE_FREQUENCIES.FOREVER ||
            watchFT === USAGE_FREQUENCIES.ONE_TIME;
          const pickerValue = watchFT === USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS;
          if (noValue) return null;

          if (pickerValue) {
            const customOnChange = (value) => {
              const newArr = Object.keys(value).filter((key) => value[key]);
              props.onChange(newArr.join(','));
            };

            const objVal =
              props.value &&
              props.value.split(',').reduce((a, b) => {
                const ret = ((a[b] = true), a);
                return ret;
              }, {});

            return (
              <CustomCheckBoxes
                label="Días de la semana"
                value={objVal}
                onChange={customOnChange}
                error={
                  errors.usageFrequencyValue
                    ? errors.usageFrequencyValue.message
                    : undefined
                }
                items={[
                  { value: '1', label: 'Lunes' },
                  { value: '2', label: 'Martes' },
                  { value: '3', label: 'Miércoles' },
                  { value: '4', label: 'Jueves' },
                  { value: '5', label: 'Viernes' },
                  { value: '6', label: 'Sabado' },
                  { value: '7', label: 'Domingo' },
                ]}
              />
            );
          }

          return (
            <CustomTextInput
              inputProps={{
                keyboardType: 'numeric',
                returnKeyType: 'done',
                defaultValue:
                  currentReward.usageFrequencyType === watchFT
                    ? currentReward.usageFrequencyValue
                    : '',
              }}
              {...props}
              ref={FTVref}
              error={
                errors.usageFrequencyValue
                  ? errors.usageFrequencyValue.message
                  : undefined
              }
              label={
                watchFT === USAGE_FREQUENCIES.EVERY_CERTAIN_DAYS
                  ? '¿ Cada cuántos días ?'
                  : '¿ Cada cuántas horas ?'
              }
            />
          );
        }}
        name="usageFrequencyValue"
        defaultValue={currentReward.usageFrequencyValue || ''}
        rules={{
          validate: async (value) => {
            /** Si la opcion es certain_weeks verificamos que selecciono mirando
             * el largo del string
             */
            if (
              watchFT === USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS &&
              value.length <= 0
            )
              return 'Elija al menos 1 día de la semana';

            /** Si la opcion es EVERY_CERTAIN_DAYS o EVERY_CERTAIN_HOURS verificamos
             * que puso un valor valido
             */
            if (
              (watchFT === USAGE_FREQUENCIES.EVERY_CERTAIN_DAYS ||
                watchFT === USAGE_FREQUENCIES.EVERY_CERTAIN_HOURS) &&
              !(await schema.isValid(value))
            )
              return 'Ingrese un número válido';

            return true;
          },
        }}
      />

      <GradientButton
        style={{ marginVertical: 15 }}
        mode="active"
        onPress={handleSubmit(onContinuePressHandler)}
        text="Continuar"
      />
    </View>
  );
};

StepAddInfo.propTypes = {};

export default StepAddInfo;
