/** External Dependencies */
import React from 'react';
import PropTypes from 'prop-types';

/** Components */
import StepSelectLevel from './Steps/StepSelectLevel.component';
import StepAddInfo from './Steps/StepAddInfo.component';
import StepReview from './Steps/StepReview.component';

const RewardsAddComponent = (props) => {
  const {
    currentStepRE,
    STEPS_RE,
    onLevelSelected,
    onContinuePressHandler,
    rewardsData,
    currentReward,
  } = props;

  if (currentStepRE === STEPS_RE.selectLevel) {
    return (
      <StepSelectLevel
        currentReward={currentReward}
        rewardsData={rewardsData}
        onContinuePressHandler={onContinuePressHandler}
        onLevelSelected={onLevelSelected}
      />
    );
  }

  if (currentStepRE === STEPS_RE.addInfo) {
    return (
      <StepAddInfo
        currentReward={currentReward}
        onContinuePressHandler={onContinuePressHandler}
      />
    );
  }

  if (currentStepRE === STEPS_RE.reviewReward) {
    return (
      <StepReview
        currentReward={currentReward}
        rewardsData={rewardsData}
        onContinuePressHandler={onContinuePressHandler}
      />
    );
  }
};

RewardsAddComponent.propTypes = {
  onContinuePressHandler: PropTypes.func.isRequired,
  currentStepRE: PropTypes.string.isRequired,
};

RewardsAddComponent.defaultProps = {};

export default RewardsAddComponent;
