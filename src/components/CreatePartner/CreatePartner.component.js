/** External Dependencies */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

/** Components */
import GenericHeader, {
  getHeaderHeight,
} from '../Common/Miscellaneous/GenericHeader.component';
import PageHeaderContainer from '../Common/Layout/PageHeaderContainer.component';
import BackgroundPattern from '../Common/Miscellaneous/BackgroundPattern.component';
import StoreProfileImageForm from './Steps/StoreProfileImageForm/StoreProfileImageForm.component';
import StoreInformationForm from './Steps/StoreInformationForm/StoreInformationForm.component';
import StorePointsForm from './Steps/StorePointsForm/StorePointsForm.component';
import StoreReview from './Steps/StoreReview/StoreReview.component';
import StoreSuccess from './Steps/StoreSuccess/StoreSuccess.component';
import StoreError from './Steps/StoreError/StoreError.component';
import StoreContactForm from './Steps/StoreContactForm/StoreContactForm.component';

/** Styles */
import globalStyles from '../../styles/global.styles';
import themeStyle from '../../styles/theme.style';

/** Utils */
import { calculateLevelsPoints } from '../../utils/stores.utils';
import { generateRNFile } from '../../utils/files.utils';
import { analyticsProvider, events } from '../../providers/analytics';

const CreatePartner = ({
  goBackOnPress,
  onInformationStepPressHandler,
  onPointsStepPressHandler,
  onReviewStepPressHandler,
  onFinishCreationPressHandler,
  STEPS,
  step,
  loading,
  error,
  storeCountries,
  storeCategories,
  storeLevelsAndRewardsById,
  createStore,
  onContactStepPressHandler,
  createdStore,
}) => {
  const [store, setStore] = useState({});

  const handleNextStepClick = (values) => {
    const updatedStore = { ...store, ...values };

    const isFirstStep = step === STEPS.profileImage;
    const isSecondStep = step === STEPS.information;
    const isThirdStep = step === STEPS.contact;
    const isFourthStep = step === STEPS.points;
    const isFifthStep = step === STEPS.review;

    let nextStepHandler = () => {};

    if (isFirstStep) {
      nextStepHandler = onInformationStepPressHandler;

      analyticsProvider.logEvent(events.partner_registration.logo_completed);
    } else if (isSecondStep) {
      updatedStore.personalPhone =
        store.countryCode !== values.countryCode ? '' : store.personalPhone;

      nextStepHandler = onContactStepPressHandler;

      analyticsProvider.logEvent(
        events.partner_registration.store_information_completed
      );
    } else if (isThirdStep) {
      nextStepHandler = onPointsStepPressHandler;

      analyticsProvider.logEvent(
        events.partner_registration.contact_phone_completed
      );
    } else if (isFourthStep) {
      const selectedCountry = storeCountries.find(
        (country) => country.name === updatedStore.countryCode
      );

      const points = calculateLevelsPoints(
        updatedStore.spendingTimes,
        updatedStore.spendingAmount,
        selectedCountry
      );

      updatedStore.points = {
        level2: points.level2.toString(),
        level3: points.level3.toString(),
      };

      nextStepHandler = onReviewStepPressHandler;

      analyticsProvider.logEvent(
        events.partner_registration.spending_information_completed
      );
    } else if (isFifthStep) {
      const {
        name,
        description,
        category,
        countryCode,
        points: { level2, level3 },
        profileImage,
        personalPhone,
        spendingAmount,
        spendingTimes,
      } = updatedStore;

      createStore({
        name,
        description,
        category,
        countryCode,
        level2,
        level3,
        profileImage: generateRNFile(profileImage),
        StoreSetting: {
          personalPhone,
          spendingTimes,
          spendingAmount,
        },
      });
    }

    setStore(updatedStore);
    nextStepHandler();
  };

  return (
    <PageHeaderContainer
      style={{ scrollView: globalStyles.centerScrollView }}
      renderHeader={() => (
        <View style={{ backgroundColor: themeStyle.CREAM_WHITE }}>
          <GenericHeader
            goBackOnPress={goBackOnPress}
            mode={step === STEPS.success ? 'logoOnly' : 'goBack'}
          />
        </View>
      )}
      renderContent={() => (
        <>
          {step === STEPS.profileImage && (
            <StoreProfileImageForm
              store={store}
              handleNextStepClick={handleNextStepClick}
            />
          )}

          {step === STEPS.information && (
            <StoreInformationForm
              storeCategories={storeCategories}
              storeCountries={storeCountries}
              store={store}
              handleNextStepClick={handleNextStepClick}
            />
          )}

          {step === STEPS.contact && (
            <StoreContactForm
              store={store}
              storeCountries={storeCountries}
              handleNextStepClick={handleNextStepClick}
            />
          )}

          {step === STEPS.points && (
            <StorePointsForm
              store={store}
              handleNextStepClick={handleNextStepClick}
            />
          )}

          {step === STEPS.review && (
            <StoreReview
              storeCategories={storeCategories}
              storeCountries={storeCountries}
              store={store}
              submitStore={handleNextStepClick}
            />
          )}

          {step === STEPS.success && (
            <StoreSuccess
              store={createdStore || {}}
              storeLevelsAndRewardsById={storeLevelsAndRewardsById}
              onFinishCreationPressHandler={onFinishCreationPressHandler}
            />
          )}

          {step === STEPS.error && <StoreError errorMessage={error} />}
        </>
      )}
      renderBackground={() => <BackgroundPattern type="golden" />}
      headerHeight={getHeaderHeight()}
      refreshing={loading}
    />
  );
};

CreatePartner.propTypes = {
  loading: PropTypes.bool,
  step: PropTypes.string.isRequired,
  STEPS: PropTypes.shape({
    profileImage: PropTypes.string.isRequired,
    information: PropTypes.string.isRequired,
    contact: PropTypes.string.isRequired,
    points: PropTypes.string.isRequired,
    review: PropTypes.string.isRequired,
    success: PropTypes.string.isRequired,
    error: PropTypes.string.isRequired,
  }).isRequired,
  goBackOnPress: PropTypes.func.isRequired,
  onInformationStepPressHandler: PropTypes.func.isRequired,
  onContactStepPressHandler: PropTypes.func.isRequired,
  onPointsStepPressHandler: PropTypes.func.isRequired,
  onReviewStepPressHandler: PropTypes.func.isRequired,
  onFinishCreationPressHandler: PropTypes.func.isRequired,
  storeCountries: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
    })
  ).isRequired,
  storeCategories: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
    })
  ).isRequired,
  createStore: PropTypes.func.isRequired,
  error: PropTypes.string,
};

CreatePartner.defaultProps = {
  loading: false,
  error: null,
};

export default CreatePartner;
