import React from 'react';
import PassiveNotification from '../../../Common/Miscellaneous/PassiveNotification.component';

export default ({ errorMessage }) => (
  <PassiveNotification
    type="genericError"
    secondaryText={
      errorMessage ||
      'Ha ocurrido un problema al crear tu Club de Clientes. Intentá nuevamente en unos instantes, por favor.'
    }
  />
);
