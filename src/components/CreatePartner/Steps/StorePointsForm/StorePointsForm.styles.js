export const stylesheet = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  picker: {
    height: 20,
    width: 80,
  },
};
