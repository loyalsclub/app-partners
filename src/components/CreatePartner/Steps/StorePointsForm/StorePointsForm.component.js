import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import { yupResolver } from '@hookform/resolvers';
import { useForm, Controller } from 'react-hook-form';
import * as yup from 'yup';
import { TextInputMask } from 'react-native-masked-text';

/** Components */
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import MainContainer from '../../../Common/Layout/MainContainer.component';
import FormGroupComponent from '../../../Common/Form/FormGroup.component';

/** Styles */
import fontStyles from '../../../../styles/font.styles';
import globalStyles from '../../../../styles/global.styles';
import themeStyle from '../../../../styles/theme.style';

/** Constants */
import FormErrorComponent from '../../../Common/Form/FormError.component';

const schema = yup.object().shape({
  spendingAmount: yup
    .number()
    .typeError('El monto debe ser numérico')
    .required('Campo requerido'),
});

const parseValue = (value) =>
  value.replace('$', '').replace('.', '').replace(',', '');

const StorePointsForm = ({ handleNextStepClick, store }) => {
  const { control, handleSubmit, watch, errors } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      spendingAmount: store.spendingAmount || '',
    },
  });

  const values = watch();

  const isFormValid = Boolean(values.spendingAmount);

  const submitWithPendingTimesMonthly = (formValues) => {
    handleNextStepClick({ ...formValues, spendingTimes: '12' });
  };

  return (
    <MainContainer style={globalStyles.centeredCon}>
      <Text
        style={[
          fontStyles.xlargeSize,
          fontStyles.center,
          fontStyles.blue,
          fontStyles.normalWeight,
          { marginBottom: 12 },
        ]}
      >
        Tu cliente regular
      </Text>

      <Text style={globalStyles.secondaryTitle}>
        ¿Cuánto gasta por mes en tu comercio?
      </Text>

      <Controller
        name="spendingAmount"
        control={control}
        render={({ value, onChange }) => {
          let borderColor = themeStyle.GREY_OP;
          if (value) {
            borderColor = errors.spendingAmount
              ? themeStyle.DANGER
              : themeStyle.GOLDEN;
          }
          return (
            <FormGroupComponent>
              <TextInputMask
                type="money"
                hidden
                options={{
                  precision: 0,
                  separator: ',',
                  delimiter: '.',
                  unit: '$ ',
                  suffixUnit: '',
                }}
                autoFocus
                placeholder="$0"
                returnKeyLabel="Aceptar"
                returnKeyType="go"
                value={value}
                style={[
                  fontStyles.largeSize,
                  fontStyles.blue,
                  fontStyles.normalWeight,
                  {
                    margin: 10,
                    textAlign: 'center',
                    borderBottomWidth: 1,
                    padding: 4,
                    borderBottomColor: borderColor,
                  },
                ]}
                onChangeText={(text) => {
                  onChange(parseValue(text));
                }}
              />
              {errors.spendingAmount && (
                <FormErrorComponent content={errors.spendingAmount.message} />
              )}
            </FormGroupComponent>
          );
        }}
        rules={{ required: true }}
      />

      <GradientButton
        mode={isFormValid ? 'active' : 'disabled'}
        onPress={handleSubmit(submitWithPendingTimesMonthly)}
        text="Siguiente"
        style={{ marginTop: 20 }}
      />
    </MainContainer>
  );
};

StorePointsForm.propTypes = {
  store: PropTypes.object.isRequired,
  handleNextStepClick: PropTypes.func.isRequired,
};

export default StorePointsForm;
