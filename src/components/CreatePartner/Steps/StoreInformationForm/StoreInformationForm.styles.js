import fontStyles from '../../../../styles/font.styles';

export const stylesheet = {
  title: [
    fontStyles.xlargeSize,
    fontStyles.center,
    fontStyles.blue,
    fontStyles.normalWeight,
    { marginBottom: 20 },
  ],
};
