import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import * as yup from 'yup';

/** Components */
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import CustomTextInput from '../../../Common/Form/CustomTextInput.component';
import CustomPicker from '../../../Common/Form/CustomPicker.component';
import MainContainer from '../../../Common/Layout/MainContainer.component';

/** Styles */
import globalStyles from '../../../../styles/global.styles';
import { stylesheet } from './StoreInformationForm.styles';

const schema = yup.object().shape({
  description: yup.string().required(),
  countryCode: yup.string().required(),
  category: yup.string().required(),
});

const StoreInformationForm = ({
  storeCategories,
  storeCountries,
  handleNextStepClick,
  store,
}) => {
  const { control, handleSubmit, watch } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      description: store.description || '',
      category: store.category || storeCategories[0].name,
      countryCode: store.countryCode || storeCountries[0].name,
    },
  });

  const values = watch();

  const isFormValid = Boolean(
    values.description && values.countryCode && values.category
  );

  return (
    <MainContainer style={globalStyles.centeredCon}>
      <Text style={stylesheet.title}>Contanos acerca de tu comercio</Text>

      <Controller
        name="description"
        control={control}
        as={
          <CustomTextInput
            minLines={2}
            maxLines={5}
            charCounter
            resize
            inputProps={{
              multiline: true,
              maxLength: 255,
              defaultValue: values.description,
              autoCorrect: false,
            }}
            label="Una breve descripción para tus clientes"
          />
        }
        rules={{ required: true }}
      />

      <Controller
        name="category"
        control={control}
        as={
          <CustomPicker
            pickerProps={{
              selectedValue: values.category,
            }}
            label="¿A qué rubro pertenece?"
            items={storeCategories.map((category) => ({
              label: category.displayName,
              value: category.name,
            }))}
          />
        }
        rules={{ required: true }}
      />

      <Controller
        name="countryCode"
        control={control}
        as={
          <CustomPicker
            pickerProps={{
              selectedValue: values.countryCode,
            }}
            label="¿En qué país opera?"
            items={storeCountries.map((country) => ({
              label: country.displayName,
              value: country.name,
            }))}
          />
        }
        rules={{ required: true }}
      />

      <GradientButton
        text="Siguiente"
        onPress={handleSubmit(handleNextStepClick)}
        mode={isFormValid ? 'active' : 'disabled'}
      />
    </MainContainer>
  );
};

StoreInformationForm.propTypes = {
  storeCategories: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
    })
  ).isRequired,
  storeCountries: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
      conversionRate: PropTypes.number.isRequired,
    })
  ).isRequired,
  handleNextStepClick: PropTypes.func.isRequired,
  store: PropTypes.object.isRequired,
};

export default StoreInformationForm;
