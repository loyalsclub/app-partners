import fontStyles from '../../../../styles/font.styles';

export default {
  title: [
    fontStyles.xlargeSize,
    fontStyles.center,
    fontStyles.blue,
    fontStyles.normalWeight,
    { marginBottom: 20 },
  ],
};
