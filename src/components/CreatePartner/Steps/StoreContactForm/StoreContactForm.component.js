import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import * as yup from 'yup';
import propTypes from '../../../../config/propTypes';

/** Components */
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import MainContainer from '../../../Common/Layout/MainContainer.component';

/** Styles */
import globalStyles from '../../../../styles/global.styles';
import stylesheet from './StoreContactForm.styles';
import PhoneInputComponent from '../../../Common/Form/PhoneInput.component';

const schema = yup.object().shape({
  personalPhone: yup.string().required(),
});

const StoreContactForm = ({ handleNextStepClick, store, storeCountries }) => {
  const { control, handleSubmit, watch } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      personalPhone: store.personalPhone || '',
    },
  });

  const values = watch();

  const isFormValid = Boolean(values.personalPhone);

  return (
    <MainContainer style={globalStyles.centeredCon}>
      <Text style={globalStyles.title}>Ayudanos a contactarte</Text>

      <Text style={globalStyles.secondaryTitle}>
        Esta información será exclusivamente de contacto y no será compartida
        públicamente
      </Text>

      <Controller
        name="personalPhone"
        control={control}
        render={({ value, onChange }) => (
          <PhoneInputComponent
            label="Número de Contacto"
            onChange={onChange}
            value={value}
            countryCode={store.countryCode}
            storeCountries={storeCountries}
          />
        )}
        style={stylesheet.field}
      />

      <GradientButton
        text="Siguiente"
        onPress={handleSubmit(handleNextStepClick)}
        mode={isFormValid ? 'active' : 'disabled'}
      />
    </MainContainer>
  );
};

StoreContactForm.propTypes = {
  handleNextStepClick: PropTypes.func.isRequired,
  store: propTypes.storePropType.isRequired,
};

export default StoreContactForm;
