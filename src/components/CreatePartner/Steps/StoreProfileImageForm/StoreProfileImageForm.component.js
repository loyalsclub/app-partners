import React from 'react';
import {
  Platform,
  Image,
  Text,
  View,
  TouchableOpacity,
  Alert,
} from 'react-native';
import PropTypes from 'prop-types';
import * as ImagePicker from 'expo-image-picker';
import { yupResolver } from '@hookform/resolvers';
import * as yup from 'yup';
import { useForm, Controller } from 'react-hook-form';

/** Components */
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import CustomTextInput from '../../../Common/Form/CustomTextInput.component';
import MainContainer from '../../../Common/Layout/MainContainer.component';

/** Styles */
import stylesheet from './StoreProfileImageForm.styles';
import globalStyles from '../../../../styles/global.styles';

const schema = yup.object().shape({
  name: yup.string().required(),
  profileImage: yup.string().required(),
});

const StoreProfileImageForm = ({ handleNextStepClick, store }) => {
  const { control, setValue, watch, handleSubmit } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      name: store.name || '',
      profileImage: store.profileImage || null,
    },
  });

  const values = watch();

  const getPermissions = async () => {
    if (Platform.OS !== 'web') return true;

    const { status } = await ImagePicker.requestCameraRollPermissionsAsync();

    return status === 'granted';
  };

  const pickImage = async () => {
    const res = await getPermissions();

    if (!res) {
      Alert.alert(
        '¡Ups!',
        'Necesitamos permisos para acceder a tu biblioteca.'
      );
      return;
    }

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      aspect: [1, 1],
      quality: 1,
    });

    if (!result.cancelled) {
      setValue('profileImage', result.uri, { shouldValidate: true });
    }
  };

  const isFormValid = Boolean(values.name && values.profileImage);

  return (
    <MainContainer style={globalStyles.centeredCon}>
      <Text style={globalStyles.title}>
        Completá el logo y nombre de tu comercio
      </Text>

      <View style={stylesheet.imageWrapper}>
        <TouchableOpacity onPress={pickImage}>
          <Controller
            name="profileImage"
            control={control}
            render={({ value }) => (
              <Image
                source={
                  value
                    ? { uri: value }
                    : require('../../../../assets/images/store-placeholder/img.png')
                }
                style={stylesheet.image}
              />
            )}
          />
        </TouchableOpacity>
      </View>

      <Controller
        name="name"
        control={control}
        as={
          <CustomTextInput
            inputProps={{
              maxLength: 35,
              autoCorrect: false,
            }}
            label="Nombre del Comercio"
          />
        }
        style={stylesheet.field}
      />

      <GradientButton
        text="Siguiente"
        onPress={handleSubmit(handleNextStepClick)}
        mode={isFormValid ? 'active' : 'disabled'}
        style={stylesheet.pickImageButton}
      />
    </MainContainer>
  );
};

StoreProfileImageForm.propTypes = {
  handleNextStepClick: PropTypes.func.isRequired,
  store: PropTypes.object.isRequired,
};

export default StoreProfileImageForm;
