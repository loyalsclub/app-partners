const IMAGE_HEIGHT = 200;

export default {
  imageWrapper: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingBottom: 20,
  },
  image: {
    width: IMAGE_HEIGHT,
    height: IMAGE_HEIGHT,
    borderRadius: IMAGE_HEIGHT / 2,
  },
  pickImageButton: {
    marginBottom: 20,
  },
  field: {
    textAlign: 'center',
    textTransform: 'capitalize',
    paddingBottom: 20,
  },
};
