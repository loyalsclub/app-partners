import fontStyles from '../../../../styles/font.styles';
import themeStyle from '../../../../styles/theme.style';

export default {
  section: {
    marginBottom: 20,
    textAlign: 'center',
  },
  storeCard: {
    borderWidth: 1,
    borderRadius: 30,
    borderColor: themeStyle.BLUE_WOOD_OP,
    padding: 15,
    backgroundColor: themeStyle.CREAM_WHITE,
    marginBottom: 30,
  },
  leftColumn: {
    flexBasis: 100,
    height: '100%',
    justifyContent: 'flex-start',
  },
  rightColumn: {
    flex: 1,
    justifyContent: 'center',
  },
  topRow: {
    marginBottom: 4,
  },
  bottomRow: {},
  col: {
    paddingVertical: 10,
  },
  label: [
    fontStyles.normalWeight,
    fontStyles.mediumSize,
    {
      paddingRight: 10,
      color: themeStyle.BLUE_WOOD,
    },
  ],
  storeDescription: [
    fontStyles.largeSize,
    fontStyles.normalWeight,
    fontStyles.blue,
  ],
  separator: {
    borderBottomWidth: 1,
    borderColor: themeStyle.GREY_OP,
  },
  storeName: [
    fontStyles.xlargeSize,
    fontStyles.normalWeight,
    { paddingLeft: 10 },
  ],
  storeProfileImage: {
    height: 90,
    width: 90,
    borderRadius: 45,
    alignSelf: 'center',
  },
  description: [
    fontStyles.normalWeight,
    fontStyles.largeSize,
    {
      color: themeStyle.BLUE_WOOD,
    },
  ],
};
