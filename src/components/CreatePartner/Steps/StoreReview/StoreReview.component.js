import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Image, Text, View } from 'react-native';

/** Componetns */
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import MainContainer from '../../../Common/Layout/MainContainer.component';

/** Styles */
import globalStyles from '../../../../styles/global.styles';
import stylesheet from './StoreReview.styles';
import propTypes from '../../../../config/propTypes';
import { analyticsProvider, events } from '../../../../providers/analytics';

const StoreReview = ({
  submitStore,
  store,
  storeCategories,
  storeCountries,
}) => {
  const { displayName: categoryLabel } = storeCategories.find(
    (category) => store.category === category.name
  );

  const { displayName: countryLabel } = storeCountries.find(
    (country) => store.countryCode === country.name
  );

  useEffect(function logAnalyticsData() {
    analyticsProvider.logEvent(events.partner_registration.review_information);
  }, []);

  return (
    <MainContainer style={globalStyles.centeredCon}>
      <Text style={globalStyles.title}>
        Revisemos la información que completaste
      </Text>
      <MainContainer style={stylesheet.storeCard}>
        <View style={globalStyles.row}>
          <View style={stylesheet.leftColumn}>
            <Image
              source={{ uri: store.profileImage }}
              style={stylesheet.storeProfileImage}
            />
          </View>
          <View style={stylesheet.rightColumn}>
            <Text style={stylesheet.storeName}>{store.name}</Text>
          </View>
        </View>
        <View style={stylesheet.col}>
          <Text style={stylesheet.storeDescription}>{store.description}</Text>
        </View>

        <View style={[stylesheet.col, stylesheet.separator]}>
          <View style={stylesheet.topRow}>
            <Text style={stylesheet.label}>Rubro:</Text>
          </View>
          <View style={stylesheet.bottomRow}>
            <Text style={stylesheet.description}>{categoryLabel}</Text>
          </View>
        </View>

        <View style={[stylesheet.col, stylesheet.separator]}>
          <View style={stylesheet.topRow}>
            <Text style={stylesheet.label}>País:</Text>
          </View>
          <View style={stylesheet.bottomRow}>
            <Text style={stylesheet.description}>{countryLabel}</Text>
          </View>
        </View>

        <View style={[stylesheet.col, stylesheet.separator]}>
          <View style={stylesheet.topRow}>
            <Text style={stylesheet.label}>Número de Contacto:</Text>
          </View>
          <View style={stylesheet.bottomRow}>
            <Text style={stylesheet.description}>{store.personalPhone}</Text>
          </View>
        </View>

        <View style={stylesheet.col}>
          <View style={stylesheet.topRow}>
            <Text style={stylesheet.label}>Consumo por Mes:</Text>
          </View>
          <View style={stylesheet.bottomRow}>
            <Text style={stylesheet.description}>
              ${` ${store.spendingAmount}`}
            </Text>
          </View>
        </View>
      </MainContainer>

      <GradientButton text="Finalizar" onPress={submitStore} mode="active" />
    </MainContainer>
  );
};

StoreReview.propTypes = {
  storeCategories: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
    })
  ).isRequired,
  storeCountries: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
      conversionRate: PropTypes.number.isRequired,
    })
  ).isRequired,
  submitStore: PropTypes.func.isRequired,
  store: propTypes.storePropType.isRequired,
};

export default StoreReview;
