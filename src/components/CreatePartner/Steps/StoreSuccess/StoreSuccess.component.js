import React, { useEffect, useState } from 'react';
import { Text } from 'react-native';

/** Compontents */
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import MainContainer from '../../../Common/Layout/MainContainer.component';

/** Styles */
import globalStyles from '../../../../styles/global.styles';
import LevelCards from '../../../Common/Miscellaneous/LevelCards.component';
import { analyticsProvider, events } from '../../../../providers/analytics';
import ExplanationItem from '../../../Common/Miscellaneous/ExplanationItem/ExplanationItem.component';
import translate from '../../../../utils/language.utils';
import OneIcon from '../../../Common/Icons/One.icon';
import TwoIcon from '../../../Common/Icons/Two.icon';
import ThreeIcon from '../../../Common/Icons/Three.icon';
import FourIcon from '../../../Common/Icons/Four.icon';
import LinkIcon from '../../../Common/Icons/Link.icon';
import InviteQRIcon from '../../../Common/Icons/InviteQR.icon';
import fontStyles from '../../../../styles/font.styles';
import themeStyle from '../../../../styles/theme.style';

const STEPS = {
  initial: 'congratulations',
  levels: 'store_levels',
  rewards: 'store_rewards',
};

export default ({
  store,
  storeLevelsAndRewardsById,
  onFinishCreationPressHandler,
}) => {
  const [successStep, setSuccessStep] = useState(STEPS.initial);

  useEffect(
    function logAnalyticsData() {
      analyticsProvider.logEvent(events.partner_registration[successStep]);
    },
    [successStep]
  );

  return (
    <MainContainer style={globalStyles.centeredCon}>
      {successStep === STEPS.initial && (
        <>
          <Text style={[globalStyles.mainTitle, { marginBottom: 40 }]}>
            {translate('CREATE_PARTNER.congratulationsTitle')}
          </Text>

          <Text
            style={[
              globalStyles.text,
              {
                textAlign: 'center',
                fontSize: 15,
                marginBottom: 40,
                color: themeStyle.GREY,
              },
            ]}
          >
            {translate('CREATE_PARTNER.congratulationsDescription')}
          </Text>

          <LevelCards levelsData={storeLevelsAndRewardsById} />

          <GradientButton
            style={{ marginTop: 40 }}
            mode="active"
            onPress={() => setSuccessStep(STEPS.levels)}
            text={translate('CREATE_PARTNER.congratulationsButton')}
          />
        </>
      )}

      {successStep === STEPS.levels && (
        <>
          <Text style={[globalStyles.mainTitle, { marginBottom: 40 }]}>
            {translate('CREATE_PARTNER.howToTitle')}
          </Text>

          <ExplanationItem
            icon={<OneIcon />}
            title={translate('CREATE_PARTNER.howToFirstStepTitle')}
            description={translate('CREATE_PARTNER.howToFirstStepDescription')}
          />

          <ExplanationItem
            icon={<TwoIcon />}
            title={translate('CREATE_PARTNER.howToSecondStepTitle')}
            description={translate('CREATE_PARTNER.howToSecondStepDescription')}
          />

          <ExplanationItem
            icon={<ThreeIcon />}
            title={translate('CREATE_PARTNER.howToThirdStepTitle')}
            description={translate('CREATE_PARTNER.howToThirdStepDescription')}
          />

          <ExplanationItem
            icon={<FourIcon />}
            title={translate('CREATE_PARTNER.howToFourthStepTitle')}
            description={translate('CREATE_PARTNER.howToFourthStepDescription')}
          />

          <GradientButton
            mode="active"
            style={{ margin: 15 }}
            onPress={() => setSuccessStep(STEPS.rewards)}
            text={translate('CREATE_PARTNER.howToButton')}
          />
        </>
      )}

      {successStep === STEPS.rewards && (
        <>
          <Text
            style={[
              globalStyles.mainTitle,
              { textAlign: 'center', marginBottom: 40 },
            ]}
          >
            {translate('CREATE_PARTNER.clientsTitle')}
          </Text>

          <Text
            style={[
              globalStyles.text,
              {
                textAlign: 'center',
                fontSize: 15,
                marginBottom: 40,
                color: themeStyle.GREY,
              },
            ]}
          >
            {translate('CREATE_PARTNER.clientsDescription')}
          </Text>

          <ExplanationItem
            icon={<LinkIcon />}
            title={translate('CREATE_PARTNER.clientsFirstStepTitle')}
            description={
              <>
                {translate('CREATE_PARTNER.clientsFirstStepDescription')}
                <Text style={fontStyles.boldWeight}>
                  {`loyals.club/${
                    store.alias ||
                    translate('CREATE_PARTNER.clientsDefaultAlias')
                  }`}
                </Text>
              </>
            }
          />

          <ExplanationItem
            icon={<InviteQRIcon />}
            title={translate('CREATE_PARTNER.clientsSecondStepTitle')}
            description={translate(
              'CREATE_PARTNER.clientsSecondStepDescription'
            )}
          />

          <GradientButton
            mode="active"
            style={{ margin: 15 }}
            onPress={onFinishCreationPressHandler}
            text={translate('CREATE_PARTNER.clientsButton')}
          />
        </>
      )}
    </MainContainer>
  );
};
