import React, { useMemo } from 'react';
import SectionPage from '../Common/Layout/SectionPage.component';
import TicketIcon from '../Common/Icons/Ticket.icon';
import EditIcon from '../Common/Icons/Edit.icon';
import GearIcon from '../Common/Icons/Gear.icon';
import translate from '../../utils/language.utils';

const Settings = ({
  onEditInformationPressHandler,
  onManageRewardsPressHandler,
}) => {
  const actions = useMemo(
    () => [
      {
        icon: <EditIcon />,
        title: translate('SETTINGS.actionEditInformationTitle'),
        description: translate('SETTINGS.actionEditInformationDescription'),
        onPressHandler: onEditInformationPressHandler,
      },
      {
        icon: <TicketIcon />,
        title: translate('SETTINGS.actionManageRewardsTitle'),
        description: translate('SETTINGS.actionManageRewardsDescription'),
        onPressHandler: onManageRewardsPressHandler,
      },
    ],
    [onManageRewardsPressHandler, onEditInformationPressHandler]
  );

  return (
    <SectionPage
      icon={<GearIcon width={80} height={80} style={{ marginBottom: 40 }} />}
      title={translate('SETTINGS.title')}
      description={translate('SETTINGS.description')}
      actions={actions}
    />
  );
};

export default Settings;
