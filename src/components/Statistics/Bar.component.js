/** External Dependencies */
import React from 'react';
import Svg, { Rect, Defs, LinearGradient, Stop } from 'react-native-svg';
import PropTypes from 'prop-types';
import { Animated } from 'react-native';

const AnimatedRect = Animated.createAnimatedComponent(Rect);

const BarComponent = (props) => {
  const { barHeight, barWidth, filledPorcentage, gradientColors } = props;

  return (
    <Svg height={barHeight} width={barWidth}>
      <Defs>
        <LinearGradient id="grad" x1="0" y1="0" x2="170" y2="0">
          <Stop offset="0" stopColor={gradientColors[0]} stopOpacity="1" />
          <Stop offset="1" stopColor={gradientColors[1]} stopOpacity="1" />
        </LinearGradient>
      </Defs>
      <Rect
        width={barWidth}
        height={barHeight}
        x="0"
        y="0"
        rx="8"
        ry="8"
        fill="rgba(35,46,64,0.1)"
      />
      <AnimatedRect
        x="0"
        y="0"
        width={filledPorcentage.interpolate({
          inputRange: [0, 1],
          outputRange: [0, barWidth],
        })}
        height={barHeight}
        rx="7"
        ry="7"
        fill="url(#grad)"
      />
    </Svg>
  );
};

BarComponent.propTypes = {
  barHeight: PropTypes.number.isRequired,
  barWidth: PropTypes.number.isRequired,
  gradientColors: PropTypes.arrayOf(String).isRequired,
  filledPorcentage: PropTypes.instanceOf(Animated.Value).isRequired,
};

export default BarComponent;
