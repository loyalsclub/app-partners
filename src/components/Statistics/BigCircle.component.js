/** External Dependencies */
import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';

/** Styles */
import themeStyle from '../../styles/theme.style';
import f from '../../styles/font.styles';

const BigCircleComponent = (props) => {
  const { width, total } = props;
  const halfSideSquare = width * 0.704;
  const marginSide = (width - halfSideSquare) / 2;

  const slength = String(total).length;
  // Dynamic font sizing in case it doesnt fit.
  const fontSize = slength <= 3 ? 90 : 90 - (slength - 3) * 12;

  return (
    <View
      style={{
        width,
        height: width,
        borderRadius: width / 2,
        alignSelf: 'center',
        borderWidth: 1,
        backgroundColor: themeStyle.CREAM_WHITE,
        borderColor: themeStyle.GOLDEN,
      }}
    >
      <View
        style={{
          marginTop: marginSide,
          marginLeft: marginSide,
          width: halfSideSquare,
          alignItems: 'center',
          justifyContent: 'center',
          height: halfSideSquare,
        }}
      >
        <Text
          style={[
            f.golden,
            f.boldWeight,
            {
              fontSize,
            },
          ]}
          numberOfLines={1}
        >
          {total}
        </Text>
        <Text style={[f.center, f.blue, f.largeSize]}>
          Clientes activos en tu plataforma
        </Text>
      </View>
    </View>
  );
};

BigCircleComponent.propTypes = {
  total: PropTypes.number.isRequired,
};

BigCircleComponent.defaultProps = {};

export default BigCircleComponent;
