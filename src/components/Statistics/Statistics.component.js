/** External Dependencies */
import React, { useMemo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Animated, View, Image, Text } from 'react-native';

/** Components */
import BarComponent from './Bar.component';
import GenericHeader, {
  getHeaderHeight,
} from '../Common/Miscellaneous/GenericHeader.component';
import PageHeaderContainer from '../Common/Layout/PageHeaderContainer.component';
import BackgroundPattern from '../Common/Miscellaneous/BackgroundPattern.component';
import PassiveNotification from '../Common/Miscellaneous/PassiveNotification.component';

/** Styles */
import globalStyles from '../../styles/global.styles';
import IconComponent from '../Common/Miscellaneous/Icons.component';
import themeStyle from '../../styles/theme.style';
import fontStyles from '../../styles/font.styles';
import MainContainer from '../Common/Layout/MainContainer.component';

const styles = {
  barRow: {
    flexDirection: 'row',
    paddingVertical: 10,
  },
  countContainer: {
    marginLeft: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  countText: {
    textAlignVertical: 'center',
    marginTop: 3,
    marginLeft: 4,
  },
  smallCrown: {
    alignSelf: 'flex-start',
    resizeMode: 'contain',
    height: 30,
    width: 30,
    marginRight: 15,
  },
};

const StatisticsComponent = (props) => {
  const { employeeGetStatistics } = props;
  const { customersTotal, level1, level2, level3 } = employeeGetStatistics;

  const level1AV = useMemo(() => new Animated.Value(0), []);
  const level2AV = useMemo(() => new Animated.Value(0), []);
  const level3AV = useMemo(() => new Animated.Value(0), []);

  useEffect(() => {
    if (
      level1 === undefined ||
      level2 === undefined ||
      level3 === undefined ||
      customersTotal === undefined
    )
      return;

    const level1PG = level1 / customersTotal || 0;
    const level2PG = level2 / customersTotal || 0;
    const level3PG = level3 / customersTotal || 0;

    Animated.parallel([
      Animated.timing(level1AV, {
        toValue: level1PG,
        duration: 1000,
        useNativeDriver: true,
      }),
      Animated.timing(level2AV, {
        toValue: level2PG,
        duration: 1000,
        useNativeDriver: true,
      }),
      Animated.timing(level3AV, {
        toValue: level3PG,
        duration: 1000,
        useNativeDriver: true,
      }),
    ]).start();
  }, [level1, level2, level3, customersTotal, level1AV, level2AV, level3AV]);

  const renderHeader = () => {
    const { goBackOnPress } = props;
    return <GenericHeader goBackOnPress={goBackOnPress} mode="goBack" />;
  };

  const renderBackground = () => <BackgroundPattern type="golden" />;

  const renderError = () => {
    const { error } = props;
    return (
      <PassiveNotification type="genericError" secondaryText={error.text} />
    );
  };

  const renderContent = () => {
    const maxWidth = 320;
    const barWidth = 200;
    const barHeight = 30;
    const slength = String(customersTotal).length;
    // Dynamic font sizing in case it doesnt fit.
    const fontSize = slength <= 3 ? 120 : 120 - (slength - 3) * 12;

    return (
      <MainContainer style={[globalStyles.centeredCon, { marginTop: 40 }]}>
        <View style={{ alignItems: 'center' }}>
          <Text
            style={[
              fontStyles.titleSize,
              fontStyles.blue,
              fontStyles.boldWeight,
            ]}
          >
            Estadísticas
          </Text>
        </View>

        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 80,
          }}
        >
          <Text
            style={[
              fontStyles.grey,
              fontStyles.boldWeight,
              {
                fontSize,
              },
            ]}
            numberOfLines={1}
          >
            {customersTotal}
          </Text>

          <Text
            style={[
              fontStyles.center,
              fontStyles.blue,
              fontStyles.xlargeSize,
              { maxWidth, marginBottom: 40 },
            ]}
          >
            Clientes activos en tu plataforma
          </Text>
        </View>

        <View
          style={{
            alignSelf: 'center',
            width: maxWidth,
            maxWidth,
          }}
        >
          <View style={styles.barRow}>
            <Image
              source={require('../../assets/images/crown-silver/img.png')}
              style={styles.smallCrown}
            />
            <BarComponent
              barHeight={barHeight}
              barWidth={barWidth}
              gradientColors={['rgba(171,185,207,1)', 'rgba(97,124,160,1)']}
              filledPorcentage={level1AV}
            />
            <View style={styles.countContainer}>
              <IconComponent
                color={themeStyle.BLUE_WOOD}
                name="user-line"
                size={22}
              />
              <Text
                style={[
                  styles.countText,
                  fontStyles.xlargeSize,
                  fontStyles.normalWeight,
                ]}
              >
                {level1}
              </Text>
            </View>
          </View>
          <View style={styles.barRow}>
            <Image
              source={require('../../assets/images/crown-gold/img.png')}
              style={styles.smallCrown}
            />
            <BarComponent
              barHeight={barHeight}
              barWidth={barWidth}
              gradientColors={['rgba(249,207,39,1)', 'rgba(221,141,0,0.4)']}
              filledPorcentage={level2AV}
            />
            <View style={styles.countContainer}>
              <IconComponent
                color={themeStyle.BLUE_WOOD}
                name="user-line"
                size={22}
              />
              <Text
                style={[
                  styles.countText,
                  fontStyles.xlargeSize,
                  fontStyles.normalWeight,
                ]}
              >
                {level2}
              </Text>
            </View>
          </View>
          <View style={styles.barRow}>
            <Image
              source={require('../../assets/images/crown-zafire/img.png')}
              style={styles.smallCrown}
            />
            <BarComponent
              barHeight={barHeight}
              barWidth={barWidth}
              gradientColors={['rgba(0,175,141,1)', 'rgba(189,255,255,1)']}
              filledPorcentage={level3AV}
            />
            <View style={styles.countContainer}>
              <IconComponent
                color={themeStyle.BLUE_WOOD}
                name="user-line"
                size={22}
              />
              <Text
                style={[
                  styles.countText,
                  fontStyles.xlargeSize,
                  fontStyles.normalWeight,
                ]}
              >
                {level3}
              </Text>
            </View>
          </View>
        </View>
      </MainContainer>
    );
  };

  const { loading, error, refetch } = props;

  return (
    <PageHeaderContainer
      error={error != null}
      renderHeader={renderHeader}
      renderContent={renderContent}
      renderBackground={renderBackground}
      renderError={renderError}
      headerHeight={getHeaderHeight()}
      refreshing={loading}
      onRefresh={refetch}
    />
  );
};

StatisticsComponent.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.shape({
    text: PropTypes.string,
  }),
  employeeGetStatistics: PropTypes.shape({
    customersTotal: PropTypes.number,
    level1: PropTypes.number,
    level2: PropTypes.number,
    level3: PropTypes.number,
  }),
};

StatisticsComponent.defaultProps = {
  error: null,
  employeeGetStatistics: {
    customersTotal: null,
    level1: null,
    level2: null,
    lebel3: null,
  },
};

export default StatisticsComponent;
