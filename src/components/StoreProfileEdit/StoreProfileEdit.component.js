import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import LayoutPage from '../Common/Layout/LayoutPage.component';
import { generateRNFile } from '../../utils/files.utils';
import MainContainer from '../Common/Layout/MainContainer.component';
import PlainPropertyForm from './components/PlainPropertyForm/PlainPropertyForm.component';
import translate from '../../utils/language.utils';
import {
  isSocialMediaProperty,
  isStoreLocationProperty,
} from '../../utils/stores.utils';
import AliasForm from './components/AliasForm/AliasForm.component';
import ProfileImageForm from './components/ProfileImageForm/ProfileImageForm.component';
import SelectPropertyForm from './components/SelectPropertyForm/SelectPropertyForm.component';
import { textInputProps, socialMediaMasks } from './StoreProfileEdit.constants';
import SocialMediaPropertyForm from './components/SocialMediaPropertyForm/SocialMediaPropertyForm.component';
import WhatsappForm from './components/WhatsappForm/WhatsappForm.component';
import { STORE_PROPERTIES } from '../../config/constants';
import AddressForm from './components/AddressForm/AddressForm.component';

const StoreProfileEdit = ({
  loading,
  error,
  updateStore,
  validateStoreAlias,
  storeCategories,
  storeCountries,
  store,
  propertyToEdit,
}) => {
  const {
    isSocialMedia,
    isWhatsapp,
    isStoreLocation,
    isProfileImage,
    isAlias,
    isCategory,
    isAddress,
  } = useMemo(
    () => ({
      isSocialMedia: isSocialMediaProperty(propertyToEdit),
      isWhatsapp: propertyToEdit === STORE_PROPERTIES.whatsapp,
      isStoreLocation: isStoreLocationProperty(propertyToEdit),
      isProfileImage: propertyToEdit === STORE_PROPERTIES.profileImage,
      isAlias: propertyToEdit === STORE_PROPERTIES.alias,
      isCategory: propertyToEdit === STORE_PROPERTIES.category,
      isAddress: propertyToEdit === STORE_PROPERTIES.address,
    }),
    [propertyToEdit]
  );

  const submitStoreUpdate = (form) => {
    const value = form[propertyToEdit];

    const {
      __typename,
      id: storeId,
      profileImage,
      StoreLocations = [{}],
      socialMedia: storeSocialMedia = {},
      ...storeToUpdate
    } = {
      ...store,
    };

    if (isProfileImage) {
      const hasProfileImageChanged = value !== profileImage;

      if (hasProfileImageChanged) {
        storeToUpdate.profileImage = generateRNFile(value);
      }
    } else if (isSocialMedia) {
      const socialMedia = { ...storeSocialMedia };

      socialMedia[propertyToEdit] = value;

      const isAnySocialMediaPopulated =
        Object.values(socialMedia).some(Boolean);

      storeToUpdate.socialMedia = isAnySocialMediaPopulated
        ? socialMedia
        : null;
    } else if (isStoreLocation) {
      const storeLocation = { ...StoreLocations[0] };

      if (isAddress) {
        storeLocation.address = value.address;
        storeLocation.geo = value.geo;
      } else {
        storeLocation[propertyToEdit] = value;
      }

      const isStoreLocationPopulated =
        Object.values(storeLocation).some(Boolean);

      if (isStoreLocationPopulated) {
        storeToUpdate.StoreLocations = [storeLocation];
      }
    } else {
      storeToUpdate[propertyToEdit] = value;
    }

    updateStore(storeId, storeToUpdate);
  };

  const getFormByPropertyType = () => {
    if (isProfileImage) {
      return <ProfileImageForm store={store} onSubmit={submitStoreUpdate} />;
    }

    if (isAlias) {
      return (
        <AliasForm
          label={translate(`STORE_PROFILE.${STORE_PROPERTIES.alias}`)}
          store={store}
          onSubmit={submitStoreUpdate}
          validateStoreAlias={validateStoreAlias}
          apiErrors={error}
        />
      );
    }

    if (isCategory) {
      return (
        <SelectPropertyForm
          name={propertyToEdit}
          label={translate(`STORE_PROFILE.${propertyToEdit}`)}
          onSubmit={submitStoreUpdate}
          options={storeCategories}
          selectedValue={store.category}
        />
      );
    }

    if (isWhatsapp) {
      return (
        <WhatsappForm
          name={propertyToEdit}
          label={translate(`STORE_PROFILE.${propertyToEdit}`)}
          onSubmit={submitStoreUpdate}
          store={store}
          storeCountries={storeCountries}
        />
      );
    }

    if (isSocialMedia && socialMediaMasks[propertyToEdit]) {
      return (
        <SocialMediaPropertyForm
          name={propertyToEdit}
          label={translate(`STORE_PROFILE.${propertyToEdit}`)}
          onSubmit={submitStoreUpdate}
          store={store}
          mask={socialMediaMasks[propertyToEdit]}
        />
      );
    }

    if (isAddress) {
      return (
        <AddressForm
          name={propertyToEdit}
          label={translate(`STORE_PROFILE.${STORE_PROPERTIES.address}`)}
          onSubmit={submitStoreUpdate}
          store={store}
          mask={socialMediaMasks[propertyToEdit]}
        />
      );
    }

    return (
      <PlainPropertyForm
        name={propertyToEdit}
        label={translate(`STORE_PROFILE.${propertyToEdit}`)}
        store={store}
        onSubmit={submitStoreUpdate}
        textInputProps={
          textInputProps[propertyToEdit] || {
            inputProps: {
              maxLength: 35,
              autoCorrect: false,
              autoCapitalize: 'none',
            },
          }
        }
      />
    );
  };

  return (
    <LayoutPage layout="top" loading={loading}>
      <MainContainer>{getFormByPropertyType()}</MainContainer>
    </LayoutPage>
  );
};

StoreProfileEdit.defaultProps = {
  error: null,
};

StoreProfileEdit.propTypes = {
  storeCategories: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
    })
  ).isRequired,
  store: PropTypes.object.isRequired,
  error: PropTypes.object,
  loading: PropTypes.bool.isRequired,
  updateStore: PropTypes.func.isRequired,
  validateStoreAlias: PropTypes.func.isRequired,
};

export default StoreProfileEdit;
