import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { View } from 'react-native';
import translate from '../../../../utils/language.utils';
import { getUsernameFromUrl } from './SocialMediaPropertyForm.utils';
import CustomTextInputComponent from '../../../Common/Form/CustomTextInput.component';
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import { schemas } from '../../StoreProfileEdit.constants';

const requiredFieldMessage = translate('FORMS_ERROR_MESSAGES.required');

const SocialMediaPropertyForm = ({ store, name, label, onSubmit, mask }) => {
  const socialMedia = store.socialMedia || {};
  const currentValue = socialMedia[name] || '';

  const { control, handleSubmit, errors } = useForm({});

  return (
    <View style={{ marginTop: 40 }}>
      <Controller
        name={name}
        control={control}
        render={({ onChange, ...props }) => {
          const change = (value) => {
            if (!value) {
              return onChange('');
            }

            const username = getUsernameFromUrl(value, mask);

            onChange(`${mask}/${username}`);
          };

          return (
            <CustomTextInputComponent
              {...props}
              inputProps={{
                maxLength: 150,
                autoCorrect: false,
                autoCapitalize: 'none',
              }}
              label={label}
              onChange={change}
              error={errors[name] && errors[name].message}
            />
          );
        }}
        defaultValue={currentValue}
        rules={{
          validate: async (value) =>
            (await schemas[name].isValid(value)) || requiredFieldMessage,
        }}
      />

      <GradientButton
        mode="active"
        onPress={handleSubmit(onSubmit)}
        text={translate('STORE_PROFILE.editButton')}
        style={{ marginTop: 20 }}
      />
    </View>
  );
};

export default SocialMediaPropertyForm;
