export const getUsernameFromUrl = (url, mask) => {
  const isDeletingMask = url === mask;
  const urlWithoutStripSlash = url.replace(/\/$/, '');
  const urlPieces = urlWithoutStripSlash.split('/');
  const lastPiece = urlPieces.pop();

  return isDeletingMask || lastPiece.includes('.com') ? '' : lastPiece;
};

export default { getUsernameFromUrl };
