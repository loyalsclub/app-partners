import React, { useEffect, useMemo, useRef, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { View, Animated } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

import ENV from '../../../../config/env.config';
import themeStyle from '../../../../styles/theme.style';
import translate from '../../../../utils/language.utils';
import stylesheet from '../../../Common/Form/Form.styles';
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';

const textboxStyle = stylesheet.textbox.normal;

const AddressForm = ({ store, name, label, onSubmit }) => {
  const [isAddressInit, setIsAddressInit] = useState(false);
  const ref = useRef();
  const { control, handleSubmit } = useForm({});

  const currentValue = useMemo(() => {
    const storeLocation =
      (store.StoreLocations &&
        store.StoreLocations.length &&
        store.StoreLocations[0]) ||
      {};

    return storeLocation || {};
  }, [store]);

  useEffect(
    function initAddress() {
      if (ref.current && currentValue.address && !isAddressInit) {
        ref.current.setAddressText(currentValue.address);
        setIsAddressInit(true);
      }
    },
    [currentValue, isAddressInit]
  );

  return (
    <View style={{ marginTop: 40 }}>
      <Controller
        name={name}
        control={control}
        render={({ onChange }) => (
          <>
            <GooglePlacesAutocomplete
              ref={ref}
              placeholder={label}
              fetchDetails
              styles={{
                textInput: {
                  ...textboxStyle,
                  paddingLeft: 0,
                  marginBottom: 0,
                  backgroundColor: 'transparent',
                },
              }}
              onPress={(_, details = null) => {
                const { lat, lng } = details.geometry.location;

                onChange({
                  address: details.formatted_address,
                  geo: {
                    type: 'Point',
                    coordinates: [lat, lng],
                  },
                });
              }}
              query={{
                key: ENV.GOOGLE_PLACES_API_KEY,
                language: 'es',
              }}
              textInputProps={{
                onChangeText: (value) => {
                  if (isAddressInit && !value) {
                    onChange({
                      address: '',
                      geo: null,
                    });
                  }
                },
              }}
            />

            <Animated.View
              style={{
                backgroundColor: themeStyle.GREY_OP,
                paddingBottom: 1,
                marginBottom: 2,
              }}
            />
          </>
        )}
        defaultValue={{
          address: currentValue.address || '',
          geo: {
            type: currentValue.geo?.type,
            coordinates: currentValue.geo?.coordinates,
          },
        }}
      />

      <GradientButton
        mode="active"
        onPress={handleSubmit(onSubmit)}
        text={translate('STORE_PROFILE.editButton')}
        style={{ marginTop: 20 }}
      />
    </View>
  );
};

export default AddressForm;
