import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { View } from 'react-native';
import translate from '../../../../utils/language.utils';
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import PhoneInputComponent from '../../../Common/Form/PhoneInput.component';
import { schemas } from '../../StoreProfileEdit.constants';

const requiredFieldMessage = translate('FORMS_ERROR_MESSAGES.required');

const WhatsappForm = ({ store, name, label, onSubmit, storeCountries }) => {
  const socialMedia = store.socialMedia || {};
  const currentValue = socialMedia[name] || '';

  const { control, handleSubmit } = useForm({});

  return (
    <View style={{ marginTop: 40 }}>
      <Controller
        name={name}
        control={control}
        render={({ value, onChange }) => (
          <PhoneInputComponent
            label={label}
            onChange={(newValue) => onChange(newValue.replace(/\s/g, ''))}
            value={value}
            countryCode={store.countryCode}
            storeCountries={storeCountries}
          />
        )}
        defaultValue={currentValue}
        rules={{
          validate: async (value) =>
            (await schemas[name].isValid(value)) || requiredFieldMessage,
        }}
      />

      <GradientButton
        mode="active"
        onPress={handleSubmit(onSubmit)}
        text={translate('STORE_PROFILE.editButton')}
        style={{ marginTop: 20 }}
      />
    </View>
  );
};

export default WhatsappForm;
