import React, { useCallback } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Text, View } from 'react-native';
import { STORE_PROPERTIES } from '../../../../config/constants';
import globalStyles from '../../../../styles/global.styles';
import { debounce } from '../../../../utils/functions.utils';
import translate from '../../../../utils/language.utils';
import CustomTextInputComponent from '../../../Common/Form/CustomTextInput.component';
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import { schemas } from '../../StoreProfileEdit.constants';

const requiredFieldMessage = translate('FORMS_ERROR_MESSAGES.required');

const AliasForm = ({
  store,
  label,
  onSubmit,
  validateStoreAlias,
  apiErrors,
}) => {
  const { control, handleSubmit, errors, trigger } = useForm({});

  const triggerAliasValidation = useCallback(
    debounce(() => trigger(STORE_PROPERTIES.alias), 500),
    []
  );

  return (
    <View style={{ marginTop: 40 }}>
      <Controller
        name={STORE_PROPERTIES.alias}
        control={control}
        render={({ onChange, ...props }) => {
          const validateAliasOnChange = (value) => {
            onChange(value);
            triggerAliasValidation();
          };

          return (
            <CustomTextInputComponent
              inputProps={{
                maxLength: 35,
                autoCorrect: false,
                autoCapitalize: 'none',
              }}
              label={label}
              {...props}
              onChange={validateAliasOnChange}
              error={
                (apiErrors && apiErrors.alias) ||
                (errors.alias && errors.alias.message)
              }
            />
          );
        }}
        defaultValue={store.alias || ''}
        rules={{
          validate: async (value) => {
            const isSchemaValid = await schemas.alias.isValid(value);

            if (!isSchemaValid) {
              return requiredFieldMessage;
            }

            if (store.alias !== value) {
              await validateStoreAlias(value);
            }

            return true;
          },
        }}
      />

      <Text
        style={[
          globalStyles.sectionPageSubtitle,
          { textAlign: 'left', marginBottom: 10 },
        ]}
      >
        {translate('STORE_PROFILE.aliasEditDescription')}
      </Text>

      <GradientButton
        mode="active"
        onPress={handleSubmit((form) => {
          if (!apiErrors) {
            return onSubmit(form);
          }
        })}
        text={translate('STORE_PROFILE.editButton')}
        style={{ marginTop: 20 }}
      />
    </View>
  );
};

export default AliasForm;
