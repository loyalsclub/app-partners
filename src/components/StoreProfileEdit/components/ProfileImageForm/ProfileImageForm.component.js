import React, { useCallback } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Image, TouchableOpacity, View } from 'react-native';
import { STORE_PROPERTIES } from '../../../../config/constants';
import useImageGallery from '../../../../hooks/useImageGallery.hook';
import translate from '../../../../utils/language.utils';
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import { schemas } from '../../StoreProfileEdit.constants';

const requiredFieldMessage = translate('FORMS_ERROR_MESSAGES.required');

const ProfileImageForm = ({ onSubmit }) => {
  const { control, handleSubmit, setValue } = useForm({});

  const onPickSuccess = useCallback(
    (image) =>
      setValue(STORE_PROPERTIES.profileImage, image, { shouldValidate: true }),
    [setValue]
  );

  const pickImage = useImageGallery(onPickSuccess);

  return (
    <View style={{ marginTop: 40 }}>
      <TouchableOpacity onPress={pickImage}>
        <Controller
          name={STORE_PROPERTIES.profileImage}
          control={control}
          render={({ value }) => (
            <Image
              source={
                value
                  ? { uri: value }
                  : require('../../../../assets/images/store-placeholder/img.png')
              }
              style={{
                height: 200,
                width: 200,
                borderRadius: 100,
                alignSelf: 'center',
              }}
            />
          )}
          rules={{
            validate: async (value) =>
              (await schemas.profileImage.isValid(value)) ||
              requiredFieldMessage,
          }}
          defaultValue=""
        />
      </TouchableOpacity>

      <GradientButton
        mode="active"
        onPress={handleSubmit(onSubmit)}
        text={translate('STORE_PROFILE.editButton')}
        style={{ marginTop: 20 }}
      />
    </View>
  );
};

export default ProfileImageForm;
