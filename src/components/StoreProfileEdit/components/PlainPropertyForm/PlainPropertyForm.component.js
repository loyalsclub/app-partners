import React, { useMemo } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { View } from 'react-native';
import translate from '../../../../utils/language.utils';
import {
  isSocialMediaProperty,
  isStoreLocationProperty,
} from '../../../../utils/stores.utils';
import CustomTextInputComponent from '../../../Common/Form/CustomTextInput.component';
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import { schemas } from '../../StoreProfileEdit.constants';

const requiredFieldMessage = translate('FORMS_ERROR_MESSAGES.required');

const PlainPropertyForm = ({
  store,
  name,
  label,
  onSubmit,
  textInputProps,
}) => {
  const { control, handleSubmit, errors } = useForm({});

  const currentValue = useMemo(() => {
    const socialMedia = store.socialMedia || {};

    const storeLocation =
      (store.StoreLocations &&
        store.StoreLocations.length &&
        store.StoreLocations[0]) ||
      {};

    const isSocialMedia = isSocialMediaProperty(name);

    const isStoreLocation = isStoreLocationProperty(name);

    let value = '';

    if (isSocialMedia) {
      value = socialMedia[name];
    } else if (isStoreLocation) {
      value = storeLocation[name];
    } else {
      value = store[name];
    }

    return value;
  }, [name, store]);

  return (
    <View style={{ marginTop: 40 }}>
      <Controller
        name={name}
        control={control}
        as={
          <CustomTextInputComponent
            {...textInputProps}
            label={label}
            error={errors.name && errors.name.message}
          />
        }
        defaultValue={currentValue}
        rules={{
          validate: async (value) =>
            (await schemas[name].isValid(value)) || requiredFieldMessage,
        }}
      />

      <GradientButton
        mode="active"
        onPress={handleSubmit(onSubmit)}
        text={translate('STORE_PROFILE.editButton')}
        style={{ marginTop: 20 }}
      />
    </View>
  );
};

export default PlainPropertyForm;
