import React, { useMemo } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { View } from 'react-native';
import translate from '../../../../utils/language.utils';
import CustomPickerComponent from '../../../Common/Form/CustomPicker.component';
import GradientButton from '../../../Common/Miscellaneous/GradientButton.component';
import { schemas } from '../../StoreProfileEdit.constants';

const requiredFieldMessage = translate('FORMS_ERROR_MESSAGES.required');

const SelectPropertyForm = ({
  name,
  label,
  onSubmit,
  options,
  selectedValue,
}) => {
  const { control, handleSubmit } = useForm({});

  const parsedOptions = useMemo(
    () =>
      options.map((option) => ({
        label: option.displayName,
        value: option.name,
      })),
    [options]
  );

  return (
    <View style={{ marginTop: 40 }}>
      <Controller
        name={name}
        control={control}
        as={
          <CustomPickerComponent
            pickerProps={{ selectedValue }}
            label={label}
            items={parsedOptions}
          />
        }
        defaultValue={selectedValue || ''}
        rules={{
          validate: async (value) =>
            (await schemas[name].isValid(value)) || requiredFieldMessage,
        }}
      />

      <GradientButton
        mode="active"
        onPress={handleSubmit(onSubmit)}
        text={translate('STORE_PROFILE.editButton')}
        style={{ marginTop: 20 }}
      />
    </View>
  );
};

export default SelectPropertyForm;
