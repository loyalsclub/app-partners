import * as yup from 'yup';

export const schemas = {
  name: yup.string().required(),
  alias: yup.string().required(),
  profileImage: yup.string().required(),
  description: yup.string().required(),
  category: yup.string().required(),
  facebook: yup.string(),
  twitter: yup.string(),
  instagram: yup.string(),
  whatsapp: yup.string(),
  website: yup.string(),
  phone: yup.string(),
  email: yup.string(),
  address: yup.string(),
};

export const textInputProps = {
  name: {
    inputProps: {
      maxLength: 35,
      autoCorrect: false,
    },
  },
  description: {
    minLines: 2,
    maxLines: 5,
    charCounter: true,
    resize: true,
    inputProps: {
      multiline: true,
      maxLength: 255,
      autoCorrect: false,
    },
  },
  whatsapp: {
    inputProps: {
      maxLength: 15,
      autoCorrect: false,
      autoCapitalize: 'none',
      keyboardType: 'numeric',
      returnKeyType: 'done',
    },
  },
  phone: {
    inputProps: {
      maxLength: 15,
      autoCorrect: false,
      autoCapitalize: 'none',
      keyboardType: 'numeric',
      returnKeyType: 'done',
    },
  },
};

export const socialMediaMasks = {
  facebook: 'https://facebook.com',
  twitter: 'https://twitter.com',
  instagram: 'https://instagram.com',
};
