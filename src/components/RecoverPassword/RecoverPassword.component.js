import React from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Text } from 'react-native';
import { yupResolver } from '@hookform/resolvers';
import * as yup from 'yup';
import globalStyles from '../../styles/global.styles';
import translate from '../../utils/language.utils';
import CustomTextInput from '../Common/Form/CustomTextInput.component';
import MainContainer from '../Common/Layout/MainContainer.component';
import GradientButton from '../Common/Miscellaneous/GradientButton.component';
import LayoutPage from '../Common/Layout/LayoutPage.component';

const schema = yup.object().shape({
  email: yup
    .string()
    .email(translate('FORMS_ERROR_MESSAGES.email'))
    .required(translate('FORMS_ERROR_MESSAGES.required')),
});

const RecoverPassword = ({
  loading,
  recoverEmailSent,
  onSubmitPressHandler,
  onLoginPressHandler,
}) => {
  const { control, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  return (
    <LayoutPage>
      <MainContainer style={globalStyles.centeredCon}>
        {!recoverEmailSent ? (
          <>
            <Text style={globalStyles.title}>
              {translate('RECOVER_PASSWORD.title')}
            </Text>

            <Controller
              name="email"
              control={control}
              as={
                <CustomTextInput
                  inputProps={{
                    defaultValue: '',
                    autoCorrect: false,
                    autoCapitalize: 'none',
                    autoCompleteType: 'email',
                    textContentType: 'emailAddress',
                    keyboardType: 'email-address',
                  }}
                  error={errors.email && errors.email.message}
                  label={translate('RECOVER_PASSWORD.email')}
                />
              }
              defaultValue=""
            />

            <GradientButton
              mode={!loading ? 'active' : 'disabled'}
              style={{ marginVertical: 15 }}
              onPress={handleSubmit(onSubmitPressHandler)}
              text={translate('RECOVER_PASSWORD.submitButton')}
            />
          </>
        ) : (
          <Text style={globalStyles.title}>
            {translate('RECOVER_PASSWORD.successText')}
          </Text>
        )}

        <GradientButton
          mode="dark"
          onPress={onLoginPressHandler}
          text={translate('RECOVER_PASSWORD.goBack')}
        />
      </MainContainer>
    </LayoutPage>
  );
};

export default RecoverPassword;
