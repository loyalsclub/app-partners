export default {
  AUTH_FORM: {
    email: 'Email',
    password: 'Contraseña',
    firstName: 'Nombre',
    lastName: 'Apellido',
    loginSubmit: 'Iniciar Sesión',
    loginError: 'Ups! Parece que hay un error en los datos',
    recoverPassword: 'Recuperar Contraseña',
  },

  RECOVER_PASSWORD: {
    title: 'Ingresá el email con el que te registraste',
    email: 'Email',
    submitButton: 'Recuperar Contraseña',
    successText:
      'Hemos enviado un email con un enlace para recuperar la contraseña.',
    goBack: 'Volver',
  },

  SIGNUP_FORM: { submitButton: 'Crear cuenta' },

  FORMS_ERROR_MESSAGES: {
    required: 'Campo requerido',
    number: 'Completá el campo con números',
    email: 'Ingresá un email válido',
    confirmPassword: 'Las contraseñas deben coincidir',
    charMin: 'Mínimo %{char} caracteres',
    invalidField: 'Ingresá un %{field} válido',
    illegalAge: 'Debes ser al menos mayor de 18 años',
    invalidAlias: 'El alias no está disponible',
  },

  STORE_CATEGORIES: {
    dress: 'Indumentaria',
    gastronomy: 'Gastronomía',
    coffee: 'Cafetería',
    bar: 'Bar',
    service: 'Servicio',
    market: 'Mercado',
  },

  AUTH_HOME: {
    welcome: 'Bienvenido a la app para gestionar tu Club de Clientes',
    logIn: 'Iniciar Sesión',
    register: 'Creá tu Club de Clientes',
    testSystem: 'Mirá estos ejemplos',
  },

  INTRO_SCREEN: {
    title: 'Tu propio Club de Clientes',
    stepOneTitle: 'Creá tu Club de Clientes',
    stepOneDescription:
      'Un sistema de puntos y benficios con 3 niveles de fidelidad',
    stepTwoTitle: 'Ofrecé Beneficios',
    stepTwoDescription:
      'Creá beneficios para tus clientes en cada uno de los 3 niveles de fidelidad',
    stepThreeTitle: 'Vinculate con tus Clientes',
    stepThreeDescription:
      'Sumales puntos con sus consumos para que suban de nivel y accedan a nuevos beneficios',
    startButton: '¡Comenzar!',
    moreInfo: 'Conocé más en nuestra web',
  },

  CREATE_PARTNER: {
    congratulationsTitle: '¡Tu Club de Clientes ya está listo!',
    congratulationsDescription: `Nuestro sistema generó 3 niveles para tu Club de Clientes. A medida que tus clientes consuman, subirán de nivel y con él mejorarán sus beneficios.`,
    congratulationsButton: '¿Cómo lo uso?',
    howToTitle: '¿Cómo uso mi Club de Clientes?',
    howToButton: '¿Cómo acceden mis clientes?',
    howToFirstStepTitle: 'Creá tus primeros beneficios',
    howToFirstStepDescription:
      'Creá beneficios para los 3 niveles y motivá a tus clientes a volver a elegirte.',
    howToSecondStepTitle: 'Empezá a registrar clientes',
    howToSecondStepDescription:
      'Enviale puntos a tus clientes nuevos para que se sumen a tu Club de Clientes.',
    howToThirdStepTitle: 'Identificá a tus clientes',
    howToThirdStepDescription:
      'Tus clientes registrados tendrán un perfil único que los identificará con tu Club de Clientes. Validá este perfil cada vez que quieran usar un beneficio.',
    howToFourthStepTitle: 'Seguí fidelizando',
    howToFourthStepDescription:
      'Seguí sumándole puntos a tus clientes cada vez que realicen un consumo para que mejoren su nivel y accedan a mejores beneficios.',
    clientsTitle: '¿Cómo acceden mis clientes a sus beneficios?',
    clientsDescription:
      'Recordá que tus clientes podrán ingresar a tu Club de Clientes para verificar su nivel y los beneficios que tienen disponibles.',
    clientsFirstStepTitle: 'A través de tu URL',
    clientsFirstStepDescription: 'Ingresando a ',
    clientsSecondStepTitle: 'Desde tu QR físico',
    clientsSecondStepDescription:
      'Escaneándolo presencialmente en tu comercio. Recibirás este QR listo para imprimir en tu correo electrónico. También podés solicitarlo en info@loyals.club.',
    clientsButton: '¡Empezar a fidelizar!',
    clientsDefaultAlias: 'tucomercio',
  },

  HOME: {
    actionLoadPointsTitle: 'Sumar Puntos',
    actionLoadPointsDescription:
      'Enviale puntos a tus clientes nuevos o ya registrados cada vez que realicen un consumo.',
    actionFindCustomerProfileTitle: 'Identificar Cliente',
    actionFindCustomerProfileDescription:
      'Verificá los beneficios que tiene tu cliente ya registrado.',
    actionMyCustomersTitle: 'Mis Clientes',
    actionMyCustomersDescription:
      'Mirá el listado de tus clientes registrados y accedé a estadísticas.',
    actionMyStoreTitle: 'Mi Club de Clientes',
    actionMyStoreDescription: 'Mirá tu Club de Clientes y compartilo.',
    actionSettingsTitle: 'Ajustes',
    actionSettingsDescription:
      'Gestioná la información de tu Club de Clientes y los beneficios que ofrecés.',
  },

  LOAD_POINTS: {
    title: 'Sumar Puntos',
    description: '¿Cómo querés sumar puntos?',
    actionSendPointsTitle: 'Enviar Puntos',
    actionSendPointsDescription:
      'A clientes nuevos o ya registrados a través de WhatsApp o Instagram.',
    actionScanProfileTitle: 'Escanear un Perfil',
    actionScanProfileDescription:
      'En consumos presenciales de clientes ya registrados.',
  },

  LOAD_PROFILE: {
    historyAction: 'Historial del cliente',
    rewardsAction: 'Beneficios disponibles',
    noHistory: 'Este cliente no tiene acciones registradas',
    emptySearch: 'No encontramos resultados para la búsqueda ingresada',
    optionAll: 'Todos',
    optionPoints: 'Puntos',
    optionRewards: 'Beneficios',
  },

  FIND_CUSTOMER_PROFILE: {
    title: 'Identificar un Cliente',
    description: 'Elegí un método de identificación',
    actionInputAliasTitle: 'Ingresar Alias',
    actionInputAliasDescription:
      'De clientes que no puedan mostrarte su QR en este momento.',
    actionScanProfileTitle: 'Escanear un Perfil',
    actionScanProfileDescription:
      'De clientes que estén presencialmente en tu comercio.',
  },

  MY_CUSTOMERS: {
    title: 'Mis Clientes',
    description: 'Conocé tus clientes y cómo se segmentan',
    actionClientListTitle: 'Listado de Clientes',
    actionClientListDescription: 'Listado completo de tus clientes.',
    actionStatisticsTitle: 'Estadísticas',
    actionStatisticsDescription:
      'Mirá como se segmentan tus clientes en los tres niveles.',
  },

  MY_STORE: {
    title: 'Mi Club de Clientes',
    description: 'Mirá la experiencia de tus clientes y compartila',
    actionVisitStoreTitle: 'Visitar mi Club de Clientes',
    actionVisitStoreDescription:
      'Mirá lo que ven tus clientes cuando ingresan a tu Club.',
    actionShareStoreTitle: 'Compartir',
    actionShareStoreDescription:
      'Compartí tu Club de Clientes con quien quieras e invitalo a unirse.',
  },

  SETTINGS: {
    title: 'Ajustes',
    description: 'Conocé a todos tus clientes y su nivel de fidelidad',
    actionEditInformationTitle: 'Editar Información',
    actionEditInformationDescription:
      'Modificá tu logo y la información de tu Club de Clientes.',
    actionManageRewardsTitle: 'Gestionar Beneficios',
    actionManageRewardsDescription:
      'Gestioná y editá los beneficios que le ofrecés a tus clientes.',
  },

  CUSTOMER_LIST: {
    title: 'Listado de Clientes',
    noCustomers: 'Todavía no disponés de clientes asociados a tu Club.',
    filter: 'Filtrar por Nombre o Alias',
    emptySearch: 'No encontramos resultados para la búsqueda ingresada',
  },

  STORE_PROFILE: {
    title: 'Editar Información',
    temporarySolution:
      'Esta función se encontrará disponible próximamente. Por el momento, si necesitás modificar la información de tu Club de Clientes, por favor envianos un email a',
    or: ' o ',
    contactWpp: 'contactanos por WhatsApp',
    name: 'Nombre',
    description: 'Descripción',
    country: 'País',
    category: 'Rubro',
    alias: 'Alias',
    aliasEditDescription:
      'Recomendamos no modificar frecuentemente el alias, ya que éste es parte de la URL a través de la cuál tus clientes acceden a tu comercio.',
    facebook: 'Facebook',
    twitter: 'Twitter',
    instagram: 'Instagram',
    whatsapp: 'WhatsApp',
    website: 'Sitio Web',
    phone: 'Teléfono',
    email: 'Email',
    address: 'Dirección',
    editButton: 'Actualizar',
  },

  MENU: {
    contactWpp: 'Contactanos por WhatsApp',
    wppPrefilledMessage: 'Hola! Necesito soporte para la App Loyals Partners.',
    logOut: 'Cerrar Sesión',
    support: 'Ayuda y Soporte',
  },

  GENERATE_COUPON: {
    loadPointsMessage: `¡Gracias por elegirnos! Te dejamos este link para que sumes puntos por tu compra y tengas beneficios para cuando vuelvas.`,
    price: 'Importe:',
    pointsToLoad: 'Puntos a sumar:',
    sendToContactActionTitle: 'Enviar Puntos',
    sendToContactActionDescription:
      'Enviale puntos a un contacto de tu teléfono vía Whatsapp o Instagram',
    newContactActionTitle: 'Enviar a un nuevo número',
    newContactActionDescription:
      '¿Tu cliente no está entre tus contactos? Indicanos su teléfono y enviale puntos',
    newContactTitle: 'Enviar a un nuevo número',
    newContactDescription:
      'Indicanos el celular de tu cliente para enviarle los puntos por WhatsApp',
    newContactPlaceholder: 'Número de Celular',
  },
};
