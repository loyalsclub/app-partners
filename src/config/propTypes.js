import PropTypes from 'prop-types';

const storePropType = PropTypes.shape({
  profileImage: PropTypes.string,
  name: PropTypes.string,
  category: PropTypes.string,
  alias: PropTypes.string,
  description: PropTypes.string,
  countryCode: PropTypes.string,
  StoreLocations: PropTypes.arrayOf(
    PropTypes.shape({
      website: PropTypes.string,
      address: PropTypes.string,
      email: PropTypes.string,
      phone: PropTypes.string,
    })
  ),
  level2: PropTypes.string,
  level3: PropTypes.string,
  socialMedia: PropTypes.shape({
    facebook: PropTypes.string,
    instagram: PropTypes.string,
    twitter: PropTypes.string,
    whatsapp: PropTypes.string,
  }),
});

export default { storePropType };
