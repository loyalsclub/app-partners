const firebaseConfig = require('./firebase.config.json');

const DEV = {
  API_URL: 'http://127.0.0.1:3001/graphql',
  USERS_BASE_URL: 'localhost:3002',
  FIREBASE_CONFIG: firebaseConfig.development,
  SENTRY_API:
    'https://a7be33c46c6740dba23976d94e2195f8@o383073.ingest.sentry.io/5412718',
  ANALYTICS: {
    PROVIDER: 'amplitude',
    API_KEY: '289d53cd9fb92e4fbf0f110a4e60b426',
  },
  GOOGLE_PLACES_API_KEY: 'AIzaSyCKpi27WgPCcOxhy4zobczKmOULwxUb1ts',
};

const PROD = {
  API_URL: 'https://api.loyals.club',
  USERS_BASE_URL: 'https://loyals.club',
  ANALYTICS: {
    PROVIDER: 'amplitude',
    API_KEY: '1fc53cc0df34ffcdba4a8d3dda733dcd',
  },
  FIREBASE_CONFIG: firebaseConfig.production,
  SENTRY_API:
    'https://a7be33c46c6740dba23976d94e2195f8@o383073.ingest.sentry.io/5412718',
  GOOGLE_PLACES_API_KEY: 'AIzaSyCKpi27WgPCcOxhy4zobczKmOULwxUb1ts',
};

module.exports = process.env.NODE_ENV === 'development' ? DEV : PROD;
