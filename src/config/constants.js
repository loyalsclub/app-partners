export const CUSTOMER_STATUS = {
  ACTIVE: 'active',
  PENDING: 'pending',
  DELETED: 'deleted',
};

export const TRANSACTION_STATUS = {
  PENDING: 'pending',
  REDEEMED: 'redeemed',
};

export const USAGE_FREQUENCIES = {
  FOREVER: 'forever',
  ONE_TIME: 'oneTime',
  EVERY_CERTAIN_DAYS: 'everyCertainDays',
  EVERY_CERTAIN_HOURS: 'everyCertainHours',
  CERTAIN_WEEK_DAYS: 'certainWeekDays',
};

export default {
  FORGOT_PASSWORD: 'FORGOT_PASSWORD',
  LOGIN: 'LOGIN',
  REGISTER: 'REGISTER',
};

export const spendingTimesList = [
  {
    value: '365',
    label: 'Todos los días',
  },
  {
    value: '144',
    label: 'Tres veces por semana',
  },
  {
    value: '96',
    label: 'Dos veces por semana',
  },
  {
    value: '48',
    label: 'Una vez por semana',
  },
  {
    value: '24',
    label: 'Cada dos semanas',
  },
  {
    value: '12',
    label: 'Una vez por mes',
  },
  {
    value: '9',
    label: 'Cada 3 meses',
  },
  {
    value: '6',
    label: 'Cada 6 meses',
  },
  {
    value: '1',
    label: 'Una vez por año',
  },
];

export const WHATSAPP_CONTACT = '+5491158135917';

export const STORE_PROPERTIES = {
  profileImage: 'profileImage',
  name: 'name',
  description: 'description',
  category: 'category',
  country: 'country',
  alias: 'alias',
  facebook: 'facebook',
  twitter: 'twitter',
  instagram: 'instagram',
  whatsapp: 'whatsapp',
  website: 'website',
  phone: 'phone',
  address: 'address',
};

export const CUSTOMER_HISTORY_TYPES = {
  ALL: 'ALL',
  POINT: 'POINT',
  STORE_REWARD_USAGE: 'STORE_REWARD_USAGE',
};
