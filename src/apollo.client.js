/** External dependencies */
import { ApolloClient } from 'apollo-client';
import { ApolloLink, Observable } from 'apollo-link';
import { createUploadLink } from 'apollo-upload-client';
import { withClientState } from 'apollo-link-state';

/** Dependencies for keeping cache. */
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  InMemoryCache,
  IntrospectionFragmentMatcher,
} from 'apollo-cache-inmemory';
import { CachePersistor } from 'apollo-cache-persist';

import typeDefs from './queries/types';
import { getAuthTokenIfLogged } from './utils/auth.utils';
import ENV from './config/env.config';
import introspectionQueryResultData from './config/fragmentTypes.json';

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData,
});

const cache = new InMemoryCache({ fragmentMatcher, addTypename: false });
const persistor = new CachePersistor({
  cache,
  storage: AsyncStorage,
});

const request = async (operation) => {
  const token = await getAuthTokenIfLogged();

  if (token) {
    operation.setContext({
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }
};

const requestLink = new ApolloLink(
  (operation, forward) =>
    new Observable((observer) => {
      let handle;

      Promise.resolve(operation)
        .then(request)
        .then(() => {
          handle = forward(operation).subscribe({
            next: observer.next.bind(observer),
            error: observer.error.bind(observer),
            complete: observer.complete.bind(observer),
          });
        })
        .catch(observer.error.bind(observer));

      return () => {
        if (handle) handle.unsubscribe();
      };
    })
);

const client = new ApolloClient({
  link: ApolloLink.from([
    requestLink,
    withClientState({ typeDefs }),
    createUploadLink({ uri: ENV.API_URL }),
  ]),
  cache,
});

client.onResetStore(async () => {
  persistor.pause(); // Pause automatic persistence.
  persistor.purge(); // Delete everything in the storage provider.
});

export default client;
