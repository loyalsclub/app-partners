import AsyncStorage from '@react-native-async-storage/async-storage';

class StorageProvider {
  constructor() {
    this.cache = {};
  }

  async set(key, value) {
    try {
      const parsedValue =
        typeof value !== 'string' ? JSON.stringify(value) : value;

      await AsyncStorage.setItem(key, parsedValue);

      this.cache[key] = value;
    } catch (e) {
      console.warn('[Storage Provider set]', e);
    }
  }

  async get(key, parseJSON = false) {
    if (!this.cache[key]) {
      try {
        const value = await AsyncStorage.getItem(key);
        const parsedValue = parseJSON ? JSON.parse(value) : value;

        this.cache[key] = parsedValue;
      } catch (e) {
        console.warn('[Storage Provider get]', e);
      }
    }

    return this.cache[key];
  }

  getSync(key) {
    return this.cache[key];
  }
}

export default new StorageProvider();
