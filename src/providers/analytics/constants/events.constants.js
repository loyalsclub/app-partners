import * as pages from '../../../pages/_constants';

export default {
  initial_screen: {
    [pages.LOGIN]: 'Initial Screen: Click log in',
    [pages.REGISTER]: 'Initial Screen: Click register',
    [pages.TEST_SYSTEM]: 'Initial Screen: Click test system',
    [pages.INTRO]: 'Initial Screen: Click intro',
    information_button: 'Initial Screen: Click information',
  },

  user_registration: {
    email_validated: 'User Registration: Email validated',
    personal_information_completed:
      'User Registration: Personal information completed',
    password_completed: 'User Registration: Password completed',
    user_created: 'User Registration: User created',
    email_verification_sent: 'User Registration: Email verification sent',
    email_verified: 'User Registration: Email verified',
    resend_email_verification_button:
      'User Registration: Click resend email verification',
  },

  partner_registration: {
    logo_completed: 'Partner Registration: Logo completed',
    store_information_completed:
      'Partner Registration: Store information completed',
    contact_phone_completed: 'Partner Registration: Contact phone completed',
    spending_information_completed:
      'Partner Registration: Spending information completed',
    review_information:
      'Partner Registration: Landed on review information screen',
    partner_created: 'Partner Registration: Partner created successfully',
    congratulations: 'Partner Registration: Landed on congratulations screen',
    store_levels: 'Partner Registration: Landed on store levels screen',
    store_rewards: 'Partner Registration: Landed on store rewards screen',
  },

  menu: {
    help_and_support_button: 'Menu: Click help and support',
    log_out_button: 'Menu: Click log out',
  },

  home: {
    [pages.LOAD_POINTS]: 'Home: Click load points',
    [pages.CLIENT_REGISTRATION]: 'Home: Click invite customer',
    [pages.REWARDS]: 'Home: Click rewards',
    [pages.STATISTICS]: 'Home: Click statistics',
    [pages.MENU]: 'Home: Click menu',
    [pages.FIND_CUSTOMER_PROFILE]: 'Home: Click find customer profile',
    [pages.MY_CUSTOMERS]: 'Home: Click my clients',
    [pages.MY_STORE]: 'Home: Click my store',
    [pages.SETTINGS]: 'Home: Click settings',
    [pages.CREATE_PARTNER]: 'Home: Click create partner',
  },

  load_points: {
    [pages.GENERATE_COUPON]: 'Load Points: Click generate coupon',
    [pages.LOAD_PROFILE]: 'Load Points: Click scan profile',
  },

  find_customer_profile: {
    [pages.LOAD_PROFILE]: 'Customer Profile: Click scan profile',
    [pages.CUSTOMER_LIST]: 'Customer Profile: Click customer list',
  },

  my_customers: {
    [pages.CUSTOMER_LIST]: 'My Customers: Click customer list',
    [pages.STATISTICS]: 'My Customers: Click statistics',
  },

  my_store: {
    view_store: 'My Store: Click view store',
    share_store: 'My Store: Click share store',
  },

  settings: {
    [pages.REWARDS]: 'Settings: Click manage rewards',
    [pages.STORE_PROFILE]: 'Settings: Click store profile',
  },

  customer_list: {
    [pages.LOAD_PROFILE]: 'Customer List: Click customer profile',
  },

  load_profile: {
    qr: 'Load Profile: QR scanned successfully',
    email_alias_button: 'Load Profile: Click insert alias / email',
    loaded_successfully: 'Load Profile: Profile loaded successfully',
    customer_added: 'Load Profile: Customer added successfully',
  },

  invite_customer: {
    create_new_customer:
      'Invite Customer: New customer created and invited successfully',
    invite_customer: 'Invite Customer: Customer invited successfully',
  },

  rewards: {
    new_reward: 'Rewards: Click create reward',
    new_reward_finished: 'Rewards: Created successfully',
    edit_reward: 'Rewards: Click edit reward',
    edit_reward_finished: 'Rewards: Edited successfully',
    delete_reward: 'Rewards: Deleted successfully',
  },

  profile: {
    load_points: 'Profile: Points loaded',
    reward_usage: 'Profile: Reward used',
  },

  generate_coupon: {
    coupon_generated: 'Generate Coupon: Coupon generated',
    coupon_shared: 'Generate Coupon: Coupon shared',
  },

  test_system: {
    test_heladeria_sabor: 'Test System: Click on Heladeria Sabor',
    test_decoracion_luna: 'Test System: Click on Decoracion Luna',
    test_cafe_hermanos: 'Test System: Click on Cafe Los Hermanos',
    test_pizzeria_gomez: 'Test System: Click on Pizzeria Gomez',
  },
};
