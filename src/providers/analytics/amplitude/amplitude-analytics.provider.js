import * as Amplitude from 'expo-analytics-amplitude';

class AmplitudeAnalytics {
  constructor() {
    this.amplitude = Amplitude;
  }

  initialize(config) {
    this.amplitude.initializeAsync(config.API_KEY);
  }

  setUserId(userId) {
    this.amplitude.setUserIdAsync(userId);
  }

  setUserProperties(properties) {
    this.amplitude.setUserPropertiesAsync(properties);
  }

  clearUserProperties() {
    this.amplitude.clearUserPropertiesAsync();
  }

  logEvent(eventName, properties = {}) {
    const hasEventProperties = Object.keys(properties).length > 0;

    if (hasEventProperties) {
      this.amplitude.logEventWithPropertiesAsync(eventName, properties);
      return;
    }

    this.amplitude.logEventAsync(eventName);
  }
}

export default AmplitudeAnalytics;
