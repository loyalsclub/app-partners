import AmplitudeAnalytics from './amplitude/amplitude-analytics.provider';
import { ANALYTICS } from '../../config/env.config';

class AnalyticsProvider {
  constructor() {
    switch (ANALYTICS.PROVIDER) {
      case 'amplitude':
        this.provider = new AmplitudeAnalytics();
        break;
      default:
        throw new Error(
          `Analytics provider ${ANALYTICS.PROVIDER} not implemented`
        );
    }
  }

  initialize() {
    this.provider.initialize(ANALYTICS);
  }

  setUserId(userId) {
    this.provider.setUserId(userId);
  }

  setUserProperties(properties) {
    this.provider.setUserProperties(properties);
  }

  clearUserProperties() {
    this.provider.clearUserProperties();
  }

  logEvent(event, properties) {
    if (!event) {
      console.warn(`Analytics event does not exist`);
      return;
    }

    this.provider.logEvent(event, properties);
  }
}

export default new AnalyticsProvider();
